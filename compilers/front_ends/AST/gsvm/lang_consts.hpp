#ifndef SK_COMPILERS_FRONTENDS_AST_GSVM_LANG_CONST_HPP
#define SK_COMPILERS_FRONTENDS_AST_GSVM_LANG_CONST_HPP

#include "SK/utilities/types.hpp"

namespace SK
{
 namespace compilers
 {
  namespace frontends
  {
   namespace ast
   {
    //
    namespace gsvm
    {
     namespace consts
     {
      namespace util = SK::utility::types;

      namespace IDs
      {
       /*
        namespace _
        {
          namespace util = SK::utility::types;
          namespace fe_ast = SK::compilers::frontends::ast;
          namespace id_i = fe_ast::gsvm::consts::IDs;
          //static const util::uint64 FirstValue = id_i::_::LastValue + 1;

          static const util::uint64 FirstValue = ;
          static const util::uint64  = ; // "" -
          static const util::uint64 LastValue = ;
          // static const util::uint64  = ; // "" -
         }
        */
       /*
         namespace _
         {
          enum __e
          { FirstValue = 0,
            __,
            LastValue
          };
         }// end of namespace _;
       */
       /*
        namespace _
        {
          namespace util = SK::utility::types;
          namespace fe_ast = SK::compilers::frontends::ast;
          namespace id_i = fe_ast::gsvm::consts::IDs;

          enum { FirstValue = FirstValue = id_i::_::LastValue + 1,
            __,
            LastValue
          };
         }
       */
      static const util::uint32 null_ = 0; // null value.
      namespace ws
      {
       namespace util = SK::utility::types;
       namespace fe_ast = SK::compilers::frontends::ast;
       namespace id_i = fe_ast::gsvm::consts::IDs;

       enum { FirstValue = = id_i::null_ + 1,
              whiteSpace = FirstValue,
              comment,
              sepperator,
              LastValue = sepperator }; // ws::sepperator
      }
      namespace operators
      { // operator name enumeration
       namespace util = SK::utility::types;
       namespace fe_ast = SK::compilers::frontends::ast;
       namespace id_i = fe_ast::gsvm::consts::IDs;

       enum { FirstValue = id_i::ws::LastValue + 1,
              functionCall = FirstValue , // "()" - FUNC_CALL_OP
              arrayIndex, //  "[]" - ARRAY_INDEX_OP
              memberViaPtr, // "->" - MEMBER_ACCESS_VIA_PTR_OP
              Increment, postIncrement, preIncrement, // "++"  - INCREMENT_OP
              Decrement, postDecrement, preDecrement, // "--"  - DECREMENT_OP
              Plus,
              addition, // "+" - ADDITION_OP
              unaryPlus, // "+" - UNARY_PLUS_OP
              Minus,
              subtraction, // "-" - SUBTRACTION_OP
              unaryMinus, // "-" - UNARY_MINUS_OP
              addressof_, // "addressof" - ADDRESSOF_OP
              logicalNot, // "!" - LOGICAL_NOT_OP
              bitwiseNot, // "~" - BITWISE_NOT_OP
              Ampersand,
              getAddress, // "&" - GETADDRESS_OP
              bitwiseAnd, // "&" - BITWISE_AND_OP
              Star,
              dereference, // "*" - DEREFERENCE_OP
              multiply, // "*" - MULTIPLY_OP
              cast, // "cast" - TYPE_CAST_OP
              dynamicAlloc, // "new" - DYNAMIC_ALLOCATE_OP
              dynamicDealloc, // "delete" - DYNAMIC_DEALLOCATE_OP
              memberViaOffset, // "->*" - MEMBER_ACCESS_VIA_PTR_TO_MEMBER_OP
              division, // "/" - DIVISION_OP
              modulus, // "%" - MODULUS_OP
              bitshiftLeft, // "<<", BITSHIFT_LEFT_OP
              bitshiftRightArith, // ">>", BITSHIFT_RIGHT_ARITHMETIC_OP
              bitshiftRightLogic, // ">>>", BITSHIFT_RIGHT_LOGICAL_OP
              lessThan, // "<", LESS_THAN_OP
              lessThanEqual, // "<=", LESS_THAN_EQUAL_OP
              greaterThan, // ">", GREATER_THAN_OP
              greaterThanEqual, // ">=", GREATER_THAN_EUQUAL_OP
              equals, // "==", EQUALS_OP
              notEquals, // "!=", NOT_EQUALS_OP
              bitwiseXor, // "^", BITWISE_XOR_OP
              bitwiseOr, // "|", BITWISE_OR_OP
              logicalAnd, // "&&", LOGICAL_AND_OP
              logicalXor, // "^^", LOGICAL_XOR_OP
              logicalOr, // "||", LOGICAL_OR_OP
              assignment, // "=", ASSIGNMENT_OP
              assignmentAdd, // "+=", ASSIGNMENT_ADDITION_OP
              assignmentSub, // "-=", ASSIGNMENT_SUBTRACTION_OP
              assignmentMul, // "*=", ASSIGNMENT_MULTIPLY_OP
              assignmentDiv, // "/=", ASSIGNMENT_DIVISION_OP
              assignmentMod, // "%=", ASSIGNMENT_MODULUS_OP
              assignmentBitAnd, // "&=", ASSIGNMENT_BITWISE_AND_OP
              assignmentBitXor, // "^=", ASSIGNMENT_BITWISE_XOR_OP
              assignmentBitOr, // "|=", ASSIGNMENT_BITWISE_OR_OP
              assignmentBitSHL, // "<<=", ASSIGNMENT_BITSHIFT_LEFT_OP
              assignmentBitSAR, // ">>=", ASSIGNMENT_BITSHIFT_RIGHT_ARITHMETIC_OP
              assignmentBitSLR, // ">>>=", ASSIGNMENT_BITSHIFT_RIGHT_LOGICAL_OP
              sequence, // ",", SEQUENCE_OP
              LastValue = sequence
            };
      }

      namespace litterals
      { // litteral type enumeration
       namespace util = SK::utility::types;
       namespace fe_ast = SK::compilers::frontends::ast;
       namespace id_i = fe_ast::gsvm::consts::IDs;

       enum {
            FirstValue = id_i::operators::LastValue + 1,
            char_ = FirstValue,
            wchar_,
            mbchar_,
            vlchar_,
            str_,
            wstr_,
            mbstr_,
            vlstr_,
            sint_, // a signed integer value
            uint_, // a unsigned integer value
            float_, // a floating point value
            bool_,
            LastValue  =  bool_
       };

      } // end of namespace litterals.
      /*
        namespace _
        {
          namespace util = SK::utility::types;
          namsspace fe_ast = SK::compilers::frontends::ast;
          namespace id_i = fe_ast::gsvm::consts::IDs;

          enum { FirstValue = FirstValue = id_i::_::LastValue + 1,
            __,
            LastValue
          };
         }
       */
      namespace builtin_funcs
      {
       namespace util = SK::utility::types;
       namespace fe_ast = SK::compilers::frontends::ast;
       namespace id_i = fe_ast::gsvm::consts::IDs;
       //static const util::uint64 FirstValue = id_i::_::LastValue + 1;
       enum { FirstValue = FirstValue = id_i::litterals::LastValue + 1,
            dynamicCast = FirstValue,  //"dynamic_cast", ::builtin_funcs::dynamicCast )
            reinterpretCast,    // "reinterpret_cast", ::builtin_funcs::reinterpretCast )
            sizeof_,            // "sizeof", ::builtin_funcs::sizeof_ )
            staticCast,         // "static_cast", ::builtin_funcs::staticCast )
            typeid_,            // "typeid", ::builtin_funcs::typeid_ )
            LastValue  = typeid_
          };

      } //end of namespace builtin_funcs.

      /*
        namespace _
        {
          namespace util = SK::utility::types;
          namespace fe_ast = SK::compilers::frontends::ast;
          namespace id_i = fe_ast::gsvm::consts::IDs;

          enum { FirstValue = FirstValue = id_i::_::LastValue + 1,
            __,
            LastValue
          };
         }
       */
      namespace builtin_types
      {
       namespace util = SK::utility::types;
       namespace fe_ast = SK::compilers::frontends::ast;
       namespace id_i = fe_ast::gsvm::consts::IDs;
       //static const util::uint64 FirstValue = id_i::_::LastValue + 1;
       // "", ::builtin_types:: ) // -
       enum { FirstValue = FirstValue = id_i::builtin_funcs::LastValue + 1,
            boolean = FirstValue, // ( "boolean", ::builtin_types::boolean ) // - BOOLEAN_BT
            byte, // ( "byte", ::builtin_types::byte ) // - BYTE_BT - unsigned 8-bit integer.
            char_, // ( "char", ::builtin_types::char_ ) // - CHAR_BT - 8-bit character type for UTF8
            double_, // ( "double", ::builtin_types::double_ ) // - DOUBLE_BT - 64-bit floating point data type.
            doubleword, // ( "doubleword", ::builtin_types::doubleword ) // - DOUBLEWORD_BT - unsigned 64-bit integer type.
            float_, // ( "float", ::builtin_types::float_ ) // - FLOAT_BT - 32-bit floating point data type.
            halfword, // ( "halfword", ::builtin_types::halfword ) // - HALFWORD_BT - unsigned 16-bit integer type.
            int_, // ( "int", ::builtin_types::int_ ) // - INT_BT - signed 32-bit integer type.
            long_, // ( "long", ::builtin_types::long_ ) // - LONG_BT - signed 64-bit integer type.
            mbchar_, // ( "mbchar", ::builtin_types::mbchar_ ) // - MBCHAR_BT - 32-bit character data type for UTF32
            short_, // ( "short", ::builtin_types::short_ ) // - SHORT_BT - signed 16-bit integer type.
            signed_, // ( "signed", ::builtin_types::signed_ ) // - SIGNED_BT - type for creating signed bitfields.
            typeinfo, // ( "typeinfo", ::builtin_types::typeinfo ) // - TYPEINFO_BT - type containg rtti
            unsigned_, // ( "unsigned", ::builtin_types::unsigned_ ) // - UNSIGNED_BT - type for creating unsigned bitfields.
            void_, // ( "void", ::builtin_types::void_ ) // - VOID_BT - empty data type.
            vlchar, // ( "vlchar", ::builtin_types::vlchar ) // - VLCHAR_BT - 64-bit character data type for *future* UTF64
            vararg, // ( "vararg", ::builtin_types::vararg ) // - VARARG_BT - a type for the creation of variable length argument lists.
            wchar,  // ( "wchar", ::builtin_types::wchar ) // - WCHAR_BT - 16-bit character data type for UTF16
            word, // ( "word", ::builtin_types::word ) // - WORD_BT - unsigned 32-bit integer type.
            octet, // ( "octet", ::builtin_types::octet ) // - signed 8 bit int.
            // specail typenames and alliases - reserved
            // -- to be added to the symbol tables once implemented
            char8, // alias for char
            char16, // alias for wchar
            char32, // alias for mbchar
            char64,  // alias for vlchar
            uint8_, // alias for byte // "" - uint[N]
            uint16_, // alias for halfword // "" - uint[N]
            uint32_, // alias for word // "" - uint[N]
            uint64_, // alias for doubleword // "" - uint[N]
            sint8_, // alias for octet // "" - sint[N]
            sint16_,// alias for short_ // "" - sint[N]
            sint32_,// alias for int_ // "" - sint[N]
            sint64_,// alias for long_ // "" - sint[N]
            uint64p8int8_, // // "" - uint[N]{p[M]int[Q]}
            uint64p4int16_, // // "" - uint[N]{p[M]int[Q]}
            uint64p2int32_, // // "" - uint[N]{p[M]int[Q]}
            sint64p8int8_, // // "" - sint[N]{p[M]int[Q]}
            sint64p4int16_, // // "" - sint[N]{p[M]int[Q]}
            sint64p2int32_, // // "" - sint[N]{p[M]int[Q]}
            float32_, // "" - float[N]
            float64_, // "" - float[N]
            ufloat32_, // "" - ufloat[N]
            ufloat64_ // "" - ufloat[N]
            float64p2float32_, // "" - float[N]{p[M]float[Q]}
            ufloat64p2float32_, // "" - ufloat[N]{p[M]float[Q]}
            LastValue = ufloat64p2float32_
       };
      }//end of namespace builtin_types;

      /*
      namespace _
        {
          namespace util = SK::utility::types;
          namespace id_i = SK::compilers::gsvm::consts::IDs;
          //namespace prev_id_i = id_i::_;

          static const util::uint32 FirstValue = prev_id_i::LastValue + 1;
          static const util::uint32  = ; // "" -
          static const util::uint32 LastValue = ;
          // static const util::uint32  = ; // "" -
        }// end of namespace _;
      */
      /*
        namespace _
        {
          namespace util = SK::utility::types;
          namespace fe_ast = SK::compilers::frontends::ast;
          namespace id_i = fe_ast::gsvm::consts::IDs;
          namespace prev_id_i = id_i::_;

          enum { FirstValue = FirstValue = prev_id_i::LastValue + 1,
            __,
            LastValue
          };
         }
      */

      namespace identifiers // the IDs of identifiers:
        {
         namespace util = SK::utility::types;
         namespace fe_ast = SK::compilers::frontends::ast;
         namespace id_i = fe_ast::gsvm::consts::IDs;
         namespace prev_id_i = id_i::builtin_types;

         enum { FirstValue = FirstValue = prev_id_i::LastValue + 1,
            basic_ = FirstValue, // "" - BASIC_IDENT
            var_, // "" - VAR_IDENT
            const_, // "" - CONST_IDENT
            typename_, // "" - TYPENAME_IDENT
            bitfield_, // "" - BITFIELD_IDENT
            enum_, // "" - ENUM_IDENT
            object_, // "" - OBJECT_IDENT
            union_, // "" - UNION_IDENT
            function_type_, // "" - FUNCTION_TYPE_IDENT
            function_type_args_, // "" - FUNCTION_IDENT_ARGS
           // function_type_arg_, // "" - FUNCTION_IDENT_ARG
            function_type_arg_var,
            function_type_arg_const,
            operator_type_, // "" - OPERATOR_TYPE_IDENT
            typeof_, // "" - QUALIFIED_TYPEOF_IDENT
            qual_typeid_, // "" - QUALIFIED_TYPEID
            unqual_typeid_, // "" - UNQUALIFIED_TYPEID
            label_, // "" - LABEL_IDENT
            function_, // "" - FUNCTION_IDENT
            operator_, // "" - OPERATOR_IDENT
            operators,
            namespace_, // "" - NAMESPACE_IDENT
            import_, // "" - IMPORT_IDENT
            builtin_types_, // "" - BUILTIN_TYPES_IDENT
            builtin_funcs_, // "" - BUILTIN_FUNC_IDENT
            nested_name_, // "" - NESTED_NAME_IDENT
            qualified_, // unused - for now
            unqualified_, // unused - for now
            scope_sepp,
            using_, // "" - USING_IDENT
            data_type_,
            LastValue = data_type_
          };
        }// end of namespace identifiers;
      /*
        namespace _
        {
          namespace util = SK::utility::types;
          namsspace fe_ast = SK::compilers::frontends::ast;
          namespace id_i = fe_ast::gsvm::consts::IDs;
          namespace prev_id_i = id_i::_;

          enum { FirstValue = FirstValue = prev_id_i::LastValue + 1,
            __,
            LastValue
          };
         }
      */
        namespace modifiers
        {
          namespace util = SK::utility::types;
          namespace fe_ast = SK::compilers::frontends::ast;
          namespace id_i = fe_ast::gsvm::consts::IDs;
          namespace prev_id_i = id_i::identifiers;

          enum { FirstValue = FirstValue = prev_id_i::LastValue + 1,
            // type modifiers
            temp_id_ = FirstValue, // "" -TEMP_ID_MOD
           // temp_operid, // "" - TEMP_OPERID_MOD
           // temp_id_args, // "" - TEMP_MOD_ARGS
            //temp_id_arg, // "" - TEMP_MOD_ARG ?
            type_prefix_,
            var_ptr_, // "" - VAR_PTR_MOD
            var_ref_, // "" - VAR_REF_MOD
            const_ptr_, // "" - CONST_PTR_MOD
            const_ref_, // "" - CONST_REF_MOD
            type_infix_ident,
            type_postfix_, // "" - TYPE_POSTFIX_MOD
            array_type_,
            callconv_, // "" - CALLCONV_MOD
            throw_, // "" - THROW_MOD
            //throw_args, // "" - THROW_ARGS_MOD
            //throw_arg, // "" - THROW_ARG_MOD ?
            // general modifiers
            lang_, // "" - LANG_MOD
            static_, // "" - STATIC_MOD
            const_, // "" - CONST_MOD
            export_, // "" - export - GENDEC_MOD
            intern_, // "" - intern - GENDEC_MOD
            volatile_, // "" - VOLATILE_MOD
            data_decl,
            // type declartion modifiers
            temp_decl_, // "" - TEMP_DECL_MOD
            //temp_operdecl, // "" - TEMP_OPERDECL_MOD ?
            //temp_decl_args, // "" - TEMP_DECL_ARGS
            temp_decl_arg_, // "" - TEMP_DECL_ARG - binary!
          //  initializer, // "" - INITIALIZER_MOD
            mutable_, // "" - MUTABLE_MOD
            abstract_, // "" - ABSTRACT_MOD
            virtual_, // "" - VIRTUAL_MOD
            func_decl_arg_,
            func_decl_args_, // "" - FUNC_DECL_ARGS
            func_ident_mods,
            func_decl_mods,
            explicit_, // "" - EXPLICIT_MOD
            ctor_decl_mods,
            ctor_init_list_, // "" - CTOR_INIT_LIST
            ctor_init_mem_, // "" - CTOR_INIT_MEM
            inherit_list_, // "" - INHERIT_LIST
            inherit_mem_, // "" - INHERIT_MEM
            dtor_decl_mods,
            type_infix_decl, // "" - TYPEDECL_INFIX_MOD
            attribute_,
            //enumerator, // "" - ENUMERATOR_MOD
            access_, // "" - ACCESS_MOD
            public_,
            protected_,
            private_,
            LastValue = private_
          };
        }// end of namespace modifiers;

        namespace expressions
        {
          namespace util = SK::utility::types;
          namespace fe_ast = SK::compilers::frontends::ast;
          namespace id_i = fe_ast::gsvm::consts::IDs;
          namespace prev_id_i = id_i::modifiers;

          enum { FirstValue = prev_id_i::LastValue + 1,
           // prime, //PRIMARY_EXPR
            this_ = FirstValue,
            self_,
           // litteral,
            ident, //IDENT_EXPR
            // sequence in parenthesis
            postfix, //POSTFIX_EXPR
            array, //ARRAY_INDEX_EXPR
            funcCall,//FUNC_CALL_EXPR
            ctorType, //EXPLICIT_CTOR_EXPR
            ctorRef,
            ctorPtr,
            memberRef, //MEMBER_REF_EXPR
            memberPtr, // MEMBER_PTR_EXPR
            dtorRef, //EXPLICIT_DTOR_EXPR
            dtorPtr,
            postIncrement,
            postDecrement,
          //  exprList, //EXPR_LIST
            unary, //UNARY_EXPR
            unaryOpers, //UNARY_OPERS ? - yes, because it`s a value_ node
            preIncrement, //INC_DEC_EXPR
            preDecrement, //INC_DEC_EXPR
            unaryPlus,
            unaryMinus,
            logicalNot,
            bitwiseNot,
            getAddress,
            addressof_,
            dereference,
            new_, //NEW_EXPR
         // newArray,
            del, //DEL_EXPR
            delArray, //DEL_ARRAY_EXPR
            // the begining of the binary expressions
            ptrMem_ptr, //PTR_MEM_EXPR
            ptrMem_ref,
            multiply, //MULTIPLY_EXPR
            division,
            modulus,
            addition, //ADDITION_EXPR
            subtraction,
            shiftLeft,
            shiftRightArith,
            shiftRightLogic,
            lesser,
            lesserEqual,
            greater,
            greaterEqual,
            equal,
            notEqual,
            bitwiseAnd,
            bitwiseXor,
            bitwiseOr,
            logicalAnd,
            logicalXor,
            logicalOr,
            conditional,
            conditional_q_sep,
            conditional_c_sep,
            assign,
            assignMult,
            assignDiv,
            assignMod,
            assignAdd,
            assignSub,
            assignShL,
            assignSAR,
            assignSLR,
            assignBitAnd,
            assignBitXor,
            assignBitOr,
            throw_,
            sequence,
            condition
            LastValue = condition
          };
         }// end of namespace expressions;
         namespace statements // ::statements::
         {
          namespace util = SK::utility::types;
          namespace fe_ast = SK::compilers::frontends::ast;
          namespace id_i = fe_ast::gsvm::consts::IDs;
          namespace prev_id_i = id_i::expressions;

          enum { FirstValue = prev_id_i::LastValue + 1,
            expression = FirstValue, // EXPR_STATE,
            switch_,
            select,
            if_,
            elif_,
            else_,
            label,
            case_,
            default_,
            goto_,
            break_,
            continue_,
            return_,
            compound_,
            exception_,
            try_,
            catch_,
            finally,
            for_,
            while_,
            do_,
            namespace_alias,
            using_,
            LastValue = using_
          };
         }// end of namespace statements;
         namespace declarations // ::declarations::
         {
          namespace util = SK::utility::types;
          namespace fe_ast = SK::compilers::frontends::ast;
          namespace id_i = fe_ast::gsvm::consts::IDs;
          namespace prev_id_i = id_i::_;

          enum { FirstValue = prev_id_i::LastValue + 1,
            data_instance = FirstValue,
            single_var,
            var_decl, // DECL_STATE,
            single_const,
            const_decl, // DECL_STATE,
            single_type,
            type_decl, // DECL_STATE,
            import_,
            sys_import,
            compound_,
            nested_name_for_namespace,
            namespace_fwd,
            namespace_,
            function_fwd,
            function_,
            operator_fwd,
            operator_,
            constructor_fwd,
            constructor_,
            destructor_fwd,
            destructor_,
            bitfield_fwd,
            bitfield_,
            enum_fwd,
            enumerator,
            enum_,
            object_fwd,
            object_,
            union_fwd,
            union_,
          //  forward_,
            friend_,
            member_,
            LastValue = member_
          };
         }// end of namespace declarations;

         // the macro IDs: macro_*
         namespace macro_builtin_funcs
         {
          namespace util = SK::utility::types;
          namespace fe_ast = SK::compilers::frontends::ast;
          namespace id_i = fe_ast::gsvm::consts::IDs;
          namespace prev_id_i = id_i::declarations;

          enum { FirstValue = prev_id_i::LastValue + 1,
            is_const = FirstValue,
            is_func,
            is_float,
            is_int,
            is_oper,
            is_str,
            is_type,
            is_var,
            length,
            typeof_,
            dynamic_cast_,
            reinterpret_cast_,
            sizeof_,
            static_cast_,
            typeid_,
            LastValue = typeid_
          };
         }// end of namespace macro_builtin_funcs;

         namespace macro_builtin_types
         {
          namespace util = SK::utility::types;
          namespace fe_ast = SK::compilers::frontends::ast;
          namespace id_i = fe_ast::gsvm::consts::IDs;
          namespace prev_id_i = id_i::macro_builtin_funcs;

          enum { FirstValue = prev_id_i::LastValue + 1,
            bool_ = FirstValue,
            char_,
            int_,
            float_,
            string_,
            typeinfo_,
            var_,
            vararg_,
            void_,
            doc_,
            code_,
            error_,
            call_,
            system_,
            LastValue = system_
          };
         }// end of namespace macro_builtin_types;
         namespace macro_operators
         {
          namespace util = SK::utility::types;
          namespace fe_ast = SK::compilers::frontends::ast;
          namespace id_i = fe_ast::gsvm::consts::IDs;
          namespace prev_id_i = id_i::macro_builtin_types;

          enum { FirstValue = prev_id_i::LastValue + 1,
            functionCall  = FirstValue,
            arrayIndex,
            memberViaPtr,
            Increment,
            Decrement,
            Plus,
            Minus,
            logicalNot,
            bitwiseNot,
            Ampersand,
            Star,
            cast,
            division,
            modulus,
            bitshiftLeft,
            bitshiftRightArith,
            bitshiftRightLogic,
            lessThan,
            lessThanEqual,
            greaterThan,
            greaterThanEqual,
            equals,
            notEquals,
            bitwiseXor,
            bitwiseOr,
            logicalAnd,
            logicalXor,
            logicalOr,
            assignment,
            assignmentAdd,
            assignmentSub,
            assignmentMul,
            assignmentDiv,
            assignmentMod,
            assignmentBitAnd,
            assignmentBitXor,
            assignmentBitOr,
            assignmentBitSHL,
            assignmentBitSAR,
            assignmentBitSLR,
            sequence
            LastValue = sequence
          };
         }// end of namespace macro_operators;
         namespace macro_identifiers
         {
          namespace util = SK::utility::types;
          namespace fe_ast = SK::compilers::frontends::ast;
          namespace id_i = fe_ast::gsvm::consts::IDs;
          namespace prev_id_i = id_i::macro_operators;

          enum { FirstValue = prev_id_i::LastValue + 1,
            basic_  = FirstValue,
            sys_var,
            var_,
            builtin_func,
            function,
            operators,
            operator_,
            builtin_type,
            type,
            namespace_,
            import_,
            nested_name,
            using_,
            data_decl_,
            LastValue = data_decl_
          };
         }// end of namespace macro_identifiers;

         namespace macro_modifiers
         {
          namespace util = SK::utility::types;
          namespace fe_ast = SK::compilers::frontends::ast;
          namespace id_i = fe_ast::gsvm::consts::IDs;
          namespace prev_id_i = id_i::macro_identifiers;

          enum { FirstValue = prev_id_i::LastValue + 1,
            tempid = FirstValue,
            tempid_arg,
            type_postfix,
            attribute,
            type_infix,
            throw_,
            func_mod,
            static_,
            const_,
            volatile_,
            export_,
            intern_,
            abstract_,
            virtual_,
            data_decl,
            temp_decl,
            temp_decl_arg,
            func_decl_mods,
            func_decl_args,
            func_decl_arg,
            expicit_,
            ctor_init_list,
            ctor_init_mem,
            ctor_decl_,
            dtor_decl_,
            type_infix_decl,
            inherit_list,
            inherit_mem,
            access,
            public_,
            protected_,
            private_,
            LastValue = private_
          };
         }// end of namespace macro_modifiers;
         namespace macro_expressions
         {
          namespace util = SK::utility::types;
          namespace fe_ast = SK::compilers::frontends::ast;
          namespace id_i = fe_ast::gsvm::consts::IDs;
          namespace prev_id_i = id_i::macro_modifiers;

          enum { FirstValue = prev_id_i::LastValue + 1,
            this_ = FirstValue,
            ident,
            postfix,
            array,
            func_call,
            ctorType,
            ctorRef,
            ctorPtr,
            memberRef,
            memberPtr,
            dtorRef,
            dtorPtr,
            postIncrement,
            postDecrement,
            unary,
            unary_opers,
            preIncrement,
            preDecrement,
            unaryPlus,
            unaryMinus,
            logicalNot,
            bitwiseNot,
            getAddress_,
            deref,
            new_,
            delete_,
            multiply,
            division,
            modulus,
            addition,
            subtraction,
            leftShift,
            rightShiftArith,
            rightShiftLogic,
            lesserThan,
            greaterThan,
            lesserEqual,
            greaterEqual,
            equal,
            notEqual,
            bitwiseAnd,
            bitwiseXor,
            bitwiseOr,
            logicalAnd,
            logicalXor,
            logicalOr,
            conditional,
            assign,
            assignMult,
            assignDiv,
            assignMod,
            assignAdd,
            assignSub,
            assignShL,
            assignSAR,
            assignSLR,
            assignBitAnd,
            assignBitXor,
            assignBitOr,
            throw_,
            sequence,
            condition,
            LastValue = condition
          };
         }// end of namespace macro_expressions;
         namespace macro_statements
         {
          namespace util = SK::utility::types;
          namespace fe_ast = SK::compilers::frontends::ast;
          namespace id_i = fe_ast::gsvm::consts::IDs;
          namespace prev_id_i = id_i::macro_expressions;

          enum { FirstValue = prev_id_i::LastValue + 1,
            expression = FirstValue,
            switch_,
            if_,
            elif,
            else_,
            select_,
            case_,
            default_,
            break_,
            continue_,
            return_,
            compound,
            exception,
            catch_,
            finally_,
            for_,
            while_,
            do_,
            namespace_alias,
            using_,
            LastValue = using_
          };
         }// end of namespace macro_statements;
         namespace macro_declarations
         {
          namespace util = SK::utility::types;
          namespace fe_ast = SK::compilers::frontends::ast;
          namespace id_i = fe_ast::gsvm::consts::IDs;
          namespace prev_id_i = id_i::macro_statements;

          enum { FirstValue = prev_id_i::LastValue + 1,
            single_var = FirstValue,
            var_,
            single_type_,
            type_,
            import_,
            compound,
            namespace_fwd,
            namespace_,
            function_fwd,
            function,
            operator_fwd,
            operator_,
            ctor_fwd,
            ctor,
            dtor_fwd,
            dtor,
            object_fwd,
            object_,
            forward_,
            friend_,
            member_,
            LastValue = member_
          };
         }// end of namespace macro_declarations;


      /* - a space for putting peices of "gsvm_ddl_const.h"

      */

      /*
         namespace _
         {
          namespace util = SK::utility::types;
          namespace fe_ast = SK::compilers::frontends::ast;
          namespace id_i = fe_ast::gsvm::consts::IDs;
          namespace prev_id_i = id_i::_;

          enum { FirstValue = prev_id_i::LastValue + 1,
            __  = FirstValue,
            LastValue = ___
          };
         }// end of namespace _;
      */

      /*
        namespace _
        {
          namespace util = SK::utility::types;
          namsspace fe_ast = SK::compilers::frontends::ast;
          namespace id_i = fe_ast::gsvm::consts::IDs;

          enum { FirstValue = FirstValue = id_i::_::LastValue + 1,
            __,
            LastValue
          };
         }
       */
    /*
      namespace _
        {
          namespace util = SK::utility::types;
          namespace fe_ast = SK::compilers::frontends::ast;
          namespace id_i = fe_ast::gsvm::consts::IDs;
          //namespace prev_id_i = id_i::_;

          static const util::uint32 FirstValue = prev_id_i::LastValue + 1;
          static const util::uint32  = ; // "" -
          static const util::uint32 LastValue = ;
          // static const util::uint32  = ; // "" -
        }// end of namespace _;
      */
      static const util::uint32 module_ = 0;
      static const util::uint32 LastValue = module_;
    } //end of namespace IDs.
   } //end of namespace consts.
  } //end of namespace gsvm.

   }// end of namespace ast;
  }// end of namespace frontends;
 } //end of namespace compilers.
} //end of namespace SK.

#endif
