#if 0



 namespace ws
      {
       namespace util = SK::utility::types;
       namespace fe_ast = SK::compilers::frontends::ast;
       namespace id_i = fe_ast::gsvm::consts::IDs;
       static const util::uint32 FirstValue = id_i::null_ + 1;
       static const util::uint32 whiteSpace = FirstValue;
       static const util::uint32 comment = FirstValue + 1;
       static const util::uint32 LastValue = comment;
       }
namespace operators
      {
       namespace util = SK::utility::types;
       namespace fe_ast = SK::compilers::frontends::ast;
       namespace id_i = fe_ast::gsvm::consts::IDs;
       static const util::uint32 FirstValue = id_i::ws::LastValue + 1;
       static const util::uint32 functionCall = FirstValue; // "()" - FUNC_CALL_OP
       static const util::uint32 arrayIndex = FirstValue + 1; //  "[]" - ARRAY_INDEX_OP
       static const util::uint32 memberViaPtr = FirstValue + 2; // "->" - MEMBER_ACCESS_VIA_PTR_OP
       static const util::uint32 increment = FirstValue + 3; // "++"  - INCREMENT_OP
        
       
        static const util::uint32 decrement = FirstValue + 4; // "--"  - DECREMENT_OP
        
        static const util::uint32 addition = FirstValue + 5; // "+" - ADDITION_OP
        
        static const util::uint32 subtraction = FirstValue + 6; // "-" - SUBTRACTION_OP
        
        static const util::uint32 addressof_ = FirstValue + 7; // "addressof" - ADDRESSOF_OP
       static const util::uint32 unaryPlus = FirstValue + 8; // "+" - UNARY_PLUS_OP
        
        static const util::uint32 unaryMinus = FirstValue + 9; // "-" - UNARY_MINUS_OP
        static const util::uint32 logicalNot = FirstValue + 10; // "!" - LOGICAL_NOT_OP
        static const util::uint32 bitwiseNot = FirstValue + 11; // "~" - BITWISE_NOT_OP
        static const util::uint32 getAddress = FirstValue + 12; // "&" - GETADDRESS_OP
        static const util::uint32 dereference = FirstValue + 13; // "*" - DEREFERENCE_OP
        static const util::uint32 bitwiseAnd = FirstValue + 14; // "&" - BITWISE_AND_OP
        static const util::uint32 multiply = FirstValue + 15; // "*" - MULTIPLY_OP
        static const util::uint32 cast = FirstValue + 16; // "cast" - TYPE_CAST_OP
        static const util::uint32 dynamicAlloc = FirstValue + 17; // "new" - DYNAMIC_ALLOCATE_OP
        static const util::uint32 dynamicDealloc = FirstValue + 18; // "delete" - DYNAMIC_DEALLOCATE_OP
        static const util::uint32 memberViaOffset = FirstValue + 19; // "->*" - MEMBER_ACCESS_VIA_PTR_TO_MEMBER_OP
        static const util::uint32 division = FirstValue + 20; // "/" - DIVISION_OP
        static const util::uint32 modulus = FirstValue + 21; // "%" - MODULUS_OP
        static const util::uint32 bitshiftLeft = FirstValue + 22; // "<<", BITSHIFT_LEFT_OP
        static const util::uint32 bitshiftRightArith = FirstValue + 23; // ">>", BITSHIFT_RIGHT_ARITHMETIC_OP
        static const util::uint32 bitshiftRightLogic = FirstValue + 24; // ">>>", BITSHIFT_RIGHT_LOGICAL_OP
        static const util::uint32 lessThan = FirstValue + 25; // "<", LESS_THAN_OP
        static const util::uint32 lessThanEqual = FirstValue + 26; // "<=", LESS_THAN_EQUAL_OP
        static const util::uint32 greaterThan = FirstValue + 27; // ">", GREATER_THAN_OP
        static const util::uint32 greaterThanEqual = FirstValue + 28; // ">=", GREATER_THAN_EUQUAL_OP
        static const util::uint32 equals = FirstValue + 29; // "==", EQUALS_OP
        static const util::uint32 notEquals = FirstValue + 30; // "!=", NOT_EQUALS_OP
        static const util::uint32 bitwiseXor = FirstValue + 31; // "^", BITWISE_XOR_OP
        static const util::uint32 bitwiseOr = FirstValue + 32; // "|", BITWISE_OR_OP
        static const util::uint32 logicalAnd = FirstValue + 33; // "&&", LOGICAL_AND_OP
        static const util::uint32 logicalXor = FirstValue + 34; // "^^", LOGICAL_XOR_OP
        static const util::uint32 logicalOr = FirstValue + 35; // "||", LOGICAL_OR_OP
        static const util::uint32 assignment = FirstValue + 36; // "=", ASSIGNMENT_OP
        static const util::uint32 assignmentAdd = FirstValue + 37; // "+=", ASSIGNMENT_ADDITION_OP
        static const util::uint32 assignmentSub = FirstValue + 38; // "-=", ASSIGNMENT_SUBTRACTION_OP
        static const util::uint32 assignmentMul = FirstValue + 39; // "*=", ASSIGNMENT_MULTIPLY_OP
        static const util::uint32 assignmentDiv = FirstValue + 40; // "/=", ASSIGNMENT_DIVISION_OP
        static const util::uint32 assignmentMod = FirstValue + 41; // "%=", ASSIGNMENT_MODULUS_OP
        static const util::uint32 assignmentBitAnd = FirstValue + 42; // "&=", ASSIGNMENT_BITWISE_AND_OP
        static const util::uint32 assignmentBitXor = FirstValue + 43; // "^=", ASSIGNMENT_BITWISE_XOR_OP
        static const util::uint32 assignmentBitOr = FirstValue + 44; // "|=", ASSIGNMENT_BITWISE_OR_OP
        static const util::uint32 assignmentBitSHL = FirstValue + 45; // "<<=", ASSIGNMENT_BITSHIFT_LEFT_OP
        static const util::uint32 assignmentBitSAR = FirstValue + 46; // ">>=", ASSIGNMENT_BITSHIFT_RIGHT_ARITHMETIC_OP
        static const util::uint32 assignmentBitSLR = FirstValue + 47; // ">>>=", ASSIGNMENT_BITSHIFT_RIGHT_LOGICAL_OP 
        static const util::uint32 sequence = FirstValue + 48; // ",", SEQUENCE_OP
        
        static const util::uint32 LastValue = sequence; 
         
      }
      namespace litterals
      {
       static const util::uint32 FirstValue = id_i::operators::LastValue + 1;
       
        static const util::uint32 char_ = FirstValue; // "" -
        static const util::uint32 wchar = FirstValue + 1; // "" -
        static const util::uint32 mbchar = FirstValue + 2; // "" -
        static const util::uint32 vlchar = FirstValue + 3; // "" -
        static const util::uint32 str = FirstValue + 4; // "" -
        static const util::uint32 wstr = FirstValue + 5; // "" -
        static const util::uint32 mbstr = FirstValue + 6; // "" -
        static const util::uint32 vlstr = FirstValue + 7; // "" -
        static const util::uint32 sint = FirstValue + 8;
        //static const util::uint32 sint8 = FirstValue + 8; // "" - octal negative int
        //static const util::uint32 sint10 = FirstValue + 9; // "" - decimal negative int
        //static const util::uint32 sint16 = FirstValue + 10; // "" - hexadecimal negative int
        static const util::uint32 uint = FirstValue + 9;
        //static const util::uint32 uint8 = FirstValue + 11; // "" - octal positive int
        //static const util::uint32 uint10 = FirstValue + 12; // "" - decimal positive int
        //static const util::uint32 uint16 = FirstValue + 13; // "" - hexadecimal positive int
        static const util::uint32 float_ = FirstValue + 10;
        //static const util::uint32 float8 = FirstValue + 14; // "" - octal floating-point
        //static const util::uint32 float10 = FirstValue + 15; // "" - decimal floating-point
        //static const util::uint32 float16 = FirstValue + 16; // "" - hexadecimal floating-point
        static const util::uint32 boolean = FirstValue + 11; // "" - boolean - true or false
        static const util::uint32 LastValue = boolean;
        
      }                    
      namespace builtin_funcs
      {
          static const util::uint32 FirstValue = id_i::litterals::LastValue + 1;
          static const util::uint32 dynamicCast = FirstValue ; // "" - DYNAMIC_CAST_BF
          static const util::uint32 reinterpretCast = FirstValue + 1; // "" - REINTERPRET_CAST_BF
          static const util::uint32 sizeof_ = FirstValue + 2; // "" - SIZEOF_BF
          static const util::uint32 staticCast = FirstValue + 3; // "" - STATIC_CAST_BF
          static const util::uint32 typeid_ = FirstValue + 4; // "" - TYPEID_BF
          static const util::uint32 LastValue = typeid_; 

      } 
      namespace builtin_types
      {
          static const util::uint32 FirstValue = id_i::builtin_funcs::LastValue + 1;
          static const util::uint32 boolean = FirstValue ; // "" - BOOLEAN_BT
          static const util::uint32 byte = FirstValue + 1; // "" - BYTE_BT
          static const util::uint32 char_ = FirstValue + 2; // "" - CHAR_BT
          static const util::uint32 double_ = FirstValue + 3; // "" - DOUBLE_BT
          static const util::uint32 doubleword = FirstValue + 4; // "" - DOUBLEWORD_BT
          static const util::uint32 float_ = FirstValue + 5; // "" - FLOAT_BT
          static const util::uint32 halfword = FirstValue + 6; // "" - HALFWORD_BT
          static const util::uint32 int_ = FirstValue + 7; // "" - INT_BT
          static const util::uint32 long_ = FirstValue + 8; // "" - LONG_BT
          static const util::uint32 mbchar_ = FirstValue + 9; // "" - MBCHAR_BT
          static const util::uint32 short_ = FirstValue + 10; // "" - SHORT_BT
          static const util::uint32 signed_ = FirstValue + 11; // "" - SIGNED_BT
          static const util::uint32 typeinfo = FirstValue + 12; // "" - TYPEINFO_BT
          static const util::uint32 unsigned_ = FirstValue + 13; // "" - UNSIGNED_BT
          static const util::uint32 void_ = FirstValue + 14; // "" - VOID_BT
          static const util::uint32 vlchar = FirstValue + 15; // "" - VLCHAR_BT
          static const util::uint32 vararg = FirstValue + 16; // "" - VARARG_BT
          static const util::uint32 wchar = FirstValue + 17; // "" - WCHAR_BT
          static const util::uint32 word = FirstValue + 18; // "" - WORD_BT
          static const util::uint32 octet = FirstValue + 19; // "" signed 8 bit int. 
          // static const util::uint32  = ; // "" - [u/s]int[N]{p[M]int[Q]}
         // static const util::uint32  = ; // "" - [u]int[N]{p[M]int[Q]}
         // static const util::uint32  = ; // "" - uint[N]
         
         static const util::uint32 uint8_ = FirstValue + 20; // "" - uint[N]
         static const util::uint32 uint16_ = FirstValue + 21; // "" - uint[N]
         static const util::uint32 uint32_ = FirstValue + 22; // "" - uint[N]
         static const util::uint32 uint64_ = FirstValue + 23; // "" - uint[N]
        // static const util::uint64  = ; // "" - sint[N]
         
         static const util::uint32 sint8_ = FirstValue + 24; // "" - sint[N]
         static const util::uint32 sint16_ = FirstValue + 25; // "" - sint[N]
         static const util::uint32 sint32_ = FirstValue + 26; // "" - sint[N]
         static const util::uint32 sint64_ = FirstValue + 27; // "" - sint[N]
         
         
         // static const util::uint32  = ; // "" - [u/s]int[N]{p[M]int[Q]}
         // static const util::uint32  = ; // "" - [u]int[N]{p[M]int[Q]}
         // static const util::uint64  = ; // "" - uint[N]{p[M]int[Q]}
         static const util::uint32 uint64p8int8_ = FirstValue + 28; // "" - uint[N]{p[M]int[Q]}
         static const util::uint32 uint64p4int16_ = FirstValue + 29; // "" - uint[N]{p[M]int[Q]}
         static const util::uint32 uint64p2int32_ = FirstValue + 30; // "" - uint[N]{p[M]int[Q]}
         
         // static const util::uint64  = ; // "" - sint[N]{p[M]int[Q]}
         static const util::uint32 sint64p8int8_ = FirstValue + 31; // "" - sint[N]{p[M]int[Q]}
         static const util::uint32 sint64p4int16_ = FirstValue + 32; // "" - sint[N]{p[M]int[Q]}
         static const util::uint32 sint64p2int32_ = FirstValue + 33; // "" - sint[N]{p[M]int[Q]}
         
         // static const util::uint32  = ; // "" - float[N]
         // static const util::uint32  = ; // "" - ufloat[N]
         // static const util::uint32  = ; // "" - float[N]{p[M]float[Q]}
         // static const util::uint32  = ; // "" - ufloat[N]{p[M]float[Q]}
         
         // static const util::uint32  = ; // "" - float[N]
         static const util::uint32 float32_ = FirstValue + 34; // "" - float[N]
         static const util::uint32 float64_ = FirstValue + 35; // "" - float[N]
         
         // static const util::uint32  = ; // "" - ufloat[N]
         static const util::uint32 ufloat32_ = FirstValue + 36; // "" - ufloat[N]
         static const util::uint32 ufloat64_ = FirstValue + 37; // "" - ufloat[N]
           
         
         // static const util::uint32  = ; // "" - float[N]{p[M]float[Q]}
         static const util::uint32 float64p2float32_ = FirstValue + 38; // "" - float[N]{p[M]float[Q]}
         
         // static const util::uint32  = ; // "" - ufloat[N]{p[M]float[Q]}
         static const util::uint32 ufloat64p2float32_ = FirstValue + 39; // "" - ufloat[N]{p[M]float[Q]}
         
         // static const util::uint32  = ; // "" - 
         // static const util::uint32  = ; // "" - 
          static const util::uint32 LastValue = ufloat64p2float32_;
      }
      namespace identifiers 
      {
         static const util::uint32 FirstValue = prev_id_i::LastValue + 1;
         static const util::uint32 basic_ = FirstValue; // "" - BASIC_IDENT
         static const util::uint32 var_ = FirstValue + 1; // "" - VAR_IDENT
         static const util::uint32 const_ = FirstValue + 2; // "" - CONST_IDENT
         static const util::uint32 typename_ = FirstValue + 3; // "" - TYPENAME_IDENT
         static const util::uint32 bitfield_ = FirstValue + 4; // "" - BITFIELD_IDENT
         static const util::uint32 enum_ = FirstValue + 5; // "" - ENUM_IDENT
         static const util::uint32 object_ = FirstValue + 6; // "" - OBJECT_IDENT
         static const util::uint32 union_ = FirstValue + 7; // "" - UNION_IDENT
         static const util::uint32 function_type_ = FirstValue + 8; // "" - FUNCTION_TYPE_IDENT
         static const util::uint32 function_type_args_ = FirstValue + 9; // "" - FUNCTION_IDENT_ARGS
         static const util::uint32 function_type_arg_ = FirstValue + 10; // "" - FUNCTION_IDENT_ARG
         static const util::uint32 qual_typeof_ = FirstValue + 11; // "" - QUALIFIED_TYPEOF_IDENT
         static const util::uint32 qual_typeid_ = FirstValue + 12; // "" - QUALIFIED_TYPEID
         static const util::uint32 unqual_typeid_ = FirstValue + 13; // "" - UNQUALIFIED_TYPEID
         static const util::uint32 label_ = FirstValue + 14; // "" - LABEL_IDENT
         static const util::uint32 function_ = FirstValue + 15; // "" - FUNCTION_IDENT
         static const util::uint32 operator_ = FirstValue + 16; // "" - OPERATOR_IDENT
         static const util::uint32 namespace_ = FirstValue + 17; // "" - NAMESPACE_IDENT
         static const util::uint32 import_ = FirstValue + 18; // "" - IMPORT_IDENT
         static const util::uint32 builtin_types_ = FirstValue + 19; // "" - BUILTIN_TYPES_IDENT
         static const util::uint32 builtin_funcs_ = FirstValue + 20; // "" - BUILTIN_FUNC_IDENT
         static const util::uint32 operator_helper_ = FirstValue + 21; // "" - OPERATOR_HELPER
         // operator_helper_, // "" - OPERATOR_HELPER ?            
         static const util::uint32 operator_type_ = FirstValue + 22; // "" - OPERATOR_TYPE_IDENT
         static const util::uint32 nested_name_ = FirstValue + 23; // "" - NESTED_NAME_IDENT
         static const util::uint32 using_ = FirstValue + 24; // "" - USING_IDENT
         static const util::uint32 LastValue = using_;
          // static const util::uint32  = ; // "" -       
      }
      namespace modifiers
      {
          static const util::uint32 FirstValue = prev_id_i::LastValue + 1;
          static const util::uint32 temp_id = FirstValue; // "" -TEMP_ID_MOD
          static const util::uint32 temp_operid = FirstValue + 1; // "" - TEMP_OPERID_MOD
          static const util::uint32 temp_args = FirstValue + 2; // "" - TEMP_MOD_ARGS
          static const util::uint32 temp_arg = FirstValue + 3; // "" - TEMP_MOD_ARG
          static const util::uint32 var_ptr = FirstValue + 4; // "" - VAR_PTR_MOD
          static const util::uint32 var_ref = FirstValue + 5; // "" - VAR_REF_MOD          
          static const util::uint32 const_ptr = FirstValue + 6; // "" - CONST_PTR_MOD
          static const util::uint32 const_ref = FirstValue + 7; // "" - CONST_REF_MOD
          static const util::uint32 type_postfix = FirstValue + 8; // "" - TYPE_POSTFIX_MOD
          static const util::uint32 callconv = FirstValue + 9; // "" - CALLCONV_MOD
          static const util::uint32 lang = FirstValue + 10; // "" - LANG_MOD
          static const util::uint32 throw_ = FirstValue + 11; // "" - THROW_MOD          
          static const util::uint32 throw_args = FirstValue + 12; // "" - THROW_ARGS_MOD
          static const util::uint32 throw_arg = FirstValue + 13; // "" - THROW_ARG_MOD
          static const util::uint32 static_ = FirstValue + 14; // "" - STATIC_MOD
          static const util::uint32 const_ = FirstValue + 15; // "" - CONST_MOD
          static const util::uint32 export = FirstValue + 16; // "" - export - GENDEC_MOD
          static const util::uint32 intern = FirstValue + 17; // "" - intern - GENDEC_MOD
          static const util::uint32 temp_decl = FirstValue + 18; // "" - TEMP_DECL_MOD
          static const util::uint32 temp_operdecl = FirstValue + 19; // "" - TEMP_OPERDECL_MOD
          static const util::uint32 temp_decl_args = FirstValue + 20; // "" - TEMP_DECL_ARGS
          static const util::uint32 temp_decl_arg = FirstValue + 21; // "" - TEMP_DECL_ARG
          static const util::uint32 volatile_ = FirstValue + 22; // "" - VOLATILE_MOD
          static const util::uint32 initializer = FirstValue + 23; // "" - INITIALIZER_MOD
          static const util::uint32 mutable_ = FirstValue + 24; // "" - MUTABLE_MOD
          static const util::uint32 abstract_ = FirstValue + 25; // "" - ABSTRACT_MOD          
          static const util::uint32 virtual_ = FirstValue + 26; // "" - VIRTUAL_MOD
          static const util::uint32 func_decl_args = FirstValue + 27; // "" - FUNC_DECL_ARGS
          static const util::uint32 func_decl_arg = FirstValue + 28; // "" - FUNC_DECL_ARG
          static const util::uint32 explicit_ = FirstValue + 29; // "" - EXPLICIT_MOD
          static const util::uint32 ctor_init_list = FirstValue + 30; // "" - CTOR_INIT_LIST
          static const util::uint32 ctor_init_mem = FirstValue + 31; // "" - CTOR_INIT_MEM
          static const util::uint32 inherit_list = FirstValue + 32; // "" - INHERIT_LIST
          static const util::uint32 inherit_mem = FirstValue + 33; // "" - INHERIT_MEM                    
          static const util::uint32 type_infix = FirstValue + 34; // "" - TYPEDECL_INFIX_MOD
          static const util::uint32 enumerator = FirstValue + 35; // "" - ENUMERATOR_MOD
          static const util::uint32 access = FirstValue + 36; // "" - ACCESS_MOD
          static const util::uint32 LastValue = access;
          // static const util::uint32  = FirstValue + ; // "" -
       
      }
      
      
#endif
