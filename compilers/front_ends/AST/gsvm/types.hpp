#ifndef SK_COMPILERS_FRONTENDS_AST_GSVM_AST_HPP 
#define SK_COMPILERS_FRONTENDS_AST_GSVM_AST_HPP 

#include <vector>
#include <string>

#include "SK/utilities/types.hpp"
#include "SK/compilers/utilities/types.hpp"
#include �SK/compilers/front_ends/AST/tree_node.h� 
#include "lang_consts.hpp"

// this file is now OBSOLETE!!! - ever since we started using 
//   the phoenix functors!!!

namespace SK   
{   
 namespace compilers   
 {
  namespace frontends
  {
   namespace ast
   {
    namespace gsvm   
    {
     namespace types = SK::utility::types;
     namespace cut = SK::compilers::utilities::types;             
     namespace pi = SK::compilers::frontends::ast::internals;
     
     namespace fe_ast = SK::compilers::frontends::ast; 
     namespace id_c = fe_ast::gsvm::consts::IDs;
    /*
     namespace _
     {
      namespace types = SK::utility::types;
      namespace cut = SK::compilers::utilities::types;
      namespace fe_ast = SK::compilers::frontends::ast;
      namespace pi = fe_ast::internals;
      namespace id_c = fe_ast::gsvm::consts::IDs;
      
     }// end of namespace _;
    */ 
     typedef pi::tag_node< id_c::null_ > null_;
     
     null_  nil_(void)
     {
      return null_(0);
     }// end of ...
     
     namespace litterals 
     {
      namespace types = SK::utility::types;
      namespace cut = SK::compilers::utilities::types;
      namespace fe_ast = SK::compilers::frontends::ast;
      namespace pi = fe_ast::internals;
      namespace id_c = fe_ast::gsvm::consts::IDs;
      
      // id_c::litterals:: 
      // numeric and boolean node types: 
      typedef pi::value_node< types::float64, 
              id_c::litterals::float_ > float_;
      typedef pi::value_node< types::sint64, 
              id_c::litterals::sint_ >  sint_;
      typedef pi::value_node< types::uint64,
              id_c::litterals::uint_ > uint_;
      typedef pi::value_node< types::boolean,
              id_c::litterals::bool_ > bool_;
              
      // character nodes:
      typedef pi::value_node< std::basic_string<char>,
              id_c::litterals::char_ >  char_, mbchar_, vlchar_;
      typedef pi::value_node< std::basic_string<wchar_t>,        
              id_c::litterals::wchar_ >  wchar_;              
      // string nodes:
      typedef pi::value_node< std::basic_string<char>,
              id_c::litterals::str_ >    str_, mbstr_, vlstr_;
      typedef pi::value_node< std::basic_string<wchar_t>,
              id_c::litterals::wstr_ >   wstr_;
     }// end of namespace litterals;
     namespace identifiers
     {
      namespace types = SK::utility::types;
      namespace cut = SK::compilers::utilities::types;
      namespace fe_ast = SK::compilers::frontends::ast;
      namespace pi = fe_ast::internals;
      namespace id_c = fe_ast::gsvm::consts::IDs;
      
      
   // typedef pi::value_node< _, id_c::identifiers:: > _; 
   // typedef pi::unary_node< id_c::identifiers:: > _;
   // typedef pi::binary_node< id_c::identifiers:: > _;
   // typedef pi::nary_node< id_c::identifiers:: > _;
      typedef pi::value_node< std::basic_string<char>,
              id_c::identifiers::basic_ > basic_;
      typedef pi::unary_node< id_c::identifiers::var_ > var_;
      typedef pi::unary_node< id_c::identifiers::const_ > const_; 
      typedef pi::unary_node< id_c::identifiers::typename_ > typename_;
      typedef pi::binary_node< id_c::identifiers::bitfield_ > bitfield_;
      typedef pi::binary_node< id_c::identifiers::enum_ > enum_;
      typedef pi::binary_node< id_c::identifiers::object_ > object_;
      typedef pi::binary_node< id_c::identifiers::union_ > union_;
      typedef pi::nary_node< id_c::identifiers::function_type_ > function_type_;
      typedef pi::nary_node< id_c::identifiers::function_type_args_ > 
              function_type_args_;
      typedef pi::nary_node< id_c::identifiers::function_type_arg_ > 
              function_type_arg_;// this has been replaced with two unary '*_const" and "*_var"
      typedef pi::nary_node< id_c::identifiers::operator_type_ > operator_type_;
      typedef pi::unary_node< id_c::identifiers::typeof_ > typeof_;
      typedef pi::nary_node< id_c::identifiers::qual_typeid_ > qual_typeid_;
      typedef pi::nary_node< id_c::identifiers::unqual_typeid_ > unqual_typeid_;
      typedef pi::unary_node< id_c::identifiers::label_ > label_;
      typedef pi::binary_node< id_c::identifiers::function_ > function_;
      typedef pi::binary_node< id_c::identifiers::operator_ > operator_;
      typedef pi::value_node< types::uint64, 
              id_c::identifiers::operators > operators;// the operator`s symbol.
      typedef pi::unary_node< id_c::identifiers::namespace_ > namespace_;
      typedef pi::unary_node< id_c::identifiers::import_ > import_;
      typedef pi::value_node< types::uint64, 
              id_c::identifiers::builtin_types_ > builtin_types_;
      typedef pi::value_node< types::uint64, 
              id_c::identifiers::builtin_funcs_ > builtin_funcs_;
      typedef pi::nary_node< id_c::identifiers::nested_name_ > nested_name_;
      typedef pi::tag_node< id_c::identifiers::scope_sepp > scope_sepp;
      typedef pi::nary_node< id_c::identifiers::using_ > using_;
      typedef pi::nary_node< id_c::identifiers::data_type_ > data_type_;
      
     }// end of namespace identifiers;
     
     // modifiers:
     namespace modifiers
     {
      namespace types = SK::utility::types;
      namespace cut = SK::compilers::utilities::types;
      namespace fe_ast = SK::compilers::frontends::ast;
      namespace pi = fe_ast::internals;
      namespace id_c = fe_ast::gsvm::consts::IDs;
      
      typedef pi::nary_node< id_c::modifiers::temp_id_ > temp_id_; 
      // children of *temp_id_* are *data_type_decl_ident_*s
      typedef pi::nary_node< id_c::modifiers::type_prefix_ > type_prefix_; 
      typedef pi::tag_node< id_c::modifiers::var_ptr_ > var_ptr_;
      typedef pi::tag_node< id_c::modifiers::var_ref_ > var_ref_;
      typedef pi::tag_node< id_c::modifiers::const_ptr_ > const_ptr_;
      typedef pi::tag_node< id_c::modifiers::const_ref_ > const_ref_;
      typedef pi::nary_node< id_c::modifiers::type_postfix_ > type_postfix_;
      typedef pi::unary_node< id_c::modifiers::array_type_ > array_type_;
      typedef pi::value_node< std::basic_string<char>,
                              id_c::modifiers::callconv_ > callconv_;
      typedef pi::nary_node< id_c::modifiers::throw_ > throw_;
      // children of *throw_node* are *data_type_decl_ident_node*s
      //typedef pi::nary_node< id_c::modifiers::throw_arg > throw_arg;
      typedef pi::unary_node< id_c::modifiers::lang_ > lang_;
      typedef pi::tag_node< id_c::modifiers::static_ > static_;
      typedef pi::tag_node< id_c::modifiers::const_ > const_;
      typedef pi::tag_node< id_c::modifiers::export_ > export_;
      typedef pi::tag_node< id_c::modifiers::intern_ > intern_;
      typedef pi::tag_node< id_c::modifiers::volatile_ > volatile_;
      typedef pi::nary_node< id_c::modifiers::temp_decl_ > temp_decl_;
      typedef pi::binary_node< id_c::modifiers::temp_decl_arg_ > 
                                                          temp_decl_arg_; 
    //  typedef pi::unary_node< id_c::modifiers::initializer > initializer_;
      typedef pi::tag_node< id_c::modifiers::mutable_ > mutable_;
      typedef pi::tag_node< id_c::modifiers::abstract_ > abstract_;
      typedef pi::tag_node< id_c::modifiers::virtual_ > virtual_;
      typedef pi::nary_node< id_c::modifiers::func_decl_args_ > func_decl_args_;
      // func_ident_mods,  func_decl_mods,
      typedef pi::tag_node< id_c::modifiers::explicit_ > explicit_;
      typedef pi::nary_node< id_c::modifiers::ctor_init_list_ > ctor_init_list_;
      typedef pi::nary_node< id_c::modifiers::ctor_init_mem_ > ctor_init_mem_;
      typedef pi::nary_node< id_c::modifiers::inherit_list_ > inherit_list_;
      typedef pi::nary_node< id_c::modifiers::inherit_mem_ > inherit_mem_;
      typedef pi::nary_node< id_c::modifiers::type_infix_ > type_infix_;
      typedef pi::binary_node< id_c::modifiers::attribute_> attribute_; // first is a basic_ident, the second is a string_value node.
    //  enumerator
      typedef pi::binary_node< id_c::modifiers::access_ > access_;
      typedef pi::tag_node< id_c::modifiers::public_ > public_;
      typedef pi::tag_node< id_c::modifiers::protected_ > protected_;
      typedef pi::tag_node< id_c::modifiers::private_ > private_;

      
     }// end of namespace modifiers;
     namespace expressions
     {
      namespace types = SK::utility::types;
      namespace cut = SK::compilers::utilities::types;
      namespace fe_ast = SK::compilers::frontends::ast;
      namespace pi = fe_ast::internals;
      namespace id_c = fe_ast::gsvm::consts::IDs;
     // id_c::expressions::
     // typedef pi::value_node< _, id_c::expressions:: > _; 
     // typedef pi::unary_node< id_c::expressions:: > _;
     // typedef pi::binary_node< id_c::expressions:: > _;
     // typedef pi::nary_node< id_c::expressions:: > _;
     // typedef pi::tag_node< id_c::expressions:: > _;
      typedef pi::tag_node< id_c::expressions::this_ > this_;
      typedef pi::binary_node< id_c::expressions::ident > ident;
      typedef pi::unary_node< id_c::expressions::array > array;
      typedef pi::nary_node< id_c::expressions::funcCall > funcCall;// use an alternate with qi::eps for detecting defaulted args!
      typedef pi::tag_node< id_c::expressions::ctorType > ctorType;
      typedef pi::tag_node< id_c::expressions::ctorRef > ctorRef;
      typedef pi::tag_node< id_c::expressions::ctorPtr > ctorPtr;
      typedef pi::unary_node< id_c::expressions::memberRef > memberRef;
      typedef pi::unary_node< id_c::expressions::memberPtr > memberPtr;
      typedef pi::tag_node< id_c::expressions::dtorRef > dtorRef;
      typedef pi::tag_node< id_c::expressions::dtorPtr > dtorPtr;
      typedef pi::tag_node< id_c::expressions::postIncrement > postIncrement;
      typedef pi::tag_node< id_c::expressions::postDecrement > postDecrement;
      typedef pi::nary_node< id_c::expressions::unary > unary;
      typedef pi::nary_node< id_c::expressions::new_ > new_;
      typedef pi::nary_node< id_c::expressions::del > del;
      
      
      
      
     }// end of namespace expressions;
      
    /*
     namespace _
     {
      namespace types = SK::utility::types;
      namespace cut = SK::compilers::utilities::types;
      namespace fe_ast = SK::compilers::frontends::ast;
      namespace pi = fe_ast::internals;
      namespace id_c = fe_ast::gsvm::consts::IDs;
      
     }// end of namespace _;
    */ 

     
// unary ecpresions types:
/*
// typedef unary_node< id_c::operators:: > _node;

//  postIncrement, preIncrement, // "++"  - INCREMENT_OP
      typedef pi::unary_node< id_c::operators::postIncrement > postIncrement_node;
      typedef pi::unary_node< id_c::operators::preIncrement > preIncrement_node;
//  postDecrement, preDecrement,  // "--"  - DECREMENT_OP
      typedef pi::unary_node< id_c::operators::postDecrement > postDecrement_node;
      typedef pi::unary_node< id_c::operators::preDecrement > preDecrement_node;

              addressof_, // "addressof" - ADDRESSOF_OP
              unaryPlus, // "+" - UNARY_PLUS_OP
              unaryMinus, // "-" - UNARY_MINUS_OP
              logicalNot, // "!" - LOGICAL_NOT_OP
              bitwiseNot, // "~" - BITWISE_NOT_OP
              getAddress, // "&" - GETADDRESS_OP
              dereference, // "*" - DEREFERENCE_OP
      typedef pi::unary_node< id_c::operators::addressof_ > addressof_node;
      typedef pi::unary_node< id_c::operators::unaryPlus > unaryPlus_node;
      typedef pi::unary_node< id_c::operators::unaryMinus > unaryMinus_node;
      typedef pi::unary_node< id_c::operators::logicalNot > logicalNot_node;
      typedef pi::unary_node< id_c::operators::bitwiseNot > bitwiseNot_node;
      typedef pi::unary_node< id_c::operators::getAddress > getAddress_node;
      typedef pi::unary_node< id_c::operators::dereference > dereference_node;
              
typedef pi::unary_node< id_c::operators:: > _node;
*/

// binry epresions: 
         
// binary expresion types:
//   typedef pi::binary_node< id_c::operators:: > _node;          
          
// n-ary epresions:      

// n-ary expresion types: 
//  typedef pi::nary_node< id_c::_:: > _node;      

// TO BE DEFINED!!!:
     typedef pi::nary_node< id_c::_ > module_node;
      
     namespace obsolete
     {
      namespace types = SK::utility::types;
      namespace cut = SK::compilers::utilities::types;
      namespace fe_ast = SK::compilers::frontends::ast;
      namespace pi = fe_ast::internals;
      namespace id_c = fe_ast::gsvm::consts::IDs;

//     id_c::litterals:: 
      // numeric and boolean node types: 
      typedef pi::value_node< types::float64, 
              id_c::litterals::float_ > float_node;
      typedef pi::value_node< types::sint64, 
              id_c::litterals::sint >  sint_node;
      typedef pi::value_node< types::uint64,
              id_c::litterals::uint > uint_node;
      typedef pi::value_node< types::boolean,
              id_c::litterals::boolean > bool_node;
      // character nodes:
      typedef pi::value_node< std::basic_string<char>,
              id_c::litterals::char_ >  char_node, mbchar_node, vlchar_node;
      typedef pi::value_node< std::basic_string<wchar_t>,        
              id_c::litterals::wchar >  wchar_node;              
      // string nodes:
      typedef pi::value_node< std::basic_string<char>,
              id_c::litterals::str >    str_node, mbstr_node, vlstr_node;
      typedef pi::value_node< std::basic_string<wchar_t>,
              id_c::litterals::wstr >   wstr_node;
     
     // identifiers:
     typedef pi::value_node< std::basic_string<char>,
              id_c::identifiers::basic_ > basic_ident_node;
//     typedef pi::unary_node< id_c::identifiers:: > _ident_node;
     typedef pi::unary_node< id_c::identifiers::var_ > var_ident_node;
     typedef pi::unary_node< id_c::identifiers::const_ > const_ident_node; 
     typedef pi::unary_node< id_c::identifiers::typename_ > typename_ident_node;    
     typedef pi::binary_node< id_c::identifiers::bitfield_ > bitfield_ident_node;
     typedef pi::binary_node< id_c::identifiers::enum_ > enum_ident_node;
     
     typedef pi::nary_node< id_c::identifiers::data_type_decl > data_type_decl_ident_node;
      // modifiers:
      typedef pi::nary_node< id_c::modifiers::temp_id > temp_id_node; 
      // children of *temp_id_node* are *data_type_decl_ident_node*s
      typedef pi::nary_node< id_c::modifiers::type_prefix > type_prefix_node; 
      typedef pi::tag_node< id_c::modifiers::var_ptr > var_ptr_node;
      typedef pi::tag_node< id_c::modifiers::var_ref > var_ref_node;
      typedef pi::tag_node< id_c::modifiers::const_ptr > const_ptr_node;
      typedef pi::tag_node< id_c::modifiers::const_ref > const_ref_node;
      typedef pi::nary_node< id_c::modifiers::type_postfix > type_postfix_node;
      typedef pi::unary_node< id_c::modifiers::array > array_type_mod_node;
      typedef pi::value_node< std::basic_string<char>,
                              id_c::modifiers::callconv > callconv_node;
      typedef pi::nary_node< id_c::modifiers::throw_ > throw_node;
      // children of *throw_node* are *data_type_decl_ident_node*s
      //typedef pi::nary_node< id_c::modifiers::throw_arg > throw_arg;
      typedef pi::value_node< std::basic_string<char>,
                              id_c::modifiers::lang > lang_node;
      typedef pi::tag_node< id_c::modifiers::static_ > static_node;
      typedef pi::tag_node< id_c::modifiers::const_ > const_node;
      typedef pi::tag_node< id_c::modifiers::export > export_node;
      typedef pi::tag_node< id_c::modifiers::intern > intern_node;
      typedef pi::tag_node< id_c::modifiers::volatile_ > volatile_node;
      typedef pi::nary_node< id_c::modifiers::temp_decl > temp_decl_node;
      typedef pi::binary_node< id_c::modifiers::temp_decl_arg > 
                                                          temp_decl_arg_node; 
      typedef pi::unary_node< id_c::modifiers::initializer > initializer_node;
      typedef pi::tag_node< id_c::modifiers::mutable_ > mutable_node;
      typedef pi::tag_node< id_c::modifiers::abstract_ > abstract_node;
      typedef pi::tag_node< id_c::modifiers::virtual_ > virtual_node;
      typedef pi::nary_node< id_c::modifiers::func_decl_args > func_decl_args_node;
      typedef pi::tag_node< id_c::modifiers::explicit_ > explicit_node;
      typedef pi::nary_node< id_c::modifiers::ctor_init_list > ctor_init_list_node;
      typedef pi::binary_node< id_c::modifiers::ctor_init_mem > ctor_init_mem_node;
      typedef pi::nary_node< id_c::modifiers::inherit_list > inherit_list_node;
      typedef pi::nary_node< id_c::modifiers::inherit_mem > inherit_mem_node;
      typedef pi::nary_node< id_c::modifiers::type_infix > type_infix_node;
    //  enumerator
      typedef pi::binary_node< id_c::modifiers::access > access_node;
      typedef pi::tag_node< id_c::modifiers::public_ > public_node;
      typedef pi::tag_node< id_c::modifiers::protected_ > protected_node;
      typedef pi::tag_node< id_c::modifiers::private_ > private_node;
 
     }// end of namespace obsolete; 
      
    }// end of namespace gsvm;         
   }// end of namespace ast;
  }// end of namespace frontends;
 }// end of namespace compilers;
}// end of namespace SK;
  

#endif
