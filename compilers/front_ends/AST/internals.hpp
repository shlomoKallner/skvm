#ifndef SK_COMPILERS_PARSERS_FRONTENDS_AST_INTERNALS_HPP
#define SK_COMPILERS_PARSERS_FRONTENDS_AST_INTERNALS_HPP

/* // the old includes - they still exist... but don`t use them...

#include "internals/tree_node.hpp"
#include "internals/utility.hpp"
#include "internals/functors.hpp"

*/
// these (below) are the new - version 2 - files:

#include "internals/tree_node2.hpp"
#include "internals/functors2.hpp"

#endif
