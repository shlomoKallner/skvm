#ifndef SK_COMPILERS_PARSERS_FRONTENDS_AST_INTERNALS_TREE_NODE_FUNCTORS_HPP
#define SK_COMPILERS_PARSERS_FRONTENDS_AST_INTERNALS_TREE_NODE_FUNCTORS_HPP

#include <boost/phoenix/function.hpp>

#include "SK/utilities/types/SK_types.h"
#include "SK/compilers/utilities/types.hpp"
#include "utility.hpp"

namespace SK
{
 namespace compilers
 {
  namespace frontends
  {
   namespace ast
   {
    namespace internals
    {
     namespace types = SK::utility::types; //::uint32;
     namespace cut = SK::compilers::utilities::types;
     /*
     
     struct _func
     {
      typedef ast_node_base::node_ptr return_type;
      return_type operator()()
      {
       return return_type( new _node(0) );
      }// end of ...
     };
     
     boost::phoenix::function< _func > const _;
     
     /////
     
     */
             
     struct empty_func
     {
      typedef ast_node_base::node_ptr return_type;
      return_type operator()()
      {
       return return_type( new empty_node(0) );
      }// end of ...
     };
     
     boost::phoenix::function< empty_func > const empty_;
     
     /////
     
     struct tag_func
     {
      typedef ast_node_base::node_ptr return_type;
      return_type operator()( types::uint32 tag_ )
      {
       return return_type( new tag_node_( tag_ ,0 ) );
      }// end of ...
     };
     
     boost::phoenix::function< tag_func > const tag_;
     
     /////
     
     struct value_func
     {
      typedef ast_node_base::node_ptr return_type;
      template< typename T >
      return_type operator()( T const& t , types::uint32 tag_ )
      {
       return return_type( new value_node_<T>( t, tag_, 0) );
      }// end of ...
     };
     
     boost::phoenix::function< value_func > const value_;
     
     /////
     
     struct unary_func
     {
      typedef ast_node_base::node_ptr return_type;
      return_type operator()( return_type & a , types::uint32 tag_ )
      {
       return return_type( new unary_node_( a, tag_, 0) );
      }// end of ...
     };
     
     boost::phoenix::function< unary_func > const unary_;
     
     /////
     
     struct binary_func
     {
      typedef ast_node_base::node_ptr return_type;
      return_type operator()( return_type & a, return_type & b, types::uint32 tag_ )
      {
       return return_type( new binary_node_( a, b, tag_, 0) );
      }// end of ...
     };
     
     boost::phoenix::function< binary_func > const binary_;
     
     /////
     
     struct nary_func
     {
      typedef ast_node_base::node_ptr return_type;
      return_type operator()( nary_node_::container_t & c, types::uint32 tag_ )
      {
       return return_type( new nary_node_( c, tag_, 0) );
      }// end of ...
     };
     
     boost::phoenix::function< nary_func > const nary_;
     
     /////
     
    
    
    /*
     
     struct _func
     {
      typedef ast_node_base::node_ptr return_type;
      return_type operator()()
      {
       return return_type( new _node(0) );
      }// end of ...
     };
     
     boost::phoenix::function< _func > const _;
     
     /////
     
     */ 
    } // end of namespace internals;
   } // end of namespace ast;
  } // end of namespace frontends;
 } // end of namespace compilers;
} // end of namespace SK;

#endif
    
