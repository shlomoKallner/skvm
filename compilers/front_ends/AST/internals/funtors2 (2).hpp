#ifndef SK_COMPILERS_PARSERS_FRONTENDS_AST_INTERNALS_TREE_NODE_FUNCTORS_HPP
#define SK_COMPILERS_PARSERS_FRONTENDS_AST_INTERNALS_TREE_NODE_FUNCTORS_HPP

#include <boost/phoenix/function.hpp>

#include "SK/utilities/types.hpp"
#include "SK/compilers/utilities/types.hpp"
#include "tree_node2.hpp"

namespace SK
{
 namespace compilers
 {
  namespace frontends
  {
   namespace ast
   {
    namespace internals
    {
     namespace types = SK::utility::types; //::uint32;
     namespace cut = SK::compilers::utilities::types;
     /*
     
     struct _func
     {
      typedef ast_node_base::node_ptr return_type;
      return_type operator()()
      {
       return return_type( new _node( , ) );
      }// end of ...
     };
     
     boost::phoenix::function< _func > const _;
     
     /////
     
     */
             
     struct empty_func
     {
      typedef ast_node_base::node_ptr return_type;
      return_type operator()(void)
      {
       return return_type( new ast_node_base() );
      }// end of ...
     };
     
     boost::phoenix::function< empty_func > const empty_;
     
     /////
     
     struct tag_func
     {
      typedef ast_node_base::node_ptr return_type;
      return_type operator()( types::uint32 tag_ )
      {
       return return_type( new ast_node_base( tag_ ) );
      }// end of ...
     };
     
     boost::phoenix::function< tag_func > const tag_;
      
     /////
     
     struct value_func
     {
      typedef ast_node_base::node_ptr return_type;
      template< typename T >
      return_type operator()( T const& t , types::uint32 tag_ )
      {
       return return_type( new ast_node<T>( t, tag_ ) );
      }// end of ...
     };
     
     boost::phoenix::function< value_func > const value_;
     
     /////
     
     struct unary_func
     {
      typedef ast_node_base::node_ptr return_type;
      return_type operator()( return_type & a , types::uint32 tag_ )
      {
       return return_type( new ast_node_base( a, tag_ ) );
      }// end of ...
     };
     
     boost::phoenix::function< unary_func > const unary_;
     
     /////
     struct binary_func
     {
      typedef ast_node_base::node_ptr return_type;
      return_type operator()( return_type & a, return_type & b, 
                              types::uint32 tag_ )
      {
       return return_type( new ast_node_base( a, b, tag_ ) );
      }// end of ...
     };
     
     boost::phoenix::function< binary_func > const binary_;
     
     /////
     
     struct nary_func
     {
      typedef ast_node_base::node_ptr return_type;
      return_type operator()( ast_node_base::container_t & c, 
                              types::uint32 tag_ )
      {
       return return_type( new ast_node_base( c, tag_ ) );
      }// end of ...
     };
     
     boost::phoenix::function< nary_func > const nary_;
     
     /////
     
    
    
    /*
     
     struct _func
     {
      typedef ast_node_base::node_ptr return_type;
      return_type operator()()
      {
       return return_type( new ast_node_base(0) );
      }// end of ...
     };
     
     boost::phoenix::function< _func > const _;
     
     /////
     
     */ 
    } // end of namespace internals;
   } // end of namespace ast;
  } // end of namespace frontends;
 } // end of namespace compilers;
} // end of namespace SK;

#endif
    
