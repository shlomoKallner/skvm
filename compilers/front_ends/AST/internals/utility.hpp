#ifndef SK_COMPILERS_PARSERS_FRONTENDS_AST_INTERNALS_TREE_NODE_UTILITY_HPP
#define SK_COMPILERS_PARSERS_FRONTENDS_AST_INTERNALS_TREE_NODE_UTILITY_HPP

#include "SK/utilities/types/SK_types.h"
#include "SK/compilers/utilities/types.hpp"
#include "tree_node.hpp"

namespace SK
{
 namespace compilers
 {
  namespace frontends
  {
   namespace ast
   {
    namespace internals
    {
     // some utility types:
     struct empty_node : public ast_node<cut::nil_t>
     {
      typedef ast_node<cut::nil_t>  base_type;
      typedef base_type::node_cPtr  node_cPtr;
      typedef base_type::node_ptr   node_ptr;
      typedef empty_node this_type;
      explicit empty_node( node_ptr p = node_ptr() )
      : base_type( base_type::container_t(), cut::nil_t(),
                    p, 0, tags::leaf ){}
      explicit empty_node( node_cPtr p = 0 )
      : base_type( base_type::container_t(), cut::nil_t(),
                    p, 0, tags::leaf ){}
      explicit empty_node( this_type const & v )
      : base_type( base_type::container_t(), cut::nil_t(), 
                   v.parent(), 0, tags::leaf ){} 
      virtual ~empty_node(){}
     };
     
     struct tag_node_ : public ast_node<cut::nil_t>
     {
      typedef ast_node<cut::nil_t>  base_type;
      typedef base_type::node_cPtr  node_cPtr;
      typedef base_type::node_ptr   node_ptr;
      typedef tag_node_ this_type;
      tag_node_( types::uint32 tag_,  node_ptr p = node_ptr() )
      : base_type( base_type::container_t(), cut::nil_t(),
                    p, tag_, tags::leaf ){}
      tag_node_( types::uint32 tag_,  node_cPtr p = 0 )
      : base_type( base_type::container_t(), cut::nil_t(),
                    p, tag_, tags::leaf ){}
      tag_node_( this_type const & v )
      : base_type( base_type::container_t(), cut::nil_t(), 
                   v.parent(), v.tag(), tags::leaf ){} 
      virtual ~tag_node_(){}
     };
     
     template< types::uint32 tag_ >
     struct tag_node : public tag_node_
     {
      typedef tag_node_  base_type;
      typedef base_type::node_cPtr  node_cPtr;
      typedef base_type::node_ptr   node_ptr;
      typedef tag_node_ this_type;
      explicit tag_node( node_ptr p = node_ptr() )
      : base_type( base_type::container_t(), cut::nil_t(),
                    p, tag_, tags::leaf ){}
      explicit tag_node( node_cPtr p = 0 )
      : base_type( base_type::container_t(), cut::nil_t(),
                    p, tag_, tags::leaf ){}
      tag_node_( this_type const & v )
      : base_type( base_type::container_t(), cut::nil_t(), 
                   v.parent(), tag_, tags::leaf ){} 
      virtual ~tag_node_(){}
     };
     
     template< typename T >
     struct value_node_ : public ast_node<T>   
     {
      typedef ast_node<T>           base_type;
      typedef base_type::node_cPtr  node_cPtr;
      typedef base_type::node_ptr   node_ptr;
      typedef value_node_< T >      this_type;      
      value_node_( T& t, types::uint32 tag_,  node_ptr p = node_ptr() )
      : base_type( base_type::container_t(), t, p, tag_, tags::leaf ){}
      value_node_( T& t, types::uint32 tag_,  node_cPtr p = 0 )
      : base_type( base_type::container_t(), t, p, tag_, tags::leaf ){}
      value_node_( this_type const & v )
      : base_type( base_type::container_t(), v.value(), 
                   v.parent(), v.tag(), tags::leaf ){}
      virtual ~value_node_(){}
     };
     
     template< typename T, types::uint32 tag_ >
     struct value_node : public value_node_<T>
     {
      typedef value_node_<T> base_type;
      typedef base_type::node_cPtr  node_cPtr;
      typedef base_type::node_ptr node_ptr;
      typedef value_node< T, tag_ > this_type;
      value_node( T& t, node_ptr p = node_ptr() )
      : base_type( t, tag_, p ){}
      value_node( T& t, node_cPtr p = 0 )
      : base_type( t, tag_, p ){}
      value_node( this_type const & v )
      : base_type( v.value(), tag_, v.parent() )
      {}
      virtual ~value_node(){}
     };

      struct unary_node_  : public ast_node<cut::nil_t>
      {
       typedef ast_node<cut::nil_t> base_type;
       typedef base_type::node_cPtr  node_cPtr;
       typedef base_type::node_ptr node_ptr;
       typedef unary_node_ this_type;
       unary_node_( node_ptr subject, types::uint32 tag_, 
                    node_ptr p = node_ptr() )
       : base_type( push_back1( subject ), cut::nil_t(),
                    p, tag_, tags::unary ) {}
       unary_node_( node_ptr subject, types::uint32 tag_, 
                    node_cPtr p = 0 )
       : base_type( push_back1( subject ), cut::nil_t(),
                    p, tag_, tags::unary ) {}
       unary_node_( this_type const & v )
       : base_type( push_back1( v.child()->clone() ), 
                    cut::nil_t(), v.parent(), v.tag(), tags::unary )
       {}
       virtual ~unary_node_(){}
       /*
       
       base_type::reference child()
       {
        return this->child(0);
       }
       base_type::const_reference child() const
       {
        return this->child(0);
       }
       
       */
       
       protected:
        
        static base_type::container_t& push_back1( node_ptr child_ )
        { // incrementing ref-count
         base_type::container_t c();
         c.push_back( child_->parent() == 0 ? 
                      child_ : child_->clone() );
         return c;
        }// end of ...
        
       
      };

     
      template< types::uint32 tag_ >
      struct unary_node : public unary_node_
      {
       typedef unary_node_ base_type;
       typedef base_type::node_cPtr  node_cPtr;
       typedef base_type::node_ptr node_ptr;
       typedef unary_node< tag_ > this_type;
       unary_node( node_ptr subject, node_ptr p = node_ptr() )
       : base_type( subject, tag_, p )  {}
       unary_node( node_ptr subject, node_cPtr p = 0 )
       : base_type( subject, tag_, p )  {}
       unary_node( this_type const & v ) // acts as a swap fuction!!!
       : base_type( v.child(), tag_, v.parent() ) {} 
       //base_type::container_t(v.begin(),v.end()) ,v.value(),
       virtual ~unary_node(){}
       
      };

      
      struct binary_node_ : public ast_node<cut::nil_t>
      {
       typedef ast_node<cut::nil_t> base_type;
       typedef base_type::node_cPtr  node_cPtr;
       typedef base_type::node_ptr node_ptr;
       typedef binary_node_ this_type;
       typedef mpl::integral_c< tags::node_tags, tags::binary > utag_type;
       utag_type util_tag;
       binary_node_( node_ptr left_, node_ptr right_, types::uint32 tag_, 
                    node_ptr p = node_ptr() )
       : base_type( push_back1( left_, right_ ), cut::nil_t(), 
                    p, tag_, tags::binary )
       {}
       binary_node_( node_ptr left_, node_ptr right_, types::uint32 tag_, 
                    node_cPtr p = 0 )
       : base_type( push_back1( left_, right_ ), cut::nil_t(), 
                    p, tag_, tags::binary )
       {}
       binary_node_( this_type const & v ) 
       : base_type( push_back1( v.left(), v.right() ), cut::nil_t(), 
                    v.parent(), v.tag(), tags::binary )
       {}
       virtual ~binary_node_(){}
       
       /* - these are now part of the ast_node_base type!
       
       base_type::reference left()
       {
        return this->child(0);
       }
       base_type::const_reference left() const
       {
        return this->child(0);
       }
       
       base_type::reference right()
       {
        return this->child(1);
       }
       base_type::const_reference right() const
       {
        return this->child(1);
       }
       
       
       */
       protected:
        static base_type::container_t& push_back1( node_ptr left_, node_ptr right_ )
        { // deep copy if already has a parent else inc ref-count.
         base_type::container_t c();
         c.push_back( left_->parent() == 0 ? left_ : left_->clone() ); // 
         c.push_back( right_->parent() == 0 ? right_ : right_->clone() );
         return c;
        }
        
      };
      

      template< types::uint32 tag_ >
      struct binary_node: public binary_node_
      {
       typedef binary_node_ base_type;
       typedef base_type::node_cPtr  node_cPtr;
       typedef base_type::node_ptr node_ptr;
       typedef binary_node<tag_> this_type;
       binary_node( node_ptr left_, node_ptr right_, node_ptr p = node_ptr() )
       : base_type( left_, right_, tag_, p  )  {}
       binary_node( node_ptr left_, node_ptr right_, node_cPtr p = 0 )
       : base_type( left_, right_, tag_, p  )  {}
       binary_node( this_type const & v )
       : base_type( v.left()->clone(),
                    v.right()->clone(), 
                    tag_, v.parent() )
       {} // base_type::container_t(v.begin(),v.end())
       virtual ~binary_node(){}
      };
      
      

      struct nary_node_ : public ast_node<cut::nil_t>
      {
       typedef ast_node<cut::nil_t> base_type;
       typedef base_type::node_cPtr  node_cPtr;
       typedef base_type::node_ptr node_ptr;
       typedef base_type::container_t  container_t;
       typedef nary_node_ this_type;
       nary_node_( container_t& c , types::uint32 tag_, 
                   node_ptr p = node_ptr() )
       : base_type( c, cut::nil_t(), p, tag_, tags::nary )
       {}
       nary_node_( container_t& c , types::uint32 tag_, 
                   node_cPtr p = 0 )
       : base_type( c, cut::nil_t(), p, tag_, tags::nary )
       {}
       nary_node_( this_type const & v )
       : base_type( copy_container( v.children_m  ), cut::nil_t(), 
                    v.parent(), v.tag(), tags::nary )
       {}
       virtual ~nary_node_(){}
      };


      template< types::uint32 tag_ >
      struct nary_node : public nary_node_
      {
       typedef nary_node_ base_type;
       typedef base_type::node_cPtr  node_cPtr;
       typedef base_type::node_ptr node_ptr;
       typedef base_type::container_t  container_t;
       typedef nary_node<tag_> this_type;
       nary_node( container_t& c , node_ptr p = node_ptr() )
       : base_type( c, tag_, p )   {}
       nary_node( container_t& c , node_cPtr p = 0 )
       : base_type( c, tag_, p )   {}
       nary_node( this_type const & v )
       : base_type( copy_container( v.children_m  ), tag_, v.parent() )
       {}
       virtual ~nary_node(){}
      };

    } // end of namespace internals;
   } // end of namespace ast;
  } // end of namespace frontends;
 } // end of namespace compilers;
} // end of namespace SK;

#endif
