#ifndef SK_COMPILERS_PARSERS_FRONTENDS_AST_INTERNALS_TREE_NODE_HPP
#define SK_COMPILERS_PARSERS_FRONTENDS_AST_INTERNALS_TREE_NODE_HPP

#include <algorithm>
#include <stdexcept>
#inlcude <boost/shared_ptr.hpp>
#include <boost/type_traits/is_same.hpp>
#include <boost/mpl/mpl.hpp>
#include "SK/utilities/types/SK_types.h"
#include "SK/compilers/utilities/types.hpp"

namespace SK
{
 namespace compilers
 {
  namespace frontends
  {
   namespace ast
   {
    namespace internals
   {
    namespace mpl = boost::mpl;
    namespace types = SK::utility::types; //::uint32;
    namespace cut = SK::compilers::utilities::types;
//    using SK::compilers::utilities::types::nil; // this should be changed.
    
    namespace tags
     {
      enum node_tags ( null_ = 0, leaf, unary, binary, nary );
     }// end of namespace tags;
    
    class ast_node_base // formerly just "ast_node"
    {
 // Note: 
 // this type is on purpose a non templated type; 
 // that way we can derive many types that can be
 // contained in the tree by simply defining pointers 
 // to derived types that are later up-casted. 
 // end of note.
 // private data members: 
         ast_node_base * parent_m;
         types::uint32 tag_m;
         tags::node_tags util_tag_m;
        public:
          typedef ast_node_base * node_cPtr;
          typedef ast_node_base this_type;
          typedef boost::shared_ptr<ast_node_base> node_ptr;
          typedef cut::nil_t value_type;
          // the constructors:
          ast_node_base(void) 
          : parent_m(0), tag_m(0) {}
          ast_node_base( node_cPtr p, types::uint32 t = 0, tags::node_tags u = tags::null_ ) 
          : parent_m(p), tag_m(t), util_tag_m(u) {}
          ast_node_base( node_ptr p, types::uint32 t = 0, tags::node_tags u = tags::null_ ) 
          : parent_m(p.get()), tag_m(t), util_tag_m(u) {}
          // the copy constructor:
          ast_node_base( const this_type& n ) 
          : parent_m( n.parent_m), tag_m( n.tag_m), util_tag_m(n.util_tag_m) {}
          // the destructor:
          virtual ~ast_node_base(){}
          // the member functions
          
          const this_type& operator=( const this_type& t )
          {
           this->assign( t );
           return *this;
          }// end of ...
          
          void assign( const this_type& a )
          {
           parent_m = a.parent_m;
           tag_m = a.tag_m;
           util_tag_m = a.util_tag_m;
          } // end of ...
          
          this_type copy(void)const
          {
           return this_type( parent_m, tag_m, util_tag_m );
          } // end of ...
          
          node_ptr clone( types::boolean use_par = false )const
          {
           return node_ptr( new this_type( use_par ? parent_m : 0, 
                                           tag_m, util_tag_m ) );
          } // end of ...
          
          void swap( this_type& a )
          {
           std::swap( parent_m, a.parent_m );
           std::swap( tag_m, a.tag_m );
           std::swap( util_tag_m, a.util_tag_m );
          } // end of ...
          
          node_cPtr parent() const
          { return parent_m; }
          void parent( node_cPtr p ) 
          { parent_m = p } 
          types::uint32 tag() const 
          { return tag_m; }
          void tag( types::uint32 t ) 
          { tag_m = t; }
          tags::node_tags util_tag() const 
          { return util_tag_m; }
          void util_tag( tags::node_tags u )
          { util_tag_m = u; }
         /* // these will be define in the real ast_node class.
          virtual types::boolean is_leaf() const
          { return false; }
          virtual types::boolean is_root() const
          { return false; }
          virtual types::boolean has_value()const
          { return false; }
          virtual types::boolean has_children()const
          { return false; } // has_children() may not be equivelent to 
                            // !is_leaf() or to is_root()!!!
          virtual types::uint32 num_children()const
          { return 0; } // if is_leaf() == true then return 0.
          virtual value_type value( void ) const
          { return value_type(); }
          virtual void value( value_type ) {}
          virtual node_ptr child( uint32 = 0)  { return node_ptr(); }
          virtual void child( uint32 = 0, node_ptr = node_ptr() ) {}
         */ 
    };

      

#define SK_TREE_NODE_USE_VECTOR 1
#undef  SK_TREE_NODE_USE_LIST
#undef  SK_TREE_NODE_USE_DEQUE

    template< typename ValueT = cut::nil_t >
    class ast_node : public ast_node_base 
    { 
      public:
        typedef ast_node_base::node_cPtr      node_cPtr;
        typedef ast_node_base::node_ptr       node_ptr;
        typedef ast_node< ValueT >            ast_node;
#ifdef SK_TREE_NODE_USE_VECTOR)
        typedef std::vector<node_ptr>         ContainerT;
#elif defined(SK_TREE_NODE_USE_LIST 
        typedef std::list<node_ptr>           ContainerT;  
#elif defined(SK_TREE_NODE_USE_DEQUE)
        typedef std::deque<node_ptr>          ContainerT;  
#endif
        typedef ValueT                        value_type;
        typedef ContainerT                    container_t;
        typedef container_t::iterator         iterator;
        typedef container_t::const_iterator   const_iterator;
        typedef container_t::reference        reference;
        typedef container_t::const_reference  const_reference;
        // the constructor:
        ast_node(void) 
        : ast_node_base(), value_m(), children_m() {}
        ast_node( container_t& c, value_type v = value_type(),
                  node_cPtr p = 0, types::uint32 t = 0, tags::node_tags u = tags::null_ )
        : ast_node_base( p, t, u ), value_m(v), 
          children_m( set_children_with_parent(c) ) {}
        ast_node( container_t& c, value_type v = value_type(),
                  node_ptr p = node_ptr(), types::uint32 t = 0, tags::node_tags u = tags::null_ )
        : ast_node_base( p, t, u ), value_m(v), 
          children_m( set_children_with_parent(c) ) {}
        // the copy constructor:
        ast_node( const ast_node& n )
        : ast_node_base( n.parent(), n.tag(), n.util_tag() ), value_m( n.value_m ), 
         children_m( set_children_with_parent( n.begin(), n.end() ) ) {}
        // the destructor
        virtual ~ast_node(){}
        
        const ast_node& operator=( const ast_node& a )
        {
         this->assign( a );
         return *this;
        }// end of ...
        
        void assign( const ast_node& a )
        {
         this->as_base().assign( a.as_base() );
         value_m = a.value_m;
         children_m.clear();
         container_t c( copy_container( a.children_m  ) );
         children_m.swap( c.begin() , c.end() );
        } // end of ....
        
        ast_node copy(void)const
        {
         return ast_node( copy_container( children_m ), 
                          value_m, parent(), tag(), util_tag() );
        } // end of ....
        
        virtual node_ptr clone( types::boolean use_par = false )const
        {
         return node_ptr( new ast_node( copy_container( children_m ), 
                              value_m, use_par ? parent() : 0, tag(), util_tag() ) );
        } // end of ....
        
        void swap( ast_node& a )
        {
          this->as_base().swap( a.as_base() ); 
          std::swap( value_m , a.value_m );
          children_m.swap( a.children_m );
          set_children_with_parent( children_m );
          a.set_children_with_parent( a.children_m );   
        } // end of ....
        // the member functions:
        virtual types::boolean is_leaf() const
        { return chilren_m.size() == 0; }
         
        virtual types::boolean is_root() const
        { return !this->is_leaf(); }
          
        virtual types::boolean has_value() const
        { return ! boost::is_same< ValueT , nil_t >::value ;  }
          
        virtual types::boolean has_children() const
        { return children_m.size() != 0; } // the same as is_root()!!!
         
        virtual types::uint32 num_children() const
        { return children_m.size(); }
         
        value_type value() const
        { return value_m; }
         
        void value( value_type v ) 
        { value_m = v; }
      
        iterator begin() 
        { return children_m.begin(); }
        
        const_iterator begin() const
        { return children_m.begin(); }
        
        iterator end() 
        { return children_m.end(); }
        
        const_iterator end() const 
        { return children_m.end(); }
        
        reference child( types::uint32 i = 0 ) 
        { // formerly "virtual node_ptr child(uint32);"
          if( i >= 0 && i < children_m.size() )
          {
#ifdef SK_TREE_NODE_USE_LIST 
           iterator j = children_m.begin()
           while( i-- > 0 ) 
           { j++; }
           return *j;
#else
           return children_m.at(i);
#endif
          }
          else
           throw std::out_of_range(""); // fix me: throw instead!!!
        } // end of function reference child( types::uint32 i );

        const_reference child( types::uint32 i = 0 ) const
        { // formerly "virtual void child(uint32, node_ptr);"
          if( i >= 0 && i < children_m.size() )
          {
#ifdef SK_TREE_NODE_USE_LIST               
           const_iterator j = children_m.begin()
           while( i-- > 0 ) 
           { j++; }
           return *j; 
#else
           return children_m.at(i); //children_m.at(i) = n;            
#endif           
          }
          else
           throw std::out_of_range(""); 
        }  // end of function const_reference child( types::uint32 i );
       
       // helper accessors: 
       reference left()
       {
        return this->child(0);
       }
       const_reference left() const
       {
        return this->child(0);
       }
       
       reference right()
       {
        return this->child(1);
       }
       const_reference right() const
       {
        return this->child(1);
       }
       
       /* // obsolete functions:
        container_t children() 
        { return children_m; }
        void children( container_t c ) 
        { children_m = set_children_with_parent(c); }
        
        iterator first_child() 
        { return children_m.begin(); }
        
        const_iterator first_child() const
        { return children_m.begin(); }
        
        iterator last_child() 
        { return --children_m.end(); }
        
        const_iterator last_child() const 
        { return --children_m.end(); }
       */ 
     protected:
      
      ast_node_base& as_base(void)const
      {
       return *static_cast< node_cPtr >( this );         
      }
        
      
      static const container_t& copy_container( const container_t& c )
      {
       container_t c1();
       if( !c.empty() )
       { 
        for( iterator i = c.begin(); i != c.end(); i++ )
        { c1.push_back( (*i)->clone() ); }
       } 
       return c1;
      }
      
      container_t const& set_children_with_parent( container_t& c )
      {
       if( !c.empty() )
       { 
        for( iterator i = c.begin(); i != c.end(); i++ )
        { i->parent(this); }
       } 
       return c;
      }
      
      container_t const& set_children_with_parent( iterator first_, iterator last_ )
      {
       container_t c();
       if( first_ != last_ )
       { 
        for( iterator i = first_; i != last_; i++ )
         c.push_back( (*i)->clone() );
       } 
       return set_children_with_parent(c);
      }
      
      ValueT value_m;
      container_t children_m;         
    };
      
      
        
    } // end of namespace internals;
   } // end of namespace ast;
  } // end of namespace frontends;
 } // end of namespace compilers;
} // end of namespace SK;

#endif
