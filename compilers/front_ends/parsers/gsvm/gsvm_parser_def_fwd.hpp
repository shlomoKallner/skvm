#ifndef SK_COMPILERS_FRONTENDS_PARSERS_GSVM_PARSE_DEF_FWD_HPP
#define SK_COMPILERS_FRONTENDS_PARSERS_GSVM_PARSE_DEF_FWD_HPP

// #include <>

#include <boost/spirit/include/qi.hpp>
#include <boost/phoenix/core.hpp>
#include <boost/phoenix/function.hpp>
#include <boost/phoenix/operator.hpp>
#include <boost/phoenix/statement.hpp>
#include <boost/phoenix/object.hpp>
#include <boost/phoenix/scope.hpp>
#include <boost/phoenix/stl/container.hpp>


// #include ""

#include "SK/utilities/types.hpp"
#include "SK/compilers/front_ends/AST/gsvm/lang_consts.hpp"
#include �SK/compilers/front_ends/AST/internals.h�
#include �SK/compilers/front_ends/parsers/internals.h�
#include "symbol_tables.hpp"

// the fallowing include is no longer needed
//  due the new phoenix functor types!!! ( both ver. 1 and ver. 2 )
//#include "SK/compilers/front_ends/AST/gsvm/types.hpp"

namespace SK
{
 namespace compilers
 {
  namespace frontends
  {
   namespace parsers
   {
    namespace gsvm
    {
     namespace spirit = boost::spirit;
     namespace qi = boost::spirit::qi;
     namespace phoenix = boost::phoenix;

     namespace types = SK::utility::types;
     namespace internals = SK::compilers::parsers::internals;
     namespace ast = SK::compilers::frontends::ast::internals;
     namespace ast_ = SK::compilers::frontends::ast::gsvm;

     namespace char_usage
     {
      enum { null_ = 0, use_char, use_wchar,
             use_mbchar, use_vlchar };
     }// end of namespace char_usage;

     namespace template_type // template_type::null_  template_type::oper
     {
      enum { null_ = 0, oper };
     }// end of namespace template_type;

     template< typename IteratorT, typename CharT >
     struct gsvm_parser;

     template< typename IteratorT, typename CharT >
     struct gsvm_litteral_parser;

     template< typename IteratorT, typename CharT >
     struct gsvm_ident_parser;

     template< typename IteratorT, typename CharT >
     struct gsvm_modifier_parser;

    } // end of namespace gsvm;
   } // end of namespace parsers;
  }// end of namespace frontends;
 } // end of namespace compilers;
} // end of namespace SK;

#endif
