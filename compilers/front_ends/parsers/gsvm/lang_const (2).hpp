#ifndef SK_COMPILERS_GSVM_LANG_CONST_HPP
#define SK_COMPILERS_GSVM_LANG_CONST_HPP

#include "SK/utilities/types.hpp"

namespace SK   
{   
 namespace compilers   
 {   
  namespace gsvm   
  {
   namespace consts
   {
     namespace util = SK::utility::types;
     
     namespace IDs
     { 
       /* 
        
        namespace _
        {
          namespace util = SK::utility::types;
          namespace id_i = SK::compilers::gsvm::consts::IDs;
          //static const util::uint64 FirstValue = id_i::_::LastValue + 1;
        
          static const util::uint64 FirstValue = ;
          static const util::uint64  = ; // "" -
          static const util::uint64 LastValue = ;
          // static const util::uint64  = ; // "" -
         }
         namespace _
         {
          enum __e 
          { FirstValue = 0,
            __,
            LastValue   
          };
         }// end of namespace _; 
       */
      static const util::uint64 null_ = 0; // null value.
      namespace ws
      {
       namespace util = SK::utility::types;
       namespace id_i = SK::compilers::gsvm::consts::IDs;
        
       static const util::uint64 FirstValue = id_i::null_ + 1;
       static const util::uint64 whiteSpace = FirstValue;
       static const util::uint64 comment = FirstValue + 1;
       static const util::uint64 LastValue = comment;
      }
      namespace operators
      {
       namespace util = SK::utility::types;
       namespace id_i = SK::compilers::gsvm::consts::IDs;
      
        static const util::uint64 FirstValue = id_i::ws::LastValue + 1;
        static const util::uint64 functionCall = FirstValue; // "()" - FUNC_CALL_OP
        static const util::uint64 arrayIndex = FirstValue + 1; //  "[]" - ARRAY_INDEX_OP
        static const util::uint64 memberViaPtr = FirstValue + 2; // "->" - MEMBER_ACCESS_VIA_PTR_OP
        static const util::uint64 increment = FirstValue + 3; // "++"  - INCREMENT_OP
        static const util::uint64 decrement = FirstValue + 4; // "--"  - DECREMENT_OP
        static const util::uint64 addition = FirstValue + 5; // "+" - ADDITION_OP
        static const util::uint64 subtraction = FirstValue + 6; // "-" - SUBTRACTION_OP
        static const util::uint64 addressof_ = FirstValue + 7; // "addressof" - ADDRESSOF_OP
        static const util::uint64 unaryPlus = FirstValue + 8; // "+" - UNARY_PLUS_OP
        static const util::uint64 unaryMinus = FirstValue + 9; // "-" - UNARY_MINUS_OP
        static const util::uint64 logicalNot = FirstValue + 10; // "!" - LOGICAL_NOT_OP
        static const util::uint64 bitwiseNot = FirstValue + 11; // "~" - BITWISE_NOT_OP
        static const util::uint64 getAddress = FirstValue + 12; // "&" - GETADDRESS_OP
        static const util::uint64 dereference = FirstValue + 13; // "*" - DEREFERENCE_OP
        static const util::uint64 bitwiseAnd = FirstValue + 14; // "&" - BITWISE_AND_OP
        static const util::uint64 multiply = FirstValue + 15; // "*" - MULTIPLY_OP
        static const util::uint64 cast = FirstValue + 16; // "cast" - TYPE_CAST_OP
        static const util::uint64 dynamicAlloc = FirstValue + 17; // "new" - DYNAMIC_ALLOCATE_OP
        static const util::uint64 dynamicDealloc = FirstValue + 18; // "delete" - DYNAMIC_DEALLOCATE_OP
        static const util::uint64 memberViaOffset = FirstValue + 19; // "->*" - MEMBER_ACCESS_VIA_PTR_TO_MEMBER_OP
        static const util::uint64 division = FirstValue + 20; // "/" - DIVISION_OP
        static const util::uint64 modulus = FirstValue + 21; // "%" - MODULUS_OP
        static const util::uint64 bitshiftLeft = FirstValue + 22; // "<<", BITSHIFT_LEFT_OP
        static const util::uint64 bitshiftRightArith = FirstValue + 23; // ">>", BITSHIFT_RIGHT_ARITHMETIC_OP
        static const util::uint64 bitshiftRightLogic = FirstValue + 24; // ">>>", BITSHIFT_RIGHT_LOGICAL_OP
        static const util::uint64 lessThan = FirstValue + 25; // "<", LESS_THAN_OP
        static const util::uint64 lessThanEqual = FirstValue + 26; // "<=", LESS_THAN_EQUAL_OP
        static const util::uint64 greaterThan = FirstValue + 27; // ">", GREATER_THAN_OP
        static const util::uint64 greaterThanEqual = FirstValue + 28; // ">=", GREATER_THAN_EUQUAL_OP
        static const util::uint64 equals = FirstValue + 29; // "==", EQUALS_OP
        static const util::uint64 notEquals = FirstValue + 30; // "!=", NOT_EQUALS_OP
        static const util::uint64 bitwiseXor = FirstValue + 31; // "^", BITWISE_XOR_OP
        static const util::uint64 bitwiseOr = FirstValue + 32; // "|", BITWISE_OR_OP
        static const util::uint64 logicalAnd = FirstValue + 33; // "&&", LOGICAL_AND_OP
        static const util::uint64 logicalXor = FirstValue + 34; // "^^", LOGICAL_XOR_OP
        static const util::uint64 logicalOr = FirstValue + 35; // "||", LOGICAL_OR_OP
        static const util::uint64 assignment = FirstValue + 36; // "=", ASSIGNMENT_OP
        static const util::uint64 assignmentAdd = FirstValue + 37; // "+=", ASSIGNMENT_ADDITION_OP
        static const util::uint64 assignmentSub = FirstValue + 38; // "-=", ASSIGNMENT_SUBTRACTION_OP
        static const util::uint64 assignmentMul = FirstValue + 39; // "*=", ASSIGNMENT_MULTIPLY_OP
        static const util::uint64 assignmentDiv = FirstValue + 40; // "/=", ASSIGNMENT_DIVISION_OP
        static const util::uint64 assignmentMod = FirstValue + 41; // "%=", ASSIGNMENT_MODULUS_OP
        static const util::uint64 assignmentBitAnd = FirstValue + 42; // "&=", ASSIGNMENT_BITWISE_AND_OP
        static const util::uint64 assignmentBitXor = FirstValue + 43; // "^=", ASSIGNMENT_BITWISE_XOR_OP
        static const util::uint64 assignmentBitOr = FirstValue + 44; // "|=", ASSIGNMENT_BITWISE_OR_OP
        static const util::uint64 assignmentBitSHL = FirstValue + 45; // "<<=", ASSIGNMENT_BITSHIFT_LEFT_OP
        static const util::uint64 assignmentBitSAR = FirstValue + 46; // ">>=", ASSIGNMENT_BITSHIFT_RIGHT_ARITHMETIC_OP
        static const util::uint64 assignmentBitSLR = FirstValue + 47; // ">>>=", ASSIGNMENT_BITSHIFT_RIGHT_LOGICAL_OP 
        static const util::uint64 sequence = FirstValue + 48; // ",", SEQUENCE_OP
        static const util::uint64 LastValue = sequence; 
       
      }
      
      namespace litterals
      {
       namespace util = SK::utility::types;
       namespace id_i = SK::compilers::gsvm::consts::IDs;
      
       static const util::uint64 FirstValue = id_i::operators::LastValue + 1;
        static const util::uint64 char_ = FirstValue; // "" -
        static const util::uint64 wchar = FirstValue + 1; // "" -
        static const util::uint64 mbchar = FirstValue + 2; // "" -
        static const util::uint64 vlchar = FirstValue + 3; // "" -
        static const util::uint64 str = FirstValue + 4; // "" -
        static const util::uint64 wstr = FirstValue + 5; // "" -
        static const util::uint64 mbstr = FirstValue + 6; // "" -
        static const util::uint64 vlstr = FirstValue + 7; // "" -
        static const util::uint64 sint = FirstValue + 8;
        //static const util::uint64 sint8 = FirstValue + 8; // "" - octal negative int
        //static const util::uint64 sint10 = FirstValue + 9; // "" - decimal negative int
        //static const util::uint64 sint16 = FirstValue + 10; // "" - hexadecimal negative int
        static const util::uint64 uint = FirstValue + 9;
        //static const util::uint64 uint8 = FirstValue + 11; // "" - octal positive int
        //static const util::uint64 uint10 = FirstValue + 12; // "" - decimal positive int
        //static const util::uint64 uint16 = FirstValue + 13; // "" - hexadecimal positive int
        static const util::uint64 float_ = FirstValue + 10;
        //static const util::uint64 float8 = FirstValue + 14; // "" - octal floating-point
        //static const util::uint64 float10 = FirstValue + 15; // "" - decimal floating-point
        //static const util::uint64 float16 = FirstValue + 16; // "" - hexadecimal floating-point
        static const util::uint64 boolean = FirstValue + 11; // "" - boolean - true or false
        static const util::uint64 LastValue = boolean;
        
      } // end of namespace litterals.
      
      namespace builtin_funcs
      {
       namespace util = SK::utility::types;
       namespace id_i = SK::compilers::gsvm::consts::IDs;
       //static const util::uint64 FirstValue = id_i::_::LastValue + 1;
      
          static const util::uint64 FirstValue = id_i::litterals::LastValue + 1;
          static const util::uint64 dynamicCast = FirstValue ; // "" - DYNAMIC_CAST_BF
          static const util::uint64 reinterpretCast = FirstValue + 1; // "" - REINTERPRET_CAST_BF
          static const util::uint64 sizeof_ = FirstValue + 2; // "" - SIZEOF_BF
          static const util::uint64 staticCast = FirstValue + 3; // "" - STATIC_CAST_BF
          static const util::uint64 typeid_ = FirstValue + 4; // "" - TYPEID_BF
          static const util::uint64 LastValue = typeid_; 
           
      } //end of namespace builtin_funcs.
      namespace builtin_types
      {
       namespace util = SK::utility::types;
       namespace id_i = SK::compilers::gsvm::consts::IDs;
       //static const util::uint64 FirstValue = id_i::_::LastValue + 1;
      
          static const util::uint64 FirstValue = id_i::builtin_funcs::LastValue + 1;
          static const util::uint64 boolean = FirstValue + 1; // "" - BOOLEAN_BT
          static const util::uint64 byte = FirstValue + 2; // "" - BYTE_BT
          static const util::uint64 char_ = FirstValue + 3; // "" - CHAR_BT
          static const util::uint64 double_ = FirstValue + 4; // "" - DOUBLE_BT
          static const util::uint64 doubleword = FirstValue + 5; // "" - DOUBLEWORD_BT
          static const util::uint64 float_ = FirstValue + 6; // "" - FLOAT_BT
          static const util::uint64 halfword = FirstValue + 7; // "" - HALFWORD_BT
          static const util::uint64 int_ = FirstValue + 8; // "" - INT_BT
          static const util::uint64 long_ = FirstValue + 9; // "" - LONG_BT
          static const util::uint64 mbchar_ = FirstValue + 10; // "" - MBCHAR_BT
          static const util::uint64 short_ = FirstValue + 11; // "" - SHORT_BT
          static const util::uint64 signed_ = FirstValue + 12; // "" - SIGNED_BT
          static const util::uint64 typeinfo = FirstValue + 13; // "" - TYPEINFO_BT
          static const util::uint64 unsigned_ = FirstValue + 14; // "" - UNSIGNED_BT
          static const util::uint64 void_ = FirstValue + 15; // "" - VOID_BT
          static const util::uint64 vlchar = FirstValue + 16; // "" - VLCHAR_BT
          static const util::uint64 vararg = FirstValue + 17; // "" - VARARG_BT
          static const util::uint64 wchar = FirstValue + 18; // "" - WCHAR_BT
          static const util::uint64 word = FirstValue + 19; // "" - WORD_BT
          static const util::uint64 octet = FirstValue + 20; // "" signed 8 bit int. 
         
         // static const util::uint64  = ; // "" - [u/s]int[N]{p[M]int[Q]}
         // static const util::uint64  = ; // "" - [u]int[N]{p[M]int[Q]}
         // static const util::uint64  = ; // "" - uint[N]
         
         static const util::uint64 uint8_ = FirstValue + 21; // "" - uint[N]
         static const util::uint64 uint16_ = FirstValue + 22; // "" - uint[N]
         static const util::uint64 uint32_ = FirstValue + 23; // "" - uint[N]
         static const util::uint64 uint64_ = FirstValue + 24; // "" - uint[N]
         
         // static const util::uint64  = ; // "" - sint[N]
         
         static const util::uint64 sint8_ = FirstValue + 25; // "" - sint[N]
         static const util::uint64 sint16_ = FirstValue + 26; // "" - sint[N]
         static const util::uint64 sint32_ = FirstValue + 27; // "" - sint[N]
         static const util::uint64 sint64_ = FirstValue + 28; // "" - sint[N]
         
         // static const util::uint64  = ; // "" - uint[N]{p[M]int[Q]}
         static const util::uint64 uint64p8int8_ = FirstValue + 29; // "" - uint[N]{p[M]int[Q]}
         static const util::uint64 uint64p4int16_ = FirstValue + 30; // "" - uint[N]{p[M]int[Q]}
         static const util::uint64 uint64p2int32_ = FirstValue + 31; // "" - uint[N]{p[M]int[Q]}
         
         // static const util::uint64  = ; // "" - sint[N]{p[M]int[Q]}
         static const util::uint64 sint64p8int8_ = FirstValue + 32; // "" - sint[N]{p[M]int[Q]}
         static const util::uint64 sint64p4int16_ = FirstValue + 33; // "" - sint[N]{p[M]int[Q]}
         static const util::uint64 sint64p2int32_ = FirstValue + 34; // "" - sint[N]{p[M]int[Q]}
         
         // static const util::uint64  = ; // "" - float[N]
         // static const util::uint64  = ; // "" - ufloat[N]
         // static const util::uint64  = ; // "" - float[N]{p[M]float[Q]}
         // static const util::uint64  = ; // "" - ufloat[N]{p[M]float[Q]}
         
         // static const util::uint64  = ; // "" - float[N]
         static const util::uint64 float32_ = FirstValue + 35; // "" - float[N]
         static const util::uint64 float64_ = FirstValue + 36; // "" - float[N]
         
         // static const util::uint64  = ; // "" - ufloat[N]
         static const util::uint64 ufloat32_ = FirstValue + 37; // "" - ufloat[N]
         static const util::uint64 ufloat64_ = FirstValue + 38; // "" - ufloat[N]
         
         // static const util::uint64  = ; // "" - float[N]{p[M]float[Q]}
         static const util::uint64 float64p2float32_ = FirstValue + 39; // "" - float[N]{p[M]float[Q]}
         
         // static const util::uint64  = ; // "" - ufloat[N]{p[M]float[Q]}
         static const util::uint64 ufloat64p2float32_ = FirstValue + 40; // "" - ufloat[N]{p[M]float[Q]}
         
         // static const util::uint64  = ; // "" - 
         // static const util::uint64  = ; // "" - 
          static const util::uint64 LastValue = ufloat64p2float32_;
          
       /* 
         static const util::uint64  = ; // "" -
       */
       
      }//end of namespace builtin_types;
      
      /*
      namespace _
        {
          namespace util = SK::utility::types;
          namespace id_i = SK::compilers::gsvm::consts::IDs;
          //namespace prev_id_i = id_i::_;
                  
          static const util::uint64 FirstValue = prev_id_i::LastValue + 1;
          static const util::uint64  = ; // "" -
          static const util::uint64 LastValue = ;
          // static const util::uint64  = ; // "" -
        }// end of namespace _;
      */
      
      
      namespace identifiers // the IDs of identifiers:
        {
         namespace util = SK::utility::types;
         namespace id_i = SK::compilers::gsvm::consts::IDs;
         namespace prev_id_i = id_i::builtin_types;
         
         static const util::uint64 FirstValue = prev_id_i::LastValue + 1;
         static const util::uint64 basic_ = FirstValue + 1; // "" - BASIC_IDENT
         static const util::uint64 var_ = FirstValue + 2; // "" - VAR_IDENT
         static const util::uint64 const_ = FirstValue + 3; // "" - CONST_IDENT
         static const util::uint64 typename_ = FirstValue + 4; // "" - TYPENAME_IDENT
         static const util::uint64 bitfield_ = FirstValue + 5; // "" - BITFIELD_IDENT
         static const util::uint64 enum_ = FirstValue + 6; // "" - ENUM_IDENT
         static const util::uint64 object_ = FirstValue + 7; // "" - OBJECT_IDENT
         static const util::uint64 union_ = FirstValue + 8; // "" - UNION_IDENT
         static const util::uint64 function_type_ = FirstValue + 9; // "" - FUNCTION_TYPE_IDENT
         static const util::uint64 function_type_args_ = FirstValue + 10; // "" - FUNCTION_IDENT_ARGS
         static const util::uint64 function_type_arg_ = FirstValue + 11; // "" - FUNCTION_IDENT_ARG
         static const util::uint64 qual_typeof_ = FirstValue + 12; // "" - QUALIFIED_TYPEOF_IDENT
         static const util::uint64 qual_typeid_ = FirstValue + 13; // "" - QUALIFIED_TYPEID
         static const util::uint64 unqual_typeid_ = FirstValue + 14; // "" - UNQUALIFIED_TYPEID
         static const util::uint64 label_ = FirstValue + 15; // "" - LABEL_IDENT
         static const util::uint64 function_ = FirstValue + 16; // "" - FUNCTION_IDENT
         static const util::uint64 operator_ = FirstValue + 17; // "" - OPERATOR_IDENT
         static const util::uint64 namespace_ = FirstValue + 18; // "" - NAMESPACE_IDENT
         static const util::uint64 import_ = FirstValue + 19; // "" - IMPORT_IDENT
         static const util::uint64 builtin_types_ = FirstValue + 20; // "" - BUILTIN_TYPES_IDENT
         static const util::uint64 builtin_funcs_ = FirstValue + 21; // "" - BUILTIN_FUNC_IDENT
         static const util::uint64 operator_helper_ = FirstValue + 22; // "" - OPERATOR_HELPER
         static const util::uint64 operator_type_ = FirstValue + 23; // "" - OPERATOR_TYPE_IDENT
         static const util::uint64 nested_name_ = FirstValue + 24; // "" - NESTED_NAME_IDENT
         static const util::uint64 using_ = FirstValue + 25; // "" - USING_IDENT
         static const util::uint64 LastValue = using_;
          // static const util::uint64  = ; // "" -
        }// end of namespace identifiers;   
      
      namespace modifiers
        {
          namespace util = SK::utility::types;
          namespace id_i = SK::compilers::gsvm::consts::IDs;
          //namespace prev_id_i = id_i::identifiers;
                  
          static const util::uint64 FirstValue = prev_id_i::LastValue + 1;
          static const util::uint64 temp_id = FirstValue; // "" -TEMP_ID_MOD
          static const util::uint64 LastValue = ;
          // static const util::uint64  = ; // "" -
        }// end of namespace modifiers;
          
      /*
      // the IDs of modifiers:
      , TEMP_OPERID_MOD,  TEMP_MOD_ARGS,
      TEMP_MOD_ARG, VAR_PTR_MOD, VAR_REF_MOD, CONST_PTR_MOD,
      CONST_REF_MOD, TYPE_POSTFIX_MOD, CALLCONV_MOD, LANG_MOD, 
      THROW_MOD, THROW_ARGS_MOD, THROW_ARG_MOD, STATIC_MOD,
      CONST_MOD, GENDEC_MOD, TEMP_DECL_MOD, TEMP_OPERDECL_MOD,
      TEMP_DECL_ARGS, TEMP_DECL_ARG, VOLATILE_MOD, 
      INITIALIZER_MOD, MUTABLE_MOD, ABSTRACT_MOD, VIRTUAL_MOD,
      FUNC_DECL_ARGS, FUNC_DECL_ARG, EXPLICIT_MOD, 
      CTOR_INIT_LIST, CTOR_INIT_MEM, INHERIT_LIST, INHERIT_MEM,
      TYPEDECL_INFIX_MOD, ENUMERATOR_MOD, ACCESS_MOD,
                      
      */
      
      
    /*
      namespace _
        {
          namespace util = SK::utility::types;
          namespace id_i = SK::compilers::gsvm::consts::IDs;
          //namespace prev_id_i = id_i::_;
                  
          static const util::uint64 FirstValue = prev_id_i::LastValue + 1;
          static const util::uint64  = ; // "" -
          static const util::uint64 LastValue = ;
          // static const util::uint64  = ; // "" -
        }// end of namespace _;
      */  
      
    } //end of namespace IDs. 
   } //end of namespace consts.
  } //end of namespace gsvm.
 } //end of namespace compilers.
} //end of namespace SK.

#endif
