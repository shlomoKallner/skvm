#ifndef SK_SGVM_EVAL_DECLARATION_EVALUATOR_H
#define SK_SGVM_EVAL_DECLARATION_EVALUATOR_H

#include "rule_eval.h"
#include "gsvm_ddl_const.h"
#include "macro_declaration_evaluator.h"
#include "macro_statement_evaluator.h"
#include "object_code_fwd.h" // replace with an include of each used 
         // object_code type`s file.

namespace SK
{
 namespace gsvm
 {
  namespace eval
  { 
   
   struct decl_eval : public rule_eval<decl_eval<IteratorT,
          FactoryT>, IteratorT,FactoryT>
   {
    using namespace SK::gsvm::object_code;
    typedef typename decl_eval                               self_t;
    typedef typename boost::shared_ptr< self_t >             self_ptr;
    typedef typename rule_eval<decl_eval>                    base_t;
                     
    typedef typename macro_state_eval::self_t 
                                                             macro_state_eval_t;
    typedef typename macro_decl_eval::self_t
                                                             macro_decl_eval_t;
    typedef typename text_checker::self_t
                                                             text_check_t;
    typedef typename typedef_object::self_t                  typedef_obj_t;
                     
    template< typename T >
    struct result
    { 
     typedef evaluator_result< nil_t > type;
     typedef std::list< type > container_t; 
    };
    
    
    template< typename T >
    inline typename eval_result<self_t,T>::type
    operator()(tree_iter const& node_ )const
    {
     return this->evaluate(node_);
    }//end of ...
    
    template< typename T >
    typename eval_result<self_t,T>::type
    evaluate( tree_iter const& node_ )const
    {
     typedef typename eval_result<self_t,T>::type return_type;
     return_type ret();
     macro_state_eval_t ms_eval();
     macro_decl_eval_t  md_eval();
     /* - the possible declarations to be found in declaration_groop_r :
         declaration_state_r , using_state_r , import_decl_r , 
         namespace_decl_r , func_decl_r , oper_decl_r , ctor_decl_r ,
         dtor_decl_r , bitfield_decl_r , enum_decl_r , object_decl_r  ,
         union_decl_r , forward_decl_r , macro_statement_groop_r ,
         macro_declaration_groop_r 
     */ 
     if( ms_eval.is_macro_state(node_) )
     { ret.add( ms_eval(node_) ); }
      else if( md_eval.is_macro_decl(node_) )
     { ret.add( md_eval(node_) ); }      
      else switch( node_->value.id() )
     {
      using namespace SK::gsvm::ddl::consts;
      case DECL_STATE: // declaration_state_r
       if( text_check_t("typedef")(node_) )
       {
        typedef_obj_t tpdf_obj_m();
        ret.concat(tpdf_obj_m); // may not actually be concat`ed!!
       }
       else if()
       {}
       else
       {}
       break;
      case USING_STATE: // using_state_r
       break;
      case IMPORT_DECL: // import_decl_r
       break;
      case NMSPC_DECL: // namespace_decl_r
       break;
      case FUNC_DECL: // func_decl_r
       break;
      case OPER_DECL: // oper_decl_r
       break;
      case CTOR_DECL: // ctor_decl_r
       break;
      case DTOR_DECL: // dtor_decl_r
       break;
      case BITFIELD_DECL: // bitfield_decl_r
       break;
      case ENUM_DECL: // enum_decl_r
       break;
      case OBJECT_DECL: // object_decl_r
       break;
      case UNION_DECL: // union_decl_r
       break;
      case FORWARD_DECL: // forward_decl_r
       break;
      
      default: // if no matching rule ID we fail.
      { // maybe this should be a throw of some error type instead of assert()?
       assert(0);
      }
         
     }
     return ret;  
    }//end of 
    
    template< typename T >
    inline typename eval_result<self_t,T>::container_t
    operator()( tree_iter const& first_, tree_iter const& last_ )const
    {
     return this->evaluate(first_,last_);
    }//end of ...
     
    template< typename T >
    typename eval_result<self_t,T>::container_t
    evaluate( tree_iter const& first_, tree_iter const& last_ )const
    {
     typedef typename eval_result<self_t,T>::container_t return_type;
     return_type ret();
     for(tree_iter i = first_ ; i != last_; i++)
     {
      ret.push_back( this->evaluate(i) );
     }
     return ret;
    }//end of ...
     
    static boolean is_eval_able( tree_iter const& node_ )const
    {
     if( macro_state_eval_t::is_macro_state(node_) )
     { return true; }
     else if( macro_decl_eval_t::is_macro_decl(node_) )
     { return true; }      
     else switch( node_->value.id() )
     {
      using namespace SK::gsvm::ddl::consts;
      case DECL_STATE: // declaration_state_r
      case USING_STATE: // using_state_r
      case IMPORT_DECL: // import_decl_r
      case NMSPC_DECL: // namespace_decl_r
      case FUNC_DECL: // func_decl_r
      case OPER_DECL: // oper_decl_r
      case CTOR_DECL: // ctor_decl_r
      case DTOR_DECL: // dtor_decl_r
      case BITFIELD_DECL: // bitfield_decl_r
      case ENUM_DECL: // enum_decl_r
      case OBJECT_DECL: // object_decl_r
      case UNION_DECL: // union_decl_r
      case FORWARD_DECL: // forward_decl_r
                        return true;
      default:
              return false;
     }
    }// end of ...
    
    
   };
   
  } 
 } 
}


#endif
