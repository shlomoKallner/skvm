#ifndef SK_SGVM_EVAL_EXPRESSION_EVALUATOR_H
#define SK_SGVM_EVAL_EXPRESSION_EVALUATOR_H

// you include the file of the base type here:
#include "rule_eval.h"
#include "gsvm_ddl_const.h"
// your defines go here:

namespace SK
{
 namespace gsvm
 {
  namespace eval
  { 
   
   struct expr_eval : public rule_eval< expr_eval >
   {
    typedef typename expr_eval                            self_t;
    typedef typename boost::shared_ptr< self_t >          self_ptr;
    typedef typename rule_eval< expr_eval >               base_t;
    
    // end of required typedefs
    
    // end of typedefs
    
    template< typename InputT >
    struct result
    { 
     typedef evaluator_result< nil_t > type;
     typedef std::list< type > container_t; 
    };
    
    template< typename InputT >
    inline typename eval_result<self_t,InputT>::type
    operator()( InputT::tree_iter const& node_ )const
    {
     return this->evaluate(node_);
    }//end of ...
    
    template< typename InputT >
    typename eval_result<self_t,InputT>::type
    evaluate( InputT::tree_iter const& node_ )const
    {
     typedef typename eval_result<self_t,InputT>::type return_type;
     return_type ret();
     return ret;  
    }//end of ...
    
    template< typename InputT >
    inline typename eval_result<self_t,InputT>::container_t
    operator()( InputT::tree_iter const& first_, 
                InputT::tree_iter const& last_ )const
    {
     return this->evaluate(first_,last_);
    }//end of ...
    
    template< typename InputT >
    typename eval_result<self_t,InputT>::container_t
    evaluate( InputT::tree_iter const& first_, 
              InputT::tree_iter const& last_ )const
    {
     typedef typename eval_result<self_t,InputT>::container_t return_type;
     return_type ret();
     for( InputT::tree_iter i = first_ ; i != last_; i++)
     {
      ret.push_back( this->evaluate(i) );
     }
     return ret;
    }//end of ...
    
    static boolean is_eval_able( InputT::tree_iter const& node_ )const
    {
     switch( node_->value.id() )
     {
      case : 
            return true;
      default:
       return false;
     }
    }// end of ...
   
   
   };
   
  } 
 } 
}



#endif
