#ifndef SK_SGVM_EVAL_XXX_EVALUATOR_H
#define SK_SGVM_EVAL_XXX_EVALUATOR_H

// you include the file of the base type here:
#include <cstdlib>
#include <vector>
#include<boost/shared_ptr.hpp>

// your defines go here:
#include "gsvm/parser_def/gsvm_ddl_types.h"

namespace SK
{
 namespace gsvm
 {
  namespace eval
  { 
   using namespace SK::types;
   
   struct litteral_evaluator : public rule_eval< litteral_evaluator >
   {
    typedef typename litteral_evaluator               self_t;
    typedef typename boost::shared_ptr< self_t >      self_ptr;
    typedef typename rule_eval< self_t >              base_t;
    typedef typename self_ptr                         embed_t;
    
    // end of required typedefs
    
    // end of typedefs
    
    template< typename InputT >
    struct result 
    {
     typedef value_object<InputT>           val_type; 
     typedef val_type::self_ptr             value_ptr;
     typedef evaluator_result< value_ptr >  type; 
     typedef std::list< type >              container_t;
     // redefine type by giving evaluator_result a different template parameter 
     // possibly based on InputT in order that 
     // self_t::template result<InputT>::type would be your result type.
    };
    
    litteral_evaluator(){}// end of ...
    litteral_evaluator( self_t const& ){}// end of ...
    virtual ~litteral_evaluator(){}// end of ...
    
    template< typename InputT >
    inline typename eval_result<self_t,InputT>::type
    operator()( InputT::tree_iter const& node_ )const
    {
     return this->evaluate(node_);
    }//end of ...
    
    template< typename InputT >
    typename eval_result<self_t,InputT>::type
    evaluate( InputT::tree_iter const& node_ )const
    {
     using namespace SK::gsvm::ddl;
     boolean s = is_eval_able( node_ );
     typedef typename eval_result<self_t,InputT>::type return_type;
     typedef result<InputT>::value_ptr ret_ptr;
     typedef result<InputT>::val_type  val_type;
     return_type ret(s);
     if( s )
     { 
      InputT::string_t str= "";
      std::vector< InputT::char_t > v;
      std::size_t node_id = node_->value.id().to_long();
      uint64 u;
      sint64 i;
      
      typedef typename InputT::tree_iter::container_t::iterator iter_t;
      parse_info<iter_t> hit;
      switch( node_id )
      {
       case CHAR_LIT : 
       case WCHAR_LIT: 
       case MBCHAR_LIT: 
       case VLCHAR_LIT:
            hit = parse( node_->value.begin(), node_->value.end(), 
            (+( c_escape_ch_p[push_back_a(v)] )) );
            if( hit )
            { 
             str.assign(v.begin(),v.end());
             ret.value( ret_ptr(new val_type( str, node_id ) ) );
            }
            break;
       case STR_LIT: 
       case WSTR_LIT: 
       case MBSTR_LIT: 
       case VLSTR_LIT: 
            hit = parse( node_->value.begin(), node_->value.end(), 
            (*( c_escape_ch_p[push_back_a(v)] )) ); 
            if( hit )
            {
             str.assign(v.begin(),v.end());
             ret.value( ret_ptr(new val_type( str, node_id ) ) );
            }
            break;
       case SINT8_LIT:
            hit = parse( node_->value.begin(), node_->value.end(), 
                         sint8_p()[assign_a(i)] ); 
            goto sint_label; 
       case SINT10_LIT:
            hit = parse( node_->value.begin(), node_->value.end(), 
                         sint10_p()[assign_a(i)] ); 
            goto sint_label; 
       case SINT16_LIT:
            hit = parse( node_->value.begin(), node_->value.end(), 
                         sint16_p()[assign_a(i)] ); 
            
           sint_label:
            if( hit )
            {  
             ret.value( ret_ptr(new val_type( i, node_id ) ) );
            } 
           break;
       case UINT8_LIT:
             hit = parse( node_->value.begin(), node_->value.end(), 
                          uint8_p()[assign_a(u)] ); 
             goto uint_label;
       case UINT10_LIT:
             hit = parse( node_->value.begin(), node_->value.end(), 
                          uint10_p()[assign_a(u)] ); 
             goto uint_label;
       case UINT16_LIT:
             hit = parse( node_->value.begin(), node_->value.end(), 
                          uint16_p()[assign_a(u)] ); 
             
            uint_label:
             if( hit )
             {
              ret.value( ret_ptr(new val_type( u, node_id ) ) );
             } 
             break;
       case FLOAT8_LIT: 
            hit = parse( node_->value.begin(), node_->value.end(), 
                         float8_p()[assign_a(f)] ); 
            goto float_label;
       case FLOAT10_LIT: 
            hit = parse( node_->value.begin(), node_->value.end(), 
                         float10_p()[assign_a(f)] ); 
            goto float_label;
       case FLOAT16_LIT:
            hit = parse( node_->value.begin(), node_->value.end(), 
                         float16_p()[assign_a(f)] ); 
            
           float_label: 
            if( hit )
            {
             ret.value( ret_ptr(new val_type( f, node_id ) ) );
            }
           break;
       case BOOL_LIT:
        boolean bol;
        str.assign( node_->value.begin(), node_->value.end() );
        if( str == "true" ) { bol = true; }
        else if( str == "false" ) { bol = false; }
        else { assert(0); }
        ret.value( ret_ptr(new val_type( bol, node_id ) ) );
           
       default:
        assert(0);
      }
     }
     return ret;  
    }//end of ...
    
    template< typename InputT >
    inline typename eval_result<self_t,InputT>::container_t
    operator()( InputT::tree_iter const& first_, 
                InputT::tree_iter const& last_ )const
    {
     return this->evaluate(first_,last_);
    }//end of ...
    
    template< typename InputT >
    typename eval_result<self_t,InputT>::container_t
    evaluate( InputT::tree_iter const& first_, 
              InputT::tree_iter const& last_ )const
    {
     typedef typename eval_result<self_t,InputT>::container_t return_type;
     return_type ret();
     for( InputT::tree_iter i = first_ ; i != last_; i++)
     {
      ret.push_back( this->evaluate(i) );
     }
     return ret;
    }//end of ...
    
    template< typename InputT > static boolean 
    is_eval_able( InputT::tree_iter const& node_ )const
    {
     switch( node_->value.id().to_long() )
     {
      case CHAR_LIT : 
      case WCHAR_LIT: 
      case MBCHAR_LIT: 
      case VLCHAR_LIT:
      case STR_LIT: 
      case WSTR_LIT: 
      case MBSTR_LIT: 
      case VLSTR_LIT: 
      case SINT8_LIT: 
      case SINT10_LIT: 
      case SINT16_LIT: 
      case UINT8_LIT: 
      case UINT10_LIT: 
      case UINT16_LIT:
      case FLOAT8_LIT: 
      case FLOAT10_LIT: 
      case FLOAT16_LIT:
      case BOOL_LIT:
          return true;
      default:
       return false;
     }
    }// end of ...
    
    static void deleter( self_t* )
    {}//end of ...
    
    embed_t to_embed( void )
    {
     return embed_t( this,deleter );
    }//end of ...
    
    static embed_t make_embed( self_t& s )
    {
     return s.to_embed();
    }//end of ...
   
   
   };
   
   litteral_evaluator const litteral_e();
   
   
  } 
 } 
}



#endif
