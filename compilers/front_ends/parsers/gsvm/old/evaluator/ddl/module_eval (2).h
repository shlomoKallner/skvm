#ifndef SK_SGVM_EVAL_PARSED_MODULE_H
#define SK_SGVM_EVAL_PARSED_MODULE_H

#include "rule_eval.h"
#include "gsvm_ddl_const.h"
#include "declaration_evaluator.h"
#include "macro_declaration_evaluator.h"
#include "macro_statement_evaluator.h"

namespace SK
{
 namespace gsvm
 {
  namespace eval
  { 
   
   struct module_eval : public rule_eval<module_eval>
   {
    typedef typename module_eval                        self_t;
    typedef typename boost::shared_ptr< self_t >        self_ptr;
    typedef typename rule_eval<module_eval>             base_t;
    typedef typename decl_eval::self_t                  decl_eval_t;
    
    template< typename T >
    struct result
    { 
     typedef evaluator_result< nil_t > type;
     typedef std::list< type > container_t; 
    }; // rewrite with real types!!!!
    
    
    template< typename T >
    inline typename eval_result<self_t,T>::type
    operator()(tree_iter const& node_ )const
    {
     return this->evaluate(node_);
    }//end of ...
    
    template< typename T >
    typename eval_result<self_t,T>::type
    evaluate( tree_iter const& node_ )const
    {
     /* - the possible declarations to be found in a module:
         declaration_state_r 
     */
     typedef typename eval_result<self_t,T>::type return_type;
     return_type ret(); // rewrite with real ctor!!!!
     decl_eval_t d_eval();
     if( d_eval.is_eval_able(node_) )
     {
      ret.add( d_eval(node_) );
     }
     return ret;
    }//end of 
    
    template< typename T >
     inline typename eval_result<self_t,T>::container_t
     operator()( tree_iter const& first_, tree_iter const& last_ )const
     {
      return this->evaluate(first_,last_);
     }//end of ...
     
     template< typename T >
     typename eval_result<self_t,T>::container_t
     evaluate( tree_iter const& first_, tree_iter const& last_ )const
     {
      typedef typename eval_result<self_t,T>::container_t return_type;
      return_type ret();// rewrite with real ctor!!!!
      for(tree_iter i = first_ ; i != last_; i++)
      {
       ret.push_back( this->evaluate(i) );
      }
      return ret;
     }//end of ...
     
     static boolean is_eval_able( tree_iter const& node_ )const
     {
      return decl_eval_t::is_eval_able( node_ );
     }//end of ...
    
    
   };
   
  } 
 } 
}

#endif
