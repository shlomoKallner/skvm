#ifndef SK_SGVM_EVAL_DECLARATION_EVALUATOR_H
#define SK_SGVM_EVAL_DECLARATION_EVALUATOR_H

#include "rule_eval.h"
#include "gsvm_ddl_const.h"
#include "macro_statement_evaluator.h"
#include "object_code_fwd.h" // replace with an include of each used 
         // object_code type`s file.

namespace SK
{
 namespace gsvm
 {
  namespace eval
  { 
   
   struct state_eval : public rule_eval<state_eval>
   {
    using namespace SK::gsvm::object_code;
    typedef typename state_eval                           self_t;
    typedef typename boost::shared_ptr< self_t >          self_ptr;
    typedef typename rule_eval<state_eval>                base_t;
                     
    typedef typename macro_state_eval::self_t             macro_state_eval_t;
    
    template< typename T >
    struct result
    { 
     typedef evaluator_result< nil_t > type;
     typedef std::list< type > container_t; 
    };
    
    template< typename T >
    inline typename eval_result<self_t,T>::type
    operator()(tree_iter const& node_ )const
    {
     return this->evaluate(node_);
    }//end of ...
    
    template< typename T >
    typename eval_result<self_t,T>::type
    evaluate( tree_iter const& node_ )const
    {
     typedef typename typename eval_result<self_t,T>::type return_type;
     return_type ret();
     return ret;  
    }//end of ...
    
    template< typename T >
    inline typename eval_result<self_t,T>::container_t
    operator()( tree_iter const& first_, tree_iter const& last_ )const
    {
     return this->evaluate(first_,last_);
    }//end of ...
    
    template< typename T >
    typename eval_result<self_t,T>::container_t
    evaluate( tree_iter const& first_, tree_iter const& last_ )const
    {
     typedef typename eval_result<self_t,T>::container_t return_type;
     return_type ret();
     for(tree_iter i = first_ ; i != last_; i++)
     {
      ret.push_back( this->evaluate(i) );
     }
     return ret;
    }//end of ...
    
    static boolean is_eval_able( tree_iter const& node_ )const
    {
     switch( node_->value.id().to_long() )
     {
      default:
       return false;
     }
    }// end of ...
   
   
   };
   
  } 
 } 
}



#endif

