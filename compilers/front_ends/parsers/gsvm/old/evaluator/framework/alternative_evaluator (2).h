#ifndef SK_SGVM_EVAL_XXX_EVALUATOR_H
#define SK_SGVM_EVAL_XXX_EVALUATOR_H

// you include the file of the base type here:

// your defines go here:

namespace SK
{
 namespace gsvm
 {
  namespace eval
  { 
   template < typename EvalT1, typename EvalT2 >
   struct alternative_evaluator 
   : public evaluator< alternative_evaluator< EvalT1,EvalT2 > >
   {
    typedef typename alternative_evaluator< EvalT1,EvalT2 >     
                                                      self_t;
    typedef typename value_ptr< self_t >              self_ptr;
    typedef self_ptr                                  embed_t;
    typedef typename evaluator< self_t >              base_t;
    typedef typename EvalT1::embed_t                  left_type;
    typedef typename EvalT2::embed_t                  right_type;
    
    // end of required typedefs
    
    // end of typedefs
    
    template< typename T >
    struct result
    { 
     typedef evaluator_result< nil_t > type;
     typedef std::list< type > container_t
    };
    
    alternative_evaluator( EvalT1& e1, EvalT2& e2 )
    : left_eval_m( make_embed(e1) ), right_eval_m( make_embed(e2) )
    {}//end of ...
    
    alternative_evaluator( self_t const& s )
    : left_eval_m( s.left_eval_m ), right_eval_m( s.right_eval_m ) 
    {}//end of ...
    
    virtual ~alternative_evaluator(){}// end of ...
    
    template< typename T >
    inline typename eval_result<self_t,T>::type
    operator()(tree_iter const& node_ )const
    {
     return this->evaluate(node_);
    }//end of ...
    
    template< typename T >
    typename eval_result<self_t,T>::type
    evaluate( tree_iter const& node_ )const
    {
     typedef typename typename eval_result<self_t,T>::type return_type;
     return_type ret();
     return ret;  
    }//end of ...
    
    template< typename T >
    inline typename eval_result<self_t,T>::container_t
    operator()( tree_iter const& first_, tree_iter const& last_ )const
    {
     return this->evaluate(first_,last_);
    }//end of ...
    
    template< typename T >
    typename eval_result<self_t,T>::container_t
    evaluate( tree_iter const& first_, tree_iter const& last_ )const
    {
     typedef typename eval_result<self_t,T>::container_t return_type;
     return_type ret();
     for(tree_iter i = first_ ; i != last_; i++)
     {
      ret.push_back( this->evaluate(i) );
     }
     return ret;
    }//end of ...
    
    static boolean is_eval_able( tree_iter const& node_ )const
    {
     switch( node_->value.id() )
     {
      default:
       return false;
     }
    }// end of ...
    
    static embed_t make_embed( self_t& s)
    {
     return embed_t( s );
    }//end of ...
    
    protected:
              left_type  left_eval_m;
              right_type right_eval_m;
              
   };
   
   template < typename EvalT1, typename EvalT2 >
   inline
   alternative_evaluator< EvalT1,EvalT2 >
   operator|( evaluator<EvalT1>& a, evaluator<EvalT2>& b)
   {
    typedef alternative_evaluator< EvalT1,EvalT2 >::self_t
            self_type;
    return self_type( a.derived(), b.derived() );
   }//end of ...
   
   
  } 
 } 
}



#endif
