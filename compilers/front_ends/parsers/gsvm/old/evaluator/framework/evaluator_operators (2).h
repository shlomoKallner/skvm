#ifndef SK_SGVM_EVAL_EVALUATOR_OPERATORS_H
#define SK_SGVM_EVAL_EVALUATOR_OPERATORS_H

// you include the file of the base type here:

// your defines go here:

namespace SK
{
 namespace gsvm
 {
  namespace eval
  { 
   /*
   // a unary operator
   
   template< typename EvalT >
   inline _evaluator<EvalT>
   operator ( evaluator<EvalT>& e )
   {
    typedef typename _evaluator<EvalT> self_type;
    return self_type( e.derived() );
   }//end of ...
   
   */
   
   template< typename EvalT >
   inline at_least_one_evaluator<EvalT>
   operator+( evaluator<EvalT>& e )
   {
    typedef typename at_least_one_evaluator<EvalT> self_type;
    return self_type( e.derived() );
   }//end of ...
   
   template< typename EvalT >
   inline zero_or_more_evaluator<EvalT>
   operator*( evaluator<EvalT>& e )
   {
    typedef typename zero_or_more_evaluator<EvalT> self_type;
    return self_type( e.derived() );
   }//end of ...
   
   template< typename EvalT >
   inline zero_or_one_evaluator<EvalT>
   operator!( evaluator<EvalT>& e )
   {
    typedef typename zero_or_one_evaluator<EvalT> self_type;
    return self_type( e.derived() );
   }//end of ...
   
   
   /*
   // a binary operator
   
   template< typename EvalT1, typename EvalT2 >
   inline _evaluator<ParentEvalT,ChildEvalT>
   operator ( evaluator<EvalT1>& e1, evaluator<EvalT2>& e2 )
   {
    typedef typename _evaluator<ParentEvalT,ChildEvalT>
            self_type;
    return self_type( e1.derived(), e2.derived() );
   }//end of ...
   
   */
   
   
   template< typename ParentEvalT, typename ChildEvalT >
   inline child_retrieve_evaluator<ParentEvalT,ChildEvalT>
   operator+( evaluator<ParentEvalT>& p, evaluator<ChildEvalT>& c )
   {
    typedef typename child_retrieve_evaluator<ParentEvalT,ChildEvalT>
            self_type;
    return self_type( p.derived(),c.derived() );
   }//end of ...
   
   template< typename EvalT1, typename EvalT2 >
   inline 
   operator+( evaluator<EvalT1>& p, evaluator<EvalT2>& c )
   {
    typedef typename child_retrieve_evaluator<ParentEvalT,ChildEvalT>
            self_type;
    return self_type( p.derived(),c.derived() );
   }//end of ...
   
   
   
  } 
 } 
}



#endif
