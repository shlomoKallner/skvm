#ifndef SK_GSVM_EVAL_PARSER_EVALUATOR_BASE_H
#define SK_GSVM_EVAL_PARSER_EVALUATOR_BASE_H

#include <string>
#include <map>
#include <list>
#include <set>
#include <boost/spirit/include/classic_spirit.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/detail/iterator.hpp>
#include "SK_types.h"
#include "evaluator/framework/evaluator_base.h"
namespace SK
{
 namespace gsvm
 {
  namespace eval
  {
   using namespace boost::spirit::classic;
   using namespace SK::types;
   
   template< typename EvalT >
   struct rule_eval : public evaluator< rule_eval< EvalT > >
   {
    typedef typename rule_eval< EvalT >                  self_t;
    typedef typename boost::shared_ptr< self_t >         self_ptr;
    typedef typename evaluator< self_t >                 base_t;
    typedef typename EvalT::embed_t                      embed_t;
    
    
    template< typename InputT >
    struct result
    {
     typedef typename EvalT::template result<InputT>::type type;
     typedef typename EvalT::template result<InputT>::container_t container_t;
    };
    
    rule_eval() : base_t(){}//end of ...
    rule_eval( self_t const& ): base_t(){}// end of ...
    virtual ~rule_eval(){}//end of ...
    
    EvalT& derived(void)
    {
      return *static_cast<EvalT*>(this);
    }//end of ...
    EvalT const& derived(void)
    {
      return *static_cast<EvalT const*>(this);
    }//end of ...
    
    template< typename InputT >
    inline typename eval_result<self_t,InputT>::type
    operator()( InputT::tree_iter const& node_ )const
    {
     return this->evaluate(node_);
    }//end of ...
    
    template< typename InputT >
    typename eval_result<self_t,InputT>::type
    evaluate( InputT::tree_iter const& node_ )
    {
     return this->derived().evaluate(node_);
    }//end of 
    
    template< typename InputT >
     inline typename eval_result<self_t,InputT>::container_t
     operator()( InputT::tree_iter const& first_, 
                 InputT::tree_iter const& last_ )const
     {
      return this->evaluate(first_,last_);
     }//end of ...
     
     template< typename InputT >
     typename eval_result<self_t,InputT>::container_t
     evaluate( InputT::tree_iter const& first_, 
               InputT::tree_iter const& last_ )const
     {
      return this->derived().evaluate(first_,last_);
     }//end of ...
     
     template< typename InputT > static boolean 
     is_eval_able( InputT::tree_iter const& node_ )const
     {
      return EvalT::is_eval_able( node_ );
     }//end of ...
     
     static embed_t make_embed( self_t& s )
     {
      return EvalT::make_embed( s );
     }//end of ...
    
   };
   
   
  }
 }
}

#endif

#if 0

/*
    template< typename T >      
     struct result
     {
      typedef typename EvalT::definition::template result<T>::type type;
     }:
    
    struct definition
    {
     template< typename T >      
     struct result
     {
      typedef nil_t type;
     };
     
     definition( self_t const& self )
     {}//end of ...
     
     virtual ~ definition(){}//end of ...
     
     template< typename T >
     typename result<T>::type
     start( tree_iter  node_  )
     {
      typedef typename result<T>::type return_type;
      return return_type();
     }
    };
    
    typedef typename EvalT::definition definition_t;
     
    template< typename T >
    typename eval_result<self_t,T>::type
    evaluate( tree_iter node_ )
    {
     return this->evaluator().def_eval_.start(node_);
    }//end of 
     
    protected:
              definition_t def_eval_;
    public:
    */

#endif 
