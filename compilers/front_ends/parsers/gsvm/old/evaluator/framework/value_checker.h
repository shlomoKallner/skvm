#ifndef SK_GSVM_EVAL_VALUE_CHECKER_H
#define SK_GSVM_EVAL_VALUE_CHECKER_H

#include "SK_types.h"
#include <boost/spirit/include/classic_spirit.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/detail/iterator.hpp>
#include <string>
#include <map>
#include <list>
#include <set>
#include "gsvm_ddl_types.h"
#include "value_eval.h"

namespace SK
{
 namespace gsvm
 {
  namespace eval
  {
   using namespace boost::spirit::classic;
   using namespace SK::types;
   
   
   struct id_checker : public value_eval< id_checker >
   {
    typedef typename id_checker                     self_t;
    typedef typename boost::shared_ptr< self_t >    self_ptr;
    typedef typename value_eval< self_t >           base_t;
    
    template< typename InputT >
    struct result
    {
     typedef boolean type;
     typedef std::vector< boolean > container_t;
    };
    
    id_checker( std::size_t i ):base_t(),id_(i) {}//end of ...
    id_checker( id_checker const& i ):base_t(),id_(i.id_){}//end of ...
    virtual ~id_checker(){}//end of ...
    
    template< typename InputT > inline boolean
    operator()( InputT::tree_iter const& node_ )const
    {
     return this->evaluate(node_);
    }//end of ...
    
    template< typename InputT > boolean
    evaluate( InputT::tree_iter const& node_ )const
    {
     //
     return parser_id(id_) == node_->value.id();
    }//end of 
    
    
    template< typename InputT > inline std::vector< boolean >    
    operator()( InputT::tree_iter const& first_, 
                InputT::tree_iter const& last_ )const
    {
     return this->evaluate(first_,last_);
    }//end of ...
     
    template< typename InputT > std::vector< boolean >
    evaluate( InputT::tree_iter const& first_, 
              InputT::tree_iter const& last_ )const
    {
     std::vector< boolean > vec();
     for( InputT::tree_iter i = first_ ; i != last_; i++)
     {
      vec.push_back( this->evaluate(i) );
     }
     return vec;
    }//end of ...
    
    template< typename InputT >
    static boolean is_eval_able( InputT::tree_iter const& node_ )const
    {
     return true;
    }//end of ...
    
    protected:
             std::size_t id_;  
    
   };
   
   struct root_checker : public value_eval< root_checker >
   {
    typedef typename root_checker                     self_t;
    typedef typename boost::shared_ptr< self_t >      self_ptr;
    typedef typename value_eval< self_t >             base_t;
    
    template< typename InputT >
    struct result
    {
     typedef boolean type;
     typedef std::vector< boolean > container_t;
    };
    
    root_checker():base_t(){}//end of ...
    root_checker( root_checker const&  ):base_t(){}//end of ...
    virtual ~root_checker(){}//end of ...
    
    template< typename InputT > inline boolean
    operator()( InputT::tree_iter const& node_ )const
    {
     return this->evaluate(node_);
    }//end of ...
    
    template< typename InputT > boolean
    evaluate( InputT::tree_iter const& node_ )const
    {
     //
     return node_->value.is_root();
    }//end of ...
    
    template< typename InputT > inline std::vector< boolean >
    operator()( InputT::tree_iter const& first_, 
                InputT::tree_iter const& last_ )const
    {
     return this->evaluate(first_,last_);
    }//end of ...
     
    template< typename InputT > std::vector< boolean >
    evaluate( InputT::tree_iter const& first_, 
              InputT::tree_iter const& last_ )const
    {
     std::vector< boolean > vec();
     for( InputT::tree_iter i = first_ ; i != last_; i++)
     {
      vec.push_back( this->evaluate(i) );
     }
     return vec;
    }//end of ...
    
    template< typename InputT >
    static boolean is_eval_able( InputT::tree_iter const& node_ )const
    {
     return true;
    }//end of ...
    
   };
   
   template< typename InputT >
   struct text_checker : public value_eval< text_checker<InputT> > 
   {
    typedef typename text_checker<InputT>                self_t;
    typedef typename boost::shared_ptr< self_t >         self_ptr;
    typedef typename value_eval< self_t >                base_t;
    typedef typename InputT::tree_iter                   tree_iter;
    typedef typename InputT::string_t                    string_t;
    
    template< typename InputT1 >
    struct result
    {
     typedef boolean type;
     typedef std::vector< boolean > container_t;
    };
    
    text_checker( string_t& s_ ):base_t(),val_(s_){}//end of ...
    text_checker( text_checker const& v_ ):base_t(),val_(v_.val_) {}//end of ...
    virtual ~ text_checker(){}//end of ...
    
    inline boolean
    operator()( tree_iter const& node_ )const
    {
     return this->evaluate(node_);
    }//end of ...
    
    boolean
    evaluate( tree_iter const& node_ )const
    {
     string_t str( node_->value.begin() , node_->value.end() );
     return val_ == str;
    }//end of 
    
    inline std::vector< boolean >
    operator()( tree_iter const& first_, tree_iter const& last_ )const
    {
     return this->evaluate(first_,last_);
    }//end of ...
     
    std::vector< boolean >
    evaluate( tree_iter const& first_, tree_iter const& last_ )const
    {
     std::vector< boolean > vec();
     for( InputT::tree_iter i = first_ ; i != last_; i++)
     {
      vec.push_back( this->evaluate(i) );
     }
     return vec;
    }//end of ...
    
    static boolean 
    is_eval_able( tree_iter const& node_ )const
    {
     return true;
    }//end of ...
    
    protected:
              string_t val_;
    
   };
   
   template< typename InputT >
   struct value_checker : public value_eval< value_checker< InputT > > 
   {
    typedef typename value_checker< T >                      self_t;
    typedef typename boost::shared_ptr< self_t >             self_ptr;
    typedef typename value_eval< self_t >                    base_t;
    typedef typename InputT::value_type                      value_type;
    typedef typename InputT1::tree_iter                      tree_iter;
    
    template< typename InputT1 >
    struct result
    {
     typedef boolean type;
     typedef std::vector< boolean > container_t;
    };
    
    value_checker( value_type& s_ ):base_t(),val_(s_){}//end of ...
    value_checker( value_checker const& v_ ):base_t(),val_(v_.val_) 
    {}//end of ...
    virtual ~ value_checker(){}//end of ...
    
    inline boolean
    operator()( tree_iter const& node_ )const
    {
     return this->evaluate(node_);
    }//end of ...
    
    boolean
    evaluate( tree_iter const& node_ )const
    {
     return val_ == node_->value.value();
    }//end of 
    
    inline std::vector< boolean >
    operator()( tree_iter const& first_, 
                tree_iter const& last_ )const
    {
     return this->evaluate(first_,last_);
    }//end of ...
     
    std::vector< boolean >
    evaluate( tree_iter const& first_, 
              tree_iter const& last_ )const
    {
     std::vector< boolean > vec();
     for( InputT::tree_iter i = first_ ; i != last_; i++)
     {
      vec.push_back( this->evaluate(i) );
     }
     return vec;
    }//end of ...
    
    
    static boolean 
    is_eval_able( tree_iter const& node_ )const
    {
     return true;
    }//end of ...
    
    protected:
              value_type& val_;
    
   };
   
  }
 }
}

#endif
