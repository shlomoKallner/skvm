#ifndef SK_GSVM_EVAL_PARSER_EVALUATOR_BASE_H
#define SK_GSVM_EVAL_PARSER_EVALUATOR_BASE_H

#include "evaluator_base.h"

namespace SK
{
 namespace gsvm
 {
  namespace eval
  {
   
   template< typename EvalT >
   struct value_eval : public evaluator< value_eval< EvalT > >
   {
    typedef typename value_eval< EvalT >                  self_t;
    typedef typename boost::shared_ptr< self_t >          self_ptr;
    typedef typename evaluator< self_t >                  base_t;
    
    template< typename InputT >
    struct result
    {
     typedef typename EvalT::template result<InputT>::type type;
     typedef typename EvalT::template result<InputT>::container_t container_t;
    };
    
    value_eval():base_t() {}//end of ...
    value_eval( value_eval const& ):base_t(){}//end of ...
    virtual ~value_eval(){}//end of ...
    
    EvalT& derived(void)
    {
      return *static_cast<EvalT*>(this);
    }//end of ...
    EvalT const& derived(void)
    {
      return *static_cast<EvalT const*>(this);
    }//end of ...
    
    template< typename InputT >
    inline typename eval_result<self_t,InputT>::type
    operator()( InputT::tree_iter const& node_ )const
    {
     return this->evaluate(node_);
    }//end of ...
    
    template< typename InputT >
    typename eval_result<self_t,InputT>::type
    evaluate( InputT::tree_iter const& node_ )const
    {
     return this->derived().evaluate(node_);
    }//end of 
    
    template< typename InputT >
     inline typename eval_result<self_t,InputT>::container_t
     operator()( InputT::tree_iter const& first_, 
                 InputT::tree_iter const& last_ )const
     {
      return this->evaluate(first_,last_);
     }//end of ...
     
     template< typename InputT >
     typename eval_result<self_t,InputT>::container_t
     evaluate( InputT::tree_iter const& first_, 
               InputT::tree_iter const& last_ )const
     {
      return this->derived().evaluate(first_,last_);
     }//end of ...
     
     template< typename InputT >
     static boolean is_eval_able( InputT::tree_iter const& node_ )const
     {
      return EvalT::is_eval_able( node_ );
     }//end of ...
    
   };
   
   
   
  }
 }
}

#endif
