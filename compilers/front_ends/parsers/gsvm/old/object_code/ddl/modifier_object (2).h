#ifndef SK_GSVM_OBJECT_CODE_MODIFIER_OBJECT_H
#define SK_GSVM_OBJECT_CODE_MODIFIER_OBJECT_H

#include "declaration_object.h"

namespace SK
{
 namespace gsvm
 {
  namespace object_code
  {
   
   enum modifier_ID{ NO_ID = 0, TEMPLATE_DECL_ID, TEMPLATE_IDENT_ID,
                     POINTER_ID, REFERENCE_ID,  };
   
   template< typename InputT, typename T >
   struct modifier_object : public declaration_object<InputT,T>
   {
    typedef declaration_object<InputT,T>     base_t;
    typedef modifier_object<InputT,T>        self_t;
    typedef shared_ptr< self_t >             self_ptr;
    typedef typename InputT::string_t        string_t;
    
    modifier_object(){}//end if ...
    modifier_object( string_t const& s , modifier_ID& m )
    : name_m(s), mid_m(m) {}//end if ...
    modifier_object( self_t const& s )
    : name_m(s.name_m), mid_m(s.mid_m){}//end if ...
    virtual ~modifier_object(){}//end if ...
    
    void id( modifier_ID& m )
    {
     mid_m = m;
    }//end of ...
    
    modifier_ID& id( void )
    {
     return mid_m;
    }//end of ...
    
    void name( string_t& str )
    {
     name_m = str;    
    }// end of ... 
    
    string_t& name( void )
    {
     return name_m;
    }//end of ...
    
    template< typename IteratorT >
    void name( IteratorT first, IteratorT last )
    {
     name_m.assign(first,last);    
    }// end of ... 
    
    const self_t& operator=( const self_t& s )
    {
     this->name_m = s.name_m;
     this->mid_m  = s.mid_m;
     return *this;
    }//end if ...
    
    boolean operator==( const self_t& s )const
    {
     return this->name_m == s.name_m && this->mid_m == s.mid_m;
    }//end if ...
    
    protected:
              string_t name_m;
              modifier_ID mid_m;
              
                       
   };
   
   
   
  }
 }
}


#endif
