#ifndef SK_GSVM_OBJECT_CODE_MODULE_OBJECT_H
#define SK_GSVM_OBJCT_CODE_MODULE_OBJECT_H

#include "declaration_object.h"

namespace SK
{
 namespace gsvm
 {
  namespace object_code
  {
   
   
   template< typename IteratorT, typename T >
   struct module_object : public declaration_object<IteratorT,T>
   {
    typedef declaration_object<IteratorT,T> base_t;
    typedef module_object<IteratorT,T>      self_t;
    typedef shared_ptr< self_t >            self_ptr;
    typedef namespace_object<IteratorT,T>   namespace_t;
    typedef namespace_t::self_ptr           namespace_ptr;
    typedef type_object<IteratorT,T>        type_t;
    typedef type_t::self_ptr                type_ptr;
    
    
    module_object(): base_t(){}//end of ...
    
    
   };
   
   
  }
 }
}

#endif
