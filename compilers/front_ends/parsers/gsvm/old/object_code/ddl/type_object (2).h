#ifndef SK_GSVM_OBJECT_CODE_TYPE_OBJECT_H
#define SK_GSVM_OBJECT_CODE_TYPE_OBJECT_H

#include "declaration_object.h"

namespace SK
{
 namespace gsvm
 {
  namespace object_code
  {
   
   
   template< typename IteratorT, typename T >
   struct type_object : public declaration_object<IteratorT,T>
   {
    typedef declaration_object<IteratorT,T>  base_t;
    typedef type_object<IteratorT,T>         self_t;
    typedef shared_ptr< self_t >             self_ptr;
    
    
   };
   
   
  }
 }
}


#endif
