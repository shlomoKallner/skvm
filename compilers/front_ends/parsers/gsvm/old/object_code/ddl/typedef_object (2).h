#ifndef SK_GSVM_OBJECT_CODE_TYPEDEF_OBJECT_H
#define SK_GSVM_OBJECT_CODE_TYPEDEF_OBJECT_H

#include "type_object.h"

namespace SK
{
 namespace gsvm
 {
  namespace object_code
  {
   
   
   template< typename IteratorT, typename T >
   struct typedef_object : public type_object<IteratorT,T>
   {
    typedef typename type_object<IteratorT,T>         base_t;
    typedef typename typedef_object<IteratorT,T>      self_t;
    typedef typename shared_ptr< self_t >             self_ptr;
    //
    typedef typename modifier_object<IteratorT,T>::self_t
                                                      modifier_t;
    typedef typename modifier_t::self_ptr             modifier_ptr;
    typedef typename base_t::self_ptr                 type_ptr;
    
    protected:
              string_t                name_m;
              std::list<modifier_ptr> modifiers_m;
              type_ptr                base_type_m;
    
   };
   
   
  }
 }
}


#endif
