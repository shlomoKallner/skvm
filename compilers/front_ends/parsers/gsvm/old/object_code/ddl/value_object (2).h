#ifndef SK_GSVM_OBJECT_CODE_TYPE_OBJECT_H
#define SK_GSVM_OBJECT_CODE_TYPE_OBJECT_H

#include "data_object.h"
#include "gsvm/general/gsvm_ddl_const.h"

namespace SK
{
 namespace gsvm
 {
  namespace object_code
  {
   using namespace SK::gsvm::ddl::consts;
   template< typename InputT, typename T = void_t >
   struct value_object : public data_object< InputT,T >
   {
    typedef data_object< InputT,T >                     base_t;
    typedef value_object< InputT,T >                    self_t;
    typedef shared_ptr< self_t >                        self_ptr;
    
    value_object( string_t str, std::size_t t )
    {
     switch( t )
     {
      case CHAR_LIT : 
      case WCHAR_LIT: 
      case MBCHAR_LIT: 
      case VLCHAR_LIT:
      case STR_LIT: 
      case WSTR_LIT: 
      case MBSTR_LIT: 
      case VLSTR_LIT:
           val_m.ch_str_val = str;
           break;
      default:
              assert(0);
     }
     lit_type_m = t;
     is_container_m = false;
    } // end of ...
    
    value_object( SK::types::boolean b, std::size_t t )
    {
     switch( t )
     {
      case BOOL_LIT : 
           val_m.b_val = b;
           break;
      default:
              assert(0);
     }
     lit_type_m = t;
     is_container_m = false;
    } // end of ...
    
    value_object( SK::types::sint64 s, std::size_t t )
    {
     switch( t )
     {
      case SINT8_LIT  : 
      case SINT10_LIT :
      case SINT16_LIT :
           val_m.s64_val = s;
           break;
      default:
              assert(0);
     }
     lit_type_m = t;
     is_container_m = false;
    } // end of ...
    
    value_object( SK::types::uint64 u, std::size_t t )
    {
     switch( t )
     {
      case UINT8_LIT  :
      case UINT10_LIT :
      case UINT16_LIT :
           val_m.u64_val = u;
           break;
      default:
              assert(0);
     }
     lit_type_m = t;
     is_container_m = false;
    } // end of ...
    
    value_object( SK::types::anyptr a, std::size_t = NULL_ID )
    {
     val_m.ap_val = a;
     lit_type_m = NULL_ID;
     is_container_m = false;
    } // end of ...
    
    value_object( SK::types::float64 f, std::size_t t )
    {
     switch( t )
     {
      case FLOAT8_LIT  : 
      case FLOAT10_LIT :
      case FLOAT16_LIT :
           val_m.f64_val = f;
           break;
      default:
              assert(0);
     }
     lit_type_m = t;
     is_container_m = false;
    } // end of ...
    
    explicit value_object(  std::vector< value_ptr >& vec )
    {
     is_container_m = true;
     cont_vals_m = vec; 
     lit_type_m = MAX_ID;
    }//end of ...
    
    std::size_t get_type( void )
    {
     return lit_type_m;
    }//end of ...
    
    SK::types:: get_b_value( void )
    {
     switch( lit_type_m )
     {
      case BOOL_LIT :
           return val_m.b_val;
      default:
              assert(0);
              return false;
     }
    }//end of ...
    
    SK::types:: get_s64_value( void )
    {
     switch( lit_type_m )
     {
      case SINT8_LIT  : 
      case SINT10_LIT :
      case SINT16_LIT :
           return val_m.s64_val;
      default:
              assert(0);
              return 0;
     }
    }//end of ...
    
    SK::types:: get_u64_value( void )
    {
     switch( lit_type_m )
     {
      case UINT8_LIT  :
      case UINT10_LIT :
      case UINT16_LIT :
           return val_m.u64_val;
      default:
              assert(0);
              return 0;
     }
    }//end of ...
    
    SK::types:: get_ap_value( void )
    {
     switch( lit_type_m )
     {
      case NULL_ID:
             return val_m.ap_val;
      default:
              assert(0);
              return 0;
     }
    }//end of ...
    
    SK::types:: get_f64_value( void )
    {
     switch( lit_type_m )
     {
      case FLOAT8_LIT  : 
      case FLOAT10_LIT :
      case FLOAT16_LIT :
           return val_m.f64_val;
      default:
              assert(0);
              return 0.0;
     }
    }//end of ...
    
    string_t get_ch_str_value( void )
    {
     switch( lit_type_m )
     {
      case CHAR_LIT : 
      case WCHAR_LIT: 
      case MBCHAR_LIT: 
      case VLCHAR_LIT:
      case STR_LIT: 
      case WSTR_LIT: 
      case MBSTR_LIT: 
      case VLSTR_LIT:
           return val_m.ch_str_val;
      default:
              assert(0);
              return string_t("");
     }
    }//end of ...
    
    std::vector< value_ptr >& get_contained( void )
    {
     if( this->is_container() )
     {
      return cont_vals_m;
     }
    }//end of ... 
    
    SK::types::boolean is_container( void )
    {
     return is_container_m;
    }// end of ...
    
    protected:
     union value_impl
     {
      using namespace SK::types;
      boolean   b_val;
      sint64    s64_val;
      uint64    u64_val;
      anyptr    ap_val;
      float64   f64_val;
      string_t  ch_str_val;
     };
     
     value_impl                val_m;
     std::size_t               lit_type_m;
     
     SK::types::boolean        is_container_m;
     std::vector< value_ptr >  cont_vals_m; 
               
   };
   
  }
 }
}


#endif
