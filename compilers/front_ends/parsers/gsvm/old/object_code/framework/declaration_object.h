#ifndef SK_GSVM_OBJECT_CODE_DECLARATION_OBJECT_H
#define SK_GSVM_OBJECT_CODE_DECLARATION_OBJECT_H

#include "object_code_fwd.h"


namespace SK
{
 namespace gsvm
 {
  namespace object_code
  {
   template< typename InputT, typename T >
   struct declaration_object
   {
     typedef declaration_object<InputT,T>                   base_t;
     typedef declaration_object<InputT,T>                   self_t;
     typedef shared_ptr< self_t >                           self_ptr;
     typedef typename InputT::char_t                        char_t;
     typedef typename InputT::string_t                      string_t;
     
     enum flags { NO_FLAGS = 0, MUST_RESOLVE = 0x1,   };
     
     declaration_object(): parent_m(0), flags_m(0) {}//end of ...
     declaration_object( self_t const& s ): parent_m(s.parent_m), 
                                            flags_m(s.flags_m)
     {}//end of ...
     virtual ~declaration_object(){}//end of ...
     
     void set_parent( self_ptr s )
     {
      parent_m = s;
     }//end of ...
     self_ptr get_parent(void)
     {
      return parent_m;
     }//end of ...
     boolean  is_parent( self_ptr const& s )
     {
      return parent_m ? parent_m == s : false ;
     }//end of ..
     
     virtual boolean is_child( self_t const& )=0;
     virtual void add( self_t const& )=0;
     virtual void concat( self_t const& )=0;
     virtual void resolve( void )=0;
     virtual list<self_ptr> find( string_t const& )=0;
     
    protected:
              self_ptr parent_m;
              uint64 flags_m;
   };
   
  }
 }
}

/*

 typename ObjectT,

ObjectT& object()
     {
      return *static_cast<ObjectT*>(this);
     }//end of ...
     ObjectT const& object()
     {
      return *static_cast<ObjectT const*>(this);
     }//end of ...

*/

#endif
