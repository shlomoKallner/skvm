#ifndef SK_GSVM_OBJECT_CODE_OBJECT_CODE_FWD_H
#define SK_GSVM_OBJECT_CODE_OBJECT_CODE_FWD_H


#include <string>
#include <map>
#include <list>
#include <set>
#include <utility>

#define NDEBUG //we are not debugging yet ...

#include <cassert>

#include <boost/shared_ptr.hpp>
#include <boost/detail/iterator.hpp>

#include "SK_types.h"

namespace SK
{
 namespace gsvm
 {
  namespace object_code
  {
   using namespace SK::types;
   using std::basic_string;
   using std::map;
   using std::multimap;
   using std::list;
   using std::set;
   using std::multiset;
   using std::pair;
   using boost::shared_ptr;
   using boost::detail::iterator_traits;
   
   /*
    
    template< typename InputT, typename T = void_t >
    struct _object;
   
   */
   struct void_t 
   {};
   
   template< typename InputT, typename T = void_t >
   struct declaration_object;
   
   
   template< typename InputT, typename T = void_t >
   struct module_object;
   
   template< typename InputT, typename T = void_t >
   struct namespace_object;
   
   
   template< typename InputT, typename T = void_t >
   struct type_object;
   
   template< typename InputT, typename T = void_t >
   struct bitfield_object;
   
   template< typename InputT, typename T = void_t >
   struct enum_object;
   
   template< typename InputT, typename T = void_t >
   struct object_object;
   
   template< typename InputT, typename T = void_t >
   struct union_object;
   
   template< typename InputT, typename T = void_t >
   struct typedef_object;
    
   template< typename InputT, typename T = void_t >
   struct pointer_object;
   
   template< typename InputT, typename T = void_t >
   struct reference_object;
   
   template< typename InputT, typename T = void_t >
   struct array_object;
   
   template< typename InputT, typename T = void_t >
   struct primitive_object;
   
   
   template< typename InputT, typename T = void_t >
   struct data_object;
   
   template< typename InputT, typename T = void_t >
   struct variable_object;
   
   template< typename InputT, typename T = void_t >
   struct constant_object;
   
   template< typename InputT, typename T = void_t >
   struct value_object;
   
   
   template< typename InputT, typename T = void_t >
   struct subroutine_object;
   
   template< typename InputT, typename T = void_t >
   struct function_object;
   
   template< typename InputT, typename T = void_t >
   struct operator_object;
   
   template< typename InputT, typename T = void_t >
   struct constructor_object;
   
   template< typename InputT, typename T = void_t >
   struct destructor_object;
   
   template< typename InputT, typename T = void_t >
   struct modifier_object;
   
   
   template< typename InputT, typename T = void_t >
   struct code_object;
   
   template< typename InputT, typename T = void_t >
   struct code_block_object;
   
   template< typename InputT, typename T = void_t >
   struct instruction_object;
   
   
   
  }
 }
}

/*
    
    template< typename InputT, typename T = void_t >
    struct _object;
   
   */

#endif
