

namespace SK
{
 namespace object_code2
 {
  namespace types
  {
   class base_type;
   
   class project_type;
   class module_type;
   class object_type;
   class type_type;
   class constructor_type;
   class destructor_type;
   class function_type;
   class operator_type;
   class block_type;
   class statement_type;
   class operation_type;
   class variable_type;
   class constant_type;
   
  }
 }
}
