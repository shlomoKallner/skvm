#ifdef  SK_GSVM_EVAL_PARSER_H
#define SK_GSVM_EVAL_PARSER_H

#ifndef SK_SGVM_EVAL__H
#define SK_SGVM_EVAL__H



#endif


namespace SK
{
 namespace gsvm
 {
  namespace eval
  {
    using namespace boost::spirit::classic;
    using namespace SK::types;
    
    //the actual types:  
    
    
  }//end of namespace.
 }
}
 /*
 { 
  public:
         typedef typename parsed_< CharT > _t;
         typedef typename boost::shared_ptr< _t > _ptr;
         typedef typename std::basic_string< CharT > string_t;
 }
 
 // obsolete types:
 template< typename CharT = char >
    class parsed_variable; 
  
 template< typename CharT >
    class parsed_variable
    { 
      public:
         typedef typename parsed_variable< CharT >             variable_t;
         typedef typename boost::shared_ptr< variable_t >      variable_ptr;
         typedef typename std::basic_string< CharT >           string_t;
         //
         typedef typename parsed_types< CharT >::types_t       types_t;
         typedef typename types_t::types_ptr                   types_ptr;
         typedef typename parsed_code< CharT >::code_t         code_t;
         typedef typename code_t::code_ptr                     code_ptr;
         typedef typename code_t::insr_iter_t                  insr_iter_t;
      protected:
                string_t     name_m;
                types_ptr    type_m;
                code_ptr     block_where_init_m;// will contain ptr to block 
                // in which this is init-ed.
                insr_iter_t  init_beg_m, init_end_m;// will contain iters to 
                // the begining of the init-ing and to the first past the end 
                // of init-ing.
    };            
 
 
 // end of obsolete types.
 */


#endif


