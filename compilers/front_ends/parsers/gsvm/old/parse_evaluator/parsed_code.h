#ifndef SK_GSVM_EVAL_PARSED_CODE_H
#define SK_GSVM_EVAL_PARSED_CODE_H

#include "evaluator_fwd.h"

namespace SK
{
 namespace gsvm
 {
  namespace eval
  {
    template< typename CharT, typename FactoryT >
    class parsed_code : evaluator_base< parsed_code< CharT, FactoryT >,
                                        CharT, FactoryT > 
    { 
      public:
         typedef typename parsed_code< CharT, FactoryT >       self_t;
         typedef typename boost::shared_ptr< self_t >          self_ptr;
         //
         typedef typename parsed_types< CharT >::self_t        types_t;
         typedef typename types_t::self_ptr                    types_ptr;
         typedef typename parsed_constant< CharT >::self_t     constant_t;
         typedef typename constant_t::self_ptr                 constant_ptr;
         typedef typename parsed_variable< CharT >::self_t     variable_t;
         typedef typename variable_t::self_ptr                 variable_ptr;
         typedef typename parsed_insr< CharT >::self_t         insr_t;
         typedef insr_t::self_ptr                              insr_ptr;
         typedef typename std::list< insr_ptr >::iterator      insr_iter_t;
      protected:
                string_t                               label_m;
                std::list< insr_ptr >                  insr_set_m;
                std::map< uint64 , code_ptr >          cont_code_m;
                // if cont_code_m is empty then the instructions are in
                // insr_set_m else insr_set_m is null.
                std::map< string_t , data_ptr >    const_decl_map_m, 
                                     var_decl_map_m;
                std::map< string_t , types_ptr >       type_decl_map_m
    };
  }
 }
}

#endif 
