#ifndef SK_GSVM_EVAL_PARSED_MODIFIER_H
#define SK_GSVM_EVAL_PARSED_MODIFIER_H

namespace SK
{
 namespace gsvm
 {
  namespace eval
  { 
   template< typename CharT >
    class parsed_modifier
    {
     public:
            typedef typename parsed_modifier< CharT >         modifier_t;
            typedef typename boost::shared_ptr< modifier_t >  modifier_ptr;
            typedef typename std::basic_string< CharT >       string_t;
            //
            typedef typename parsed_param< CharT >::param_t   param_t;
            typedef typename param_t::param_ptr               param_ptr;
    };
  } 
 } 
}

#endif
