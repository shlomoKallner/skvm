#ifndef GSVM_DDL_PARSED_OBJ_FILE_H
#define GSVM_DDL_PARSED_OBJ_FILE_H

namespace SK
{
 namespace gsvm
 {
  namespace eval
  { 
   template< typename CharT >
    class parsed_obj_file
    { 
     public:
            typedef typename parsed_obj_file< CharT >         obj_file_t;
            typedef typename boost::shared_ptr< obj_file_t >  obj_file_ptr;
            typedef typename std::basic_string< CharT >       string_t;      
    };
  }
 }
}

#endif
