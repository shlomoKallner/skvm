#ifndef SK_SGVM_EVAL_PARSED_PROJECT_H
#define SK_SGVM_EVAL_PARSED_PROJECT_H

namespace SK
{
 namespace gsvm
 {
  namespace eval
  { 
   template< typename CharT >
    class parsed_project
    { 
          public:
                 typedef typename parsed_project< CharT >          project_t;
                 typedef typename boost::shared_ptr< project_t >   project_ptr;
                 typedef typename std::basic_string< CharT >       string_t;
                 //
                 typedef typename parsed_module< CharT >::module_t module_t;
                 typedef typename module_t::module_ptr             module_ptr;
                 typedef typename parsed_obj_file< CharT >::obj_file_t      
                                                                   obj_file_t;
                 typedef typename obj_file_t::obj_file_ptr         obj_file_ptr;
          protected:
                    std::multimap< string_t , module_ptr > module_map_m;
                    std::map< string_t , obj_file_ptr >    obj_file_map_m;
    };//end of parsed_project.         
              
  } 
 } 
}

#endif
