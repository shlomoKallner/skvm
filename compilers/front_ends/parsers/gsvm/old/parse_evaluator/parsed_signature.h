#ifndef GSVM_DDL_PARSED_SIGNATURE_H
#define GSVM_DDL_PARSED_SIGNATURE_H

namespace SK
{
 namespace gsvm
 {
  namespace eval
  { 
   template< typename CharT >
    class parsed_signature
    {
     public:
      typedef typename parsed_signature< CharT >         signature_t;
      typedef typename boost::shared_ptr< signature_t >  signature_ptr;
      typedef typename std::basic_string< CharT >        string_t;
      //
      typedef typename parsed_modifier< CharT >::modifier_t  modifier_t;
      typedef typename modifier_t::modifier_ptr              modifier_ptr;
      typedef typename parsed_param< CharT >::param_t        param_t;
      typedef typename param_t::param_ptr                    param_ptr;
     protected:
               param_ptr                                     ret_val_m;
               std::list< param_ptr >                        param_list_m;
               std::list< modifier_ptr >                     modifier_list_m;
    };
  } 
 } 
}

#endif
