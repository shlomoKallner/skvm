#ifndef GSVM_DDL_PARSED_TYPES_H
#define GSVM_DDL_PARSED_TYPES_H

namespace SK
{
 namespace gsvm
 {
  namespace eval
  { 
   template< typename CharT >
    class parsed_types
    { 
     public:
      typedef typename parsed_types< CharT >         types_t;
      typedef typename boost::shared_ptr< types_t >  types_ptr;
      typedef typename std::basic_string< CharT >    string_t;
      //
      typedef typename parsed_modifier< CharT >::modifier_t  modifier_t;
      typedef typename modifier_t::modifier_ptr              modifier_ptr;
      typedef typename parsed_data< CharT >::data_t          data_t;
      typedef typename data_t::data_ptr                      data_ptr;
      typedef typename parsed_function< CharT >::function_t  function_t;
      typedef typename function_t::function_ptr              function_ptr;
      typedef typename parsed_operator< CharT >::operator_t  operator_t;
      typedef typename operator_t::operator_ptr              operator_ptr;
      typedef typename parsed_ctor< CharT >::ctor_t          ctor_t;
      typedef typename ctor_t::ctor_ptr                      ctor_ptr;
      typedef typename parsed_dtor< CharT >::dtor_t          dtor_t;
      typedef typename dtor_t::dtor_ptr                      dtor_ptr;
      
      typedef typename std::multimap< string_t , types_ptr >::iterator  
                                                             typeIter_t;   
      typedef typename std::multimap< string_t , data_ptr >::iterator     
                                                             dataIter_t;
      typedef typename std::multimap< string_t , function_ptr >::iterator 
                                                             funcIter_t;
      typedef typename std::multimap< string_t , operator_ptr >::iterator 
                                                             operIter_t;
                                                             
      
      enum types_of_types_t{ NULL_T, PRIMITIVE_T, ARRAY, POINTER_T, 
            REFERENCE_T, BITFIELD, ENUM_T, OBJECT_T, UNION_T, MAX_T };
      
     protected:
               string_t name_m;
               types_of_types_t                              type_of_m;
               std::list< modifier_ptr >                     modifier_list_m;
               // NOTE: if type is a primitive, an array, a pointer, 
               // or a reference then all of these ( fallowing members ) 
               // will be empty
               std::multimap< string_t , types_ptr >         cont_type_map_m;
               std::map< typeIter_t , uint64 >               contTypeLayout_m;
               //
               std::multimap< string_t , data_ptr >          cont_data_map_m;
               std::map< dataIter_t , uint64 >               contDataLayout_m;
               //
               std::multimap< string_t , function_ptr >      cont_func_map_m;
               std::map< funcIter_t , uint64 >               contFuncLayout_m;
               //
               std::multimap< string_t , operator_ptr >      cont_oper_map_m;
               std::map< operIter_t , uint64 >               contOperLayout_m;
               //
               std::list< ctor_ptr >                         cont_ctor_map_m;
               dtor_ptr                                      cont_dtor_m;
    };
  } 
 } 
}

#endif
