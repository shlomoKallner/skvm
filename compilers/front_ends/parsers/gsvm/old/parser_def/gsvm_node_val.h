#ifndef GSVM_NODE_VAL_H
#define GSVM_NODE_VAL_H

#include "SK_types.h"

namespace SK
{
 namespace gsvm
 {
  namespace ddl
  {
   using namespace SK::types;
        
   class GSVM_node_val
    {
          public:
                 enum node_flags { null_flag = 0, uint_flag, sint_flag,
                                   float_flag, char_flag, wchar_flag,
                                   mbchar_flag, vlchar_flag, bool_flag,
                                   str_flag, wstr_flag, mbstr_flag,
                                   vlstr_flag };
          protected:
                    node_flags  flags_m;
                    uint64      uint_val_m;
                    sint64      sint_val_m;
                    float64     float_val_m;
                    boolean     bool_val_m;
          public:
                 // the assignment operator:
                 const GSVM_node_val& operator=(const GSVM_node_val& r)
                 {
                   this->flags_m     = r.flags_m;
                   this->uint_val_m  = r.uint_val_m;
                   this->sint_val_m  = r.sint_val_m;
                   this->float_val_m = r.float_val_m;
                   this->bool_val_m  = r.bool_val_m;  
                   return *this;
                 }//end of ...
                 // a bunch of ctors:
                 // default ctor
                 GSVM_node_val( void ): flags_m( null_flag ), uint_val_m(0),
                  sint_val_m(0), float_val_m(0.0), bool_val_m(false)
                 {}//end of ...
                 // construct from flag ctor
                 explicit GSVM_node_val( node_flags& f): flags_m( f ), 
                  uint_val_m(0), sint_val_m(0), float_val_m(0.0),
                  bool_val_m(false)
                 {}// end of ...
                 
                 explicit GSVM_node_val( uint64 u ): flags_m( uint_flag ), 
                  uint_val_m(u), sint_val_m(0), float_val_m(0.0), 
                  bool_val_m(false)
                 {}//end of ...
                 explicit GSVM_node_val( sint64 s ): flags_m( sint_flag ), 
                  uint_val_m(0), sint_val_m(s), float_val_m(0.0), 
                  bool_val_m(false)
                 {}//end of ...
                 explicit GSVM_node_val( float64 d ): flags_m( float_flag ), 
                  uint_val_m(0), sint_val_m(0), float_val_m(d), 
                  bool_val_m(false)
                 {}//end of ...
                 explicit GSVM_node_val( boolean b ): flags_m( bool_flag ), 
                  uint_val_m(0), sint_val_m(0), float_val_m(0.0), 
                  bool_val_m(b)
                 {}//end of ...
                 //accessor functions:
                 node_flags get_flag( void )
                 {
                  return this->flags_m;
                 }//end of ...
                 void       set_flag( node_flags n )
                 {
                  this->flags_m     = n;
                  this->uint_val_m  = 0;
                  this->sint_val_m  = 0;
                  this->float_val_m = 0.0;
                  this->bool_val_m  = false;          
                 }//end of ...
                 uint64     get_uint( void )
                 {
                  return this->uint_val_m;
                 }//end of ...
                 void       set_uint( uint64 u )
                 {
                  this->flags_m     = uint_flag;
                  this->uint_val_m  = u;
                  this->sint_val_m  = 0;
                  this->float_val_m = 0.0;
                  this->bool_val_m  = false;          
                 }//end of ...
                 sint64     get_sint( void )
                 {
                  return this->sint_val_m;
                 }//end of ...
                 void       set_sint( sint64 s )
                 {
                  this->flags_m     = sint_flag;
                  this->uint_val_m  = 0;
                  this->sint_val_m  = s;
                  this->float_val_m = 0.0;
                  this->bool_val_m  = false;          
                 }//end of ...
                 float64    get_float( void )
                 {
                  return this->float_val_m;
                 }//end of ...
                 void       set_float( float64 f )
                 {
                  this->flags_m     = float_flag;
                  this->uint_val_m  = 0;
                  this->sint_val_m  = 0;
                  this->float_val_m = f;
                  this->bool_val_m  = false;          
                 }//end of ...
                 boolean    get_bool( void )
                 {
                  return this->bool_val_m;
                 }//end of ...
                 void       set_bool( boolean b )
                 {
                  this->flags_m     = bool_flag;
                  this->uint_val_m  = 0;
                  this->sint_val_m  = 0;
                  this->float_val_m = 0.0;
                  this->bool_val_m  = b;          
                 }//end of ...
                 //value type check functions:
                 boolean has_value( void )
                 {
                  return ( this->flags_m == uint_flag 
                         || this->flags_m == sint_flag )
                         ||( this->flags_m == float_flag 
                         || this->flags_m == bool_flag );
                 }//end of ...
                 boolean has_flag( void )
                 {
                  return this->flags_m != null_flag ;
                 }//end of ...
                 boolean has_uint( void )
                 {
                  return this->flags_m == uint_flag ;
                 }//end of ...
                 boolean has_sint( void )
                 {
                  return this->flags_m == sint_flag ;
                 }
                 boolean has_float( void )
                 {
                  return this->flags_m == float_flag ;
                 }//end of ...
                 boolean has_bool( void )
                 {
                  return this->flags_m == bool_flag ;
                 }//end of ...
                 
                 boolean is_char( void )
                 {
                  return this->flags_m == char_flag ;
                 }//end of ...
                 boolean is_wchar( void )
                 {
                  return this->flags_m == wchar_flag ;
                 }//end of ...
                 boolean is_mbchar( void )
                 {
                  return this->flags_m == mbchar_flag ;
                 }//end of ...
                 boolean is_vlchar( void )
                 {
                  return this->flags_m == vlchar_flag ;
                 }//end of ...
                 
                 boolean is_str( void )
                 {
                  return this->flags_m == str_flag ;
                 }//end of ...
                 boolean is_wstr( void )
                 {
                  return this->flags_m == wstr_flag ;
                 }//end of ...
                 boolean is_mbstr( void )
                 {
                  return this->flags_m ==  mbstr_flag ;
                 }//end of ...
                 boolean is_vlstr( void )
                 {
                  return this->flags_m == vlstr_flag ;
                 }//end of ...
    };
    
  }
 }
}

#endif
