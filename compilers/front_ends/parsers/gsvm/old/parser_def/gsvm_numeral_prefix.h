#ifndef SK_GSVM_DDL_NUMERAL_PREFIX_H
#define SK_GSVM_DDL_NUMERAL_PREFIX_H

#include <boost/spirit/include/classic_spirit.hpp>

namespace SK
{
 namespace gsvm
 {
  namespace ddl
  {
    using namespace boost::spirit::classic;
    
    // the numeral prefix parsers:
           
    /* 
       Currently available parsers are for bases 8, 10 and 16 only.
       If you want to make available parsers for other bases you have to
       explicitly specify them.
    */
    
    template< const std::size_t Radix >
    struct numeral_prefix;
    
    template<>
    struct numeral_prefix< 8 > : parser< numeral_prefix<8> >
    {
           template <typename ScannerT>
           struct result
           {
            typedef typename match_result<ScannerT, nil_t>::type type;
           };
           typedef typename numeral_prefix< 8 > self_t;
           
           template< typename ScannerT , typename MatchT >
           typename parser_result<self_t, ScannerT>::type 
           parse( ScannerT& scan )
           {
            typename ScannerT::iterator_t save = scan.first;   
            MatchT hit = ch_p('0').parse( scan ); 
            if( hit )
            {
             return ScannerT::create_match( hit.length() , nil_t(),
                                             save, scan.first);
            }
            else
            { return ScannerT::no_match(); }
           }// end of ...
    };//
    template<>
    struct numeral_prefix< 10 > : parser< numeral_prefix<10> >
    { // this is actually a no-op, as decimal numbers have no prefixes.
           template <typename ScannerT>
           struct result
           {
            typedef typename match_result<ScannerT, nil_t>::type type;
           };
           typedef typename numeral_prefix< 10 > self_t;
           
           template< typename ScannerT >
           typename parser_result<self_t, ScannerT>::type 
           parse( ScannerT& scan )
           {
            return ScannerT::create_match( 0 , nil_t() ,
                                           scan.first , scan.first);
           }// end of ...
    };//
    template<>
    struct numeral_prefix< 16 > : parser< numeral_prefix<16> >
    {
           template <typename ScannerT>
           struct result
           {
            typedef typename match_result<ScannerT, nil_t>::type type;
           };
           typedef typename numeral_prefix< 16 > self_t;
           
           template< typename ScannerT, typename MatchT >
           typename parser_result<self_t, ScannerT>::type 
           parse( ScannerT& scan )
           {
            typename ScannerT::iterator_t save = scan.first;   
            MatchT hit = ( str_p("0x") | str_p("0X") ).parse( scan ); 
            if( hit )
            {
             return ScannerT::create_match( hit.length() , nil_t(),
                                             save, scan.first);
            }
            else
            { return ScannerT::no_match(); }
           }// end of ...
    };//
    
  }
 }
}  

#endif
