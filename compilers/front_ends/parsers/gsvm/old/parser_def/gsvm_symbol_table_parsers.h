#ifndef SK_GSVM_DDL_SYMBOL_TABLE_PARSERS_H
#define SK_GSVM_DDL_SYMBOL_TABLE_PARSERS_H

#include "SK/common/SK_types.h"
#include <boost/spirit/include/classic_spirit.hpp>
#include "SK/gsvm/general/gsvm_ddl_const.h"

namespace SK
{
 namespace gsvm
 {
  namespace ddl
  {
    using namespace boost;
    using namespace boost::spirit;
    using namespace boost::spirit::classic;
    using namespace SK;
    using namespace SK::types;
    using namespace SK::gsvm;
    using namespace SK::gsvm::ddl;
    using namespace SK::gsvm::ddl::consts;
    
    
    struct keyword_symbols : public symbols<std::size_t,char>
    {
     keyword_symbols()
     {
      add("abstract")("addressof")("bitfield")("boolean")("break")("byte")
         ("case")("cast")("callconv")("catch")("char")("const")
         ( "constructor")("continue")("default")("delete")
         ("destructor")("do")("double")("doubleword")
         ("dynamic_cast")("elif")("else")("enum")("explicit")
         ("export")( "false")("finaly")("float")("for")("friend")
         ("function")("goto")("halfword")("if")("int")("intern")
         ("import")("lang")("long")("label")("mbchar")("namespace")
         ("new")("object")("private")("protected")("public")("return")
         ("reinterpret_cast")("short")("signed")("sizeof")("static_cast")
         ("static")("switch")("template")("this")("throw")("true")("try")
         ("typedef")("typeid")("typeof")("typename")("typeinfo")("union")
         ("unsigned")("typename")("using")("var")("virtual")("vararg")("void")
         ("volatile")("vlchar")("wchar")("while")("word");
     }//end of ...
     
    };
    
    struct operator_symbols : public symbols<std::size_t,char>
    {
     operator_symbols()
     {
      add( "()", FUNC_CALL_OP_ID )( "[]", ARRAY_INDEX_OP_ID )
         ( "->", MEMBER_ACCESS_VIA_PTR_OP )( "++", INCREMENT_OP )
         ( "--", DECREMENT_OP )( "+", ADDITION_OP )
         ( "-", SUBTRACTION_OP )( "!", LOGICAL_NOT_OP )
         ( "~", BITWISE_NOT_OP )( "&", BITWISE_AND_OP )
         ( "*", MULTIPLY_OP )( "cast", TYPE_CAST_OP )
         ( "new", DYNAMIC_ALLOCATE_OP )
         ( "delete", DYNAMIC_DEALLOCATE_OP )
         ( "->*", MEMBER_ACCESS_VIA_PTR_TO_MEMBER_OP )
         ( "/", DIVISION_OP )
         ( "%", MODULUS_OP )( "<<", BITSHIFT_LEFT_OP )
         ( ">>", BITSHIFT_RIGHT_ARITHMETIC_OP )
         ( ">>>", BITSHIFT_RIGHT_LOGICAL_OP )
         ( "<", LESS_THAN_OP )( "<=", LESS_THAN_EQUAL_OP )
         ( ">", GREATER_THAN_OP )( ">=", GREATER_THAN_EUQUAL_OP )
         ( "==", EQUALS_OP )( "!=", NOT_EQUALS_OP )
         ( "^", BITWISE_XOR_OP )( "|", BITWISE_OR_OP )
         ( "&&", LOGICAL_AND_OP )( "^^", LOGICAL_XOR_OP )
         ( "||", LOGICAL_OR_OP )( "=", ASSIGNMENT_OP )
         ( "+=", ASSIGNMENT_ADDITION_OP )
         ( "-=", ASSIGNMENT_SUBTRACTION_OP )
         ( "*=", ASSIGNMENT_MULTIPLY_OP )
         ( "/=", ASSIGNMENT_DIVISION_OP )
         ( "%=", ASSIGNMENT_MODULUS_OP )
         ( "&=", ASSIGNMENT_BITWISE_AND_OP )
         ( "^=", ASSIGNMENT_BITWISE_XOR_OP )
         ( "|=", ASSIGNMENT_BITWISE_OR_OP )
         ( "<<=", ASSIGNMENT_BITSHIFT_LEFT_OP )
         ( ">>=", ASSIGNMENT_BITSHIFT_RIGHT_ARITHMETIC_OP )
         ( ">>>=", ASSIGNMENT_BITSHIFT_RIGHT_LOGICAL_OP )
         ( ",", SEQUENCE_OP );
     }//end of ...
     
    };
    
    struct builtin_function_symbols : public symbols<std::size_t,char>
    {
     builtin_function_symbols()
     {
      add( "dynamic_cast" , DYNAMIC_CAST_BF )
         ( "reinterpret_cast" , REINTERPRET_CAST_BF )
         ( "sizeof" , SIZEOF_BF )
         ( "static_cast" , STATIC_CAST_BF )
         ( "typeid" , TYPEID_BF );
     }//end of ...
     
    };
    
    struct builtin_type_symbols : public symbols<std::size_t,char>
    {
     builtin_type_symbols()
     {
      add( "boolean", BOOLEAN_BT )( "byte", BYTE_BT )
         ( "char", CHAR_BT )( "double", DOUBLE_BT )
         ( "doubleword", DOUBLEWORD_BT )( "float", FLOAT_BT )
         ( "halfword", HALFWORD_BT )( "int", INT_BT )
         ( "long", LONG_BT )( "mbchar", MBCHAR_BT )
         ( "short", SHORT_BT )( "signed", SIGNED_BT )
         ( "typeinfo", TYPEINFO_BT )
         ( "unsigned", UNSIGNED_BT )
         ( "void", VOID_BT )( "vlchar", VLCHAR_BT )
         ( "vararg", VARARG_BT )( "wchar", WCHAR_BT )
         ( "word", WORD_BT );
     }//end of ...
     
    };
    
    struct builtin_macro_func_symbols : public symbols<std::size_t,char>
    {
     builtin_macro_func_symbols()
     {
      add( "is_const" , IS_CONST_MBF )( "is_func" , IS_FUNC_MBF )
         ( "is_float" , IS_FLOAT_MBF )( "is_int" , IS_INT_MBF )
         ( "is_oper" , IS_OPER_MBF )( "is_str" , IS_STR_MBF )
         ( "is_type" , IS_TYPE_MBF )( "is_var" , IS_VAR_MBF )
         ( "length" , LENGTH_MBF )( "typeof" , TYPEOF_MBF )
         ( "dynamic_cast" , DYNAMIC_CAST_MBF )
         ( "reinterpret_cast" , REINTERPRET_CAST_MBF )
         ( "sizeof" , SIZEOF_MBF )( "static_cast" , STATIC_CAST_MBF )
         ( "typeid" , TYPEID_MBF );
     }//end of ...
     
    };
    
    struct builtin_macro_type_symbols : public symbols<std::size_t,char>
    {
     builtin_macro_type_symbols()
     {
      add( "boolean" , BOOLEAN_MBT )
         ( "char" , CHAR_MBT )( "int" , INT_MBT )
         ( "float" , FLOAT_MBT )( "string" , STRING_MBT )
         ( "typeinfo" , TYPEINFO_MBT )( "var" , VAR_MBT )
         ( "vararg" , VARARG_MBT )( "void" , VOID_MBT )
         ( "doc_type" , DOC_MBT )( "code_type" , CODE_MBT )
         ( "error_type" , ERROR_MBT )( "call_type" , CALL_MBT )
         ( "system_type" , SYSTEM_MBT );
     }//end of ...
     
    };
    
    struct builtin_macro_var_symbols : public symbols<std::size_t,char>
    {
     builtin_macro_var_symbols()
     {
      add( "system" , SYSTEM_MBV )
     }//end of ...
     
    };
    
    struct unary_operator_symbols : public symbols<std::size_t,char>
    {
     unary_operator_symbols()
     {
      add( "++" , INCREMENT_OP )( "--" , DECREMENT_OP )( "+" , UNARY_PLUS_OP )
       ( "-" , UNARY_MINUS_OP )( "!" , LOGICAL_NOT_OP )( "~" , BITWISE_NOT_OP )
       ( "&" , GETADDRESS_OP )( "*" , DEREFERENCE_OP )
       ( "addressof" , ADDRESSOF_OP );
     }//end of ...
    };
    
    struct assignment_operator_symbols : public symbols<std::size_t,char>
    {
     assignment_operator_symbols()
     {
      add( "=" , ASSIGNMENT_OP )( "*=" , ASSIGNMENT_MULTIPLY_OP )
       ( "/=" , ASSIGNMENT_DIVISION_OP )( "%=" , ASSIGNMENT_MODULUS_OP )
       ( "+=" , ASSIGNMENT_ADDITION_OP )( "-=" , ASSIGNMENT_SUBTRACTION_OP )
       ( "<<=" , ASSIGNMENT_BITSHIFT_LEFT_OP )
       ( ">>=" , ASSIGNMENT_BITSHIFT_RIGHT_ARITHMETIC_OP )
       ( ">>>=" , ASSIGNMENT_BITSHIFT_RIGHT_LOGICAL_OP )
       ( "&=" , ASSIGNMENT_BITWISE_AND_OP )( "^=" , ASSIGNMENT_BITWISE_XOR_OP )
       ( "|=" , ASSIGNMENT_BITWISE_OR_OP );
     }//end of ...
     
    };
    
    /* - the type template:
    
    struct _symbols : public symbols<std::size_t,char>
    {
     _symbols()
     {
      add( , );
     }//end of ...
     
    };
    
    */
    
    /* - out of ddl_grammar just in case ...
    
    // the symbol tables of builtin idents, operators and keywords:
             keywords = "abstract","addressof","bitfield","boolean","break",
                       "byte","case", "cast","callconv", "catch","char",
                       "const", "constructor","continue", "default", "delete",
                       "destructor", "do","double", "doubleword", 
                       "dynamic_cast", "elif","else","enum", "explicit",
                       "export", "false", "finaly", "float","for","friend",
                       "function", "goto", "halfword", "if","int", "intern",
                       "import", "lang", "long", "label","mbchar", "namespace",
                       "new", "object", "private","protected", "public",
                       "return", "reinterpret_cast","short", "signed","sizeof",
                       "static_cast", "static","switch", "template", "this",
                       "throw", "true", "try","typedef", "typeid", "typename",
                       "typeinfo", "union","unsigned", "using", "var",
                       "virtual", "vararg", "void","volatile", "vlchar",
                       "wchar", "while", "word";
             operators.add ( "()", FUNC_CALL_OP_ID )( "[]", ARRAY_INDEX_OP_ID )
                        ( "->", MEMBER_ACCESS_VIA_PTR_OP )( "++", INCREMENT_OP )
                        ( "--", DECREMENT_OP )( "+", ADDITION_OP )
                        ( "-", SUBTRACTION_OP )( "!", LOGICAL_NOT_OP )
                        ( "~", BITWISE_NOT_OP )( "&", BITWISE_AND_OP )
                        ( "*", MULTIPLY_OP )( "cast", TYPE_CAST_OP )
                        ( "new", DYNAMIC_ALLOCATE_OP )
                        ( "delete", DYNAMIC_DEALLOCATE_OP )
                        ( "->*", MEMBER_ACCESS_VIA_PTR_TO_MEMBER_OP )
                        ( "/", DIVISION_OP )
                        ( "%", MODULUS_OP )( "<<", BITSHIFT_LEFT_OP )
                        ( ">>", BITSHIFT_RIGHT_ARITHMETIC_OP )
                        ( ">>>", BITSHIFT_RIGHT_LOGICAL_OP )
                        ( "<", LESS_THAN_OP )( "<=", LESS_THAN_EQUAL_OP )
                        ( ">", GREATER_THAN_OP )( ">=", GREATER_THAN_EUQUAL_OP )
                        ( "==", EQUALS_OP )( "!=", NOT_EQUALS_OP )
                        ( "^", BITWISE_XOR_OP )( "|", BITWISE_OR_OP )
                        ( "&&", LOGICAL_AND_OP )( "^^", LOGICAL_XOR_OP )
                        ( "||", LOGICAL_OR_OP )( "=", ASSIGNMENT_OP )
                        ( "+=", ASSIGNMENT_ADDITION_OP )
                        ( "-=", ASSIGNMENT_SUBTRACTION_OP )
                        ( "*=", ASSIGNMENT_MULTIPLY_OP )
                        ( "/=", ASSIGNMENT_DIVISION_OP )
                        ( "%=", ASSIGNMENT_MODULUS_OP )
                        ( "&=", ASSIGNMENT_BITWISE_AND_OP )
                        ( "^=", ASSIGNMENT_BITWISE_XOR_OP )
                        ( "|=", ASSIGNMENT_BITWISE_OR_OP )
                        ( "<<=", ASSIGNMENT_BITSHIFT_LEFT_OP )
                        ( ">>=", ASSIGNMENT_BITSHIFT_RIGHT_ARITHMETIC_OP )
                        ( ">>>=", ASSIGNMENT_BITSHIFT_RIGHT_LOGICAL_OP )
                        ( ",", SEQUENCE_OP );
                        
             builtin_funcs.add( "dynamic_cast" , DYNAMIC_CAST_BF )
                        ( "reinterpret_cast" , REINTERPRET_CAST_BF )
                        ( "sizeof" , SIZEOF_BF )
                        ( "static_cast" , STATIC_CAST_BF )
                        ( "typeid" , TYPEID_BF );
             builtin_types.add( "boolean", BOOLEAN_BT )( "byte", BYTE_BT )
                        ( "char", CHAR_BT )( "double", DOUBLE_BT )
                        ( "doubleword", DOUBLEWORD_BT )( "float", FLOAT_BT )
                        ( "halfword", HALFWORD_BT )( "int", INT_BT )
                        ( "long", LONG_BT )( "mbchar", MBCHAR_BT )
                        ( "short", SHORT_BT )( "signed", SIGNED_BT )
                        ( "typeinfo", TYPEINFO_BT )
                        ( "unsigned", UNSIGNED_BT )
                        ( "void", VOID_BT )( "vlchar", VLCHAR_BT )
                        ( "vararg", VARARG_BT )( "wchar", WCHAR_BT )
                        ( "word", WORD_BT );
                        
             builtin_macro_funcs.add( "is_const" , IS_CONST_MBF )
                        ( "is_func" , IS_FUNC_MBF )( "is_float" , IS_FLOAT_MBF )
                        ( "is_int" , IS_INT_MBF )( "is_oper" , IS_OPER_MBF )
                        ( "is_str" , IS_STR_MBF )( "is_type" , IS_TYPE_MBF )
                        ( "is_var" , IS_VAR_MBF )( "length" , LENGTH_MBF )
                        ( "typeof" , TYPEOF_MBF )
                        ( "dynamic_cast" , DYNAMIC_CAST_MBF )
                        ( "reinterpret_cast" , REINTERPRET_CAST_MBF )
                        ( "sizeof" , SIZEOF_MBF )
                        ( "static_cast" , STATIC_CAST_MBF )
                        ( "typeid" , TYPEID_MBF );
             builtin_macro_types.add( "boolean" , BOOLEAN_MBT )
                        ( "char" , CHAR_MBT )( "int" , INT_MBT )
                        ( "float" , FLOAT_MBT )( "string" , STRING_MBT )
                        ( "typeinfo" , TYPEINFO_MBT )( "var" , VAR_MBT )
                        ( "vararg" , VARARG_MBT )( "void" , VOID_MBT )
                        ( "doc_type" , DOC_MBT )( "code_type" , CODE_MBT )
                        ( "error_type" , ERROR_MBT )( "call_type" , CALL_MBT )
                        ( "system_type" , SYSTEM_MBT );
             builtin_macro_vars.add( "system" , SYSTEM_MBV );
             
    
    */
    
    
    
  }
 }
}

#endif
