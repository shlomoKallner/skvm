#ifndef GSVM_TREE_CONTEXT_H
#define GSVM_TREE_CONTEXT_H

#include <boost/spirit/include/classic_spirit.hpp>
#include "gsvm_node_val.h"

namespace SK
{
 namespace gsvm
 {
  namespace ddl
  {
    using namespace boost::spirit::classic;
    
    // Query: is setting the ID of a node necessary or can we depend on 
    //   parser_tag<N> to do it for us?
    // Answer : unnecessary, we can depend on parser_tag<N> to do it for us.
    
    // context for uints, sints, floats and bools.
    template< typename T > 
    struct GSVM_tree_lit_context : public parser_context<T>
    { 
     typedef parser_context_linker<GSVM_tree_lit_context<T> > context_linker_t;
     
     template <typename ParserT>
     GSVM_tree_lit_context(ParserT const& p) : parser_context(p) {}//end ...
     
     template <typename ParserT, typename ScannerT>
     void pre_parse(ParserT const& p, ScannerT const& scan) {}

     template <typename iter_t, typename ParserT, typename ScannerT, 
               typename factory_t>
     tree_match<iter_t,factory_t,T>&
     post_parse(tree_match<iter_t,factory_t,T>& hit, ParserT const& p, 
                ScannerT const& scan)
     {  
      if( hit && hit.has_valid_attribute())
      { 
       tree_match<iter_t,factory_t,T>::tree_iterator iter1 =hit.trees.begin();
       T t1 = hit.value();
       iter1->value.value(GSVM_node_val(t1));
      } 
      return hit; 
     }//end of ...
    };
    //context for flag only nodes.
    template< const GSVM_node_val::node_flags F >
    struct GSVM_tree_flag_context : public parser_context<T>
    { 
     typedef parser_context_linker<GSVM_tree_flag_context<T> > context_linker_t;
     
     template <typename ParserT>
     GSVM_tree_flag_context(ParserT const& p) : parser_context(p) {}//end ...
     
     template <typename ParserT, typename ScannerT>
     void pre_parse(ParserT const& p, ScannerT const& scan) {}

     template <typename iter_t, typename ParserT, typename ScannerT, 
               typename factory_t>
     tree_match<iter_t,factory_t,T>&
     post_parse(tree_match<iter_t,factory_t,T>& hit, ParserT const& p, 
                ScannerT const& scan)
     {  
      if( hit && hit.has_valid_attribute())
      { 
       tree_match<iter_t,factory_t,T>::tree_iterator iter1 =hit.trees.begin();
       iter1->value.value(GSVM_node_val(F));
      } 
      return hit; 
     }//end of ...
    };
    
  }
 }
}

#endif
