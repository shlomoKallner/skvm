#ifndef GSVM_DDL_GRAMMAR_H
#define GSVM_DDL_GRAMMAR_H

#include "SK/common/SK_types.h"
#include <boost/spirit/include/classic_spirit.hpp>
#include "gsvm_ddl_types.h" 
#include "gsvm/general/gsvm_ddl_const.h"
#include "gsvm_symbol_table_parsers.h"

namespace SK
{
 namespace gsvm
 {
  namespace ddl
  {
    using namespace boost::spirit::classic;
    using namespace SK::types;
    using namespace SK::gsvm::ddl::consts;
    
    struct ddl_grammar : public grammar<ddl_grammar>
    {
        template <typename ScannerT>
        struct definition
        {
            // the start rule:
            rule<ScannerT>              start_m;
            
            // the symbol table parsers:
            keyword_symbols             keywords; 
            operator_symbols            operators;
            builtin_function_symbols    builtin_funcs;
            builtin_type_symbols        builtin_types;
            builtin_macro_func_symbols  builtin_macro_funcs;
            builtin_macro_type_symbols  builtin_macro_types;
            builtin_macro_var_symbols   builtin_macro_vars;
            
            // the integeral and floating-point parsers:
            // the numeral prefix parsers:
            numeral_prefix< 8 >   base8_prefix_m;
            numeral_prefix< 16 >  base16_prefix_m;
            // the unsigned integer parsers:
            rule<ScannerT, parser_tag<UINT8_LIT> >  uint8_lit_r;
            rule<ScannerT, parser_tag<UINT10_LIT> > uint10_lit_r;
            rule<ScannerT, parser_tag<UINT16_LIT> > uint16_lit_r;
            rule<ScannerT>                          uint_groop_r;
            // the signed integer parsers:
            rule<ScannerT, parser_tag<SINT8_LIT> >    sint8_lit_r;
            rule<ScannerT, parser_tag<SINT10_LIT> >   sint10_lit_r;
            rule<ScannerT, parser_tag<SINT16_LIT> >   sint16_lit_r;
            rule<ScannerT>                            sint_groop_r;
            // the floating-point number parsers:
            rule<ScannerT, parser_tag<FLOAT8_LIT> >  float8_lit_r;
            rule<ScannerT, parser_tag<FLOAT10_LIT> > float10_lit_r;
            rule<ScannerT, parser_tag<FLOAT16_LIT> > float16_lit_r;
            rule<ScannerT>                           float_groop_r;
            
            rule<ScannerT> number_groop_r;
            // the boolean parser:
            rule<ScannerT, parser_tag<BOOL_LIT> > bool_lit_r;
                           
            //rule<ScannerT, GSVM_tree_lit_context<>,parser_tag<N> > ;
            
            // char type parsers:
            rule<ScannerT, parser_tag<CHAR_LIT> >    char_lit_r;
            rule<ScannerT, parser_tag<WCHAR_LIT> >   wchar_lit_r;               
            rule<ScannerT, parser_tag<MBCHAR_LIT> >  mbchar_lit_r;
            rule<ScannerT, parser_tag<VLCHAR_LIT> >  vlchar_lit_r;
            rule<ScannerT> char_groop_r ;
            // str type parsers:
            rule<ScannerT, parser_tag<STR_LIT> >    str_lit_r;
            rule<ScannerT, parser_tag<WSTR_LIT> >   wstr_lit_r;
            rule<ScannerT, parser_tag<MBSTR_LIT> >  mbstr_lit_r;
            rule<ScannerT, parser_tag<VLSTR_LIT> >  vlstr_lit_r;
            rule<ScannerT> str_groop_r ; 
            
            rule<ScannerT> litteral_groop_r; 
            // the identifier parsers:
            rule<ScannerT,parser_tag<BASIC_IDENT> >    basic_ident_r;
            rule<ScannerT,parser_tag<VAR_IDENT> >      qualified_var_r;
            rule<ScannerT,parser_tag<CONST_IDENT> >    qualified_const_r;
            rule<ScannerT,parser_tag<TYPENAME_IDENT> > qualified_typename_r;
            rule<ScannerT,parser_tag<BITFIELD_IDENT> > qualified_bitfield_r;
            rule<ScannerT,parser_tag<ENUM_IDENT> >     qualified_enum_r;
            rule<ScannerT,parser_tag<OBJECT_IDENT> >   qualified_object_r;
            rule<ScannerT,parser_tag<UNION_IDENT> >    qualified_union_r;
            rule<ScannerT,parser_tag<FUNCTION_TYPE_IDENT> > 
                                                       qualified_func_type_r;
            rule<ScannerT,parser_tag<FUNCTION_IDENT_ARGS> > 
                                                       function_ident_args_r;
            rule<ScannerT,parser_tag<FUNCTION_IDENT_ARG> > 
                                                       function_ident_arg_r;
            rule<ScannerT,parser_tag<OPERATOR_TYPE_IDENT> > 
                                                       qualified_oper_type_r;
            rule<ScannerT,parser_tag<QUALIFIED_TYPEOF_IDENT> >
                                                       qualified_typeof_expr_r;
            rule<ScannerT,parser_tag<QUALIFIED_TYPEID> >  qualified_typeid_r;
            rule<ScannerT,parser_tag<UNQUALIFIED_TYPEID> > unqualified_typeid_r;
            rule<ScannerT,parser_tag<LABEL_IDENT> >    qualified_label_r;
            rule<ScannerT,parser_tag<FUNCTION_IDENT> > qualified_function_r;
            rule<ScannerT,parser_tag<OPERATOR_IDENT> > qualified_operator_r;
            rule<ScannerT,parser_tag<NAMESPACE_IDENT> > qualified_namespace_r;
            rule<ScannerT,parser_tag<IMPORT_IDENT> > qualified_import_r;
            rule<ScannerT,parser_tag<BUILTIN_TYPES_IDENT> > builtin_types_r;
            rule<ScannerT,parser_tag<BUILTIN_FUNC_IDENT> > builtin_function_r;
            rule<ScannerT,parser_tag<OPERATOR_HELPER> >    operators_ident_r;
            rule<ScannerT>                            qualified_identifier_r,
                                                      unqualified_identifier_r;
            rule<ScannerT,parser_tag<NESTED_NAME_IDENT> > nested_namespace_r;
            rule<ScannerT,parser_tag<USING_IDENT> >       using_ident_r;
            
            // modifier parsers:
            rule<ScannerT,parser_tag<TEMP_ID_MOD> >       temp_id_mod_r;
            rule<ScannerT,parser_tag<TEMP_OPERID_MOD> >   temp_operid_mod_r;
            rule<ScannerT,parser_tag<TEMP_MOD_ARGS> >     temp_id_args_r;
            rule<ScannerT>                                temp_id_arg_r;
            rule<ScannerT>                                type_prefix_mod_r;
            rule<ScannerT,parser_tag<VAR_PTR_MOD> >       var_ptr_mod_r;
            rule<ScannerT,parser_tag<VAR_REF_MOD> >       var_ref_mod_r;
            rule<ScannerT,parser_tag<CONST_PTR_MOD> >     const_ptr_mod_r;
            rule<ScannerT,parser_tag<CONST_REF_MOD> >     const_ref_mod_r;
            rule<ScannerT,parser_tag<TYPE_POSTFIX_MOD> >  type_postfix_mod_r;
            rule<ScannerT>                                typeid_infix_mod_r;
            
            rule<ScannerT,parser_tag<CALLCONV_MOD> >   callconv_mod_r;
            rule<ScannerT,parser_tag<LANG_MOD> >       lang_mod_r;
            rule<ScannerT,parser_tag<THROW_MOD> >      throw_mod_r;
            rule<ScannerT,parser_tag<THROW_ARGS_MOD> > throw_args_r;
            rule<ScannerT,parser_tag<THROW_ARG_MOD> >  throw_arg_r;
            rule<ScannerT,parser_tag<STATIC_MOD> >     static_mod_r;
            rule<ScannerT,parser_tag<CONST_MOD> >      const_mod_r;
            rule<ScannerT>                             func_id_mod_r;
            rule<ScannerT>                             oper_id_mod_r;
            
            rule<ScannerT,parser_tag<GENDEC_MOD> >        general_decl_mods_r;
            rule<ScannerT,parser_tag<TEMP_DECL_MOD> >     temp_decl_mod_r;
            rule<ScannerT,parser_tag<TEMP_OPERDECL_MOD> > temp_operdecl_mod_r;
            rule<ScannerT,parser_tag<TEMP_DECL_ARGS> >    temp_decl_args_r;
            rule<ScannerT,parser_tag<TEMP_DECL_ARG> >     temp_decl_arg_r;
            rule<ScannerT,parser_tag<VOLATILE_MOD> >      volatile_mod_r;
            rule<ScannerT>                                data_decl_mods_r;
            rule<ScannerT,parser_tag<INITIALIZER_MOD> >   initializer_r;
            rule<ScannerT,parser_tag<MUTABLE_MOD> >       mutable_mod_r;
            rule<ScannerT>                                func_decl_mod_r;
            rule<ScannerT>                                oper_decl_mod_r;
            rule<ScannerT,parser_tag<ABSTRACT_MOD> >      abstract_mod_r;
            rule<ScannerT,parser_tag<VIRTUAL_MOD> >       virtual_mod_r;
            rule<ScannerT,parser_tag<FUNC_DECL_ARGS> >    func_decl_args_r;
            rule<ScannerT,parser_tag<FUNC_DECL_ARG> >     func_decl_arg_r;
            rule<ScannerT>                                ctor_decl_mod_r;
            rule<ScannerT,parser_tag<EXPLICIT_MOD> >      explicit_mod_r;
            rule<ScannerT>                                dtor_decl_mod_r;
            rule<ScannerT,parser_tag<CTOR_INIT_LIST> >    ctor_init_list_r;
            rule<ScannerT,parser_tag<CTOR_INIT_MEM> >     ctor_init_mem_r;
            rule<ScannerT,parser_tag<INHERIT_LIST> >      inherit_list_r;
            rule<ScannerT,parser_tag<INHERIT_MEM> >       inherit_mem_r;
            rule<ScannerT,parser_tag<TYPEDECL_INFIX_MOD> >type_decl_infix_mod_r;
            rule<ScannerT,parser_tag<ENUMERATOR_MOD> >    enumerator_r;
            rule<ScannerT,parser_tag<ACCESS_MOD> >        access_mod_r;
            
            // expression pasers:
            rule<ScannerT,parser_tag<IDENT_EXPR> >         ident_expr_r;
            rule<ScannerT,parser_tag<PRIMARY_EXPR> >       prime_expr_r;
            rule<ScannerT,parser_tag<POSTFIX_EXPR> >       postfix_expr_r;
            rule<ScannerT,parser_tag<ARRAY_INDEX_EXPR> >   array_expr_r;
            rule<ScannerT,parser_tag<FUNC_CALL_EXPR> >     func_call_expr_r;
            rule<ScannerT,parser_tag<EXPLICIT_CTOR_EXPR> > expl_ctor_expr_r;
            rule<ScannerT,parser_tag<MEMBER_REF_EXPR> >    mem_ref_expr_r;
            rule<ScannerT,parser_tag<MEMBER_PTR_EXPR> >    mem_ptr_expr_r;
            rule<ScannerT,parser_tag<EXPLICIT_DTOR_EXPR> > expl_dtor_expr_r;
            rule<ScannerT,parser_tag<INC_DEC_EXPR> >       inc_dec_expr_r;
            rule<ScannerT,parser_tag<EXPR_LIST> >          expression_list_r;
            rule<ScannerT,parser_tag<NEW_EXPR> >           new_expr_r;
            rule<ScannerT,parser_tag<DEL_EXPR> >           del_expr_r;
            rule<ScannerT,parser_tag<DEL_ARRAY_EXPR> >     del_array_expr_r;
            rule<ScannerT,parser_tag<UNARY_EXPR> >         unary_expr_r;
            unary_operator_symbols                         unary_ops_sym;
            rule<ScannerT,parser_tag<UNARY_OPERS> >        unary_opers_r;
            rule<ScannerT,parser_tag<PTR_MEM_EXPR> >       ptr_mem_expr_r;
            rule<ScannerT,parser_tag<MULTIPLY_EXPR> >      multiply_expr_r;
            rule<ScannerT,parser_tag<ADDITION_EXPR> >      addition_expr_r;
            rule<ScannerT,parser_tag<BITSHFT_EXPR> >       bitshift_expr_r;
            rule<ScannerT,parser_tag<RELATIONAL_EXPR> >    relational_expr_r;
            rule<ScannerT,parser_tag<EQUALITY_EXPR> >      equality_expr_r;
            rule<ScannerT,parser_tag<BITWISEAND_EXPR> >    bitwiseAnd_expr_r;
            rule<ScannerT,parser_tag<BITWISEXOR_EXPR> >    bitwiseXor_expr_r;
            rule<ScannerT,parser_tag<BITWISEOR_EXPR> >     bitwiseOr_expr_r;
            rule<ScannerT,parser_tag<LOGICALAND_EXPR> >    logicalAnd_expr_r;
            rule<ScannerT,parser_tag<LOGICALXOR_EXPR> >    logicalXor_expr_r;
            rule<ScannerT,parser_tag<LOGICALOR_EXPR> >     logicalOr_expr_r;
            rule<ScannerT,parser_tag<CONDITIONAL_EXPR> >   conditional_expr_r;
            rule<ScannerT,parser_tag<ASSIGNMENT_EXPR> >    assignment_expr_r;
            assignment_operator_symbols                    assignment_oper_sym;
            rule<ScannerT,parser_tag<ASSIGNMENT_OPERS> >   assignment_opers_r;
            rule<ScannerT,parser_tag<SEQUENCE_EXPR> >      sequence_expr_r;
            rule<ScannerT,parser_tag<THROW_EXPR> >         throw_expr_r;
            rule<ScannerT,parser_tag<CONDITION_EXPR> >     condition_expr_r;
            
            // statement parsers:
            rule<ScannerT,parser_tag<EXPR_STATE> >    expression_state_r;
            rule<ScannerT,parser_tag<DECL_STATE> >    declaration_state_r;
            rule<ScannerT,parser_tag<SELECT_STATE> >  select_state_r;
            rule<ScannerT,parser_tag<IF_STATE> >      if_state_r;
            rule<ScannerT,parser_tag<ELIF_STATE> >    elif_state_r;
            rule<ScannerT,parser_tag<ELSE_STATE> >    else_state_r;
            rule<ScannerT,parser_tag<SWITCH_STATE> >  switch_state_r;
            rule<ScannerT,parser_tag<LABEL_STATE> >   label_state_r;
            rule<ScannerT,parser_tag<JUMP_STATE> >    jump_state_r;
            rule<ScannerT,parser_tag<CMPND_STATE> >   compound_state_r;
            rule<ScannerT,parser_tag<EXCPTN_STATE> >  exception_state_r;
            rule<ScannerT,parser_tag<CATCH_STATE> >   catch_state_r;
            rule<ScannerT,parser_tag<FINALLY_STATE> > finally_state_r;
            rule<ScannerT,parser_tag<LOOP_STATE> >    loop_state_r;
            rule<ScannerT,parser_tag<USING_STATE> >  using_state_r;
            rule<ScannerT>                            statement_groop_r;
            
            // declaration parsers:
            rule<ScannerT,parser_tag<CONST_DECL> >        const_decl_r;
            rule<ScannerT,parser_tag<VAR_DECL> >          var_decl_r;
            rule<ScannerT,parser_tag<DATA_DECL> >         data_decl_r;
            rule<ScannerT,parser_tag<TYPEDEF_DECL> >      typedef_decl_r;
            rule<ScannerT,parser_tag<TYPEDEF_DEF_DECL> >  typedef_def_r;
            rule<ScannerT,parser_tag<CONSTANT_DECL> >     constant_decl_r;
            rule<ScannerT,parser_tag<VARIABLE_DECL> >     variable_decl_r;
            rule<ScannerT,parser_tag<IMPORT_DECL> >       import_decl_r;
            rule<ScannerT,parser_tag<CMPND_DECL> >        compound_decl_r;
            rule<ScannerT,parser_tag<NMSPC_FWD_DECL> >    namespace_fwd_decl_r;
            rule<ScannerT,parser_tag<NMSPC_DECL> >        namespace_decl_r;
            rule<ScannerT,parser_tag<FUNC_FWD_DECL> >     func_fwd_decl_r;
            rule<ScannerT,parser_tag<FUNC_DECL> >         func_decl_r;
            rule<ScannerT,parser_tag<OPER_FWD_DECL> >     oper_fwd_decl_r;
            rule<ScannerT,parser_tag<OPER_DECL> >         oper_decl_r;
            rule<ScannerT,parser_tag<CTOR_FWD_DECL> >     ctor_fwd_decl_r;
            rule<ScannerT,parser_tag<CTOR_DECL> >         ctor_decl_r;
            rule<ScannerT,parser_tag<DTOR_FWD_DECL> >     dtor_fwd_decl_r;
            rule<ScannerT,parser_tag<DTOR_DECL> >         dtor_decl_r;
            rule<ScannerT,parser_tag<BITFIELD_FWD_DECL> > bitfield_fwd_decl_r;
            rule<ScannerT,parser_tag<BITFIELD_DECL> >     bitfield_decl_r;
            rule<ScannerT,parser_tag<ENUM_FWD_DECL> >     enum_fwd_decl_r;
            rule<ScannerT,parser_tag<ENUM_DECL> >         enum_decl_r;
            rule<ScannerT,parser_tag<OBJECT_FWD_DECL> >   object_fwd_decl_r;
            rule<ScannerT,parser_tag<OBJECT_DECL> >       object_decl_r;
            rule<ScannerT,parser_tag<UNION_FWD_DECL> >    union_fwd_decl_r;
            rule<ScannerT,parser_tag<UNION_DECL> >        union_decl_r;
            rule<ScannerT,parser_tag<FORWARD_DECL> >      forward_decl_r;
            rule<ScannerT,parser_tag<FRIEND_DECL> >       friend_decl_r;
            rule<ScannerT,parser_tag<MEMBER_DECL> >       member_decl_r;
            rule<ScannerT>                                declaration_groop_r;
            
            
            // the macro parsers:
            // the macro identifier parsers:
            rule<ScannerT,parser_tag<BASIC_MID> >      macro_basic_ident_r;
            rule<ScannerT,parser_tag<VAR_MID> >        macro_var_ident_r;
            rule<ScannerT,parser_tag<FUNCTION_MID> >   macro_func_ident_r;
            rule<ScannerT,parser_tag<OPERATOR_MID> >   macro_oper_ident_r;
            rule<ScannerT,parser_tag<TYPE_MID> >       macro_type_ident_r;
            rule<ScannerT>                             macro_identifier_r;
            rule<ScannerT,parser_tag<NMSPC_MID> >      macro_namespace_r;
            rule<ScannerT,parser_tag<IMPORT_MID> >     macro_import_r;
            rule<ScannerT,parser_tag<NNAME_MID> >      macro_nested_name_r;
            rule<ScannerT,parser_tag<USING_MID> >      macro_using_ident_r;
            // macro ident parser wrappers:
            rule<ScannerT,parser_tag<BFUNC_MID> > macro_builtin_func_wrapper_r;
            rule<ScannerT,parser_tag<BOPER_MID> > macro_builtin_oper_wrapper_r;
            rule<ScannerT,parser_tag<BTYPE_MID> > macro_builtin_type_wrapper_r;
            rule<ScannerT,parser_tag<BVAR_MID> >  macro_builtin_var_wrapper_r;
            
            // the macro modifier parsers:
            rule<ScannerT,parser_tag<TEMPID_MMOD> > macro_tempid_mod_r;
            rule<ScannerT,parser_tag<TEMPID_OPER_MMOD> > macro_temp_operid_r;
            rule<ScannerT,parser_tag<TEMPID_ARGS_MMOD> > macro_tempid_args_r;
            rule<ScannerT>                macro_tempid_arg_r;
            rule<ScannerT,parser_tag<TYPE_POSTFIX_MMOD> > 
                                                  macro_type_postfix_mod_r;
            rule<ScannerT,parser_tag<THROW_MMOD> > macro_throw_mod_r;
            rule<ScannerT,parser_tag<THROW_ARGS_MMOD> > macro_throw_args_r;
            rule<ScannerT,parser_tag<THROW_ARG_MMOD> > macro_throw_arg_r;
            rule<ScannerT>                macro_func_mod_r;
            rule<ScannerT,parser_tag<STATIC_MMOD> > macro_static_mod_r;
            rule<ScannerT,parser_tag<CONST_MMOD> > macro_const_mod_r;
            rule<ScannerT>                macro_oper_mod_r;
            rule<ScannerT>                macro_data_decl_mods_r;
            rule<ScannerT,parser_tag<VOLATILE_MMOD> > macro_volatile_mod_r;
            rule<ScannerT,parser_tag<INITIALIZER_MMOD> > macro_initializer_r;
            rule<ScannerT,parser_tag<GEN_DECL_MMOD> > macro_gen_decl_mod_r;
            rule<ScannerT,parser_tag<ABSTRACT_MMOD> > macro_abstract_mod_r;
            rule<ScannerT,parser_tag<VIRTUAL_MMOD> > macro_virtual_mod_r;
            rule<ScannerT,parser_tag<TEMPDECL_MMOD> > macro_temp_decl_mod_r;
            rule<ScannerT,parser_tag<TEMPDECL_OPER_MMOD> > 
                                                  macro_temp_oper_decl_mod_r;
            rule<ScannerT,parser_tag<TEMPDECL_ARGS_MMOD> > 
                                                  macro_temp_decl_args_r;
            rule<ScannerT,parser_tag<TEMPDECL_ARG_MMOD> > 
                                                  macro_temp_decl_arg_r;
            rule<ScannerT>                        macro_func_decl_mod_r;
            rule<ScannerT>                        macro_oper_decl_mod_r;
            rule<ScannerT,parser_tag<FUNCDECL_ARGS_MMOD> > 
                                                  macro_func_decl_args_r;
            rule<ScannerT,parser_tag<FUNCDECL_ARG_MMOD> > 
                                                  macro_func_decl_arg_r;
            rule<ScannerT,parser_tag<EXPLICIT_MMOD> > macro_expicit_mod_r;
            rule<ScannerT,parser_tag<CTOR_INITLIST_MMOD> > 
                                                      macro_ctor_init_list_r;
            rule<ScannerT,parser_tag<CTOR_INITMEM_MMOD> > macro_ctor_init_mem_r;
            rule<ScannerT> macro_ctor_decl_mod_r;
            rule<ScannerT> macro_dtor_decl_mod_r;
            rule<ScannerT,parser_tag<INHERITLIST_MMOD> > macro_inherit_list_r;
            rule<ScannerT,parser_tag<INHERITMEM_MMOD> > macro_inherit_mem_r;
            rule<ScannerT,parser_tag<ACCESS_MMOD> > macro_access_mod_r;
            
            
            //rule<ScannerT,parser_tag<N> > macro__mod_r;
            // the macro expression parsers:
            rule<ScannerT,parser_tag<IDENT_MEXPR> > macro_ident_expr_r;
            rule<ScannerT,parser_tag<PRRIME_MEXPR> > macro_prime_expr_r;
            rule<ScannerT,parser_tag<POSTFIX_MEXPR> > macro_postfix_expr_r;
            rule<ScannerT,parser_tag<ARRAY_MEXPR> > macro_array_expr_r;
            rule<ScannerT,parser_tag<EXPRLIST_MEXPR> > macro_expression_list_r;
            rule<ScannerT,parser_tag<FUNCCALL_MEXPR> > macro_func_call_expr_r;
            rule<ScannerT,parser_tag<EXPLCTCTOR_MEXPR> > macro_ctor_expr_r;
            rule<ScannerT,parser_tag<MEMBER_MEXPR> > macro_member_expr_r;
            rule<ScannerT,parser_tag<EXPLCTDTOR_MEXPR> > macro_dtor_expr_r;
            rule<ScannerT,parser_tag<UNARY_MEXPR> > macro_unary_expr_r;
            rule<ScannerT,parser_tag<UNARY_MOPERS> > macro_unary_opers_r;
            rule<ScannerT,parser_tag<NEW_MEXPR> > macro_new_expr_r;
            rule<ScannerT,parser_tag<NEWCTOR_MEXPR> > macro_ctor_r; 
            rule<ScannerT,parser_tag<DELETE_MEXPR> > macro_delete_expr_r;
            rule<ScannerT,parser_tag<MULTIPLY_MEXPR> > macro_multiply_expr_r;
            rule<ScannerT,parser_tag<ADDITION_MEXPR> > macro_addition_expr_r;
            rule<ScannerT,parser_tag<BITSHIFT_MEXPR> > macro_bitshift_expr_r;
            rule<ScannerT,parser_tag<RELATIONAL_MEXPR> > 
                                                       macro_relational_expr_r;
            rule<ScannerT,parser_tag<EQUALITY_MEXPR> > macro_equality_expr_r;
            rule<ScannerT,parser_tag<BITWISEAND_MEXPR> > 
                                                       macro_bitwiseAnd_expr_r;
            rule<ScannerT,parser_tag<BITWISEXOR_MEXPR> > 
                                                       macro_bitwiseXor_expr_r;
            rule<ScannerT,parser_tag<BITWISEOR_MEXPR> > macro_bitwiseOr_expr_r;
            rule<ScannerT,parser_tag<LOGICALAND_MEXPR> > 
                                                       macro_logicalAnd_expr_r;
            rule<ScannerT,parser_tag<LOGICALXOR_MEXPR> > 
                                                       macro_logicalXor_expr_r;
            rule<ScannerT,parser_tag<LOGICALOR_MEXPR> > macro_logicalOr_expr_r;
            rule<ScannerT,parser_tag<CONDITIONAL_MEXPR> > 
                                                      macro_conditional_expr_r;
            rule<ScannerT,parser_tag<ASSIGNMENT_MEXPR> > 
                                                      macro_assignment_expr_r;
            rule<ScannerT,parser_tag<THROW_MEXPR> > macro_throw_expr_r;
            rule<ScannerT,parser_tag<SEQUENCE_MEXPR> > macro_sequence_expr_r;
            rule<ScannerT,parser_tag<CONDITION_MEXPR> > macro_condition_expr_r;
            
            // macro statement parsers:
            rule<ScannerT,parser_tag<EXPR_MSTATE> > macro_expression_state_r;
            rule<ScannerT,parser_tag<DECL_MSTATE> > macro_declaration_state_r;
            rule<ScannerT,parser_tag<SWITCH_MSTATE> > macro_switch_state_r;
            rule<ScannerT,parser_tag<IF_MSTATE> > macro_if_state_r;
            rule<ScannerT,parser_tag<ELIF_MSTATE> > macro_elif_state_r;
            rule<ScannerT,parser_tag<ELSE_MSTATE> > macro_else_state_r;
            rule<ScannerT,parser_tag<SELECT_MSTATE> > macro_select_state_r;
            rule<ScannerT,parser_tag<LABEL_MSTATE> > macro_label_state_r;
            rule<ScannerT,parser_tag<JUMP_MSTATE> > macro_jump_state_r;
            rule<ScannerT,parser_tag<CMPND_MSTATE> > macro_compound_state_r;
            rule<ScannerT,parser_tag<EXCPTN_MSTATE> > macro_exception_state_r;
            rule<ScannerT,parser_tag<CATCH_MSTATE> > macro_catch_state_r;
            rule<ScannerT,parser_tag<FINALLY_MSTATE> > macro_finally_state_r;
            rule<ScannerT,parser_tag<LOOP_MSTATE> > macro_loop_state_r;
            rule<ScannerT,parser_tag<USING_MSTATE> > macro_using_state_r;
            rule<ScannerT > macro_statement_groop_r;
            
            // macro declaration parsers:
            rule<ScannerT,parser_tag<VAR_MDECL> > macro_var_decl_r;
            rule<ScannerT,parser_tag<DATA_MDECL> > macro_data_decl_r;
            rule<ScannerT,parser_tag<VARIABLE_MDECL> > macro_variable_decl_r;
            rule<ScannerT,parser_tag<IMPORT_MDECL> > macro_import_decl_r;
            rule<ScannerT,parser_tag<CMPND_MDECL> > macro_compound_decl_r;
            rule<ScannerT,parser_tag<NMSPC_FWD_MDECL> > 
                                                    macro_namespace_fwd_decl_r;
            rule<ScannerT,parser_tag<NMSPC_MDECL> > macro_namespace_decl_r;
            rule<ScannerT,parser_tag<FUNC_FWD_MDECL> > macro_func_fwd_decl_r;
            rule<ScannerT,parser_tag<FUNC_MDECL> > macro_func_decl_r;
            rule<ScannerT,parser_tag<OPER_FWD_MDECL> > macro_oper_fwd_decl_r;
            rule<ScannerT,parser_tag<OPER_MDECL> > macro_oper_decl_r;
            rule<ScannerT,parser_tag<CTOR_FWD_MDECL> > macro_ctor_fwd_decl_r;
            rule<ScannerT,parser_tag<CTOR_MDECL> > macro_ctor_decl_r;
            rule<ScannerT,parser_tag<DTOR_FWD_MDECL> > macro_dtor_fwd_decl_r;
            rule<ScannerT,parser_tag<DTOR_MDECL> > macro_dtor_decl_r;
            rule<ScannerT,parser_tag<OBJECT_FWD_MDECL> > 
                                                       macro_object_fwd_decl_r;
            rule<ScannerT,parser_tag<OBJECT_MDECL> > macro_object_decl_r;
            rule<ScannerT,parser_tag<FORWARD_MDECL> > macro_forward_decl_r;
            rule<ScannerT,parser_tag<FRIEND_MDECL> > macro_friend_decl_r;
            rule<ScannerT,parser_tag<MEMBER_MDECL> > macro_member_decl_r;
            rule<ScannerT>  macro_declaration_groop_r;
            
            // rule<ScannerT,parser_tag<N> > ;
            
            definition(my_grammar const& self)  
            { 
             // the litteral parsers:
             // character parsers:
             char_lit_r = discard_node_d[ ch_p('\'') ] >> 
                leaf_node_d[ +( c_escape_ch_p - ch_p('\'') ) ] 
                        >> discard_node_d[ ch_p('\'') ] ;
             wchar_lit_r = discard_node_d[ str_p("L\'") ] >> 
                leaf_node_d[ +( c_escape_ch_p - ch_p('\'') ) ] 
                        >> discard_node_d[ ch_p('\'') ] ;
             mbchar_lit_r = discard_node_d[ str_p("LL\'") ] >> 
                leaf_node_d[ +( c_escape_ch_p - ch_p('\'') ) ] 
                        >> discard_node_d[ ch_p('\'') ] ;
             vlchar_lit_r = discard_node_d[ str_p("LLL\'") ] >> 
                leaf_node_d[ +( c_escape_ch_p - ch_p('\'') ) ] 
                        >> discard_node_d[ ch_p('\'') ] ;
             char_groop_r = ( char_lit_r | wchar_lit_r )
                            | ( mbchar_lit_r | vlchar_lit_r );
             // string parsers:
             str_lit_r = discard_node_d[ ch_p('\"') ] >> 
                leaf_node_d[ *( c_escape_ch_p - ch_p('\"') ) ] 
                        >> discard_node_d[ ch_p('\"') ] ;
             wstr_lit_r = discard_node_d[ str_p("L\"") ] >> 
                leaf_node_d[ *( c_escape_ch_p - ch_p('\"') ) ] 
                        >> discard_node_d[ ch_p('\"') ] ;
             mbstr_lit_r = discard_node_d[ str_p("LL\"") ] >> 
                leaf_node_d[ *( c_escape_ch_p - ch_p('\"') ) ] 
                        >> discard_node_d[ ch_p('\"') ] ;
             vlstr_lit_r = discard_node_d[ str_p("LLL\"") ] >> 
                leaf_node_d[ *( c_escape_ch_p - ch_p('\"') ) ] 
                        >> discard_node_d[ ch_p('\"') ] ;
             str_groop_r = ( str_lit_r | wstr_lit_r ) 
                           | ( mbstr_lit_r | vlstr_lit_r );
             
             // numeral ( integer and floating-point ) parsers:
             uint8_lit_r = leaf_node_d[ esp_p(base8_prefix_m) >> 
                           discard_node_d[base8_prefix_m] >> uint8_p() ];
             uint10_lit_r = leaf_node_d[ uint10_p() ];
             uint16_lit_r = leaf_node_d[ esp_p(base16_prefix_m) >> 
                             discard_node_d[base16_prefix_m] >> uint16_p() ];
             uint_groop_r =  longest_d[ uint16_lit_r | uint8_lit_r | 
                           uint10_lit_r ];
             
             sint8_lit_r = leaf_node_d[ esp_p(base8_prefix_m) >> 
                           discard_node_d[base8_prefix_m] >> sint8_p()  ];
             sint10_lit_r = leaf_node_d[ sint10_p() ];
             sint16_lit_r = leaf_node_d[ esp_p(base16_prefix_m) >> 
                            discard_node_d[base16_prefix_m] >> sint16_p() ];
             sint_groop_r = longest_d[ sint16_lit_r | sint8_lit_r
                            | sint10_lit_r ] ];
                            
             float8_lit_r = leaf_node_d[ esp_p(base8_prefix_m) >> 
                            discard_node_d[base8_prefix_m] >> float8_p() ];
             float10_lit_r = leaf_node_d[ float10_p() ];
             float16_lit_r = leaf_node_d[ esp_p(base16_prefix_m) >> 
                             discard_node_d[base16_prefix_m] >> float16_p() ];
             float_groop_r =longest_d[ float16_lit_r | float8_lit_r 
                                     | float10_lit_r ];
                         
             number_groop_r = longest_d[ ( sint_groop_r | uint_groop_r )
                                | float_groop_r ];
             // the boolean parser:
             bool_lit_r = leaf_node_d[ str_p("true") | str_p("false") ];
             
             litteral_groop_r = ( char_groop_r | str_groop_r )
                                |( number_groop_r | bool_lit_r ); 
                                
             // identifier parsers:                   
             basic_ident_r = leaf_node_d[ ( ( alpha_p | ch_p('_') ) 
                             >> *( ( alpha_p | ch_p('_') | digit_p ) ) ] 
                             - keywords ;
             qualified_var_r = root_node_d[ str_p("var") ] >> basic_ident_r;
             qualified_const_r = root_node_d[ str_p("const") ] >> basic_ident_r;
             qualified_typename_r = root_node_d[ str_p("typename") ] 
                                    >> basic_ident_r ;
             qualified_bitfield_r = root_node_d[ str_p("bitfield") ] 
                                  >> *type_infix_mod_r >> basic_ident_r;
             qualified_enum_r = root_node_d[ str_p("enum") ] 
                                  >> *type_infix_mod_r >> basic_ident_r;
             qualified_object_r = root_node_d[ str_p("object") ] 
                                  >> *type_infix_mod_r >> basic_ident_r;
             qualified_union_r = root_node_d[ str_p("union") ] 
                                  >> *type_infix_mod_r >> basic_ident_r;
             qualified_func_type_r = root_node_d[ str_p("function") ] 
                                     >> *func_id_mod_r >> ch_p('(')
                                     >> function_ident_args_r >> ch_p(')') 
                                     >> ch_p(':') >> function_ident_arg_r;
             qualified_oper_type_r = root_node_d[ str_p("operator") ] 
                                     >> *oper_id_mod_r >> ch_p('(')
                                     >> function_ident_args_r >> ch_p(')') 
                                     >> ch_p(':') >> function_ident_arg_r;
             qualified_typeof_expr_r = root_node_d[ str_p("typeof") ] 
                                     >> discard_node_d[ ch_p('(') ] 
                                     >> sequence_expr_r 
                                     >> discard_node_d[ ch_p(')') ];
             builtin_types_r = leaf_node_d[builtin_types];
             qualified_typeid_r = *type_prefix_mod_r >> root_node_d[
                      ( builtin_types_r | qualified_bitfield_r | 
                       qualified_enum_r | qualified_func_type_r | 
                       qualified_object_r | qualified_oper_type_r |
                       qualified_typename_r | qualified_union_r |
                       qualified_typeof_expr_r ) ] 
                       // old last alternate: qualified_type_expr_r
                       >> *type_postfix_mod_r;
             unqualified_typeid_r = *type_prefix_mod_r >> 
                 root_node_d[ basic_ident_r ] >> *type_postfix_mod_r;              
             qualified_label_r = root_node_d[ str_p("label") ] >> basic_ident_r;              
             builtin_function_r = leaf_node_d[ builtin_funcs ];              
             qualified_function_r = root_node_d[ str_p("function") ] 
                                    >> *func_id_mod_r >> ( builtin_function_r |
                                    basic_ident_r ) ;
             operators_ident_r = leaf_node_d[ operators ];
             qualified_operator_r = root_node_d[ str_p("operator") ] 
                                    >> *oper_id_mod_r >> operators_ident_r;
             qualified_namespace_r = root_node_d[ str_p("namespace") ]
                                   >> !basic_ident_r ;
             qualified_import_r = root_node_d[ str_p("import") ]
                                   >> basic_ident_r ;
             qualified_identifier_r = qualified_var_r | qualified_const_r |
                                      qualified_typeid_r | qualified_function_r
                                      | qualified_operator_r;
             unqualified_identifier_r = basic_ident_r | unqualified_typeid_r ;
             nested_namespace_r = !( qualified_import_r | basic_ident_r ) 
                             >> root_node_d[str_p("::")] 
                             >> *( !( qualified_namespace_r | basic_ident_r ) 
                             >> root_node_d[str_p("::")] )
                             >> *( !( qualified_typeid_r | basic_ident_r ) 
                             >> root_node_d[str_p("::")] );
             using_ident_r = (!nested_namespace_r >> root_node_d[ ( 
                              qualified_var_r | qualified_const_r | 
                              qualified_typename_r ) ] ) 
                             | root_node_d[ ( namespace_fwd_decl_r | 
                                func_fwd_decl_r | oper_fwd_decl_r | 
                                ctor_fwd_decl_r | dtor_fwd_decl_r | 
                                bitfield_fwd_decl_r | enum_fwd_decl_r | 
                                object_fwd_decl_r | union_fwd_decl_r ) ] >> 
                             !macro_assignment_expr_r ; 
             
             // the modifier parsers:
             temp_id_mod_r = !str_p("template") >> root_node_d[ ch_p('<') ]
                             >> !temp_id_args_r >> ch_p('>');
             temp_operid_mod_r = root_node_d[ str_p("template") ] >>
                                inner_node_d[ ch_p('<') >> !temp_id_args_r >>
                                 ch_p('>') ];
             temp_id_args_r = !temp_id_arg_r >> *( root_node_d[ ch_p(',') ]
                              >> !temp_id_arg_r );
             temp_id_arg_r = throw_arg_r | conditional_expr_r;
             
             type_prefix_mod_r = longest_d[ ( var_ptr_mod_r | var_ref_mod_r )
                                 | ( const_ptr_mod_r | const_ref_mod_r ) ];
             var_ptr_mod_r = root_node_d[ str_p("var") ] >> ch_p('*');
             var_ref_mod_r = root_node_d[ str_p("var") ] >> ch_p('&');
             const_ptr_mod_r = root_node_d[ str_p("const") ] >> ch_p('*');
             const_ref_mod_r = root_node_d[ str_p("const") ] >> ch_p('&');
             type_postfix_mod_r = root_node_d[ ch_p('[') ] >> sequence_expr_r
                                  >> ch_p(']');
             typeid_infix_mod_r = temp_id_mod_r | lang_mod_r;
             
             callconv_mod_r = root_node_d[ str_p("callconv") ] >> 
                    inner_node_d[ ch_p('(') >> str_groop_r >> ch_p(')') ];
             lang_mod_r = root_node_d[ str_p("lang") ] >> 
                    inner_node_d[ ch_p('(') >> str_groop_r >> ch_p(')') ];
             throw_mod_r = root_node_d[ str_p("throw") ] >> 
                    inner_node_d[ ch_p('(') >> !throw_args_r >> ch_p(')') ];
             throw_args_r = throw_arg_r >> *( root_node_d[ ch_p(',') ] 
                            >> throw_arg_r );
             throw_arg_r = *type_prefix_mod_r >> root_node_d[ ident_expr_r ]
                           >> *type_postfix_mod_r;
             static_mod_r = leaf_node_d[ str_p("static") ];
             const_mod_r = leaf_node_d[ str_p("const") ];
             func_id_mod_r = temp_id_mod_r | callconv_mod_r | lang_mod_r 
                             | throw_mod_r | static_mod_r | const_mod_r ;
             oper_id_mod_r = temp_operid_mod_r | callconv_mod_r | lang_mod_r 
                             | throw_mod_r | static_mod_r | const_mod_r ;
             
             function_ident_args_r = function_ident_arg_r >> 
                                   *( root_node_d[ ch_p(',') ] >>
                                     function_ident_arg_r );
             function_ident_arg_r = root_node_d[ str_p("const") | str_p("var") ] 
                                  >> throw_arg_r;
             general_decl_mods_r = leaf_node_d[str_p("export")|str_p("intern")];
             temp_decl_mod_r = !str_p("template") >> root_node_d[ch_p('<')]
                             >> !temp_decl_args_r >> ch_p('>');
             temp_operdecl_mod_r = root_node_d[str_p("template")] >> ch_p('<')
                                 >> !temp_decl_args_r >> ch_p('>');
             temp_decl_args_r = !( temp_decl_arg_r >> !macro_assignment_expr_r )
                  >> *( root_node_d[ch_p(',')] >> !( temp_decl_arg_r >> 
                  !macro_assignment_expr_r ) );
             temp_decl_arg_r = ( throw_arg_r >> !( root_node_d[ch_p('=')] 
                  >> throw_arg_r ) ) | const_decl_r | macro_var_decl_r |
                  conditional_expr_r;
             volatile_mod_r = leaf_node_d[str_p("volatile")];
             mutable_mod_r = leaf_node_d[str_p("mutable")];
             data_decl_mods_r = general_decl_mods_r | static_mod_r | 
                              lang_mod_r | volatile_mod_r | mutable_mod_r;
             initializer_r = ( root_node_d[ch_p('=')] >> assignment_expr_r ) 
                             | func_call_expr_r;
             func_decl_mod_r = general_decl_mods_r | temp_decl_mod_r | 
                               throw_mod_r | const_mod_r | lang_mod_r | 
                               static_mod_r | abstract_mod_r | virtual_mod_r |
                               callconv_mod_r;
             oper_decl_mod_r = general_decl_mods_r | temp_operdecl_mod_r | 
                               throw_mod_r | const_mod_r | lang_mod_r | 
                               static_mod_r | abstract_mod_r | virtual_mod_r | 
                               callconv_mod_r;
             abstract_mod_r = leaf_node_d[ str_p("abstract") ];
             virtual_mod_r = leaf_node_d[ str_p("virtual") ];
             func_decl_args_r = func_decl_arg_r >> *( root_node_d[ch_p(',')] 
                              >> func_decl_arg_r );
             func_decl_arg_r = root_node_d[str_p("const")|str_p("var")] >>
                               *data_decl_mods_r >> throw_arg_r >> 
                               !basic_ident_r >> !initializer_r >> 
                               !macro_assignment_expr_r;
             ctor_decl_mod_r = general_decl_mods_r | temp_decl_mod_r | 
                               throw_mod_r | explicit_mod_r;
             explicit_mod_r = leaf_node_d[ str_p("explicit") ];
             dtor_decl_mod_r = general_decl_mods_r | temp_decl_mod_r | 
                               virtual_mod_r;
             ctor_init_list_r = root_node_d[ch_p(':')] >> ctor_init_mem_r >>
                              *(root_node_d[ch_p(',')] >> ctor_init_mem_r );
             ctor_init_mem_r = root_node_d[ ident_expr_r ] >> func_call_expr_r 
                               >> !macro_assignment_expr_r;
             inherit_list_r = root_node_d[ch_p(':')] >> inherit_mem_r >>
                              *(root_node_d[ch_p(',')] >> inherit_mem_r );
             inherit_mem_r = root_node_d[ access_mod_r ] >> !virtual_mod_r >> 
                             ident_expr_r >> !macro_assignment_expr_r ;
             access_mod_r = leaf_node_d[str_p("public")|str_p("protected")|
                            str_p("private") ];
             type_decl_infix_mod_r = general_decl_mods_r|temp_decl_mod_r|
                                     lang_mod_r;
             enumerator_r = basic_ident_r >> !( root_node_d[ch_p('=')] >> 
                            conditional_expr_r ) >> !macro_assignment_expr_r ;
             
             
             // the expression parsers:
             ident_expr_r = !nested_namespace_r >> 
               root_node_d[ longest_d[ qualified_identifier_r | 
                unqualified_identifier_r ] ];
             prime_expr_r = leaf_node_d[str_p("this")] | litteral_groop_r |
                           ident_expr_r | 
                           ( root_node_d[ ch_p('(') ] >> sequence_expr_r >> 
                             ch_p(')') ) ;
             postfix_expr_r = root_node_d[ prime_expr_r ] >> *( array_expr_r |
                        func_call_expr_r | expl_ctor_expr_r | mem_ref_expr_r | 
                        mem_ptr_expr_r | expl_dtor_expr_r | inc_dec_expr_r );
             array_expr_r = root_node_d[ ch_p('[') ] >> sequence_expr_r >> 
                            ch_p(']');
             func_call_expr_r = root_node_d[ ch_p('(') ] >> expression_list_r 
                                >> ch_p(')') ;
             expl_ctor_expr_r = root_node_d[ ( str_p("::") | ch_p('.') 
                                 | str_p("->") ) ] >> str_p("constructor");
             mem_ref_expr_r = root_node_d[ ch_p('.') ] 
                              >> ( new_expr_r | del_expr_r | ident_expr_r );
             mem_ptr_expr_r = root_node_d[ str_p("->") ] 
                              >> ( new_expr_r | del_expr_r | ident_expr_r );
             expl_dtor_expr_r = root_node_d[ ( ch_p('.') | str_p("->") ) ] 
                                >> str_p("destructor");
             inc_dec_expr_r = leaf_node_d[ str_p("++") | str_p("--") ];
             expression_list_r = !assignment_expr_r  >> 
                               *( root_node_d[ ch_p(',') ] >> 
                                  !assignment_expr_r  );
             new_expr_r = root_node_d[ str_p("new") ] >> !func_call_expr_r >>
                          throw_arg_r >> !( str_p("::") 
                          >> str_p("constructor") ) >> func_call_expr_r;
             del_expr_r = root_node_d[ str_p("delete") ] >> !func_call_expr_r
                          >> *del_array_expr_r >> unary_expr_r ;
             del_array_expr_r = ch_p('[') >> !sequence_expr_r >> ch_p(']');
             unary_expr_r = ( *root_node_d[ unary_opers_r ] >> ( postfix_expr_r 
                          | new_expr_r ) ) | del_expr_r ;
             unary_opers_r = leaf_node_d[ unary_ops_sym ];
             ptr_mem_expr_r = unary_expr_r >> *( root_node_d[ str_p("->*") 
                              | str_p(".*") ] >> unary_expr_r );
             multiply_expr_r = ptr_mem_expr_r >> *( root_node_d[ ch_p('*') | 
                               ch_p('/') | ch_p('%') ] >> ptr_mem_expr_r );
             addition_expr_r = multiply_expr_r >> *( root_node_d[ ch_p('+') | 
                               ch_p('-') ] >> multiply_expr_r );
             bitshift_expr_r = addition_expr_r >> *( root_node_d[ str_p("<<") | 
                             str_p(">>") | str_p(">>>") ] >> addition_expr_r );
             relational_expr_r = bitshift_expr_r >> *( root_node_d[ ch_p('<') | 
                               ch_p('>') | str_p("<=") | str_p(">=") ] 
                               >> bitshift_expr_r );
             equality_expr_r = relational_expr_r >> *( root_node_d[ str_p("==") 
                               | str_p("!=") ] >> relational_expr_r );
             bitwiseAnd_expr_r = equality_expr_r >> *( root_node_d[ ch_p('&') ]
                               >> equality_expr_r );
             bitwiseXor_expr_r = bitwiseAnd_expr_r >> *(root_node_d[ch_p('^')]
                               >> bitwiseAnd_expr_r );
             bitwiseOr_expr_r = bitwiseXor_expr_r >> *( root_node_d[ch_p('|')] 
                              >> bitwiseXor_expr_r );
             logicalAnd_expr_r = bitwiseOr_expr_r >> *(root_node_d[str_p("&&")] 
                               >> bitwiseOr_expr_r );
             logicalXor_expr_r = logicalAnd_expr_r >> *(root_node_d[str_p("^^")] 
                               >> logicalAnd_expr_r );
             logicalOr_expr_r = logicalXor_expr_r >> *(root_node_d[str_p("||")]
                               >> logicalXor_expr_r );
             conditional_expr_r = logicalOr_expr_r >> !(root_node_d[ch_p('?')]
                                >> assignment_expr_r >> root_node_d[ch_p(':')]
                                >> assignment_expr_r );
             assignment_expr_r = ( logicalOr_expr_r >> 
                                 * ( root_node_d[ assignment_opers_r ] 
                                 >> conditional_expr_r ) ) | conditional_expr_r 
                                 | throw_expr_r ;
             assignment_opers_r = leaf_node_d[ assignment_oper_sym ];
             sequence_expr_r = assignment_expr_r >> *( root_node_d[ ch_p(',') ] 
                             >> assignment_expr_r);
             throw_expr_r = root_node_d[ str_p("throw") ] >> sequence_expr_r ;
             condition_expr_r = (const_decl_r|var_decl_r|assignment_expr_r) >> 
                      *( root_node_d[ ch_p(',') ] >> 
                       (const_decl_r |var_decl_r |assignment_expr_r) );
             
             // statement parsers:
             expression_state_r = !sequence_expr_r >> root_node_d[ ch_p(';') ]; 
             declaration_state_r = root_node_d[ ( constant_decl_r | 
                 variable_decl_r | typedef_decl_r ) ] >> ch_p(';'); 
             select_state_r = switch_state_r | ( root_node_d[ if_state_r ] >> 
                            *elif_state_r >> !else_state_r );
             if_state_r = root_node_d[str_p("if")] >> ch_p('(') >>
                         condition_expr_r >> ch_p(')') >> statement_groop_r;
             elif_state_r = root_node_d[str_p("elif")] >> ch_p('(') >>
                         condition_expr_r >> ch_p(')') >> statement_groop_r;
             else_state_r = root_node_d[str_p("else")] >> statement_groop_r;
             switch_state_r = root_node_d[str_p("switch")] >> ch_p('(') >>
                         condition_expr_r >> ch_p(')') >> statement_groop_r;
             label_state_r = ((root_node_d[str_p("label")] >> basic_ident_r )
                             |( root_node_d[str_p("case")] >> sequence_expr_r)
                             | root_node_d[str_p("default")]) >> ch_p(':') >>
                             statement_groop_r;
             jump_state_r = (( root_node_d[str_p("goto")] >> 
                               ( qualified_label_r | basic_ident_r ) )
                      |( root_node_d[str_p("break")|str_p("continue")] >>
                               !( qualified_label_r | basic_ident_r  ) ) 
                      |( root_node_d[str_p("return")] >> !sequence_expr_r ))
                      >> ch_p(';');
             compound_state_r = root_node_d[ ch_p('{') ] >> *statement_groop_r 
                                >> ch_p('}');
             exception_state_r = root_node_d[str_p("try")] >> compound_state_r
                               >> *catch_state_r >> !finally_state_r ;
             catch_state_r = root_node_d[str_p("catch")] >>  ch_p('(') >>
                             ( const_decl_r | var_decl_r ) >> ch_p(')')
                             >> compound_state_r;
             finally_state_r = root_node_d[str_p("finally")]>>compound_state_r;
             loop_state_r = ( root_node_d[str_p("for")] >> ch_p('(') >>
                  !condition_expr_r >> ch_p(';') >> 
                  !condition_expr_r >> ch_p(';') >> 
                  !sequence_expr_r >> ch_p(')' ) >> statement_groop_r )|
                            ( root_node_d[str_p("while")] >> ch_p('(') >>
                  condition_expr_r >> ch_p(')') >> statement_groop_r )|
                            ( root_node_d[str_p("do")] >> statement_groop_r >>
                             root_node_d[str_p("while")] >> ch_p('(') >>
                  condition_expr_r >> ch_p(')') >> ch_p(';') );
             using_state_r = root_node_d[str_p("using")] >> using_ident_r >> 
                             *( root_node_d[ch_p(',')] >> using_ident_r );
             statement_groop_r = longest_d[ declaration_state_r | 
                      expression_state_r | select_state_r | label_state_r | 
                      jump_state_r | compound_state_r | exception_state_r |
                      loop_state_r | using_state_r | macro_statement_groop_r ];
             
             // declaration parsers:
             const_decl_r = root_node_d[str_p("const")] >> *data_decl_mods_r 
                 >> throw_arg_r >> data_decl_r;
             var_decl_r = root_node_d[str_p("var")] >> *data_decl_mods_r 
                 >> throw_arg_r >> data_decl_r;
             data_decl_r = *data_decl_mods_r>>root_node_d[unqualified_typeid_r] 
                         >> !initializer_r >> !macro_assignment_expr_r ;
             constant_decl_r = const_decl_r >> *( root_node_d[ch_p(',')] 
                             >> data_decl_r );
             variable_decl_r = var_decl_r >> *( root_node_d[ch_p(',')] 
                             >> data_decl_r );
             typedef_decl_r = root_node_d[str_p("typedef")]>>*data_decl_mods_r
                            >> throw_arg_r >> typedef_def_r >> 
                            *( root_node_d[ch_p(',')] >> typedef_def_r );
             typedef_def_r = *data_decl_mods_r >> root_node_d[ 
                unqualified_typeid_r ] >> !macro_assignment_expr_r;
             import_decl_r = root_node_d[str_p("import")] >> basic_ident_r >> 
                             !macro_assignment_expr_r >>
                             root_node_d[ch_p('=')] >> str_groop_r >> 
                             !macro_assignment_expr_r >> 
                             *( root_node_d[ch_p(',')] >> str_groop_r >> 
                             !macro_assignment_expr_r )
                             >> ch_p(';');
             compound_decl_r = root_node_d[ ch_p('{') ] >> *member_decl_r >> 
                               ch_p('}') >> ch_p(';');
             namespace_fwd_decl_r = !(!( qualified_import_r | basic_ident_r ) 
                                    >> root_node_d[str_p("::")] ) >> 
                                    root_node_d[str_p("namespace")] >> 
                                    !general_decl_mods_r >> !basic_ident_r 
                                    >> !macro_assignment_expr_r;
             namespace_decl_r = root_node_d[ namespace_fwd_decl_r ] >> 
                                compound_decl_r;
             func_fwd_decl_r = !nested_namespace_r >> 
                               root_node_d[str_p("function")] >> 
                               *func_decl_mod_r >> basic_ident_r >> 
                               !macro_assignment_expr_r >> ch_p('(')
                               >> !func_decl_args_r >> ch_p(')') >> ch_p(':') 
                               >> func_decl_arg_r;
             func_decl_r = root_node_d[ func_fwd_decl_r ] >> ( compound_state_r 
                           | exception_state_r );
             oper_fwd_decl_r = !nested_namespace_r >> 
                               root_node_d[str_p("operator")] >> 
                               *oper_decl_mod_r >> basic_ident_r >> 
                               !macro_assignment_expr_r >> ch_p('(')
                               >> !func_decl_args_r >> ch_p(')') >> ch_p(':') 
                               >> func_decl_arg_r;
             oper_decl_r = root_node_d[ oper_fwd_decl_r ] >> ( compound_state_r 
                           | exception_state_r );
             ctor_fwd_decl_r = !nested_namespace_r >> 
                               root_node_d[str_p("constructor")] >> 
                               *ctor_decl_mod_r >> !macro_assignment_expr_r >> 
                               ch_p('(') >> !func_decl_args_r >> ch_p(')');
             ctor_decl_r = root_node_d[ ctor_fwd_decl_r ] >> ( compound_state_r 
                           | exception_state_r );
             dtor_fwd_decl_r = !nested_namespace_r >> 
                               root_node_d[str_p("destructor")] >> 
                               *dtor_decl_mod_r >> !macro_assignment_expr_r >> 
                               ch_p('(') >> !func_decl_args_r >> ch_p(')');
             dtor_decl_r = root_node_d[ dtor_fwd_decl_r ] >> compound_state_r ;
             bitfield_fwd_decl_r = !nested_namespace_r >> 
                               root_node_d[str_p("bitfield")] >> 
                               *type_decl_infix_mod_r >> basic_ident_r >> 
                               !macro_assignment_expr_r;
             bitfield_decl_r = root_node_d[ bitfield_fwd_decl_r ] >> 
                             !inherit_list_r >> compound_decl_r;
             enum_fwd_decl_r = !nested_namespace_r >> 
                               root_node_d[str_p("enum")] >> // formerly "bitfield"
                               *type_decl_infix_mod_r >> basic_ident_r >> 
                               !macro_assignment_expr_r;
             enum_decl_r = root_node_d[ enum_fwd_decl_r ] >> !inherit_list_r >>
                         ch_p('{') >> enumerator_r >> *( root_node_d[ch_p(',')] 
                         >> enumerator_r ) >> ch_p('}') >> ch_p(';');
             object_fwd_decl_r = !nested_namespace_r >> 
                               root_node_d[str_p("object")] >> // formerly "bitfield"
                               *type_decl_infix_mod_r >> basic_ident_r >> 
                               !macro_assignment_expr_r;
             object_decl_r = root_node_d[ object_fwd_decl_r ] >> 
                             !inherit_list_r >> compound_decl_r;
             union_fwd_decl_r = !nested_namespace_r >> 
                               root_node_d[str_p("union")] >> // formerly "bitfield"
                               *type_decl_infix_mod_r >> basic_ident_r >> 
                               !macro_assignment_expr_r;
             union_decl_r = root_node_d[ union_fwd_decl_r ] >> !inherit_list_r 
                               >> compound_decl_r;
             forward_decl_r = root_node_d[ ( namespace_fwd_decl_r | 
                                func_fwd_decl_r | oper_fwd_decl_r | 
                                ctor_fwd_decl_r | dtor_fwd_decl_r | 
                                bitfield_fwd_decl_r | enum_fwd_decl_r | 
                                object_fwd_decl_r | union_fwd_decl_r ) ] 
                              >> ch_p(';');
             friend_decl_r = root_node_d[str_p("friend")] >> forward_decl_r ;
             member_decl_r = ( root_node_d[ access_mod_r ] >> 
                               declaration_groop_r ) | friend_decl_r | 
                               using_state_r | macro_statement_groop_r;
             declaration_groop_r = declaration_state_r | using_state_r |
                                   import_decl_r | namespace_decl_r |
                                   func_decl_r | oper_decl_r | ctor_decl_r |
                                   dtor_decl_r | bitfield_decl_r | enum_decl_r 
                                   | object_decl_r | union_decl_r
                                   | forward_decl_r | macro_statement_groop_r |
                                   macro_declaration_groop_r ;
             
             
             // the macro parsers:
             // the macro identifier parsers:
             macro_basic_ident_r = leaf_node_d[ ( alpha_p | ch_p('_') ) 
                             >> *( ( alpha_p | ch_p('_') | digit_p ) ];
             macro_var_ident_r = root_node_d[ch_p('$')] >> 
                  ( macro_builtin_var_wrapper_r | macro_basic_ident_r );
             macro_func_ident_r = root_node_d[ch_p('@')] >> *macro_func_mod_r 
                  >> ( macro_builtin_func_wrapper_r | macro_basic_ident_r );
             macro_oper_ident_r = root_node_d[str_p("@@")] >> *macro_oper_mod_r 
                  >> macro_builtin_oper_wrapper_r;
             macro_type_ident_r = root_node_d[str_p("$$")]>>!macro_tempid_mod_r
                  >> ( macro_builtin_type_wrapper_r | macro_basic_ident_r )
                  >> *macro_type_postfix_mod_r;
             macro_identifier_r = macro_var_ident_r | macro_type_ident_r |
                                  macro_func_ident_r | macro_oper_ident_r;
             macro_namespace_r = root_node_d[ch_p('#')>>str_p("namespace")] 
                               >> macro_basic_ident_r;
             macro_import_r = root_node_d[ch_p('#')>>str_p("import")] 
                               >> macro_basic_ident_r;
             macro_nested_name_r = !macro_import_r >> root_node_d[str_p("::")]
                   >> *( !macro_namespace_r >> root_node_d[str_p("::")] ) 
                   >> *( !macro_type_ident_r >> root_node_d[str_p("::")] );
             macro_using_ident_r = (!macro_nested_name_r >> root_node_d[ 
                   macro_var_ident_r ] ) | macro_namespace_fwd_decl_r 
                  | macro_func_fwd_decl_r | macro_oper_fwd_decl_r 
                  | macro_ctor_fwd_decl_r | macro_dtor_fwd_decl_r 
                  | macro_object_fwd_decl_r; 
             
             // macro parser wrappers ( for evaluation purposes ):     
             macro_builtin_func_wrapper_r = leaf_node_d[ builtin_macro_funcs ];
             macro_builtin_oper_wrapper_r = leaf_node_d[ ( operators - 
                   ( str_p("new") | str_p("delete") | str_p("->*") ) ) ];
             macro_builtin_type_wrapper_r = leaf_node_d[ builtin_macro_types ];
             macro_builtin_var_wrapper_r = leaf_node_d[ builtin_macro_vars ];
             
             // the macro modifier parsers:
             macro_tempid_mod_r = !leaf_node_d[(ch_p("#")>>str_p("template"))]
               >> root_node_d[ch_p('<')] >> !macro_tempid_args_r >> ch_p('>');
             macro_temp_operid_r = root_node_d[(ch_p("#")>>str_p("template"))]
                >> ch_p('<') >> !macro_tempid_args_r >> ch_p('>');
             macro_tempid_args_r = !macro_tempid_arg_r >> 
                   *(root_node_d[ch_p(',')] >> !macro_tempid_arg_r );
             macro_tempid_arg_r = macro_throw_arg_r | macro_conditional_expr_r;
             macro_type_postfix_mod_r = root_node_d[ ch_p('[') ] >> 
                                      macro_sequence_expr_r >> ch_p(']');
             macro_throw_mod_r = root_node_d[ ch_p('#') >> str_p("throw") ]
                        >> ch_p('(') >> !macro_throw_args_r >> ch_p(')');
             macro_throw_args_r = macro_throw_arg_r >> *(root_node_d[ch_p(',')] 
                                >> macro_throw_arg_r  );
             macro_throw_arg_r = root_node_d[ macro_ident_expr_r ] 
                               >> *macro_type_postfix_mod_r;
             macro_func_mod_r = macro_tempid_mod_r | macro_throw_mod_r | 
                              macro_static_mod_r | macro_const_mod_r;
             macro_oper_mod_r = macro_temp_operid_r | macro_throw_mod_r | 
                              macro_static_mod_r | macro_const_mod_r;
             macro_static_mod_r = leaf_node_d[(ch_p('#')>>str_p("static"))];
             macro_const_mod_r = leaf_node_d[(ch_p('#')>>str_p("const"))];
             macro_volatile_mod_r = leaf_node_d[ch_p('#')>>str_p("volatile")];
             macro_data_decl_mods_r = macro_gen_decl_mod_r | macro_static_mod_r 
                   | macro_volatile_mod_r | macro_const_mod_r;
             macro_initializer_r = ( root_node_d[ch_p('=')] >> 
                   macro_assignment_expr_r ) | macro_func_call_expr_r;
             macro_gen_decl_mod_r = leaf_node_d[ ch_p('#') >> ( str_p("export") 
                                   | str_p("intern")) ]; 
             macro_abstract_mod_r = leaf_node_d[ ch_p('#') >> 
                                    ( str_p("abstract") ];
             macro_virtual_mod_r = leaf_node_d[ ch_p('#') >> 
                                    ( str_p("virtual") ];
             macro_temp_decl_mod_r = !leaf_node_d[(ch_p("#") >> 
                   str_p("template"))] >> root_node_d[ch_p('<')] >> 
                   !macro_temp_decl_args_r >> ch_p('>');
             macro_temp_oper_decl_mod_r = root_node_d[ ch_p("#") >>
                   str_p("template") ] >> ch_p('<') >> !macro_temp_decl_args_r 
                   >> ch_p('>');
             macro_temp_decl_args_r = !macro_temp_decl_arg_r >> 
                   *( root_node_d[ch_p(',')] >> !macro_temp_decl_arg_r );
             macro_temp_decl_arg_r = ( macro_throw_arg_r >> 
                   !( root_node_d[(ch_p("=") >> macro_throw_arg_r ) ) 
                   | macro_conditional_expr_r | macro_var_decl_r;
             macro_func_decl_mod_r = macro_gen_decl_mod_r | 
                  macro_temp_decl_mod_r | macro_throw_mod_r | macro_const_mod_r
                  | macro_static_mod_r | macro_abstract_mod_r | 
                  macro_virtual_mod_r;
             macro_oper_decl_mod_r = macro_gen_decl_mod_r | 
                  macro_temp_oper_decl_mod_r | macro_throw_mod_r | 
                  macro_const_mod_r | macro_static_mod_r | macro_abstract_mod_r 
                  | macro_virtual_mod_r;
             macro_func_decl_args_r = macro_func_decl_arg_r >> *( 
                   root_node_d[ch_p(',')] >> macro_func_decl_arg_r);
             macro_func_decl_arg_r = root_node_d[ch_p("#")>>str_p("var")] >>
                  *macro_data_decl_mods_r >> macro_throw_arg_r >> 
                  !( macro_basic_ident_r - macro_builtin_var_wrapper_r ) >> 
                  !macro_initializer_r;
             macro_expicit_mod_r = leaf_node_d[(ch_p("#")>>str_p("explicit"))];
             macro_ctor_init_list_r = root_node_d[ch_p(':')] >> 
                  macro_ctor_init_mem_r >> *( root_node_d[ch_p(',')] >>
                  macro_ctor_init_mem_r );
             macro_ctor_init_mem_r = root_node_d[ macro_ident_expr_r ] >> 
                                     macro_func_call_expr_r;
             macro_ctor_decl_mod_r = macro_gen_decl_mod_r | 
                  macro_temp_decl_mod_r | macro_throw_mod_r | 
                  macro_expicit_mod_r;
             macro_dtor_decl_mod_r = macro_gen_decl_mod_r | 
                  macro_temp_decl_mod_r | macro_virtual_mod_r;
             macro_inherit_list_r = root_node_d[ch_p(':')] >> 
                  macro_inherit_mem_r >> *( root_node_d[ch_p(',')] >>
                  macro_inherit_mem_r );
             macro_inherit_mem_r = root_node_d[ macro_access_mod_r ] >> 
                   !macro_virtual_mod_r >> !macro_nested_name_r >> 
                   macro_type_ident_r;
             macro_access_mod_r = leaf_node_d[ch_p("#") >> ( str_p("public") |
                                  str_p("protected") | str_p("private"))];
             
             // macro expression parsers:
             macro_ident_expr_r = !macro_nested_name_r >> root_node_d[ 
                                macro_identifier_r ];
             macro_prime_expr_r = leaf_node_d[ch_p('#')>>str_p("this")] | 
                   litteral_groop_r | macro_ident_expr_r |
                   ( root_node_d[ ch_p('(') ] >> macro_sequence_expr_r >>
                     ch_p(')') );
             macro_postfix_expr_r = root_node_d[ macro_prime_expr_r ] >> 
                   *( macro_array_expr_r | macro_func_call_expr_r | 
                   macro_ctor_expr_r | macro_member_expr_r | macro_dtor_expr_r
                   | str_p("++") | str_p("--") );
             macro_array_expr_r = root_node_d[ ch_p('[') ] >> 
                                macro_sequence_expr_r >> ch_p(']');
             macro_expression_list_r = !macro_assignment_expr_r >> 
                   *( root_node_d[ch_p(',')] >> !macro_assignment_expr_r ) ;
             macro_func_call_expr_r = root_node_d[ ch_p('(') ] >> 
                   macro_expression_list_r >> ch_p(')');
             macro_ctor_expr_r = root_node_d[( str_p("::") | ch_p('.') | 
                               str_p("->") )]
                               >> leaf_node_d[ch_p('#')>>str_p("constructor")];
             macro_member_expr_r = root_node_d[( ch_p('.') | str_p("->") )]
                            >> macro_ident_expr_r ;
             macro_dtor_expr_r =  root_node_d[( ch_p('.') | str_p("->") )]
                            >> leaf_node_d[ch_p('#')>>str_p("destructor")];
             macro_unary_expr_r =  (*root_node_d[macro_unary_opers_r] >> 
                               ( macro_postfix_expr_r | macro_new_expr_r ) ) 
                               | macro_delete_expr_r ; 
             macro_unary_opers_r = leaf_node_d[( str_p("++")|str_p("--")|
                                ch_p('+')|ch_p('-')|ch_p('!')|ch_p('~')|
                                ch_p('*') )] ;
             macro_new_expr_r = root_node_d[ch_p('#')>>str_p("new")]>> 
                 macro_type_ident_r >> !macro_ctor_r >> macro_func_call_expr_r;
             macro_delete_expr_r = root_node_d[ch_p('#')>>str_p("delete")]
                                >> macro_unary_expr_r;
             macro_ctor_r = root_node_d[str_p("::")] >> 
                  leaf_node_d[ch_p('#')>>str_p("constructor")];
             macro_multiply_expr_r = macro_unary_expr_r >>*( root_node_d[ 
                  ch_p('*') | ch_p('/') | ch_p('%') ] >> macro_unary_expr_r );
             macro_addition_expr_r = macro_multiply_expr_r >>*(root_node_d[ 
                  ch_p('+') | ch_p('-') ] >> macro_multiply_expr_r );      
             macro_bitshift_expr_r = macro_addition_expr_r >>*(root_node_d[ 
                  str_p("<<") | str_p(">>") | str_p(">>>") ] 
                  >> macro_addition_expr_r );
             macro_relational_expr_r = macro_bitshift_expr_r >>*(root_node_d[ 
                  ch_p('<') | ch_p('>') | str_p("<=") | str_p(">=") ]
                                  >> macro_bitshift_expr_r );
             macro_equality_expr_r = macro_relational_expr_r >>*(root_node_d[ 
                  str_p("==") | str_p("!=") ] >> macro_relational_expr_r );
             macro_bitwiseAnd_expr_r = macro_equality_expr_r >>*(root_node_d[
                  ch_p('&') ] >> macro_equality_expr_r );
             macro_bitwiseXor_expr_r = macro_bitwiseAnd_expr_r >>*(root_node_d[
                  ch_p('^') ] >> macro_bitwiseAnd_expr_r );
             macro_bitwiseOr_expr_r = macro_bitwiseXor_expr_r >>*(root_node_d[
                  ch_p('|') ] >> macro_bitwiseXor_expr_r );
             macro_logicalAnd_expr_r = macro_bitwiseOr_expr_r >>*(root_node_d[
                  str_p("&&") ] >> macro_bitwiseOr_expr_r );
             macro_logicalXor_expr_r = macro_logicalAnd_expr_r >>*(root_node_d[
                  str_p("^^") ] >> macro_logicalAnd_expr_r );
             macro_logicalOr_expr_r = macro_logicalXor_expr_r >>*(root_node_d[
                  str_p("||") ] >> macro_logicalXor_expr_r );
             macro_conditional_expr_r = macro_logicalOr_expr_r >> 
                  !( root_node_d[ch_p('?')] >> macro_assignment_expr_r 
                  >> root_node_d[ch_p(':')] >> macro_assignment_expr_r );
             macro_assignment_expr_r =( macro_logicalOr_expr_r >>
              *(root_node_d[assignment_opers_r] >> macro_conditional_expr_r ) ) 
              | macro_conditional_expr_r | macro_throw_expr_r;
             macro_sequence_expr_r = macro_assignment_expr_r >> 
                  *( root_node_d[ch_p(',')] >> macro_assignment_expr_r ) ;
             macro_throw_expr_r = root_node_d[ch_p('#')>>str_p("throw")] 
                               >> macro_sequence_expr_r;
             macro_condition_expr_r = ( macro_var_decl_r | 
                   macro_assignment_expr_r ) >> *( root_node_d[ch_p(',')] 
                   >> ( macro_var_decl_r | macro_assignment_expr_r ) );
             
             // macro statement parsers:
             macro_expression_state_r = !macro_sequence_expr_r >> root_node_d[ 
                  ch_p(';') ] ;
             macro_declaration_state_r = root_node_d[ macro_variable_decl_r ] 
                                       >> ch_p(';') ;
             macro_switch_state_r = root_node_d[ch_p('#')>>str_p("switch")] 
                  >> ch_p('(') >> macro_condition_expr_r >> ch_p(')') 
                  >> macro_statement_groop_r ;
             macro_if_state_r = root_node_d[ch_p('#')>>str_p("if")] 
                  >> ch_p('(') >> macro_condition_expr_r >> ch_p(')') 
                  >> macro_statement_groop_r ;
             macro_elif_state_r = root_node_d[ch_p('#')>>str_p("elif")] >> 
                  >> ch_p('(') >> macro_condition_expr_r >> ch_p(')') 
                  >> macro_statement_groop_r ;
             macro_else_state_r = root_node_d[ch_p('#')>>str_p("else")] >> 
                  >> macro_statement_groop_r;
             macro_select_state_r = macro_switch_state_r |  ( root_node_d[ 
                  macro_if_state_r ] >> *macro_elif_state_r >> 
                  !macro_else_state_r );
             macro_label_state_r = (( root_node_d[ch_p('#')>>str_p("case")] 
                                     >> macro_sequence_expr_r )
                                   |root_node_d[ch_p('#')>>str_p("default")] )
                                   >> ch_p(':') >> macro_statement_groop_r;
             macro_jump_state_r = (( root_node_d[ch_p('#')>>str_p("break")] 
                                   |root_node_d[ch_p('#')>>str_p("continue")])
                                   |( root_node_d[ch_p('#')>>str_p("return")]
                                   >> !macro_sequence_expr_r) ) >> ch_p(';');
             macro_compound_state_r = root_node_d[ch_p('#')>>str_p("begin")] >>
                                      *macro_statement_groop_r >>
                                      leaf_node_d[ch_p('#')>>str_p("end")];
             macro_exception_state_r = root_node_d[ch_p('#')>>str_p("try")] 
                   >> macro_compound_state_r >> *macro_catch_state_r 
                   >> !macro_finally_state_r ;
             macro_catch_state_r = root_node_d[ch_p('#')>>str_p("catch")] >> 
                   ch_p('(') >> macro_var_decl_r >> ch_p(')') 
                   >> macro_compound_state_r ;
             macro_finally_state_r = root_node_d[ch_p('#')>>str_p("finally")] 
                                   >> macro_compound_state_r ;
             macro_loop_state_r = ( root_node_d[ch_p('#')>>str_p("for")] >> 
                 ch_p('(') >> !macro_condition_expr_r >> ch_p(';') >> 
                 !macro_condition_expr_r >> ch_p(';') >> !macro_sequence_expr_r 
                 >> ch_p(')') >> macro_statement_groop_r )
                                  |(root_node_d[ch_p('#')>>str_p("while")] >>
                    ch_p('(') >> macro_condition_expr_r >> ch_p(')') 
                              >> macro_statement_groop_r )
                                  |(root_node_d[ch_p('#')>>str_p("do")] >>
                    macro_statement_groop_r >> root_node_d[ch_p('#')>>
                    str_p("while")] >> ch_p('(') >> macro_condition_expr_r >> 
                    ch_p(')') >> ch_p(';') );
             macro_using_state_r = root_node_d[ch_p('#')>>str_p("using")] >>
                   macro_using_ident_r >> *( root_node_d[ch_p(',')] 
                   >> macro_using_ident_r ) ;
             macro_statement_groop_r = longest_d[ macro_expression_state_r |
                   macro_declaration_state_r | macro_select_state_r |
                   macro_label_state_r | macro_jump_state_r |
                   macro_compound_state_r | macro_exception_state_r |
                   macro_loop_state_r | macro_using_state_r ];
                      
             // macro declaration parsers:
             macro_var_decl_r = root_node_d[ch_p('#')>>str_p("var")] >> 
                              *macro_data_decl_mods_r >> macro_throw_arg_r
                              >> macro_data_decl_r;
             macro_data_decl_r = *macro_data_decl_mods_r >> 
                   root_node_d[ ( macro_identifier_r - builtin_macro_vars ) ] 
                   >> *macro_type_postfix_mod_r >> !macro_initializer_r;
             macro_variable_decl_r = macro_var_decl_r >> 
                   *( root_node_d[ch_p(',')] >> macro_data_decl_r );
             macro_import_decl_r = root_node_d[ch_p('#')>>str_p("import")] >>
                   macro_basic_ident_r >> root_node_d[ch_p('=')] >> str_groop_r
                   >> *( root_node_d[ch_p(',')] >> str_groop_r ) >> ch_p(';');
             macro_compound_decl_r = root_node_d[ch_p('#')>>str_p("begin")] >>
                   *macro_member_decl_r >> leaf_node_d[ch_p('#')>>str_p("end")]
                   >> ch_p(';');
             macro_namespace_fwd_decl_r = !(macro_import_r >> root_node_d[
                  str_p("::")] )>> *(root_node_d[ch_p('#')>>str_p("namespace")]
                  >> !macro_basic_ident_r >> root_node_d[ str_p("::")] ) 
                  >> root_node_d[ch_p('#')>>str_p("namespace")]
                  >> !macro_basic_ident_r;
             macro_namespace_decl_r = root_node_d[ macro_namespace_fwd_decl_r ] 
                                    >> macro_compound_decl_r;
             macro_func_fwd_decl_r = !macro_nested_name_r >> root_node_d[
                   ch_p('#')>>str_p("function")] >> *macro_func_decl_mod_r >>
                   ( macro_basic_ident_r - builtin_macro_funcs ) >> ch_p('(')
                   >> !macro_func_decl_args_r >> ch_p(')') >> ch_p(':') >>
                   macro_func_decl_arg_r;
             macro_func_decl_r = root_node_d[ macro_func_fwd_decl_r ] >> 
                   ( macro_compound_state_r | macro_exception_state_r );
             macro_oper_fwd_decl_r = !macro_nested_name_r >> root_node_d[
                   ch_p('#')>>str_p("operator")] >> *macro_oper_decl_mod_r >>
                   ( macro_builtin_oper_wrapper_r ) >> ch_p('(')
                   >> !macro_func_decl_args_r >> ch_p(')') >> ch_p(':') >>
                   macro_func_decl_arg_r;
             macro_oper_decl_r = root_node_d[ macro_oper_fwd_decl_r ] >> 
                   ( macro_compound_state_r | macro_exception_state_r );
             macro_ctor_fwd_decl_r = !macro_nested_name_r >> root_node_d[
                   ch_p('#')>>str_p("constructor")] >> *macro_ctor_decl_mod_r
                   >> ch_p('(') >> !macro_func_decl_args_r >>  ch_p(')');
             macro_ctor_decl_r = root_node_d[ macro_ctor_fwd_decl_r ] >> 
                               !macro_ctor_init_list_r >> 
                  ( macro_compound_state_r | macro_exception_state_r );
             macro_dtor_fwd_decl_r = !macro_nested_name_r >> root_node_d[
                   ch_p('#')>>str_p("destructor")] >> *macro_dtor_decl_mod_r
                   >> ch_p('(') >> ch_p(')');
             macro_dtor_decl_r = root_node_d[ macro_dtor_fwd_decl_r ] >>
                                 macro_compound_state_r;
             macro_object_fwd_decl_r = !macro_nested_name_r >> root_node_d[
                   ch_p('#')>>str_p("object")] >> *( macro_gen_decl_mod_r | 
                   macro_temp_decl_mod_r ) >> ( macro_basic_ident_r - 
                   builtin_macro_types );
             macro_object_decl_r = root_node_d[ macro_object_fwd_decl_r ] >> 
                   !macro_inherit_list_r >> macro_compound_decl_r;
             macro_forward_decl_r = root_node_d[ longest_d[ 
                  macro_namespace_fwd_decl_r | macro_func_fwd_decl_r 
                  | macro_oper_fwd_decl_r | macro_ctor_fwd_decl_r 
                  | macro_dtor_fwd_decl_r | macro_object_fwd_decl_r ] ] 
                  >> ch_p(';');
             macro_friend_decl_r = root_node_d[ ch_p('#') >> str_p("friend") ] 
                   >> macro_forward_decl_r;
             macro_member_decl_r = macro_friend_decl_r | macro_using_state_r |
               (root_node_d[macro_access_mod_r] >> macro_declaration_groop_r );
             macro_declaration_groop_r = macro_declaration_state_r | 
                   macro_using_state_r | macro_import_decl_r | 
                   macro_namespace_decl_r | macro_func_decl_r | 
                   macro_oper_decl_r | macro_ctor_decl_r | macro_dtor_decl_r |
                   macro_object_decl_r | macro_forward_decl_r;
             
             // the start rule: 
             start_m = +declaration_groop_r;
            }// end of ...
            rule<ScannerT> const& start() const 
            { return start_m; }// end of ...
        };
    };
    
    struct whitespace_grammar : public grammar<whitespace_grammar>
    {
        template <typename ScannerT>
        struct definition
        {
            rule<ScannerT,parser_tag<WHITESPACE_ID> >  whitespace_r;
            rule<ScannerT,parser_tag<COMMENT_ID> >     comment_r;
            rule<ScannerT>  r;
            definition(my_grammar const& self)  
            { 
             r = comment_r | whitespace_r;
             whitespace_r = space_p | eol_p;
             comment_r = comment_p("//") | comment_p("/*", "*/");
            }// end of ...
            rule<ScannerT> const& start() const 
            { return r; }// end of ...
        };
    };
            
  }      
 }
}

#endif
