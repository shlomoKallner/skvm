#include <cstdlib>
#include <iostream>
#include <string>
#include <boost/spirit/include/classic_spirit.hpp>

using namespace std;
using namespace BOOST_SPIRIT_CLASSIC_NS;

struct keyword_symbols : public symbols<nil_t,char>
    {
     keyword_symbols()
     {
      add("abstract")("addressof")("bitfield")("boolean")("break")("byte")
         ("case")("cast")("callconv")("catch")("char")("const")
         ( "constructor")("continue")("default")("delete")
         ("destructor")("do")("double")("doubleword")
         ("dynamic_cast")("elif")("else")("enum")("explicit")
         ("export")( "false")("finaly")("float")("for")("friend")
         ("function")("goto")("halfword")("if")("int")("intern")
         ("import")("lang")("long")("label")("mbchar")("namespace")
         ("new")("object")("private")("protected")("public")("return")
         ("reinterpret_cast")("short")("signed")("sizeof")("static_cast")
         ("static")("switch")("template")("this")("throw")("true")("try")
         ("typedef")("typeid")("typeof")("typename")("typeinfo")("union")
         ("unsigned")("typename")("using")("var")("virtual")("vararg")("void")
         ("volatile")("vlchar")("wchar")("while")("word");
     }//end of ...
     
    };
    
typedef string::iterator iter_t;

int main(int argc, char *argv[])
{
    
    parse_info< iter_t > hit;
    cout << "parsing: abstract typeof vararg." << endl;
    keyword_symbols keywords;
    string str = "abstract typeof vararg";
    iter_t first = str.begin(), last = str.end();
    rule<phrase_scanner_t> r = +keywords;
    hit = parse< iter_t >( first, last, r , space_p );
    //std::string str2(first,str.end());
    if( hit.hit )
     cout << "parse successful." << endl;
    else
     cout << "parse failure." << endl;
    system("PAUSE");
    return EXIT_SUCCESS;
}
