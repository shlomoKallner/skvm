#ifndef SK_COMPILERS_FRONTENDS_PARSERS_GSVM_PARSER_GSVM_IDENTIFIER_PARSER_HPP
#define SK_COMPILERS_FRONTENDS_PARSERS_GSVM_PARSER_GSVM_IDENTIFIER_PARSER_HPP

#include "gsvm_parser_def_fwd.hpp"

namespace SK
{
 namespace compilers
 {
  namespace frontends
  {
   namespace parsers
   {
    namespace gsvm
    {
     namespace spirit  = boost::spirit;
     namespace qi      = boost::spirit::qi;
     namespace phoenix = boost::phoenix;

     namespace types     = SK::utility::types;
     namespace internals = SK::compilers::parsers::internals;
     namespace ast       = SK::compilers::frontends::ast::internals;
     namespace ast_      = SK::compilers::frontends::ast::gsvm::consts::IDs;
     //formerly:  SK::compilers::frontends::ast::gsvm;

     template< typename IteratorT >
     struct gsvm_ident_parser< IteratorT, char >
     : qi:grammar< IteratorT, ast::ast_node::node_ptr(),
                   internals::c_lang_skipper_type<IteratorT, char>,
                   qi::locals<ast::ast_node::container_t>
                 >
     {
      typedef  ast::ast_node::node_ptr node_ptr;
      typedef  ast::ast_node::container_t container_t;
      typedef  char char_type;
      typedef  wchar_t wchar_type;
      typedef  std::basic_string< char_type > string_type;
      typedef  std::basic_string< wchar_type > wstring_type;
      typedef  internals::c_lang_skipper_type<IteratorT, char_type> skipper_type;
      typedef  internals::c_lang_escape_parser<IteratorT, char_type> char_escape_parser_type;
      typedef  internals::c_lang_escape_parser<IteratorT, wchar_type> wchar_escape_parser_type;
      typedef  keyword_parser_type<IteratorT, char_type> keyword_parser_t;
      typedef  keyword_symbols< char_type > keyword_sym_type;
      typedef  builtin_type_sym_table< char_type > builtin_types_sym_type;
      typedef  operator_sym_table< char_type > operator_sym_type;
      typedef  builtin_function_sym_table< char_type > builtin_function_sym_type;

       // "general utility" ( rule types ) typedefs
      typedef qi::rule< IteratorT, node_ptr( void ), skipper_type > pass_rule_type;

     typedef qi::rule< IteratorT, node_ptr( void ), skipper_type,
                                         qi::locals<container_t>  > grammar_sig_rule_type;
     // "grammar_sig_rule_type" - aka: "nary_rule_type"

     typedef qi::rule< IteratorT, node_ptr( void ), skipper_type,
                                            qi::locals<node_ptr>  > unary_rule_type;

     typedef qi::rule< IteratorT, node_ptr( void ), skipper_type,
                            qi::locals<node_ptr, types::uint32 >  > unary_plus_rule_type;

     typedef qi::rule< IteratorT, node_ptr( void ), skipper_
                                                         type,
                                  qi::locals<node_ptr, node_ptr>  > binary_rule_type;

     typedef qi::rule< IteratorT, node_ptr( types::uint8 ), skipper_type,
                                         qi::locals<container_t>  > nary_rule_with_options_type;
     // the start rule
     grammar_sig_rule_type start;



      // the parser rules:

      // identifiers
     pass_rule_type qualified_ident_pm, unqualified_ident_pm;
     grammar_sig_rule_type data_type_pm;
     grammar_sig_rule_type nested_name_ident_pm, using_ident_pm;
     pass_rule_type namespace_ident_pm, import_ident_pm;
     unary_rule_type function_ident_pm, operator_ident_pm;
     pass_rule_type label_ident_pm;
     grammar_sig_rule_type qual_typeid_ident_pm, unqual_typeid_ident_pm;
     pass_rule_type typeof_ident_pm;
     grammar_sig_rule_type function_type_arg_ident_pm, operator_type_ident_pm;
     grammar_sig_rule_type function_type_ident_pm, function_type_args_ident_pm;
     unary_rule_type union_ident_pm;
     unary_rule_type bitfield_ident_pm, enum_ident_pm, object_ident_pm;
     pass_rule_type var_ident_pm, const_ident_pm, typename_ident_pm;
     qi::rule< IteratorT, node_ptr( void ), skipper_type, qi::locals< string_type >
             > basic_ident_pm;



      // the constructor
      gsvm_ident_parser() : base_type(start,
                    "G.S. Script Language � char parser, identifier parser component" )
      {

          // identifiers // identifiers:: // identifiers::
        basic_ident_pm = qi::eps[ qi::_a = phoenix::construct< string_type >() ]
          >> ( !keyword_pm >> qi::lexeme[ ( ( qi::hold[ qi::standard::alpha
                           [ phoenix::push_back( qi::_a, qi::_1 ) ] ]
               | qi::standard::char_('_')[ phoenix::push_back( qi::_a, '_' ) ] )
               >> *( qi::hold[ qi::standard::alnum[
                               phoenix::push_back( qi::_a, qi::_1 ) ] ]
                  | qi::standard::char_('_')[ phoenix::push_back( qi::_a, '_' ) ] ) ) ] )
          >> qi::eps[ qi::_val = ast::value_( qi::_a, ast_::identifiers::basic_ ) ]; //

        /*
         version 2( current version ):
         ! keyword_pm >> qi::lexeme[ ( ( qi::hold[ qi::standard::alpha
                           [ phoenix::push_back( qi::_a, qi::_1 ) ] ]
                  | qi::standard::char_('_')[ phoenix::push_back( qi::_a, '_' ) ] )
                 >> *( qi::hold[ qi::standard::alnum[
                                 phoenix::push_back( qi::_a, qi::_1 ) ] ]
                    | qi::standard::char_('_')[ phoenix::push_back( qi::_a, '_' ) ] ) ) ]
         version 1:
                 ( qi::lexeme[
                ( qi::hold[ qi::standard::alpha
                           [ phoenix::push_back( qi::_a, qi::_1 ) ] ]
                  | qi::standard::char_('_')[ phoenix::push_back( qi::_a, '_' ) ] )
                 >> *( qi::hold[ qi::standard::alnum[
                                 phoenix::push_back( qi::_a, qi::_1 ) ] ]
                    | qi::standard::char_('_')[ phoenix::push_back( qi::_a, '_' ) ] )
                 ] - keyword_pm )

        */

        var_ident_pm = keyword_parser_pm("var") >> basic_ident_pm
                     [ qi::_val = ast::unary_( qi::_1, ast_::identifiers::var_ ) ];

        const_ident_pm = keyword_parser_pm("const") >> basic_ident_pm
                     [ qi::_val = ast::unary_( qi::_1, ast_::identifiers::const_ ) ]; //

        typename_ident_pm = keyword_parser_pm("typename") >> basic_ident_pm
                     [ qi::_val = ast::unary_( qi::_1, ast_::identifiers::typename_ ) ]; //

        bitfield_ident_pm = keyword_parser_pm("bitfield")
            >> type_infix_pm[ qi::_a = qi::_1 ]
            >> basic_ident_pm[ qi::_val = ast::binary_( qi::_1, qi::_a, ast_::identifiers::bitfield_ ) ]; //

        enum_ident_pm = keyword_parser_pm("enum")
            >> type_infix_pm[ qi::_a = qi::_1 ]
            >> basic_ident_pm[ qi::_val = ast::binary_( qi::_1, qi::_a, ast_::identifiers::enum_ ) ]; //

        object_ident_pm = keyword_parser_pm("object")
            >> type_infix_pm[ qi::_a = qi::_1 ]
            >> basic_ident_pm[ qi::_val = ast::binary_( qi::_1, qi::_a, ast_::identifiers::object_ ) ]; //

        union_ident_pm = keyword_parser_pm("union")
            >> type_infix_pm[ qi::_a = qi::_1 ]
            >> basic_ident_pm[ qi::_val = ast::binary_( qi::_1, qi::_a, ast_::identifiers::union_ ) ]; //

        function_type_ident_pm = keyword_parser_pm("function")
                    [ qi::_a = phoenix::construct< container_t >()]
            >> func_ident_mods_pm(template_type::null_)[ phoenix::push_back( qi::_a, qi::_1 ) ]
            >> qi::lit('(')
            >> function_type_args_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
            >> qi::lit(')') >> qi::lit(':')
            >> function_type_arg_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
            >> qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::identifiers::function_type_ ) ]; //

        function_type_args_ident_pm = qi::eps
            [ qi::_a = phoenix::construct< container_t >()]
            >> ( function_type_arg_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
               % qi::lit(',') )
            >> qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::identifiers::function_type_args_ ) ]; //

        function_type_arg_ident_pm =
           qi::hold[ keyword_parser_pm("var") > data_type_pm
           [ qi::_val = ast::unary_( qi::_1, ast_::identifiers::function_type_arg_var ) ] ]
           | keyword_parser_pm("const") > data_type_pm
           [ qi::_val = ast::unary_( qi::_1, ast_::identifiers::function_type_arg_const ) ]  ;

        operator_type_ident_pm = keyword_parser_pm("operator")
                    [ qi::_a = phoenix::construct< container_t >()]
            >> func_ident_mods_pm(template_type::oper)[ phoenix::push_back( qi::_a, qi::_1 ) ]
            >> qi::lit('(')
            >> function_type_args_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
            >> qi::lit(')') >> qi::lit(':')
            >> function_type_arg_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
            >> qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::identifiers::operator_type_ ) ]; //

        typeof_ident_pm = keyword_parser_pm("typeof") >> qi::lit('(')
               >> sequence_expr_pm[ qi::_val =  ast::unary_( qi::_1, ast_::identifiers::typeof_ ) ] //
               >> qi::lit(')');

        qual_typeid_ident_pm = qi::eps
         [ qi::_a = phoenix::construct< container_t >()]
         >> type_prefix_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         >> ( qi::hold[ builtin_types_pm
          [ push_back( _a, phoenix::construct<node_ptr>(
            phoenix::new_<ast_::identifiers::builtin_types_>( qi::_1 ) ) ) ] ]
          | qi::hold[ bitfield_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
          | qi::hold[ enum_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
          | qi::hold[ function_type_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
          | qi::hold[ object_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
          | qi::hold[ operator_type_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
          | qi::hold[ typename_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
          | qi::hold[ union_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
          | typeof_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
           )
         >> type_postfix_pm[ phoenix::push_back(qi::_a, qi::_1 ) ]
         >> qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::identifiers::qual_typeid_ ) ] ; //

        unqual_typeid_ident_pm = qi::eps
             [ qi::_a = phoenix::construct< container_t >()]
             >> type_prefix_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
             >> basic_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
             >> type_postfix_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
             >> qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::identifiers::unqual_typeid_ ) ] ; //

        label_ident_pm = keyword_parser_pm("label") >> basic_ident_pm
              [ qi::_val = ast::unary_( qi::_1, ast_::identifiers::label_ ) ]; //

        function_ident_pm = keyword_parser_pm("function")
            >> func_ident_mods_pm(template_type::null_)[ qi::_a = qi::_1 ]
            >> ( qi::hold[ builtin_function_pm //
            [ qi::_val = ast::binary_( qi::_a, ast::value_( qi::_1,
                                               ast_::identifiers::builtin_funcs_ ),
                                       ast_::identifiers::function_ ) ] ]
            | basic_ident_pm[ qi::_val = ast::binary_( qi::_a, qi::_1, ast_::identifiers::function_ ) ] ); //

        operator_ident_pm = keyword_parser_pm("operator")
            >> func_ident_mods_pm(template_type::oper)[ qi::_a = qi::_1 ]
            >> operators_pm[ qi::_val =  ast::binary_( qi::_a, ast::value_( qi::_1,
                                               ast_::identifiers::operators ),
                                       ast_::identifiers::operator_ ) ];

        namespace_ident_pm = keyword_parser_pm("namespace") >> basic_ident_pm
             [ qi::_val = ast::unary_( qi::_1 , ast_::identifiers::namespace_ ) ];

        import_ident_pm = keyword_parser_pm("import") >> basic_ident_pm
             [ qi::_val = ast::unary_( qi::_1 , ast_::identifiers::import_ ) ]; //

        qualified_ident_pm %= qi::hold[ var_ident_pm ]
                            | qi::hold[ const_ident_pm ]
                            | qi::hold[ label_ident_pm ]
                            | qi::hold[ qual_typeid_ident_pm ]
                            | qi::hold[ function_ident_pm ]
                            | operator_ident_pm ;

        unqualified_ident_pm %= qi::hold[ basic_ident_pm ]
                              | unqual_typeid_ident_pm;

        nested_name_ident_pm = qi::eps[ qi::_a = phoenix::construct< container_t >()]
               >> ( qi::hold[ import_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
                      >> qi::lit("::") ]
                   | qi::hold[ basic_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
                      >> qi::lit("::") ]
                   | qi::lit("::")
              [ phoenix::push_back( qi::_a, ast::tag_( ast_::ws::sepperator ) ) ] )
               >> *( qi::hold[ namespace_ident_pm[
                      phoenix::push_back( qi::_a, qi::_1 ) ] >> qi::lit("::") ]
                   | qi::hold[ basic_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
                     >> qi::lit("::") ]
                   | qi::lit("::")
              [ phoenix::push_back( qi::_a, ast::tag_( ast_::ws::sepperator ) ) ] )
               >> *( qi::hold[ qual_typeid_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
                   | qi::hold[ unqual_typeid_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
                   | qi::lit("::")
              [ phoenix::push_back( qi::_a, ast::tag_( ast_::ws::sepperator ) ) ] )
               >> qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::identifiers::nested_name_ ) ] ; //

        using_ident_pm = qi::eps[ qi::_a = phoenix::construct< container_t >()]
                >>( qi::hold[ nested_name_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
                   >> ( qi::hold[ var_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
                      | qi::hold[ const_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
                      | qi::hold[ qual_typeid_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
                      | unqual_typeid_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ) ]
                  | forward_decl_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
                >> macro_assignment_expr_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
                >> qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::identifiers::using_ ) ] ;//

        data_type_pm = qi::eps[ qi::_a = phoenix::construct< container_t >()]
             >> type_prefix_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
             >> -nested_name_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
             >> ( qual_typeid_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
              | unqual_typeid_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
             >> type_postfix_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
             >> qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::identifiers::data_type_ ) ] ; //

        start = qi::eps[ qi::_a = phoenix::construct< container_t >() ]
         >> -( nested_name_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
         >> ( qi::hold[ qualified_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
            | unqualified_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
         >> qi::eps[ qi::_val = ast::nary_( qi::_a , ast_::expressions::ident ) ];
         /// formerly "identifier_pm"



      }//end of ctor....

     };// end of ...

    } // end of namespace gsvm;
   } // end of namespace parsers;
  }// end of namespace frontends;
 } // end of namespace compilers;
} // end of namespace SK;

#endif

