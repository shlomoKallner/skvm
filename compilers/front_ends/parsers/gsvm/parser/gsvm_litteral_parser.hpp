#ifndef SK_COMPILERS_FRONTENDS_PARSERS_GSVM_PARSER_GSVM_LITTERAL_PARSER_HPP
#define SK_COMPILERS_FRONTENDS_PARSERS_GSVM_PARSER_GSVM_LITTERAL_PARSER_HPP

#include "gsvm_parser_def_fwd.hpp"

namespace SK
{
 namespace compilers
 {
  namespace frontends
  {
   namespace parsers
   {
    namespace gsvm
    {
     namespace spirit = boost::spirit;
     namespace qi = boost::spirit::qi;
     namespace phoenix = boost::phoenix;

     namespace types = SK::utility::types;
     namespace internals = SK::compilers::parsers::internals;
     namespace ast = SK::compilers::frontends::ast::internals;
     namespace ast_ = SK::compilers::frontends::ast::gsvm::consts::IDs;
     //formerly:  SK::compilers::frontends::ast::gsvm;


     template< typename IteratorT >
     struct gsvm_litteral_parser< IteratorT, char >
     : qi:grammar< IteratorT, ast::ast_node::node_ptr(),
                 internals::c_lang_skipper_type<IteratorT, char>
                 >
     {
       typedef  ast::ast_node::node_ptr node_ptr;
       typedef  ast::ast_node::container_t container_t;
       typedef  char char_type;
       typedef  wchar_t wchar_type;
       typedef  std::basic_string< char_type > string_type;
       typedef  std::basic_string< wchar_type > wstring_type;
       typedef  internals::c_lang_skipper_type<IteratorT, char_type> skipper_type;
       typedef  internals::c_lang_escape_parser<IteratorT, char_type> char_escape_parser_type;
       typedef  internals::c_lang_escape_parser<IteratorT, wchar_t> wchar_escape_parser_type;
       typedef  keyword_parser_type<IteratorT, char_type> keyword_parser_t;
       //typedef qi::rule< IteratorT, node_ptr( void ), skipper_type > pass_rule_type;
       typedef qi::rule< IteratorT, node_ptr( void ), skipper_type > grammar_sig_rule_type;

       /// this is now the only "charset usage" modification point now
       /// (it anyways only had to do with our parser`s inabilities - not the languages`)
       typedef  mpl::integral_c< types::uint8, char_usage::use_wchar > char_used;
       // the escape parsers as member variables.
       char_escape_parser_type esc_pm;
       wchar_escape_parser_type wesc_pm;
       // the numeric parsers as member variables.
       internals::ubin_p  ubin_pm;
       internals::uoct_p  uoct_pm;
       internals::udec_p  udec_pm;
       internals::uhex_p  uhex_pm;
       internals::sbin_p  sbin_pm;
       internals::soct_p  soct_pm;
       internals::sdec_p  sdec_pm;
       internals::shex_p  shex_pm;
       // internals::ufloat_p ufloat_pm;
       internals::sfloat_p  sfloat_pm;
       keyword_parser_t                keyword_parser_pm;

       // the start rule
       grammar_sig_rule_type start;

       grammar_sig_rule_type number_lit_pm, bool_lit_pm;
       grammar_sig_rule_type uint_lit_pm, float_lit_pm;
       qi::rule< IteratorT, node_ptr( void ),
               skipper_type, qi::locals< types::boolean > > sint_lit_pm;
       qi::rule< IteratorT, node_ptr( void ), /// changed from "types::uint8" to "void"
                skipper_type, qi::locals< string_type, wstring_type >
               > char_lit_pm, str_lit_pm;

     // the constructor
     gsvm_litteral_parser() : base_type(start,
                    "G.S. Script Language � char parser, literal parser component" )
     {
        // litterals
        bool_lit_pm = qi::hold[ keyword_parser_pm("true")
             [ qi::_val = ast::value_( true, ast_::litterals::bool_ ) ] ]
             | keyword_parser_pm("false")
             [ qi::_val = ast::value_( false, ast_::litterals::bool_ ) ];

        uint_lit_pm = qi::lexeme[
              qi::hold[ qi::lit("0b") >> ubin_pm
             [ qi::_val = ast::value_( qi::_1, ast_::litterals::uint_ )] ]
             |  qi::hold[ qi::lit("0o") >> uoct_pm
               [ qi::_val = ast::value_( qi::_1, ast_::litterals::uint_ ) ] ]
             |  qi::hold[ qi::lit("0x") >> uhex_pm
               [ qi::_val = ast::value_( qi::_1, ast_::litterals::uint_ ) ] ]
             | udec_pm
               [ qi::_val = ast::value_( qi::_1, ast_::litterals::uint_ ) ]
                                ];
        sint_lit_pm = qi::lexeme[
             ( qi::hold[ qi::lit("-")[ qi::_a = true ] ]
             | qi::lit("+")[ qi::_a = false ] ) >>
             ( qi::hold[ ( qi::lit("0b") >> sbin_pm
               [ phoenix::if_( qi::_a == true)
               [ qi::_val = ast::value_( - qi::_1, ast_::litterals::sint_ ) ]
               .else_[ qi::_val = ast::value_( qi::_1, ast_::litterals::sint_ ) ]
                                                        ] ) ]
             | qi::hold[ ( qi::lit("0o") >> soct_pm
               [ phoenix::if_( qi::_a == true)
               [ qi::_val = ast::value_( - qi::_1, ast_::litterals::sint_ ) ]
               .else_[ qi::_val = ast::value_( qi::_1, ast_::litterals::sint_ ) ]
                                                        ] ) ]
             | qi::hold[ ( qi::lit("0x") >> shex_pm
               [ phoenix::if_( qi::_a == true)
               [ qi::_val = ast::value_( - qi::_1, ast_::litterals::sint_ ) ]
               .else_[ qi::_val = ast::value_( qi::_1, ast_::litterals::sint_ ) ]
                                                        ] ) ]
             | ( sdec_pm
               [ phoenix::if_( qi::_a == true)
               [ qi::_val = ast::value_( - qi::_1, ast_::litterals::sint_ ) ]
               .else_[ qi::_val = ast::value_( qi::_1, ast_::litterals::sint_ ) ]
                                                        ] ) ) ];
        float_lit_pm =
            /*
              qi::hold[ ( ufloat_pm
              [ qi::_val = ast::value_( qi::_1, ast_::litterals::float_ ) ] ) ]
             |
            */
            sfloat_pm[ qi::_val = ast::value_( qi::_1, ast_::litterals::float_ ) ];

        number_lit_pm %= qi::hold[ float_lit_pm ] | qi::hold[ uint_lit_pm ]
                       | sint_lit_pm;

        char_lit_pm = qi::lexeme[
             qi::hold[
              (  qi::eps( char_used::value >= char_usage::use_vlchar )
              >> qi::lit("LLL\'")[ qi::_a = phoenix::construct< string_type >() ]
              >> +((esc_pm - qi::lit('\'') )[ phoenix::push_back( qi::_a, qi::_1 ) ])
              >> qi::lit('\'')[ qi::_val = ast::value_( qi::_a, ast_::litterals::vlchar_ ) ] )    //
                     ]
              // when we finally have is_xxx() functions for 64-bit chars
              // we will add 1] new esc pasers, 2] new string type,
              // and 3] we will change this appropriately.
            | qi::hold[
              ( qi::eps( char_used::value >= char_usage::use_mbchar )
              >> qi::lit("LL\'")[ qi::_a = phoenix::construct< string_type >() ]
              >> +((esc_pm - qi::lit('\'') )[ phoenix::push_back( qi::_a, qi::_1 ) ])
              >> qi::lit('\'')[ qi::_val = ast::value_( qi::_a, ast_::litterals::mbchar_ ) ] ) //
                      ]
              // when we finally have is_xxx() functions for 32-bit chars
              // we will add 1] new esc pasers, 2] new string type,
              // and 3] we will change this appropriately.
            | qi::hold[   // wchar is allways supported.
              ( qi::lit("L\'")[ qi::_b = phoenix::construct< wstring_type >() ]
              >> +((wesc_pm - qi::lit('\'') )[ phoenix::push_back( qi::_b, qi::_1 ) ])
              >> qi::lit('\'')[ qi::_val = ast::value_( qi::_b, ast_::litterals::wchar_ ) ] ) //
                      ]   // char is allways supported.
            | ( qi::lit("\'")[ qi::_a = phoenix::construct< string_type >() ]
              >> +((esc_pm - qi::lit('\'') )[ phoenix::push_back( qi::_a, qi::_1 ) ])
              >> qi::lit('\'')[ qi::_val = ast::value_( qi::_a, ast_::litterals::char_ ) ] ) //
                                 ];
        str_lit_pm = qi::lexeme[ // litterals:: //
            qi::hold[
            ( qi::eps( qi::_r1 >= char_usage::use_vlchar )
              >> qi::lit("LLL\"")[ qi::_a = phoenix::construct< string_type >() ]
              >> *((esc_pm - qi::lit('\"'))[ phoenix::push_back( qi::_a, qi::_1 ) ] )
              >> qi::lit('\"')[ qi::_val = ast::value_( qi::_a, ast_::litterals::vlstr_ ) ] ) //
                    ]

              // when we finally have is_xxx() functions for 64-bit chars
              // we will add 1] new esc pasers, 2] new string types,
              // and 3] we will change this appropriately.
            | qi::hold[
              ( qi::eps( qi::_r1 >= char_usage::use_mbchar )
              >> qi::lit("LL\"")[ qi::_a = phoenix::construct< string_type >() ]
              >> *((esc_pm - qi::lit('\"'))[ phoenix::push_back( qi::_a, qi::_1 ) ] )
              >> qi::lit('\"')[ qi::_val = ast::value_( qi::_a, ast_::litterals::mbstr_ ) ] ) //
                      ]
              // when we finally have is_xxx() functions for 32-bit chars
              // we will add 1] new esc pasers, 2] new string type,
              // and 3] new node types + change this appropriately.
              // wchar is allways supported.
            | qi::hold[
               ( qi::lit("L\"")[ qi::_b = phoenix::construct< wstring_type >() ]
              >> *((wesc_pm - qi::lit('\"'))[ phoenix::push_back( qi::_b, qi::_1 ) ])
              >> qi::lit('\"')[ qi::_val = ast::value_( qi::_a, ast_::litterals::wstr_ ) ] )  //
                      ]
              // char is allways supported.
            | ( qi::lit('\"')[ qi::_a = phoenix::construct< string_type >() ]
              >> *(( esc_pm - qi::lit('\"') )[ phoenix::push_back( qi::_a, qi::_1 ) ] )
              >> qi::lit('\"')[ qi::_val = ast::value_( qi::_a, ast_::litterals::str_ ) ] ) //
                                 ] ;

        start %= qi::hold[ number_lit_pm ] | qi::hold[ bool_lit_pm ]
                     | qi::hold[ char_lit_pm ]
                     | str_lit_pm; /////
                     /// code removed from the char and string litteral parsers:
                     ///"(char_used::value)"
       }



     }; // end of struct gsvm_parser;

    } // end of namespace gsvm;
   } // end of namespace parsers;
  }// end of namespace frontends;
 } // end of namespace compilers;
} // end of namespace SK;

#endif
