#ifndef SK_COMPILERS_FRONTENDS_PARSERS_GSVM_PARSER_GSVM_MODIFIERS_PARSER_HPP
#define SK_COMPILERS_FRONTENDS_PARSERS_GSVM_PARSER_GSVM_MODIFIERS_PARSER_HPP

#include "gsvm_parser_def_fwd.hpp"

namespace SK
{
 namespace compilers
 {
  namespace frontends
  {
   namespace parsers
   {
    namespace gsvm
    {
     namespace spirit = boost::spirit;
     namespace qi = boost::spirit::qi;
     namespace phoenix = boost::phoenix;

     namespace types = SK::utility::types;
     namespace internals = SK::compilers::parsers::internals;
     namespace ast = SK::compilers::frontends::ast::internals;
     namespace ast_ = SK::compilers::frontends::ast::gsvm::consts::IDs;
     //formerly:  SK::compilers::frontends::ast::gsvm;

      template< typename IteratorT >
      struct gsvm_modifier_parser< IteratorT, char >
      : qi:grammar< IteratorT, ast::ast_node::node_ptr(),
                    internals::c_lang_skipper_type<IteratorT, char>,
                    qi::locals<ast::ast_node::container_t>
                  >
      {
       typedef  ast::ast_node::node_ptr node_ptr;
       typedef  ast::ast_node::container_t container_t;
       typedef  char char_type;
       typedef  wchar_t wchar_type;
       typedef  std::basic_string< char_type > string_type;
       typedef  std::basic_string< wchar_type > wstring_type;
       typedef  internals::c_lang_skipper_type<IteratorT, char_type> skipper_type;
       typedef  internals::c_lang_escape_parser<IteratorT, char_type> char_escape_parser_type;
       typedef  internals::c_lang_escape_parser<IteratorT, wchar_type> wchar_escape_parser_type;
       typedef  keyword_parser_type<IteratorT, char_type> keyword_parser_t;
       typedef  keyword_symbols< char_type > keyword_sym_type;

       typedef  builtin_type_sym_table< char_type > builtin_types_sym_type;
       typedef  operator_sym_table< char_type > operator_sym_type;
       typedef  builtin_function_sym_table< char_type > builtin_function_sym_type;

       // "general utility" ( rule types ) typedefs
       typedef qi::rule< IteratorT, node_ptr( void ), skipper_type >
                                                      pass_rule_type;

       typedef qi::rule< IteratorT, node_ptr( void ), skipper_type,
               qi::locals<container_t>  > grammar_sig_rule_type;
       // "grammar_sig_rule_type" - aka: "nary_rule_type"

       typedef qi::rule< IteratorT, node_ptr( void ), skipper_type,
                       qi::locals<node_ptr>  > unary_rule_type;

       typedef qi::rule< IteratorT, node_ptr( void ), skipper_type,
                       qi::locals<node_ptr, types::uint32 >  > unary_plus_rule_type;

       typedef qi::rule< IteratorT, node_ptr( void ), skipper_type,
                       qi::locals<node_ptr, node_ptr>  > binary_rule_type;

       typedef qi::rule< IteratorT, node_ptr( types::uint8 ), skipper_type,
              qi::locals<container_t>  > nary_rule_with_options_type;
       // the start rule
       grammar_sig_rule_type start;


       gsvm_modifier_parser() : base_type(start,
            "G.S. Script Language � char parser, modifier parser component" )
       {

           // general modifiers // modifiers:: //
        genDecl_mod_pm = qi::hold[ keyword_parser_pm("export")
                 [ qi::_val = ast::tag_( ast_::modifiers::export_ ) ] ]
                 | keyword_parser_pm("intern")
                 [ qi::_val = ast::tag_( ast_::modifiers::intern_ ) ] ; //

        // type modifiers // modifiers:: // modifiers::
        type_prefix_pm = qi::eps[ qi::_a = phoenix::construct< container_t >() ]
          >> *( qi::hold[ keyword_parser_pm("const")
                 >> ( qi::hold[ qi::lit('&')
              [ phoenix::push_back( qi::_a, ast::tag_( ast_::modifiers::const_ref_ ) ) ] ]
                  | qi::lit('*')
              [ phoenix::push_back( qi::_a, ast::tag_( ast_::modifiers::const_ptr_ ) ) ] ) ]
          // | qi::hold[  ]
           | qi::hold[ keyword_parser_pm("var")
                 >> ( qi::hold[ qi::lit('&')
                 [ phoenix::push_back( qi::_a, ast::tag_( ast_::modifiers::var_ref_ ) ) ] ]
                  | qi::lit('*')
                 [ phoenix::push_back( qi::_a, ast::tag_( ast_::modifiers::var_ptr_ ) ) ] ) ]
           | attribute_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
           >> qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::modifiers::type_prefix_ ) ]; //
                           // modifiers:: //
        type_infix_pm = qi::eps[ qi::_a = phoenix::construct< container_t >() ]
             >>  *( qi::hold[ template_ident_pm(template_type::ident)
                       [ phoenix::push_back( qi::_a, qi::_1 ) ] ]
                  | qi::hold[ lang_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
                  |  attribute_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
             >> qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::modifiers::type_infix_ident ) ] ; //

        type_postfix_pm = qi::eps[ qi::_a = phoenix::construct< container_t >()]
             >> *( qi::lit('[')
             >> sequence_expr_pm
                  [ phoenix::push_back( qi::_a, ast::unary_( qi:: _1 , ast_::modifiers::array_type_ ) ) ] //
             >> qi::lit(']') )
             >> qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::modifiers::type_postfix_ ) ]; //

        template_ident_pm = qi::eps
                              [ qi::_a = phoenix::construct< container_t >()]
             >> ( ( qi::eps( qi::_r1 == template_type::oper )
                   >> keyword_parser_pm("template") )
                | - keyword_parser_pm("template") )
             >> qi::lit('<')
             >> ( ( -( qi::hold[ data_type_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]  ]
              | qi::hold[ conditional_expr_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
              | macro_assignment_expr_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ) )
             % qi::lit(',') )
             >> qi::lit('>')
             >> qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::modifiers::temp_id_ ) ]; //

        lang_pm = keyword_parser_pm("lang") >> qi::lit('(')
                   >> ( qi::hold[ litteral_pm(char_used::value)
                    [ qi::_val = ast::unary_( qi:: _1 , ast_::modifiers::lang_ ) ] ] //
                   | macro_assignment_expr_pm
                    [ qi::_val = ast::unary_( qi:: _1 , ast_::modifiers::lang_ ) ] )
                   >> qi::lit(')');

        attribute_pm = keyword_parser_pm("attribute") >> qi::lit('(')
           >> basic_ident_pm[ qi::_a = qi:: _1 ]
           >> qi::lit(',')
           >> ( qi::hold[ litteral_pm(char_used::value)
           [ qi::_val = ast::binary_( qi::_a, qi:: _1, ast_::modifiers::attribute_ ) ] ] //
           | macro_assignment_expr_pm
           [ qi::_val = ast::binary_( qi::_a, qi:: _1, ast_::modifiers::attribute_ ) ] )
           >> qi::lit(')');

        callconv_pm = keyword_parser_pm("callconv") >> qi::lit('(')
          >> ( qi::hold[ litteral_pm(char_used::value)
           [ qi::_val = ast::unary_( qi:: _1 , ast_::modifiers::callconv_ ) ] ] //
           | macro_assignment_expr_pm
           [ qi::_val = ast::unary_( qi:: _1 , ast_::modifiers::callconv_ ) ] )
          >> qi::lit(')');

        throw_pm = keyword_parser_pm("throw")[ qi::_a = phoenix::construct< container_t >()]
                >> qi::lit('(')
                >> data_type_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] % qi::lit(',')
                >> qi::lit(')')[ qi::_val = ast::nary_( qi::_a, ast_::modifiers::throw_ ) ]; //

        static_pm = keyword_parser_pm("static")[ qi::_val = ast::tag_( ast_::modifiers::static_ ) ]; //

        const_pm = keyword_parser_pm("const")[ qi::_val = ast::tag_( ast_::modifiers::const_ ) ]; //

        volatile_pm = keyword_parser_pm("volatile")
         [ qi::_val = ast::tag_( ast_::modifiers::volatile_ ) ]; //

        template_decl_arg_pm =
         ( qi::hold[ data_type_pm[ qi::_a = qi::_1 ]
          >> -( qi::lit('=') >> data_type_pm
           [ qi::_val = ast::binary_( qi::_a, qi:: _1, ast_::modifiers::temp_decl_arg_ ) ] ) ] //
         | qi::hold[ single_var_decl_pm
           [ qi::_val = ast::unary_( qi:: _1 , ast_::modifiers::temp_decl_arg_ ) ] ]
         | qi::hold[ single_const_decl_pm
           [ qi::_val = ast::unary_( qi:: _1 , ast_::modifiers::temp_decl_arg_ ) ] ]
         | macro_single_var_decl_pm
           [ qi::_val = ast::unary_( qi:: _1 , ast_::modifiers::temp_decl_arg_ ) ] ) */
         );

        template_decl_pm = qi::eps
         [ qi::_a = phoenix::construct< container_t >()]
         >> ( ( qi::eps( qi::_r1 == template_type::oper )
             >> keyword_parser_pm("template") )
             | - keyword_parser_pm("template") )
         >> qi::lit('<')
         >> ( ( qi::hold[ ( template_decl_arg_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ) ]
             | qi::eps[ phoenix::push_back( qi::_a, ast::tag_( ast_::ws::sepperator ) ) ] )
         % qi::lit(',') )
         >> qi::lit('>')
         >> qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::modifiers::temp_decl_ ) ]; //


        mutable_pm = keyword_parser_pm("mutable")
                  [ qi::_val = ast::tag_( ast_::modifiers::mutable_ ) ];//

        abstract_pm = keyword_parser_pm("abstract")
                  [ qi::_val = ast::tag_( ast_::modifiers::abstract_ ) ];//

        virtual_pm = keyword_parser_pm("virtual")
                  [ qi::_val = ast::tag_( ast_::modifiers::virtual_ ) ];//

        func_ident_mods_pm = qi::eps[ qi::_a = phoenix::construct< container_t >() ]
            >> *(
                qi::hold[ template_ident_pm(qi::_r1)
                [ phoenix::push_back( qi::_a, qi::_1 ) ] ]
               |qi::hold[ callconv_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
               |qi::hold[ lang_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
               |qi::hold[ attribute_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
               |qi::hold[ throw_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
               |qi::hold[ static_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
               | const_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
                )
            >> qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::modifiers::func_ident_mods ) ];

        func_decl_mods_pm = qi::eps[ qi::_a = phoenix::construct< container_t >() ]
            >> *( qi::hold[ genDecl_mod_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
               | qi::hold[ template_decl_pm(qi::_r1)
                 [ phoenix::push_back( qi::_a, qi::_1 ) ] ]
               | qi::hold[ throw_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
               | qi::hold[ const_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
               | qi::hold[ lang_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
               | qi::hold[ attribute_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
               | qi::hold[ static_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
               | qi::hold[ abstract_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
               | qi::hold[ virtual_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
               | callconv_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
            )
            >> qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::modifiers::func_decl_mods ) ]; //

        func_decl_arg_pm = ( qi::hold[ single_const_decl_pm[ qi::_a = qi::_1 ] ]
        | single_var_decl_pm[ qi::_a = qi::_1 ] )
        > (  qi::hold[ macro_assignment_expr_pm
        [ qi::_val = ast::binary_( qi::_a, qi::_1, ast_::modifiers::func_decl_arg_ ) ] ]
        | qi::eps[ qi::_val = ast::binary_( qi::_a, qi::_1, ast_::modifiers::func_decl_arg_ ) ] );

        func_decl_args_pm = qi::eps[ qi::_a = phoenix::construct< container_t >() ]
         > -( func_decl_arg_pm % qi::lit(',') )
         > qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::modifiers::func_decl_args_ ) ];

        // ast::tag_(  )

        explicit_pm = keyword_parser_pm("explicit")
               [ qi::_val = ast::tag_( ast_::modifiers::explicit_ ) ]; //

        ctor_decl_mods_pm = qi::eps[ qi::_a = phoenix::construct< container_t >() ]
         > *( qi::hold[ explicit_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
            | qi::hold[ genDecl_mod_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
            | qi::hold[ template_decl_pm(template_type::null_)
                 [ phoenix::push_back( qi::_a, qi::_1 ) ] ]
            | qi::hold[ throw_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
            | qi::hold[ lang_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
            | qi::hold[ attribute_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
            | callconv_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
            )
         > qi::eps[ qi::_val = ast::nary_( qi::_a , ast_::modifiers::ctor_decl_mods ) ];

        ctor_init_list_pm = qi::lit(':')
            [ qi::_a = phoenix::construct< container_t >() ]
          >> ctor_init_mem_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] % qi::lit(',')
          >> qi::eps[ qi::_val = ast::nary_( qi::_a , ast_::modifiers::ctor_init_list_ ) ];//

        ctor_init_mem_pm = qi::eps[ qi::_a = phoenix::construct< container_t >() ]
            >> identifier_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
            >> funcCall_expr_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
            >> -( macro_assignment_expr_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
            >> qi::eps[ qi::_val = ast::nary_( qi::_a , ast_::modifiers::ctor_init_mem_ ) ]; //

        type_infix_decl_mods_pm = qi::eps[ qi::_a = phoenix::construct< container_t >() ]
         > *( qi::hold[ genDecl_mod_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
            | qi::hold[ template_decl_pm(template_type::null_)
                 [ phoenix::push_back( qi::_a, qi::_1 ) ] ]
            | qi::hold[ lang_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
            | qi::hold[ attribute_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
            | qi::hold[ virtual_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
            | abstract_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
         > qi::eps[ qi::_val = ast::nary_( qi::_a , ast_::modifiers::type_infix_decl ) ];

        inherit_list_pm = qi::lit(':')
            [ qi::_a = phoenix::construct< container_t >() ]
            >> inherit_mem_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] % qi::lit(',')
            >> qi::eps[ qi::_val = ast::nary_( qi::_a , ast_::modifiers::inherit_list_ ) ]; //

        inherit_mem_pm = qi::eps[ qi::_a = phoenix::construct< container_t >() ]
            >> access_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
            >> identifier_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
            >> -macro_assignment_expr_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
            >> qi::eps[ qi::_val = ast::nary_( qi::_a , ast_::modifiers::inherit_mem_ ) ]; //

        dtor_decl_mods_pm = qi::eps[ qi::_a = phoenix::construct< container_t >() ]
         > *( qi::hold[ explicit_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
            | qi::hold[ genDecl_mod_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
            | qi::hold[ template_decl_pm(template_type::null_)
                 [ phoenix::push_back( qi::_a, qi::_1 ) ] ]
            | qi::hold[ lang_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
            | qi::hold[ attribute_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
            | qi::hold[ callconv_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
            | virtual_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
            )
         > qi::eps[ qi::_val = ast::nary_( qi::_a , ast_::modifiers::dtor_decl_mods ) ];

        access_pm = ( qi::hold[ keyword_parser_pm("public")
                     [ qi::_a = ast::tag_( ast_::modifiers::public_ ) ] ]//
                    | qi::hold[ keyword_parser_pm("protected")
                      [ qi::_a = ast::tag_( ast_::modifiers::protected_ ) ] ] //
                    | keyword_parser_pm("private")
                      [ qi::_a = ast::tag_( ast_::modifiers::private_ ) ] ) //
                 >> ( qi::hold[ virtual_pm
                 [ qi::_val = ast::binary_(  qi::_a, qi::_1, ast_::modifiers::access_ ) ] ] //
                 | qi::eps[ qi::_val = ast::unary_( qi::_a , ast_::modifiers::access_ ) ] ); //

        data_decl_pm = qi::eps[ qi::_a = phoenix::construct< container_t >() ]
          >> *( qi::hold[ keyword_parser_pm("const")
                 > ( qi::hold[ qi::lit('&')
              [ phoenix::push_back( qi::_a, ast::tag_( ast_::modifiers::const_ref_ ) ) ] ]
                  | qi::lit('*')
              [ phoenix::push_back( qi::_a, ast::tag_( ast_::modifiers::const_ptr_ ) ) ] ) ]
          // | qi::hold[  ]
           | qi::hold[ keyword_parser_pm("var")
                 > ( qi::hold[ qi::lit('&')
                 [ phoenix::push_back( qi::_a, ast::tag_( ast_::modifiers::var_ref_ ) ) ] ]
                  | qi::lit('*')
                 [ phoenix::push_back( qi::_a, ast::tag_( ast_::modifiers::var_ptr_ ) ) ] ) ]
           | qi::hold[ genDecl_mod_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
           | qi::hold[ lang_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
           | qi::hold[ attribute_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
           | qi::hold[ static_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
           | qi::hold[ volatile_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
           | mutable_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
           >> qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::modifiers::data_decl ) ];


       }/// end of ctor...



     };
    } // end of namespace gsvm;
   } // end of namespace parsers;
  }// end of namespace frontends;
 } // end of namespace compilers;
} // end of namespace SK;

#endif


