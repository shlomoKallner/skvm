#ifndef SK_COMPILERS_FRONTENDS_PARSERS_GSVM_PARSE_DEF_HPP
#define SK_COMPILERS_FRONTENDS_PARSERS_GSVM_PARSE_DEF_HPP

#include "gsvm_parser_def_fwd.hpp"

namespace SK
{
 namespace compilers
 {
  namespace frontends
  {
   namespace parsers
   {
    namespace gsvm
    {
     namespace spirit = boost::spirit;
     namespace qi = boost::spirit::qi;
     namespace phoenix = boost::phoenix;

     namespace types = SK::utility::types;
     namespace internals = SK::compilers::parsers::internals;
     namespace ast = SK::compilers::frontends::ast::internals;
     namespace ast_ = SK::compilers::frontends::ast::gsvm::consts::IDs;
     //formerly:  SK::compilers::frontends::ast::gsvm;


   template< typename IteratorT >
   struct gsvm_parser< IteratorT, char >
   : qi:grammar< IteratorT, ast::ast_node::node_ptr(),
                 internals::c_lang_skipper_type<IteratorT, char>,
                 qi::locals<ast::ast_node::container_t>
               >
   {
     typedef  ast::ast_node::node_ptr node_ptr;
     typedef  ast::ast_node::container_t container_t;
     typedef  char char_type;
     typedef  wchar_t wchar_type;
     typedef  std::basic_string< char_type > string_type;
     typedef  std::basic_string< wchar_type > wstring_type;
     typedef  internals::c_lang_skipper_type<IteratorT, char_type> skipper_type;
     typedef  internals::c_lang_escape_parser<IteratorT, char_type> char_escape_parser_type;
     typedef  internals::c_lang_escape_parser<IteratorT, wchar_type> wchar_escape_parser_type;
     typedef  keyword_parser_type<IteratorT, char_type> keyword_parser_t;
     typedef  keyword_symbols< char_type > keyword_sym_type;
     typedef  builtin_type_sym_table< char_type > builtin_types_sym_type;
     typedef  operator_sym_table< char_type > operator_sym_type;
     typedef  builtin_function_sym_table< char_type > builtin_function_sym_type;
     typedef  unary_expr_sym_table< char_type > unary_expr_sym_type;
     typedef  assignment_sym_table< char_type > assignment_expr_sym_type;

     typedef  builtin_macro_func_sym_table< char_type > builtin_macro_func_sym_type;
     typedef  macro_operators_sym_table< char_type > macro_operators_sym_type;
     typedef  builtin_macro_type_sym_table< char_type > builtin_macro_type_sym_type;
     typedef  macro_unary_expr_sym_table< char_type > macro_unary_expr_sym_type;
     typedef  macro_assignment_expr_sym_table< char_type > macro_assignment_expr_sym_type;

     typedef  mpl::integral_c< types::uint8, char_usage::use_wchar > char_used;
     // the escape parsers as member variables.
     char_escape_parser_type esc_pm;
     wchar_escape_parser_type wesc_pm;
     // the numeric parsers as member variables.
     internals::ubin_p  ubin_pm;
     internals::uoct_p  uoct_pm;
     internals::udec_p  udec_pm;
     internals::uhex_p  uhex_pm;
     internals::sbin_p  sbin_pm;
     internals::soct_p  soct_pm;
     internals::sdec_p  sdec_pm;
     internals::shex_p  shex_pm;
     // internals::ufloat_p ufloat_pm;
     internals::sfloat_p  sfloat_pm;
     // other symbol tables:
     keyword_sym_type                keyword_pm;
     builtin_types_sym_type          builtin_types_pm;
     operator_sym_type               operators_pm;
     builtin_function_sym_type       builtin_function_pm;
     keyword_parser_t                keyword_parser_pm;
     unary_expr_sym_type             unary_expr_sym_pm;
     assignment_expr_sym_type        assignment_expr_sym_pm;

     builtin_macro_func_sym_type     builtin_macro_func_sym_pm;
     macro_operators_sym_type        macro_operators_sym_pm;
     builtin_macro_type_sym_type     builtin_macro_type_sym_pm;
     macro_unary_expr_sym_type       macro_unary_expr_sym_pm;
     macro_assignment_expr_sym_type  macro_assignment_expr_sym_pm;

     gsvm_parser() : base_type(start, "G.S. Script Language � char parser" )
     {
        // litterals
        bool_lit_pm = qi::hold[ keyword_parser_pm("true")
             [ qi::_val = ast::value_( true, ast_::litterals::bool_ ) ] ]
             | keyword_parser_pm("false")
             [ qi::_val = ast::value_( false, ast_::litterals::bool_ ) ];

        uint_lit_pm = qi::lexeme[
              qi::hold[ qi::lit("0b") >> ubin_pm
             [ qi::_val = ast::value_( qi::_1, ast_::litterals::uint_ )] ]
             |  qi::hold[ qi::lit("0o") >> uoct_pm
               [ qi::_val = ast::value_( qi::_1, ast_::litterals::uint_ ) ] ]
             |  qi::hold[ qi::lit("0x") >> uhex_pm
               [ qi::_val = ast::value_( qi::_1, ast_::litterals::uint_ ) ] ]
             | udec_pm
               [ qi::_val = ast::value_( qi::_1, ast_::litterals::uint_ ) ]
                                ];
        sint_lit_pm = qi::lexeme[
             ( qi::hold[ qi::lit("-")[ qi::_a = true ] ]
             | qi::lit("+")[ qi::_a = false ] ) >>
             ( qi::hold[ ( qi::lit("0b") >> sbin_pm
               [ phoenix::if_( qi::_a == true)
               [ qi::_val = ast::value_( - qi::_1, ast_::litterals::sint_ ) ]
               .else_[ qi::_val = ast::value_( qi::_1, ast_::litterals::sint_ ) ]
                                                        ] ) ]
             | qi::hold[ ( qi::lit("0o") >> soct_pm
               [ phoenix::if_( qi::_a == true)
               [ qi::_val = ast::value_( - qi::_1, ast_::litterals::sint_ ) ]
               .else_[ qi::_val = ast::value_( qi::_1, ast_::litterals::sint_ ) ]
                                                        ] ) ]
             | qi::hold[ ( qi::lit("0x") >> shex_pm
               [ phoenix::if_( qi::_a == true)
               [ qi::_val = ast::value_( - qi::_1, ast_::litterals::sint_ ) ]
               .else_[ qi::_val = ast::value_( qi::_1, ast_::litterals::sint_ ) ]
                                                        ] ) ]
             | ( sdec_pm
               [ phoenix::if_( qi::_a == true)
               [ qi::_val = ast::value_( - qi::_1, ast_::litterals::sint_ ) ]
               .else_[ qi::_val = ast::value_( qi::_1, ast_::litterals::sint_ ) ]
                                                        ] ) ) ];
        float_lit_pm =
            /*
              qi::hold[ ( ufloat_pm
              [ qi::_val = ast::value_( qi::_1, ast_::litterals::float_ ) ] ) ]
             |
            */
            sfloat_pm[ qi::_val = ast::value_( qi::_1, ast_::litterals::float_ ) ];

        number_lit_pm %= qi::hold[ float_lit_pm ] | qi::hold[ uint_lit_pm ]
                       | sint_lit_pm;

        char_lit_pm = qi::lexeme[
             qi::hold[
              (  qi::eps( qi::_r1 >= char_usage::use_vlchar )
              >> qi::lit("LLL\'")[ qi::_a = phoenix::construct< string_type >() ]
              >> +((esc_pm - qi::lit('\'') )[ phoenix::push_back( qi::_a, qi::_1 ) ])
              >> qi::lit('\'')[ qi::_val = ast::value_( qi::_a, ast_::litterals::vlchar_ ) ] )    //
                     ]
              // when we finally have is_xxx() functions for 64-bit chars
              // we will add 1] new esc pasers, 2] new string type,
              // and 3] we will change this appropriately.
            | qi::hold[
              ( qi::eps( qi::_r1 >= char_usage::use_mbchar )
              >> qi::lit("LL\'")[ qi::_a = phoenix::construct< string_type >() ]
              >> +((esc_pm - qi::lit('\'') )[ phoenix::push_back( qi::_a, qi::_1 ) ])
              >> qi::lit('\'')[ qi::_val = ast::value_( qi::_a, ast_::litterals::mbchar_ ) ] ) //
                      ]
              // when we finally have is_xxx() functions for 32-bit chars
              // we will add 1] new esc pasers, 2] new string type,
              // and 3] we will change this appropriately.
            | qi::hold[   // wchar is allways supported.
              ( qi::lit("L\'")[ qi::_b = phoenix::construct< wstring_type >() ]
              >> +((wesc_pm - qi::lit('\'') )[ phoenix::push_back( qi::_b, qi::_1 ) ])
              >> qi::lit('\'')[ qi::_val = ast::value_( qi::_b, ast_::litterals::wchar_ ) ] ) //
                      ]   // char is allways supported.
            | ( qi::lit("\'")[ qi::_a = phoenix::construct< string_type >() ]
              >> +((esc_pm - qi::lit('\'') )[ phoenix::push_back( qi::_a, qi::_1 ) ])
              >> qi::lit('\'')[ qi::_val = ast::value_( qi::_a, ast_::litterals::char_ ) ] ) //
                                 ];
        str_lit_pm = qi::lexeme[ // litterals:: //
            qi::hold[
            ( qi::eps( qi::_r1 >= char_usage::use_vlchar )
              >> qi::lit("LLL\"")[ qi::_a = phoenix::construct< string_type >() ]
              >> *((esc_pm - qi::lit('\"'))[ phoenix::push_back( qi::_a, qi::_1 ) ] )
              >> qi::lit('\"')[ qi::_val = ast::value_( qi::_a, ast_::litterals::vlstr_ ) ] ) //
                    ]

              // when we finally have is_xxx() functions for 64-bit chars
              // we will add 1] new esc pasers, 2] new string types,
              // and 3] we will change this appropriately.
            | qi::hold[
              ( qi::eps( qi::_r1 >= char_usage::use_mbchar )
              >> qi::lit("LL\"")[ qi::_a = phoenix::construct< string_type >() ]
              >> *((esc_pm - qi::lit('\"'))[ phoenix::push_back( qi::_a, qi::_1 ) ] )
              >> qi::lit('\"')[ qi::_val = ast::value_( qi::_a, ast_::litterals::mbstr_ ) ] ) //
                      ]
              // when we finally have is_xxx() functions for 32-bit chars
              // we will add 1] new esc pasers, 2] new string type,
              // and 3] new node types + change this appropriately.
              // wchar is allways supported.
            | qi::hold[
               ( qi::lit("L\"")[ qi::_b = phoenix::construct< wstring_type >() ]
              >> *((wesc_pm - qi::lit('\"'))[ phoenix::push_back( qi::_b, qi::_1 ) ])
              >> qi::lit('\"')[ qi::_val = ast::value_( qi::_a, ast_::litterals::wstr_ ) ] )  //
                      ]
              // char is allways supported.
            | ( qi::lit('\"')[ qi::_a = phoenix::construct< string_type >() ]
              >> *(( esc_pm - qi::lit('\"') )[ phoenix::push_back( qi::_a, qi::_1 ) ] )
              >> qi::lit('\"')[ qi::_val = ast::value_( qi::_a, ast_::litterals::str_ ) ] ) //
                                 ] ;
        litteral_pm %= qi::hold[ number_lit_pm ] | qi::hold[ bool_lit_pm ]
                     | qi::hold[ char_lit_pm(qi::_r1) ] | str_lit_pm(qi::_r1);

        // identifiers // identifiers:: // identifiers::
        basic_ident_pm = qi::eps[ qi::_a = phoenix::construct< string_type >() ]
          >> ( !keyword_pm >> qi::lexeme[ ( ( qi::hold[ qi::standard::alpha
                           [ phoenix::push_back( qi::_a, qi::_1 ) ] ]
               | qi::standard::char_('_')[ phoenix::push_back( qi::_a, '_' ) ] )
               >> *( qi::hold[ qi::standard::alnum[
                               phoenix::push_back( qi::_a, qi::_1 ) ] ]
                  | qi::standard::char_('_')[ phoenix::push_back( qi::_a, '_' ) ] ) ) ] )
          >> qi::eps[ qi::_val = ast::value_( qi::_a, ast_::identifiers::basic_ ) ]; //

        /*
         version 2( current version ):
         ! keyword_pm >> qi::lexeme[ ( ( qi::hold[ qi::standard::alpha
                           [ phoenix::push_back( qi::_a, qi::_1 ) ] ]
                  | qi::standard::char_('_')[ phoenix::push_back( qi::_a, '_' ) ] )
                 >> *( qi::hold[ qi::standard::alnum[
                                 phoenix::push_back( qi::_a, qi::_1 ) ] ]
                    | qi::standard::char_('_')[ phoenix::push_back( qi::_a, '_' ) ] ) ) ]
         version 1:
                 ( qi::lexeme[
                ( qi::hold[ qi::standard::alpha
                           [ phoenix::push_back( qi::_a, qi::_1 ) ] ]
                  | qi::standard::char_('_')[ phoenix::push_back( qi::_a, '_' ) ] )
                 >> *( qi::hold[ qi::standard::alnum[
                                 phoenix::push_back( qi::_a, qi::_1 ) ] ]
                    | qi::standard::char_('_')[ phoenix::push_back( qi::_a, '_' ) ] )
                 ] - keyword_pm )

        */

        var_ident_pm = keyword_parser_pm("var") >> basic_ident_pm
                     [ qi::_val = ast::unary_( qi::_1, ast_::identifiers::var_ ) ];

        const_ident_pm = keyword_parser_pm("const") >> basic_ident_pm
                     [ qi::_val = ast::unary_( qi::_1, ast_::identifiers::const_ ) ]; //

        typename_ident_pm = keyword_parser_pm("typename") >> basic_ident_pm
                     [ qi::_val = ast::unary_( qi::_1, ast_::identifiers::typename_ ) ]; //

        bitfield_ident_pm = keyword_parser_pm("bitfield")
            >> type_infix_pm[ qi::_a = qi::_1 ]
            >> basic_ident_pm[ qi::_val = ast::binary_( qi::_1, qi::_a, ast_::identifiers::bitfield_ ) ]; //

        enum_ident_pm = keyword_parser_pm("enum")
            >> type_infix_pm[ qi::_a = qi::_1 ]
            >> basic_ident_pm[ qi::_val = ast::binary_( qi::_1, qi::_a, ast_::identifiers::enum_ ) ]; //

        object_ident_pm = keyword_parser_pm("object")
            >> type_infix_pm[ qi::_a = qi::_1 ]
            >> basic_ident_pm[ qi::_val = ast::binary_( qi::_1, qi::_a, ast_::identifiers::object_ ) ]; //

        union_ident_pm = keyword_parser_pm("union")
            >> type_infix_pm[ qi::_a = qi::_1 ]
            >> basic_ident_pm[ qi::_val = ast::binary_( qi::_1, qi::_a, ast_::identifiers::union_ ) ]; //

        function_type_ident_pm = keyword_parser_pm("function")
                    [ qi::_a = phoenix::construct< container_t >()]
            >> func_ident_mods_pm(template_type::null_)[ phoenix::push_back( qi::_a, qi::_1 ) ]
            >> qi::lit('(')
            >> function_type_args_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
            >> qi::lit(')') >> qi::lit(':')
            >> function_type_arg_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
            >> qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::identifiers::function_type_ ) ]; //

        function_type_args_ident_pm = qi::eps
            [ qi::_a = phoenix::construct< container_t >()]
            >> ( function_type_arg_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
               % qi::lit(',') )
            >> qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::identifiers::function_type_args_ ) ]; //

        function_type_arg_ident_pm =
           qi::hold[ keyword_parser_pm("var") > data_type_pm
           [ qi::_val = ast::unary_( qi::_1, ast_::identifiers::function_type_arg_var ) ] ]
           | keyword_parser_pm("const") > data_type_pm
           [ qi::_val = ast::unary_( qi::_1, ast_::identifiers::function_type_arg_const ) ]  ;

        operator_type_ident_pm = keyword_parser_pm("operator")
                    [ qi::_a = phoenix::construct< container_t >()]
            >> func_ident_mods_pm(template_type::oper)[ phoenix::push_back( qi::_a, qi::_1 ) ]
            >> qi::lit('(')
            >> function_type_args_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
            >> qi::lit(')') >> qi::lit(':')
            >> function_type_arg_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
            >> qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::identifiers::operator_type_ ) ]; //

        typeof_ident_pm = keyword_parser_pm("typeof") >> qi::lit('(')
               >> sequence_expr_pm[ qi::_val =  ast::unary_( qi::_1, ast_::identifiers::typeof_ ) ] //
               >> qi::lit(')');

        qual_typeid_ident_pm = qi::eps
         [ qi::_a = phoenix::construct< container_t >()]
         >> type_prefix_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         >> ( qi::hold[ builtin_types_pm
          [ push_back( _a, phoenix::construct<node_ptr>(
            phoenix::new_<ast_::identifiers::builtin_types_>( qi::_1 ) ) ) ] ]
          | qi::hold[ bitfield_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
          | qi::hold[ enum_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
          | qi::hold[ function_type_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
          | qi::hold[ object_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
          | qi::hold[ operator_type_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
          | qi::hold[ typename_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
          | qi::hold[ union_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
          | typeof_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
           )
         >> type_postfix_pm[ phoenix::push_back(qi::_a, qi::_1 ) ]
         >> qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::identifiers::qual_typeid_ ) ] ; //

        unqual_typeid_ident_pm = qi::eps
             [ qi::_a = phoenix::construct< container_t >()]
             >> type_prefix_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
             >> basic_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
             >> type_postfix_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
             >> qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::identifiers::unqual_typeid_ ) ] ; //

        label_ident_pm = keyword_parser_pm("label") >> basic_ident_pm
              [ qi::_val = ast::unary_( qi::_1, ast_::identifiers::label_ ) ]; //

        function_ident_pm = keyword_parser_pm("function")
            >> func_ident_mods_pm(template_type::null_)[ qi::_a = qi::_1 ]
            >> ( qi::hold[ builtin_function_pm //
            [ qi::_val = ast::binary_( qi::_a, ast::value_( qi::_1,
                                               ast_::identifiers::builtin_funcs_ ),
                                       ast_::identifiers::function_ ) ] ]
            | basic_ident_pm[ qi::_val = ast::binary_( qi::_a, qi::_1, ast_::identifiers::function_ ) ] ); //

        operator_ident_pm = keyword_parser_pm("operator")
            >> func_ident_mods_pm(template_type::oper)[ qi::_a = qi::_1 ]
            >> operators_pm[ qi::_val =  ast::binary_( qi::_a, ast::value_( qi::_1,
                                               ast_::identifiers::operators ),
                                       ast_::identifiers::operator_ ) ];

        namespace_ident_pm = keyword_parser_pm("namespace") >> basic_ident_pm
             [ qi::_val = ast::unary_( qi::_1 , ast_::identifiers::namespace_ ) ];

        import_ident_pm = keyword_parser_pm("import") >> basic_ident_pm
             [ qi::_val = ast::unary_( qi::_1 , ast_::identifiers::import_ ) ]; //

        qualified_ident_pm %= qi::hold[ var_ident_pm ]
                            | qi::hold[ const_ident_pm ]
                            | qi::hold[ label_ident_pm ]
                            | qi::hold[ qual_typeid_ident_pm ]
                            | qi::hold[ function_ident_pm ]
                            | operator_ident_pm ;

        unqualified_ident_pm %= qi::hold[ basic_ident_pm ]
                              | unqual_typeid_ident_pm;

        nested_name_ident_pm = qi::eps[ qi::_a = phoenix::construct< container_t >()]
               >> ( qi::hold[ import_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
                      >> qi::lit("::") ]
                   | qi::hold[ basic_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
                      >> qi::lit("::") ]
                   | qi::lit("::")
              [ phoenix::push_back( qi::_a, ast::tag_( ast_::ws::sepperator ) ) ] )
               >> *( qi::hold[ namespace_ident_pm[
                      phoenix::push_back( qi::_a, qi::_1 ) ] >> qi::lit("::") ]
                   | qi::hold[ basic_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
                     >> qi::lit("::") ]
                   | qi::lit("::")
              [ phoenix::push_back( qi::_a, ast::tag_( ast_::ws::sepperator ) ) ] )
               >> *( qi::hold[ qual_typeid_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
                   | qi::hold[ unqual_typeid_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
                   | qi::lit("::")
              [ phoenix::push_back( qi::_a, ast::tag_( ast_::ws::sepperator ) ) ] )
               >> qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::identifiers::nested_name_ ) ] ; //

        using_ident_pm = qi::eps[ qi::_a = phoenix::construct< container_t >()]
                >>( qi::hold[ nested_name_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
                   >> ( qi::hold[ var_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
                      | qi::hold[ const_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
                      | qi::hold[ qual_typeid_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
                      | unqual_typeid_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ) ]
                  | forward_decl_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
                >> macro_assignment_expr_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
                >> qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::identifiers::using_ ) ] ;//

        data_type_pm = qi::eps[ qi::_a = phoenix::construct< container_t >()]
             >> type_prefix_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
             >> -nested_name_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
             >> ( qual_typeid_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
              | unqual_typeid_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
             >> type_postfix_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
             >> qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::identifiers::data_type_ ) ] ; //



        // general modifiers // modifiers:: //
        genDecl_mod_pm = qi::hold[ keyword_parser_pm("export")
                 [ qi::_val = ast::tag_( ast_::modifiers::export_ ) ] ]
                 | keyword_parser_pm("intern")
                 [ qi::_val = ast::tag_( ast_::modifiers::intern_ ) ] ; //

        // type modifiers // modifiers:: // modifiers::
        type_prefix_pm = qi::eps[ qi::_a = phoenix::construct< container_t >() ]
          >> *( qi::hold[ keyword_parser_pm("const")
                 >> ( qi::hold[ qi::lit('&')
              [ phoenix::push_back( qi::_a, ast::tag_( ast_::modifiers::const_ref_ ) ) ] ]
                  | qi::lit('*')
              [ phoenix::push_back( qi::_a, ast::tag_( ast_::modifiers::const_ptr_ ) ) ] ) ]
          // | qi::hold[  ]
           | qi::hold[ keyword_parser_pm("var")
                 >> ( qi::hold[ qi::lit('&')
                 [ phoenix::push_back( qi::_a, ast::tag_( ast_::modifiers::var_ref_ ) ) ] ]
                  | qi::lit('*')
                 [ phoenix::push_back( qi::_a, ast::tag_( ast_::modifiers::var_ptr_ ) ) ] ) ]
           | attribute_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
           >> qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::modifiers::type_prefix_ ) ]; //
                           // modifiers:: //
        type_infix_pm = qi::eps[ qi::_a = phoenix::construct< container_t >() ]
             >>  *( qi::hold[ template_ident_pm(template_type::ident)
                       [ phoenix::push_back( qi::_a, qi::_1 ) ] ]
                  | qi::hold[ lang_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
                  |  attribute_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
             >> qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::modifiers::type_infix_ident ) ] ; //

        type_postfix_pm = qi::eps[ qi::_a = phoenix::construct< container_t >()]
             >> *( qi::lit('[')
             >> sequence_expr_pm
                  [ phoenix::push_back( qi::_a, ast::unary_( qi:: _1 , ast_::modifiers::array_type_ ) ) ] //
             >> qi::lit(']') )
             >> qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::modifiers::type_postfix_ ) ]; //

        template_ident_pm = qi::eps
                              [ qi::_a = phoenix::construct< container_t >()]
             >> ( ( qi::eps( qi::_r1 == template_type::oper )
                   >> keyword_parser_pm("template") )
                | - keyword_parser_pm("template") )
             >> qi::lit('<')
             >> ( ( -( qi::hold[ data_type_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]  ]
              | qi::hold[ conditional_expr_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
              | macro_assignment_expr_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ) )
             % qi::lit(',') )
             >> qi::lit('>')
             >> qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::modifiers::temp_id_ ) ]; //

        lang_pm = keyword_parser_pm("lang") >> qi::lit('(')
                   >> ( qi::hold[ litteral_pm(char_used::value)
                    [ qi::_val = ast::unary_( qi:: _1 , ast_::modifiers::lang_ ) ] ] //
                   | macro_assignment_expr_pm
                    [ qi::_val = ast::unary_( qi:: _1 , ast_::modifiers::lang_ ) ] )
                   >> qi::lit(')');

        attribute_pm = keyword_parser_pm("attribute") >> qi::lit('(')
           >> basic_ident_pm[ qi::_a = qi:: _1 ]
           >> qi::lit(',')
           >> ( qi::hold[ litteral_pm(char_used::value)
           [ qi::_val = ast::binary_( qi::_a, qi:: _1, ast_::modifiers::attribute_ ) ] ] //
           | macro_assignment_expr_pm
           [ qi::_val = ast::binary_( qi::_a, qi:: _1, ast_::modifiers::attribute_ ) ] )
           >> qi::lit(')');

        callconv_pm = keyword_parser_pm("callconv") >> qi::lit('(')
          >> ( qi::hold[ litteral_pm(char_used::value)
           [ qi::_val = ast::unary_( qi:: _1 , ast_::modifiers::callconv_ ) ] ] //
           | macro_assignment_expr_pm
           [ qi::_val = ast::unary_( qi:: _1 , ast_::modifiers::callconv_ ) ] )
          >> qi::lit(')');

        throw_pm = keyword_parser_pm("throw")[ qi::_a = phoenix::construct< container_t >()]
                >> qi::lit('(')
                >> data_type_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] % qi::lit(',')
                >> qi::lit(')')[ qi::_val = ast::nary_( qi::_a, ast_::modifiers::throw_ ) ]; //

        static_pm = keyword_parser_pm("static")[ qi::_val = ast::tag_( ast_::modifiers::static_ ) ]; //

        const_pm = keyword_parser_pm("const")[ qi::_val = ast::tag_( ast_::modifiers::const_ ) ]; //

        volatile_pm = keyword_parser_pm("volatile")
         [ qi::_val = ast::tag_( ast_::modifiers::volatile_ ) ]; //

        template_decl_arg_pm =
         ( qi::hold[ data_type_pm[ qi::_a = qi::_1 ]
          >> -( qi::lit('=') >> data_type_pm
           [ qi::_val = ast::binary_( qi::_a, qi:: _1, ast_::modifiers::temp_decl_arg_ ) ] ) ] //
         | qi::hold[ single_var_decl_pm
           [ qi::_val = ast::unary_( qi:: _1 , ast_::modifiers::temp_decl_arg_ ) ] ]
         | qi::hold[ single_const_decl_pm
           [ qi::_val = ast::unary_( qi:: _1 , ast_::modifiers::temp_decl_arg_ ) ] ]
         | macro_single_var_decl_pm
           [ qi::_val = ast::unary_( qi:: _1 , ast_::modifiers::temp_decl_arg_ ) ] ) */
         );

        template_decl_pm = qi::eps
         [ qi::_a = phoenix::construct< container_t >()]
         >> ( ( qi::eps( qi::_r1 == template_type::oper )
             >> keyword_parser_pm("template") )
             | - keyword_parser_pm("template") )
         >> qi::lit('<')
         >> ( ( qi::hold[ ( template_decl_arg_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ) ]
             | qi::eps[ phoenix::push_back( qi::_a, ast::tag_( ast_::ws::sepperator ) ) ] )
         % qi::lit(',') )
         >> qi::lit('>')
         >> qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::modifiers::temp_decl_ ) ]; //


        mutable_pm = keyword_parser_pm("mutable")
                  [ qi::_val = ast::tag_( ast_::modifiers::mutable_ ) ];//

        abstract_pm = keyword_parser_pm("abstract")
                  [ qi::_val = ast::tag_( ast_::modifiers::abstract_ ) ];//

        virtual_pm = keyword_parser_pm("virtual")
                  [ qi::_val = ast::tag_( ast_::modifiers::virtual_ ) ];//

        func_ident_mods_pm = qi::eps[ qi::_a = phoenix::construct< container_t >() ]
            >> *(
                qi::hold[ template_ident_pm(qi::_r1)
                [ phoenix::push_back( qi::_a, qi::_1 ) ] ]
               |qi::hold[ callconv_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
               |qi::hold[ lang_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
               |qi::hold[ attribute_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
               |qi::hold[ throw_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
               |qi::hold[ static_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
               | const_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
                )
            >> qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::modifiers::func_ident_mods ) ];

        func_decl_mods_pm = qi::eps[ qi::_a = phoenix::construct< container_t >() ]
            >> *( qi::hold[ genDecl_mod_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
               | qi::hold[ template_decl_pm(qi::_r1)
                 [ phoenix::push_back( qi::_a, qi::_1 ) ] ]
               | qi::hold[ throw_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
               | qi::hold[ const_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
               | qi::hold[ lang_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
               | qi::hold[ attribute_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
               | qi::hold[ static_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
               | qi::hold[ abstract_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
               | qi::hold[ virtual_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
               | callconv_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
            )
            >> qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::modifiers::func_decl_mods ) ]; //

        func_decl_arg_pm = ( qi::hold[ single_const_decl_pm[ qi::_a = qi::_1 ] ]
        | single_var_decl_pm[ qi::_a = qi::_1 ] )
        > (  qi::hold[ macro_assignment_expr_pm
        [ qi::_val = ast::binary_( qi::_a, qi::_1, ast_::modifiers::func_decl_arg_ ) ] ]
        | qi::eps[ qi::_val = ast::binary_( qi::_a, qi::_1, ast_::modifiers::func_decl_arg_ ) ] );

        func_decl_args_pm = qi::eps[ qi::_a = phoenix::construct< container_t >() ]
         > -( func_decl_arg_pm % qi::lit(',') )
         > qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::modifiers::func_decl_args_ ) ];

        // ast::tag_(  )

        explicit_pm = keyword_parser_pm("explicit")
               [ qi::_val = ast::tag_( ast_::modifiers::explicit_ ) ]; //

        ctor_decl_mods_pm = qi::eps[ qi::_a = phoenix::construct< container_t >() ]
         > *( qi::hold[ explicit_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
            | qi::hold[ genDecl_mod_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
            | qi::hold[ template_decl_pm(template_type::null_)
                 [ phoenix::push_back( qi::_a, qi::_1 ) ] ]
            | qi::hold[ throw_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
            | qi::hold[ lang_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
            | qi::hold[ attribute_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
            | callconv_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
            )
         > qi::eps[ qi::_val = ast::nary_( qi::_a , ast_::modifiers::ctor_decl_mods ) ];

        ctor_init_list_pm = qi::lit(':')
            [ qi::_a = phoenix::construct< container_t >() ]
          >> ctor_init_mem_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] % qi::lit(',')
          >> qi::eps[ qi::_val = ast::nary_( qi::_a , ast_::modifiers::ctor_init_list_ ) ];//

        ctor_init_mem_pm = qi::eps[ qi::_a = phoenix::construct< container_t >() ]
            >> identifier_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
            >> funcCall_expr_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
            >> -( macro_assignment_expr_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
            >> qi::eps[ qi::_val = ast::nary_( qi::_a , ast_::modifiers::ctor_init_mem_ ) ]; //

        type_infix_decl_mods_pm = qi::eps[ qi::_a = phoenix::construct< container_t >() ]
         > *( qi::hold[ genDecl_mod_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
            | qi::hold[ template_decl_pm(template_type::null_)
                 [ phoenix::push_back( qi::_a, qi::_1 ) ] ]
            | qi::hold[ lang_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
            | qi::hold[ attribute_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
            | qi::hold[ virtual_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
            | abstract_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
         > qi::eps[ qi::_val = ast::nary_( qi::_a , ast_::modifiers::type_infix_decl ) ];

        inherit_list_pm = qi::lit(':')
            [ qi::_a = phoenix::construct< container_t >() ]
            >> inherit_mem_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] % qi::lit(',')
            >> qi::eps[ qi::_val = ast::nary_( qi::_a , ast_::modifiers::inherit_list_ ) ]; //

        inherit_mem_pm = qi::eps[ qi::_a = phoenix::construct< container_t >() ]
            >> access_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
            >> identifier_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
            >> -macro_assignment_expr_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
            >> qi::eps[ qi::_val = ast::nary_( qi::_a , ast_::modifiers::inherit_mem_ ) ]; //

        dtor_decl_mods_pm = qi::eps[ qi::_a = phoenix::construct< container_t >() ]
         > *( qi::hold[ explicit_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
            | qi::hold[ genDecl_mod_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
            | qi::hold[ template_decl_pm(template_type::null_)
                 [ phoenix::push_back( qi::_a, qi::_1 ) ] ]
            | qi::hold[ lang_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
            | qi::hold[ attribute_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
            | qi::hold[ callconv_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
            | virtual_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
            )
         > qi::eps[ qi::_val = ast::nary_( qi::_a , ast_::modifiers::dtor_decl_mods ) ];

        access_pm = ( qi::hold[ keyword_parser_pm("public")
                     [ qi::_a = ast::tag_( ast_::modifiers::public_ ) ] ]//
                    | qi::hold[ keyword_parser_pm("protected")
                      [ qi::_a = ast::tag_( ast_::modifiers::protected_ ) ] ] //
                    | keyword_parser_pm("private")
                      [ qi::_a = ast::tag_( ast_::modifiers::private_ ) ] ) //
                 >> ( qi::hold[ virtual_pm
                 [ qi::_val = ast::binary_(  qi::_a, qi::_1, ast_::modifiers::access_ ) ] ] //
                 | qi::eps[ qi::_val = ast::unary_( qi::_a , ast_::modifiers::access_ ) ] ); //

        data_decl_pm = qi::eps[ qi::_a = phoenix::construct< container_t >() ]
          >> *( qi::hold[ keyword_parser_pm("const")
                 > ( qi::hold[ qi::lit('&')
              [ phoenix::push_back( qi::_a, ast::tag_( ast_::modifiers::const_ref_ ) ) ] ]
                  | qi::lit('*')
              [ phoenix::push_back( qi::_a, ast::tag_( ast_::modifiers::const_ptr_ ) ) ] ) ]
          // | qi::hold[  ]
           | qi::hold[ keyword_parser_pm("var")
                 > ( qi::hold[ qi::lit('&')
                 [ phoenix::push_back( qi::_a, ast::tag_( ast_::modifiers::var_ref_ ) ) ] ]
                  | qi::lit('*')
                 [ phoenix::push_back( qi::_a, ast::tag_( ast_::modifiers::var_ptr_ ) ) ] ) ]
           | qi::hold[ genDecl_mod_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
           | qi::hold[ lang_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
           | qi::hold[ attribute_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
           | qi::hold[ static_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
           | qi::hold[ volatile_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
           | mutable_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
           >> qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::modifiers::data_decl ) ];

        // expression: // ast_::expressions::
        this_expr_pm = qi::hold[ keyword_parser_pm("this")
         [ qi::_val = ast::tag_( ast_::expressions::this_ ) ] ]
                       | keyword_parser_pm("self")
         [ qi::_val = ast::tag_( ast_::expressions::self_ ) ] ;

        identifier_pm = qi::eps[ qi::_a = phoenix::construct< container_t >() ]
         >> -( nested_name_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
         >> ( qi::hold[ qualified_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
            | unqualified_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
         >> qi::eps[ qi::_val = ast::nary_( qi::_a , ast_::expressions::ident ) ];

        prime_expr_pm %= qi::hold[ this_expr_pm ]
                       | qi::hold[ litteral_pm(char_used::value) ]
                       | qi::hold[ identifier_pm ]
                       | ( qi::lit('(')
                         > sequence_expr_pm[ qi::_val = qi::_1 ]
                         > qi::lit(')') );

        array_expr_pm = qi::lit('[')
         >> sequence_expr_pm[ qi::_val = ast::unary_( qi::_1 , ast_::expressions::array ) ]
         >> qi::lit(']');

        funcCall_expr_pm = qi::lit('(')[ qi::_a = phoenix::construct< container_t >() ]
         >> ( ( qi::hold[ assignment_expr_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
           | qi::eps[ phoenix::push_back( qi::_a, ast::tag_( ast_::ws::sepperator ) ) ] )
            % qi::lit(',') )
         >> qi::lit(')')[ qi::_val = ast::nary_( qi::_a , ast_::expressions::funcCall ) ];

        expl_ctor_expr_pm = qi::hold[ ( qi::lit("::")
             >> keyword_parser_pm("constructor")
             [ qi::_val = ast::tag_( ast_::expressions::ctorType ) ] ) ]
                          | qi::hold[ ( qi::lit('.')
             >> keyword_parser_pm("constructor")
             [ qi::_val = ast::tag_( ast_::expressions::ctorRef ) ] ) ]
                          | ( qi::lit("->")
             >> keyword_parser_pm("constructor")
             [ qi::_val = ast::tag_( ast_::expressions::ctorPtr ) ] ) ;

        memberRef_expr_pm = qi::lit('.') >>
          ( qi::hold[ new_expr_pm[ qi::_val = ast::unary_( qi::_1 , ast_::expressions::memberRef ) ] ]
          | qi::hold[ del_expr_pm[ qi::_val = ast::unary_( qi::_1 , ast_::expressions::memberRef ) ] ]
          | identifier_pm[ qi::_val = ast::unary_( qi::_1 , ast_::expressions::memberRef ) ] );

        memberPtr_expr_pm = qi::lit("->") >>
          ( qi::hold[ new_expr_pm[ qi::_val = ast::unary_( qi::_1 , ast_::expressions::memberPtr ) ] ]
          | qi::hold[ del_expr_pm[ qi::_val = ast::unary_( qi::_1 , ast_::expressions::memberPtr ) ] ]
          | identifier_pm[ qi::_val = ast::unary_( qi::_1 , ast_::expressions::memberPtr ) ] );

        expl_dtor_expr_pm = ( qi::hold[ ( qi::lit('.') >> keyword_parser_pm("destructor")
        [ qi::_val = ast::tag_( ast_::expressions::dtorRef ) ] ) ]
          | ( qi::lit("->") >> keyword_parser_pm("destructor")
        [ qi::_val = ast::tag_( ast_::expressions::dtorPtr ) ] ) );

        postfix_expr_pm = qi::eps[ qi::_a = phoenix::construct< container_t >() ]
         >> prime_expr_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > *( qi::hold[ array_expr_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
             | qi::hold[ funcCall_expr_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
             | qi::hold[ expl_ctor_expr_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
             | qi::hold[ memberRef_expr_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
             | qi::hold[ memberPtr_expr_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
             | qi::hold[ expl_dtor_expr_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
             | qi::hold[ qi::lit("++")
             [ phoenix::push_back( qi::_a, ast::tag_( ast_::expressions::postIncrement ) ) ] ] //
             | qi::lit("--")
             [ phoenix::push_back( qi::_a, ast::tag_( ast_::expressions::postDecrement ) ) ] ) //
         > qi::eps[ qi::_val = ast::nary_( qi::_a , ast_::expressions::postfix ) ];

        new_expr_pm = keyword_parser_pm("new")[ qi::_a = phoenix::construct< container_t >() ]
         > -( funcCall_expr_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
         > data_type_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > -( qi::lit("::") >> keyword_parser_pm("constructor")
            [ phoenix::push_back( qi::_a, ast::tag_( ast_::expressions::ctorType ) ) ] )
         > funcCall_expr_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > qi::eps[ qi::_val = ast::nary_( qi::_a , ast_::expressions::new_ ) ]; //

        del_expr_pm = keyword_parser_pm("delete")[ qi::_a = phoenix::construct< container_t >() ]
         > -( funcCall_expr_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
         > *( qi::lit('[')
         > ( qi::hold[ sequence_expr_pm
          [ phoenix::push_back( qi::_a, ast::unary_( qi::_1 , ast_::expressions::delArray ) ) ] ]
          | qi::eps[ phoenix::push_back( qi::_a, ast::tag_( ast_::expressions::delArray ) ) ] )
         > qi::lit(']') )
         > unary_expr_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > qi::eps[ qi::_val = ast::nary_( qi::_a , ast_::expressions::del ) ]; //

        unary_expr_pm = qi::eps[ qi::_a = phoenix::construct< container_t >() ]
         >> ( qi::hold[ ( *(unary_expr_sym_pm[ phoenix::push_back( qi::_a, ast::tag_( qi::_1 ) ) ] )
         > ( qi::hold[ postfix_expr_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
            | new_expr_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ) ) ]
         | del_expr_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
         > qi::eps[ qi::_val = ast::nary_( qi::_a , ast_::expressions::unary ) ];

        ptrMem_expr_pm = unary_expr_pm[ qi::_a = qi::_1 ]
         > *( qi::hold[ ( qi::lit("->*")
               > unary_expr_pm[ qi::_a = ast::binary_( qi::_a, qi::_1, ast_::expressions::ptrMem_ptr ) ] ) ]
            | ( qi::lit(".*")
               > unary_expr_pm[ qi::_a = ast::binary_( qi::_a, qi::_1, ast_::expressions::ptrMem_ref ) ] ) )
         > qi::eps[ qi::_val = qi::_a ]; //

        multiply_expr_pm = ptrMem_expr_pm[ qi::_a = qi::_1 ]
         > *( qi::hold[ ( qi::lit('*') > ptrMem_expr_pm
         [ qi::_a = ast::binary_( qi::_a, qi::_1, ast_::expressions::multiply ) ] ) ] //
            | qi::hold[ ( qi::lit('/') > ptrMem_expr_pm
         [ qi::_a = ast::binary_( qi::_a, qi::_1, ast_::expressions::division ) ] //
            | ( qi::lit('%') > ptrMem_expr_pm
         [ qi::_a = ast::binary_( qi::_a, qi::_1, ast_::expressions::modulus ) ] ) ) //
         > qi::eps[ qi::_val = qi::_a ];

        addition_expr_pm = multiply_expr_pm[ qi::_a = qi::_1 ]
         > *( qi::hold[ ( qi::lit('+') > multiply_expr_pm
         [ qi::_a = ast::binary_( qi::_a, qi::_1, ast_::expressions::addition ) ] ) ] //
            | ( qi::lit('-') > multiply_expr_pm
         [ qi::_a = ast::binary_( qi::_a, qi::_1, ast_::expressions::subtraction ) ] ) ) //
         > qi::eps[ qi::_val = qi::_a ];

        // code snipet for reuse ( for binaryexpressions ):
        // qi::hold[ ( qi::lit() >  ) ]
        // [ qi::_a = ast::binary_( qi::_a, qi::_1, ast_::expressions:: ) ]

        bitshift_expr_pm = addition_expr_pm[ qi::_a = qi::_1 ]
         > *( qi::hold[ ( qi::lit("<<") > addition_expr_pm
         [ qi::_a = ast::binary_( qi::_a, qi::_1, ast_::expressions::shiftLeft ) ] ) ] //
            | qi::hold[ ( qi::lit(">>") > addition_expr_pm
         [ qi::_a = ast::binary_( qi::_a, qi::_1, ast_::expressions::shiftRightArith ) ] ) ] //
            | ( qi::lit(">>>") > addition_expr_pm
         [ qi::_a = ast::binary_( qi::_a, qi::_1, ast_::expressions::shiftRightLogic ) ] ) ) //
         > qi::eps[ qi::_val = qi::_a ];

        relational_expr_pm = bitshift_expr_pm[ qi::_a = qi::_1 ]
         > *( qi::hold[ ( qi::lit('<') > bitshift_expr_pm
         [ qi::_a = ast::binary_( qi::_a, qi::_1, ast_::expressions::lesser ) ] ) ] //
            | qi::hold[ ( qi::lit('>') > bitshift_expr_pm
         [ qi::_a = ast::binary_( qi::_a, qi::_1, ast_::expressions::greater ) ] ) ] //
            | qi::hold[ ( qi::lit("<=") > bitshift_expr_pm
         [ qi::_a = ast::binary_( qi::_a, qi::_1, ast_::expressions::lesserEqual ) ] ) ] //
            | ( qi::lit(">=") > bitshift_expr_pm
         [ qi::_a = ast::binary_( qi::_a, qi::_1, ast_::expressions::greaterEqual ) ] ) ) //
         > qi::eps[ qi::_val = qi::_a ];

        equality_expr_pm = relational_expr_pm[ qi::_a = qi::_1 ]
         > *( qi::hold[ ( qi::lit("==") > relational_expr_pm
         [ qi::_a = ast::binary_( qi::_a, qi::_1, ast_::expressions::equal ) ] ) ] //
            | ( qi::lit("!=") > relational_expr_pm
         [ qi::_a = ast::binary_( qi::_a, qi::_1, ast_::expressions::notEqual ) ] ) ) //
         > qi::eps[ qi::_val = qi::_a ];

        bitwiseAnd_expr_pm = equality_expr_pm[ qi::_a = qi::_1 ]
         > *( qi::lit('&') > equality_expr_pm
         [ qi::_a = ast::binary_( qi::_a, qi::_1, ast_::expressions::bitwiseAnd ) ] )
         > qi::eps[ qi::_val = qi::_a ];

        bitwiseXor_expr_pm = bitwiseAnd_expr_pm[ qi::_a = qi::_1 ]
         > *( qi::lit('^') > bitwiseAnd_expr_pm
         [ qi::_a = ast::binary_( qi::_a, qi::_1, ast_::expressions::bitwiseXor ) ] )
         > qi::eps[ qi::_val = qi::_a ];

        bitwiseOr_expr_pm = bitwiseXor_expr_pm[ qi::_a = qi::_1 ]
         > *( qi::lit('|') > bitwiseXor_expr_pm
         [ qi::_a = ast::binary_( qi::_a, qi::_1, ast_::expressions::bitwiseOr ) ] )
         > qi::eps[ qi::_val = qi::_a ];

        logicalAnd_expr_pm = bitwiseOr_expr_pm[ qi::_a = qi::_1 ]
         > *( qi::lit("&&") > bitwiseOr_expr_pm
         [ qi::_a = ast::binary_( qi::_a, qi::_1, ast_::expressions::logicalAnd ) ] )
         > qi::eps[ qi::_val = qi::_a ];

        logicalXor_expr_pm = logicalAnd_expr_pm[ qi::_a = qi::_1 ]
         > *( qi::lit("^^") > logicalAnd_expr_pm
         [ qi::_a = ast::binary_( qi::_a, qi::_1, ast_::expressions::logicalXor ) ] )
         > qi::eps[ qi::_val = qi::_a ];

        logicalOr_expr_pm = logicalXor_expr_pm[ qi::_a = qi::_1 ]
         > *( qi::lit("||") > logicalXor_expr_pm
         [ qi::_a = ast::binary_( qi::_a, qi::_1, ast_::expressions::logicalOr ) ] )
         > qi::eps[ qi::_val = qi::_a ];

        conditional_expr_pm = qi::eps[ qi::_a = phoenix::construct< container_t >() ]
         > logicalOr_expr_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > -( qi::lit('?')
            > ( qi::hold[ assignment_expr_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
            | qi::eps[ phoenix::push_back( qi::_a, ast::tag_( ast_::ws::sepperator ) ) ] )
            // old version: "| qi::eps[ phoenix::push_back( qi::_a, ast::tag_( ast_::expressions::conditional_q_sep ) ) ]"
            > qi::lit(':')
            > ( qi::hold[ assignment_expr_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
            | qi::eps[ phoenix::push_back( qi::_a, ast::tag_( ast_::ws::sepperator ) ) ] )
            // old version: | qi::eps[ phoenix::push_back( qi::_a, ast::tag_( ast_::expressions::conditional_c_sep ) ) ]
            )
         > qi::eps[ qi::_val = ast::nary_( qi::_a , ast_::expressions::conditional ) ];

         // | qi::eps[ phoenix::push_back( qi::_a, ast::tag_( ast_::ws::sepperator ) ) ]

        assignment_expr_pm = ( qi::hold[ ( logicalOr_expr_pm[ qi::_a = qi::_1 ]
         > *( assignment_expr_sym_pm[ qi::_b = qi::_1 ]
          > conditional_expr_pm
          [ qi::_a = ast::binary_( qi::_a, qi::_1, qi::_b ) ] ) ) ]
         | qi::hold[ conditional_expr_pm[ qi::_a = qi::_1 ] ]
         | throw_expr_pm[ qi::_a = qi::_1 ] )
         > qi::eps[ qi::_val = qi::_a ]; //

        sequence_expr_pm = assignment_expr_pm[ qi::_a = qi::_1 ]
         > *( qi::lit(',') > assignment_expr_pm
         [ qi::_a = ast::binary_( qi::_a, qi::_1, ast_::expressions::sequence ) ] )
         > qi::eps[ qi::_val = qi::_a ];

        // code snipet for reuse ( for binaryexpressions ):
        // qi::hold[ ( qi::lit() >  ) ]
        // [ qi::_a = ast::binary_( qi::_a, qi::_1, ast_::expressions:: ) ]

        throw_expr_pm = keyword_parser_pm("throw")
         > ( qi::hold[ sequence_expr_pm[ qi::_val = ast::unary_( qi::_1, ast_::expressions::throw_ ) ] ]
         | qi::eps[ qi::_val = ast::tag_( ast_::expressions::throw_ ) ] ) ;

        // | qi::eps[ phoenix::push_back( qi::_a, ast::tag_( ast_::ws::sepperator ) ) ]

        condition_expr_pm = qi::eps[ qi::_a = phoenix::construct< container_t >() ]
         > ( qi::hold[ single_var_decl_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
           | qi::hold[ single_const_decl_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
           | assignment_expr_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
           % qi::lit(',')
         > qi::eps[ qi::_val = ast::nary_( qi::_a , ast_::expressions::condition ) ];

        // statements: // ast_::statements::

        expression_state_pm = ( qi::hold[
         sequence_expr_pm[ qi::_val = ast::unary_( qi::_1, ast_::statements::expression ) ] ]
         | qi::eps[ qi::_val = ast::tag_( ast_::statements::expression ) ] )
         >  qi::lit(';');

        declaration_state_pm %= qi::hold[ var_decl_pm ]
                              | qi::hold[ const_decl_pm ]
                              | type_decl_pm;

        switch_state_pm = keyword_parser_pm("switch") > qi::lit('(')
         > condition_expr_pm[ qi::_a = qi::_1 ] > qi::lit(')')
         > statements_pm
         [ qi::_val = ast::binary_( qi::_a , qi::_1 , ast_::statements::switch_ ) ];

        if_state_pm = keyword_parser_pm("if") > qi::lit('(')
         > condition_expr_pm[ qi::_a = qi::_1 ] > qi::lit(')')
         > statements_pm
         [ qi::_val = ast::binary_( qi::_a , qi::_1 , ast_::statements::if_ ) ];

        elif_state_pm = keyword_parser_pm("elif") > qi::lit('(')
         > condition_expr_pm[ qi::_a = qi::_1 ] > qi::lit(')')
         > statements_pm
         [ qi::_val = ast::binary_( qi::_a , qi::_1 , ast_::statements::elif_ ) ];

        else_state_pm = keyword_parser_pm("else") > statements_pm
         [ qi::_val = ast::unary_( qi::_1 , ast_::statements::else_ ) ];

        select_state_pm = qi::hold[ switch_state_pm[ qi::_val = qi::_1 ] ]
          | ( qi::eps[ qi::_a = phoenix::construct< container_t >() ]
          > if_state_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
          > *( elif_state_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
          > else_state_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
          > qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::statements::select ) ] );

        label_state_pm = keyword_parser_pm("label")
         > basic_ident_pm[ qi::_a = qi::_1 ]
         > qi::lit(':') > statements_pm
         [ qi::_val = ast::binary_( qi::_a , qi::_1 , ast_::statements::label ) ];

        case_state_pm = keyword_parser_pm("case")
         > sequence_expr_pm[ qi::_a = qi::_1 ]
         > qi::lit(':') > statements_pm
         [ qi::_val = ast::binary_( qi::_a , qi::_1 , ast_::statements::case_ ) ];

        default_state_pm = keyword_parser_pm("default") > qi::lit(':')
         > statements_pm
         [ qi::_val = ast::unary_( qi::_1 , ast_::statements::default_ ) ];

        destination_state_pm = ( qi::hold[ case_state_pm[ qi::_val = qi::_1 ] ]
                               | qi::hold[ default_state_pm[ qi::_val = qi::_1 ] ]
                               | label_state_pm[ qi::_val = qi::_1 ] )
         > qi::lit(';');

        break_state_pm = keyword_parser_pm("break")
         > ( qi::hold[ label_ident_pm
         [ qi::_val = ast::unary_( qi::_1 , ast_::statements::break_ ) ] ]
           | basic_ident_pm
         [ qi::_val = ast::unary_( qi::_1 , ast_::statements::break_ ) ] );

        continue_state_pm = keyword_parser_pm("continue")
         > ( qi::hold[ label_ident_pm
         [ qi::_val = ast::unary_( qi::_1 , ast_::statements::continue_ ) ] ]
           | basic_ident_pm
         [ qi::_val = ast::unary_( qi::_1 , ast_::statements::continue_ ) ] );

        return_state_pm = keyword_parser_pm("return")
         > ( qi::hold[ sequence_expr_pm
         [ qi::_val = ast::unary_( qi::_1 , ast_::statements::return_ ) ] ]
           | qi::eps[ qi::_val = ast::tag_( ast_::statements::return_ ) ] );

        jump_state_pm = ( qi::hold[ break_state_pm[ qi::_val = qi::_1 ] ]
                        | qi::hold[ continue_state_pm[ qi::_val = qi::_1 ] ]
                        | qi::hold[ return_state_pm[ qi::_val = qi::_1 ] ]
                        | return_state_pm[ qi::_val = qi::_1 ]  )
         > qi::lit(';');

        compound_state_pm = qi::lit('{')[ qi::_a = phoenix::construct< container_t >() ]
         > *(statements_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
         > qi::lit('}')[ qi::_val = ast::nary_( qi::_a, ast_::statements::compound_ ) ];

        try_state_pm = keyword_parser_pm("try")
         > compound_state_pm
        [ qi::_val = ast::unary_( qi::_1 , ast_::statements::try_ ) ];

        catch_state_pm = keyword_parser_pm("catch")
         > qi::lit('(')
         > ( qi::hold[ single_var_decl_pm[ qi::_a = qi::_1 ] ]
           | single_const_decl_pm[ qi::_a = qi::_1 ] )
         > qi::lit(')')
         > compound_state_pm
        [ qi::_val = ast::binary_( qi::_a , qi::_1 , ast_::statements::catch_ ) ];

        finally_state_pm = keyword_parser_pm("finally")
         > compound_state_pm
        [ qi::_val = ast::unary_( qi::_1 , ast_::statements::finally ) ];

        exception_state_pm = qi::eps[ qi::_a = phoenix::construct< container_t >() ]
         > try_state_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > *( catch_state_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
         > -( finally_state_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
         > qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::statements::exception_ ) ];

        for_state_pm = keyword_parser_pm("for")
         [ qi::_a = phoenix::construct< container_t >() ]
         > qi::lit('(')
         > -( condition_expr_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
         > qi::lit(',')
         > -( conditional_expr_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
         > qi::lit(',')
         > -( sequence_expr_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
         > qi::lit(')')
         > statements_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::statements::for_ ) ];

        while_state_pm = keyword_parser_pm("while")
         > qi::lit('(')
         > condition_expr_pm[ qi::_a = qi::_1 ]
         > qi::lit(')')
         > statements_pm
        [ qi::_val = ast::binary_( qi::_a , qi::_1 , ast_::statements::while_ ) ];

        do_state_pm = keyword_parser_pm("do")
         > statements_pm[ qi::_a = qi::_1 ]
         > keyword_parser_pm("while") > qi::lit('(')
         > conditional_expr_pm
         [ qi::_val = ast::binary_( qi::_a , qi::_1 , ast_::statements::do_ )
         > qi::lit(')') > qi::lit(';');

        loop_state_pm %= qi::hold[ for_state_pm ]
                      | qi::hold[ while_state_pm ]
                      | do_state_pm;

        using_state_pm = keyword_parser_pm("using")[ qi::_a = phoenix::construct< container_t >() ]
         > using_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] % qi::lit(',')
         > qi::lit(';')[ qi::_val = ast::nary_( qi::_a, ast_::statements::using_ ) ];

        namespace_alias_pm = keyword_parser_pm("namespace")
         [ qi::_a = phoenix::construct< container_t >() ]
         > basic_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > qi::lit('=')
         > -( nested_name_for_namespace_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
         > ( qi::hold[ namespace_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
           | basic_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
         > qi::lit(';')[ qi::_val = ast::nary_( qi::_a, ast_::statements::namespace_alias ) ];

        statements_pm %= qi::hold[ declaration_state_pm ]
                       | qi::hold[ expression_state_pm ]
                       | qi::hold[ select_state_pm ]
                       | qi::hold[ destination_state_pm ]
                       | qi::hold[ jump_state_pm ]
                       | qi::hold[ compound_state_pm ]
                       | qi::hold[ exception_state_pm ]
                       | qi::hold[ loop_state_pm ]
                       | qi::hold[ using_state_pm ]
                       | qi::hold[ namespace_alias_pm ]
                       | macro_statement_pm ;

        // declarations:
        data_instance_decl_pm = qi::eps[ qi::_a = phoenix::construct< container_t >() ]
         >> data_decl_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > basic_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > type_postfix_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > -( qi::hold[ qi::lit('=') > assignment_expr_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
            | funcCall_expr_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
         > qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::declarations::data_instance ) ];

        single_var_decl_pm = keyword_parser_pm("var")
         > data_type_pm[ qi::_a = qi::_1 ]
         > data_instance_decl_pm[ qi::_val = ast::binary_( qi::_a, qi::_1, ast_::declarations::single_var ) ];

        var_decl_pm = keyword_parser_pm("var")[ qi::_a = phoenix::construct< container_t >() ]
         > data_type_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > ( data_instance_decl_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
           % qi::lit(',') )
         > qi::lit(';')[ qi::_val = ast::nary_( qi::_a, ast_::declarations::var_decl ) ];

        single_const_decl_pm = keyword_parser_pm("const")
         > data_type_pm[ qi::_a = qi::_1 ]
         > data_instance_decl_pm[ qi::_val = ast::binary_( qi::_a, qi::_1, ast_::declarations::single_const ) ];

        const_decl_pm = keyword_parser_pm("const")[ qi::_a = phoenix::construct< container_t >() ]
         > data_type_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > ( data_instance_decl_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
           % qi::lit(',') )
         >  qi::lit(';')[ qi::_val = ast::nary_( qi::_a, ast_::declarations::const_decl ) ];

        single_type_decl_pm = keyword_parser_pm("typedef")
         > data_type_pm[ qi::_a = qi::_1 ]
         > data_instance_decl_pm[ qi::_val = ast::binary_( qi::_a, qi::_1, ast_::declarations::single_type ) ];

        type_decl_pm = keyword_parser_pm("typedef")[ qi::_a = phoenix::construct< container_t >() ]
         > data_type_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > ( data_instance_decl_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
           % qi::lit(',') )
         > qi::lit(';')[ qi::_val = ast::nary_( qi::_a, ast_::declarations::type_decl ) ];

        import_sys_str_pm = qi::lit('<')
         [ qi::_a = phoenix::construct< std::basic_string<char> >() ]
         > +( qi::hold[ qi::lit("\\>")[ phoenix::push_back( qi::_a, '>' ) ] ]
            | (esc_pm - qi::lit('>') )[ phoenix::push_back( qi::_a, qi::_1 ) ] )
         > qi::lit('>')[ qi::_val = ast::value_( qi::_a, ast_::declarations::sys_import ) ];

        import_decl_pm = keyword_parser_pm("import")[ qi::_a = phoenix::construct< container_t >() ]
         > basic_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > -( macro_assignment_expr_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
         > ( ( qi::hold[ import_sys_str_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
           | str_lit_pm(char_used::value)[ phoenix::push_back( qi::_a, qi::_1 ) ] )
           > -( macro_assignment_expr_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ) )
           % qi::lit(',')
         > qi::lit(';')[ qi::_val = ast::nary_( qi::_a, ast_::declarations::import_ ) ];

        compound_decl_pm = qi::lit('{')[ qi::_a = phoenix::construct< container_t >() ]
         > *( member_decl_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ) > qi::lit('}')
         > qi::lit(';')[ qi::_val = ast::nary_( qi::_a, ast_::declarations::compound_ ) ];

        nested_name_for_namespace_pm = qi::eps[ qi::_a = phoenix::construct< container_t >() ]
         > ( qi::hold[ import_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
                      >> qi::lit("::") ]
           | qi::hold[ basic_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
                      >> qi::lit("::") ]
           | qi::lit("::")
              [ phoenix::push_back( qi::_a, ast::tag_( ast_::ws::sepperator ) ) ] )
         > *( qi::hold[ namespace_ident_pm[
                      phoenix::push_back( qi::_a, qi::_1 ) ] >> qi::lit("::") ]
                   | qi::hold[ basic_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
                     >> qi::lit("::") ]
                   | qi::lit("::")
              [ phoenix::push_back( qi::_a, ast::tag_( ast_::ws::sepperator ) ) ] )
         > qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::declarations::nested_name_for_namespace ) ];

        namespace_fwd_decl_pm = qi::eps[ qi::_a = phoenix::construct< container_t >() ]
         > -( nested_name_for_namespace_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
         > keyword_parser_pm("namespace")
         > *( qi::hold[ genDecl_mod_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
            | qi::hold[ lang_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
            | attribute_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
         > -( basic_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
         > -( macro_assignment_expr_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
         > qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::declarations::namespace_fwd ) ];

        namespace_decl_pm = namespace_fwd_decl_pm[ qi::_a = qi::_1 ]
         > compound_decl_pm
         [ qi::_val = ast::binary_( qi::_a, qi::_1, ast_::declarations::namespace_ ) ] ;

        function_fwd_decl_pm = qi::eps[ qi::_a = phoenix::construct< container_t >() ]
         > -( nested_name_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
         > keyword_parser_pm("function")
         > func_decl_mods_pm(template_type::null_)[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > basic_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > qi::lit('(')
         > func_decl_args_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > qi::lit(')') > qi::lit(':')
         > func_decl_arg_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > -macro_assignment_expr_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::declarations::function_fwd ) ];

        function_decl_pm = function_fwd_decl_pm[ qi::_a = qi::_1 ]
         > ( qi::hold[ compound_state_pm
        [ qi::_val = ast::binary_( qi::_a, qi::_1, ast_::declarations::function_ ) ] ]
         | exception_state_pm
        [ qi::_val = ast::binary_( qi::_a, qi::_1, ast_::declarations::function_ ) ] );

        operator_fwd_decl_pm = qi::eps[ qi::_a = phoenix::construct< container_t >() ]
         > -( nested_name_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
         > keyword_parser_pm("operator")
         > func_decl_mods_pm(template_type::oper)[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > basic_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > qi::lit('(')
         > func_decl_args_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > qi::lit(')') > qi::lit(':')
         > func_decl_arg_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > -macro_assignment_expr_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::declarations::operator_fwd ) ];

        operator_decl_pm = operator_fwd_decl_pm[ qi::_a = qi::_1 ]
         > ( qi::hold[ compound_state_pm
        [ qi::_val = ast::binary_( qi::_a, qi::_1, ast_::declarations::operator_ ) ] ]
         | exception_state_pm
        [ qi::_val = ast::binary_( qi::_a, qi::_1, ast_::declarations::function_ ) ] ) ;


        constructor_fwd_decl_pm = qi::eps[ qi::_a = phoenix::construct< container_t >() ]
         > -( nested_name_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
         > keyword_parser_pm("constructor")
         > ctor_decl_mods_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > qi::lit('(')
         > func_decl_args_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > qi::lit(')')
         > -macro_assignment_expr_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::declarations::constructor_fwd ) ]; // ctor_decl_mods_pm

        constructor_decl_pm = qi::eps[ qi::_a = phoenix::construct< container_t >() ]
         > constructor_fwd_decl_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > -( ctor_init_list_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
         > ( qi::hold[ compound_state_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
           | exception_state_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
         > qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::declarations::constructor_ ) ];

        destructor_fwd_decl_pm = qi::eps[ qi::_a = phoenix::construct< container_t >() ]
         > -( nested_name_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
         > keyword_parser_pm("destructor")
         > dtor_decl_mods_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > qi::lit('(')
         /* > func_decl_args_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] */
         // - there is no point for this, as destructors *don`t* take arguments
         //  and that is simply because they can`t! they come into situations where arguments *can`t*
         //  be provided sometimes!!!!
         > qi::lit(')')
         > -macro_assignment_expr_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         [ qi::_val = ast::nary_( qi::_a, ast_::declarations::destructor_fwd ) ];

        destructor_decl_pm = destructor_fwd_decl_pm[ qi::_a = qi::_1 ]
         > compound_state_pm
        [ qi::_val = ast::binary_( qi::_a, qi::_1, ast_::declarations::destructor_ ) ];

        bitfield_fwd_decl_pm = qi::eps[ qi::_a = phoenix::construct< container_t >() ]
         > -( nested_name_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
         > keyword_parser_pm("bitfield")
         > type_infix_decl_mods_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > basic_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > -macro_assignment_expr_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::declarations::bitfield_fwd ) ];

        bitfield_decl_pm = qi::eps[ qi::_a = phoenix::construct< container_t >() ]
         > bitfield_fwd_decl_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > -( inherit_list_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
         > compound_decl_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::declarations::bitfield_fwd ) ];

        enum_fwd_decl_pm = qi::eps[ qi::_a = phoenix::construct< container_t >() ]
         > -( nested_name_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
         > keyword_parser_pm("enum")
         > type_infix_decl_mods_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > basic_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > -macro_assignment_expr_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::declarations::enum_fwd ) ];

        enumerator_pm = qi::eps[ qi::_a = phoenix::construct< container_t >() ]
         > basic_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > ( qi::hold[ qi::lit('=') > assignment_expr_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
           | qi::hold[ funcCall_expr_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
           | qi::eps )
         > -macro_assignment_expr_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::declarations::enumerator ) ];

        enum_decl_pm = qi::eps[ qi::_a = phoenix::construct< container_t >() ]
         > enum_fwd_decl_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > qi::lit('{')
         > ( enumerator_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
         % qi::lit(',')
         > qi::lit('}') > qi::lit(';')
         > qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::declarations::enum_ ) ];

        object_fwd_decl_pm = qi::eps[ qi::_a = phoenix::construct< container_t >() ]
         > -( nested_name_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
         > keyword_parser_pm("object")
         > type_infix_decl_mods_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > basic_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > -macro_assignment_expr_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::declarations::object_fwd ) ];

        object_decl_pm = qi::eps[ qi::_a = phoenix::construct< container_t >() ]
         > object_fwd_decl_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > -( inherit_list_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
         > compound_decl_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::declarations::object_ ) ];

        union_fwd_decl_pm = qi::eps[ qi::_a = phoenix::construct< container_t >() ]
         > -( nested_name_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
         > keyword_parser_pm("union")
         > type_infix_decl_mods_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > basic_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > -macro_assignment_expr_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::declarations::union_fwd ) ];

        union_decl_pm = qi::eps[ qi::_a = phoenix::construct< container_t >() ]
         > union_fwd_decl_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > -( inherit_list_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
         > compound_decl_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::declarations::union_ ) ];

        forward_decl_pm = ( qi::hold[ namespace_fwd_decl_pm[ qi::_val = qi::_1 ] ]
         | qi::hold[ function_fwd_decl_pm[ qi::_val = qi::_1 ] ]
         | qi::hold[ operator_fwd_decl_pm[ qi::_val = qi::_1 ] ]
         | qi::hold[ constructor_fwd_decl_pm[ qi::_val = qi::_1 ] ]
         | qi::hold[ destructor_fwd_decl_pm[ qi::_val = qi::_1 ] ]
         | qi::hold[ bitfield_fwd_decl_pm[ qi::_val = qi::_1 ] ]
         | qi::hold[ enum_fwd_decl_pm[ qi::_val = qi::_1 ] ]
         | qi::hold[ object_fwd_decl_pm[ qi::_val = qi::_1 ] ]
         | union_fwd_decl_pm[ qi::_val = qi::_1 ]
         );

        friend_decl_pm = keyword_parser_pm("friend")
         > forward_decl_pm[ qi::_val = ast::unary_( qi::_1, ast_::declarations::friend_ ) ]
         > qi::lit(';');

        member_decl_pm = qi::hold[ ( access_pm[ qi::_a = qi::_1 ]
         > declaration_pm
        [ qi::_val = ast::binary_( qi::_a , qi::_1 , ast_::declarations::member_ ) ] ) ]
         | qi::hold[ friend_decl_pm[ qi::_val = qi::_1 ] ]
         | qi::hold[ using_state_pm[ qi::_val = qi::_1 ] ]
         | macro_statement_pm[ qi::_val = qi::_1 ] ;

        declaration_pm %= qi::hold[ var_decl_pm ]
                        | qi::hold[ const_decl_pm ]
                        | qi::hold[ type_decl_pm ]
                        | qi::hold[ import_decl_pm ]
                        | qi::hold[ namespace_decl_pm ]
                        | qi::hold[ function_decl_pm ]
                        | qi::hold[ operator_decl_pm ]
                        | qi::hold[ constructor_decl_pm ]
                        | qi::hold[ destructor_decl_pm ]
                        | qi::hold[ bitfield_decl_pm ]
                        | qi::hold[ enum_decl_pm ]
                        | qi::hold[ object_decl_pm ]
                        | qi::hold[ union_decl_pm ]
                  //old:  | qi::hold[ friend_decl_pm ]
                        | qi::hold[ forward_decl_pm > qi::lit(';') ]
                        | qi::hold[ using_state_pm ]
                        | qi::hold[ namespace_alias_pm ]
                        | qi::hold[ macro_statement_pm ]
                        | macro_declaration_pm ;

        // macros:
        // macro identifiers:

        macro_basic_ident_pm = qi::eps[ qi::_a = phoenix::construct< std::string >() ]
          >> ( qi::lexeme[ ( ( qi::hold[ qi::standard::alpha
                           [ phoenix::push_back( qi::_a, qi::_1 ) ] ]
               | qi::standard::char_('_')[ phoenix::push_back( qi::_a, '_' ) ] )
               >> *( qi::hold[ qi::standard::alnum[
                               phoenix::push_back( qi::_a, qi::_1 ) ] ]
                  | qi::standard::char_('_')[ phoenix::push_back( qi::_a, '_' ) ] ) ) ] )
          >> qi::eps[ qi::_val = ast::value_( qi::_a, ast_::macro_identifiers::basic_ ) ];

        macro_var_ident_pm = qi::lexeme[ qi::lit('$')
         > ( qi::hold[ keyword_parser_pm("system")
        [ qi::_val = ast::tag_( ast_::macro_identifiers::sys_var ) ] ]
         | macro_basic_ident_pm
        [ qi::_val = ast::unary_( qi::_1 , ast_::macro_identifiers:: ) ] ];

        macro_func_ident_pm = qi::lit('@')
         > macro_func_mod_pm(template_type::null_)[ qi::_a = qi::_1 ]
         > ( qi::hold[ builtin_macro_func_sym_pm
        [ qi::_val = ast::binary_( qi::_a ,
           ast::value_( qi::_1 , ast_::macro_identifiers::builtin_func ),
           ast_::macro_identifiers::function ) ] ] //
         | macro_basic_ident_pm
        [ qi::_val = ast::binary_( qi::_a , qi::_1 , ast_::macro_identifiers::function ) ] );

        macro_oper_ident_pm = qi::lit("@@")
         > macro_func_mod_pm(template_type::oper)[ qi::_a = qi::_1 ]
         > macro_operators_sym_pm
        [ qi::_val = ast::binary_( qi::_a ,
           ast::value_( qi::_1 , ast_::macro_identifiers::operators ),
           ast_::macro_identifiers::operator_ ) ] ];//

        macro_type_ident_pm = qi::lit("$$")[ qi::_a = phoenix::construct< container_t >() ]
         > macro_type_infix_mods_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > ( qi::hold[ builtin_macro_type_sym_pm
         [ phoenix::push_back( qi::_a,
           ast::value_( qi::_1 , ast_::macro_identifiers::builtin_type ) ) ] ]
         | macro_basic_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
         > macro_type_postfix_mod_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::macro_identifiers::type ) ];

        macro_identifier_pm %= qi::hold[ macro_var_ident_pm ]
                             | qi::hold[ macro_func_ident_pm ]
                             | qi::hold[ macro_oper_ident_pm ]
                             | macro_type_ident_pm ;

        macro_namespace_pm = keyword_parser_pm("#namespace")
         > macro_basic_ident_pm
        [ qi::_val = ast::unary_( qi::_1 , ast_::macro_identifiers::namespace_ ) ] ;

        macro_import_pm = keyword_parser_pm("#import")
         > macro_basic_ident_pm
        [ qi::_val = ast::unary_( qi::_1 , ast_::macro_identifiers::import_ ) ];

        macro_nested_name_pm = qi::eps[ qi::_a = phoenix::construct< container_t >() ]
         > ( qi::hold[ macro_import_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
           > qi::lit("::") ]
           | qi::lit("::")[ phoenix::push_back( qi::_a, ast::tag_( ast_::ws::sepperator ) ) ] ) //
         > ( qi::hold[ macro_namespace_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
           | qi::eps[ phoenix::push_back( qi::_a, ast::tag_( ast_::ws::sepperator ) ) ] )
           % qi::lit("::")
         > ( qi::hold[ macro_type_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
           | qi::eps[ phoenix::push_back( qi::_a, ast::tag_( ast_::ws::sepperator ) ) ] )
           % qi::lit("::")
         > qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::macro_identifiers::nested_name ) ];

        macro_using_ident_pm = qi::hold[ macro_nested_name_pm[ qi::_a = qi::_1 ]
                                 > macro_identifier_pm
        [ qi::_val = ast::binary_( qi::_a , qi::_1 , ast_::macro_identifiers::using_ ) ] ]
                        // old:  | qi::hold[ macro_identifier_pm[ qi::_val = qi::_1 ] ]
                               | qi::hold[ macro_namespace_fwd_decl_pm[ qi::_val = qi::_1 ] ]
                               | qi::hold[ macro_func_fwd_decl_pm[ qi::_val = qi::_1 ] ]
                               | qi::hold[ macro_oper_fwd_decl_pm[ qi::_val = qi::_1 ] ]
                               | qi::hold[ macro_ctor_fwd_decl_pm[ qi::_val = qi::_1 ] ]
                               | qi::hold[ macro_dtor_fwd_decl_pm[ qi::_val = qi::_1 ] ]
                               | macro_object_fwd_decl_pm[ qi::_val = qi::_1 ];

        macro_data_decl_pm = qi::eps[ qi::_a = phoenix::construct< container_t >() ]
         > -( macro_nested_name_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
         > macro_type_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         // old: "macro_identifier_pm"
         > *( macro_type_postfix_mod_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
         > qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::macro_identifiers::data_decl_ ) ];

        // the macro modifiers:
        macro_tempid_mod_pm = qi::eps[ qi::_a = phoenix::construct< container_t >() ]
         > ( ( qi::eps( qi::_r1 == template_type::oper )
             > keyword_parser_pm("#template") )
             | - keyword_parser_pm("#template") )
         > qi::lit('<')
         > ( qi::hold[ macro_data_decl_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
           | macro_condition_expr_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
         % qi::lit(',')
         > qi::lit('>')
         > qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::macro_modifiers::tempid ) ];

        macro_type_postfix_mod_pm = qi::lit('[')
         > qi::hold[ macro_sequence_expr_pm
        [ qi::_val = ast::unary_( qi::_1, ast_::macro_modifiers::type_postfix ) ] ]
         | qi::eps[ qi::_val = ast::tag_( ast_::macro_modifiers::type_postfix ) ]
         > qi::lit(']');

        macro_throw_mod_pm = keyword_parser_pm("#throw")
        [ qi::_a = phoenix::construct< container_t >() ]
         > qi::lit('(')
         > macro_data_decl_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         % qi::lit(',')
         > qi::lit(')')
        [ qi::_val = ast::nary_( qi::_a, ast_::macro_modifiers::throw_ ) ];

        macro_func_mod_pm = qi::eps[ qi::_a = phoenix::construct< container_t >() ]
         > * ( qi::hold[ macro_tempid_mod_pm(qi::_r1)[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
             | qi::hold[ macro_throw_mod_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
             | qi::hold[ macro_static_mod_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
             | qi::hold[ macro_attribute_mod_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
             | macro_const_mod_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
             )
         > qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::macro_modifiers::func_mod ) ];

        macro_static_mod_pm = keyword_parser_pm("#static")
         [ qi::_val = ast::tag_( ast_::macro_modifiers::static_ ) ];

        macro_const_mod_pm = keyword_parser_pm("#const")
         [ qi::_val = ast::tag_( ast_::macro_modifiers::const_ ) ];

        macro_volatile_mod_pm = keyword_parser_pm("#volatile")
         [ qi::_val = ast::tag_( ast_::macro_modifiers::volatile_ ) ];

        macro_gen_decl_mod_pm = qi::hold[ keyword_parser_pm("#export")
         [ qi::_val = ast::tag_( ast_::macro_modifiers::export_ ) ] ]
                              | keyword_parser_pm("#intern")
         [ qi::_val = ast::tag_( ast_::macro_modifiers::intern_ ) ];

        macro_data_decl_mods_pm = qi::eps[ qi::_a = phoenix::construct< container_t >() ]
         > *( qi::hold[ macro_gen_decl_mod_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
            | qi::hold[ macro_static_mod_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
            | qi::hold[ macro_volatile_mod_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
            | qi::hold[ macro_attribute_mod_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
            | macro_const_mod_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
            )
         > qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::macro_modifiers::data_decl ) ];

        macro_attribute_mod_pm = keyword_parser_pm("#attribute")
         > qi::lit('(') > macro_basic_ident_pm[ qi::_a = qi::_1 ]
         > qi::lit(',') > litteral_pm( char_used::value )
        [ qi::_val = ast::binary_( qi::_a, qi::_1 , ast_::macro_modifiers::attribute ) ]
         > qi::lit(')');

        macro_type_infix_mods_pm = qi::eps[ qi::_a = phoenix::construct< container_t >() ]
         > *( qi::hold[ macro_tempid_mod_pm(template_type::null_)
         [ phoenix::push_back( qi::_a, qi::_1 ) ] ]
            | macro_attribute_mod_pm
         [ phoenix::push_back( qi::_a, qi::_1 ) ] )
         > qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::macro_modifiers::type_infix ) ];

        macro_abstract_mod_pm = keyword_parser_pm("#abstract")
         [ qi::_val = ast::tag_( ast_::macro_modifiers::abstract_ ) ];

        macro_virtual_mod_pm = keyword_parser_pm("#virtual")
         [ qi::_val = ast::tag_( ast_::macro_modifiers::virtual_ ) ];

        macro_temp_decl_mod_pm = qi::eps[ qi::_a = phoenix::construct< container_t >() ]
         > ( ( qi::eps( qi::_r1 == template_type::oper )
             > keyword_parser_pm("#template") )
             | - keyword_parser_pm("#template") )
         > qi::lit('<')
         > ( qi::hold[ ( macro_data_decl_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
              > qi::lit('=')
              > macro_data_decl_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ) ]
           | qi::hold[ macro_single_var_decl_pm ]
           | macro_condition_expr_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
         % qi::lit(',')
         > qi::lit('>')
         > qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::macro_modifiers::temp_decl ) ];

         macro_func_decl_mod_pm = qi::eps[ qi::_a = phoenix::construct< container_t >() ]
          > *( qi::hold[ macro_gen_decl_mod_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
             | qi::hold[ macro_temp_decl_mod_pm(qi::_r1)[ phoenix::push_back( qi::_a, qi::_1 ) ]]
             | qi::hold[ macro_throw_mod_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
             | qi::hold[ macro_const_mod_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
             | qi::hold[ macro_static_mod_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
             | qi::hold[ macro_abstract_mod_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
             | qi::hold[ macro_attribute_mod_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
             | macro_virtual_mod_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
             )
          > qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::macro_modifiers::func_decl_mods ) ];

        macro_func_decl_args_pm = qi::eps[ qi::_a = phoenix::construct< container_t >() ]
         > *( macro_single_var_decl_pm % qi::lit(',') )
         > qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::macro_modifiers::func_decl_args ) ];

        macro_explicit_mod_pm = keyword_parser_pm("#explicit")
         [ qi::_val = ast::tag_( ast_::macro_modifiers::expicit_ ) ];

        macro_ctor_init_mem_pm = macro_identifier_expr_pm[  qi::_a = qi::_1 ]
         > macro_func_call_expr_pm
        [ qi::_val = ast::binary_( qi::_a, qi::_1 , ast_::macro_modifiers::ctor_init_mem ) ];

        macro_ctor_init_list_pm = qi::lit(':')[ qi::_a = phoenix::construct< container_t >() ]
         > macro_ctor_init_mem_pm
         % qi::lit(',')
         > qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::macro_modifiers::ctor_init_list ) ];

        macro_ctor_decl_mod_pm = qi::eps[ qi::_a = phoenix::construct< container_t >() ]
         > *( qi::hold[ macro_gen_decl_mod_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
             | qi::hold[ macro_temp_decl_mod_pm(template_type::null_)
         [ phoenix::push_back( qi::_a, qi::_1 ) ] ]
             | qi::hold[ macro_throw_mod_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
             | qi::hold[ macro_attribute_mod_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
             | macro_explicit_mod_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
              )
         > qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::macro_modifiers::ctor_decl_ ) ];

        macro_dtor_decl_mod_pm = qi::eps[ qi::_a = phoenix::construct< container_t >() ]
         > *( qi::hold[ macro_gen_decl_mod_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
            | qi::hold[ macro_temp_decl_mod_pm(template_type::null_)
               [ phoenix::push_back( qi::_a, qi::_1 ) ] ]
            | qi::hold[ macro_attribute_mod_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
            | macro_virtual_mod_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
            )
         > qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::macro_modifiers::ctor_decl_ ) ];

        macro_type_infix_decl_mods_pm = qi::eps[ qi::_a = phoenix::construct< container_t >() ]
         > *( qi::hold[ macro_gen_decl_mod_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
            | qi::hold[ macro_temp_decl_mod_pm(template_type::null_)
               [ phoenix::push_back( qi::_a, qi::_1 ) ] ]
            | macro_attribute_mod_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
         > qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::macro_modifiers::type_infix_decl ) ];

        macro_inherit_list_pm = qi::lit(':')[ qi::_a = phoenix::construct< container_t >() ]
         > macro_inherit_mem_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         % qi::lit(',')
         > qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::macro_modifiers::inherit_list ) ];

        macro_inherit_mem_pm = qi::eps[ qi::_a = phoenix::construct< container_t >() ]
         > macro_access_mod_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > -( macro_virtual_mod_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
         > *( macro_attribute_mod_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
         > -( macro_nested_name_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
         > macro_type_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::macro_modifiers::inherit_mem ) ];

        macro_access_mod_pm = qi::hold[ keyword_parser_pm("#public")
         [ qi::_val = ast::tag_( ast_::macro_modifiers::public_ ) ] ]
                            | qi::hold[ keyword_parser_pm("#protected")
         [ qi::_val = ast::tag_( ast_::macro_modifiers::protected_ ) ] ]
                            | keyword_parser_pm("#private")
         [ qi::_val = ast::tag_( ast_::macro_modifiers::private_ ) ];


        // macro expressions:
        macro_this_expr_pm = keyword_parser_pm("#this")
         [ qi::_val = ast::tag_( ast_::macro_expressions::this_ ) ];

        macro_identifier_expr_pm = qi::eps[ qi::_a = phoenix::construct< container_t >() ]
         > -( macro_nested_name_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
         > macro_identifier_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::macro_expressions::ident ) ];

        macro_prime_expr_pm %= qi::hold[ macro_this_expr_pm ]
                             | qi::hold[ macro_identifier_expr_pm ]
                             | qi::hold[ litteral_pm( char_used::value ) ]
                             | ( qi::lit('(') > macro_sequence_expr_pm
              [ qi::_val = qi::_1 ]
                               > qi::lit(')') );

        macro_array_expr_pm = qi::lit('[') >
         macro_sequence_expr_pm
         [ qi::_val = ast::unary_( qi::_1, ast_::macro_expressions::array ) ]
                               > qi::lit(']');

        macro_func_call_expr_pm = qi::lit('(')[ qi::_a = phoenix::construct< container_t >() ]
         > ( qi::hold[ macro_assignment_expr_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
           | qi::eps[ phoenix::push_back( qi::_a, ast::tag_( ast_::ws::sepperator ) ) ] )
           % qi::lit(',')
         > qi::lit(')')[ qi::_val = ast::nary_( qi::_a, ast_::macro_expressions::func_call ) ];

        macro_ctor_expr_pm = qi::hold[ qi::lit("::") > keyword_parser_pm("#constructor")
         [ qi::_val = ast::tag_( ast_::macro_expressions::ctorType ) ] ]
                           | qi::hold[ qi::lit("->") > keyword_parser_pm("#constructor")
         [ qi::_val = ast::tag_( ast_::macro_expressions::ctorPtr ) ] ]
                           | qi::lit('.') > keyword_parser_pm("#constructor")
         [ qi::_val = ast::tag_( ast_::macro_expressions::ctorRef ) ];

        macro_member_expr_pm = qi::hold[ qi::lit("->") > macro_identifier_expr_pm
         [ qi::_val = ast::unary_( qi::_1, ast_::macro_expressions::memberPtr ) ] ]
                             | qi::lit('.') > macro_identifier_expr_pm
         [ qi::_val = ast::unary_( qi::_1, ast_::macro_expressions::memberRef ) ];

        macro_dtor_expr_pm = qi::hold[ qi::lit("->") > keyword_parser_pm("#destructor")
         [ qi::_val = ast::tag_( ast_::macro_expressions::dtorPtr ) ] ]
                           | qi::lit('.') > keyword_parser_pm("#destructor")
         [ qi::_val = ast::tag_( ast_::macro_expressions::dtorRef ) ];

        macro_postfix_expr_pm = qi::eps[ qi::_a = phoenix::construct< container_t >() ]
         > macro_prime_expr_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > *( qi::hold[ macro_array_expr_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
            | qi::hold[ macro_func_call_expr_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
            | qi::hold[ macro_ctor_expr_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
            | qi::hold[ macro_member_expr_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
            | qi::hold[ macro_dtor_expr_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
            | qi::hold[ qi::lit("--")[ phoenix::push_back( qi::_a,
                                   ast::tag_( ast_::macro_expressions::postDecrement ) ) ] ] //
            | qi::lit("++")[ phoenix::push_back( qi::_a,
                                   ast::tag_( ast_::macro_expressions::postIncrement ) ) ] )
         > qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::macro_expressions::postfix ) ];

        /* [ phoenix::push_back( qi::_a, ast::tag_( ast_::macro_expressions:: ) ) ] */

        macro_new_expr_pm = keyword_parser_pm("#new")
         [ qi::_a = phoenix::construct< container_t >() ]
         > macro_data_decl_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > -( qi::lit("::") > keyword_parser_pm("#constructor")
         [ phoenix::push_back( qi::_a, ast::tag_( ast_::macro_expressions::ctorType ) ) ] )
         > macro_func_call_expr_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::macro_expressions::new_ ) ];

        macro_delete_expr_pm = keyword_parser_pm("#delete")
         > macro_unary_expr_pm
        [ qi::_val = ast::unary_( qi::_1 , ast_::macro_expressions::delete_ ) ];

        macro_unary_expr_pm = qi::hold[ macro_delete_expr_pm[ qi::_val = qi::_1 ] ]
        | ( qi::eps[ qi::_a = phoenix::construct< container_t >() ]
           > *( macro_unary_expr_sym_pm
         [ phoenix::push_back( qi::_a, ast::tag_( qi::_1 ) ) ] )
           > ( qi::hold[ macro_postfix_expr_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
           | macro_new_expr_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
           > qi::eps
         [ qi::_val = ast::nary_( qi::_a, ast_::macro_expressions::unary ) ]
         );

        macro_multiply_expr_pm = macro_unary_expr_pm[ qi::_a = qi::_1 ]
         > *( qi::hold[ qi::lit('*') >  macro_unary_expr_pm
        [ qi::_a = ast::binary_( qi::_a, qi::_1, ast_::macro_expressions::multiply ) ] ]
         | qi::hold[ qi::lit('/') >  macro_unary_expr_pm
        [ qi::_a = ast::binary_( qi::_a, qi::_1, ast_::macro_expressions::division ) ] ]
         | qi::lit('%') >  macro_unary_expr_pm
        [ qi::_a = ast::binary_( qi::_a, qi::_1, ast_::macro_expressions::modulus ) ] )
         > qi::eps[ qi::_val = qi::_a ];

        macro_addition_expr_pm = macro_multiply_expr_pm[ qi::_a = qi::_1 ]
         > *( qi::hold[ qi::lit('+') > macro_multiply_expr_pm
        [ qi::_a = ast::binary_( qi::_a, qi::_1, ast_::macro_expressions::addition ) ] ]
            | qi::lit('-') > macro_multiply_expr_pm
        [ qi::_a = ast::binary_( qi::_a, qi::_1, ast_::macro_expressions::subtraction ) ] )
         > qi::eps[ qi::_val = qi::_a ];

        macro_bitshift_expr_pm = macro_addition_expr_pm[ qi::_a = qi::_1 ]
         > *( qi::hold[ qi::lit("<<") > macro_addition_expr_pm
        [ qi::_a = ast::binary_( qi::_a, qi::_1, ast_::macro_expressions::leftShift ) ] ]
            | qi::hold[ qi::lit(">>") > macro_addition_expr_pm
        [ qi::_a = ast::binary_( qi::_a, qi::_1, ast_::macro_expressions::rightShiftArith ) ] ]
            | qi::lit(">>>") > macro_addition_expr_pm
        [ qi::_a = ast::binary_( qi::_a, qi::_1, ast_::macro_expressions::rightShiftLogic ) ] )
         > qi::eps[ qi::_val = qi::_a ];

        macro_relational_expr_pm = macro_bitshift_expr_pm[ qi::_a = qi::_1 ]
         > *( qi::hold[ qi::lit("<=") > macro_bitshift_expr_pm
        [ qi::_a = ast::binary_( qi::_a, qi::_1, ast_::macro_expressions::lesserEqual ) ] ]
            | qi::hold[ qi::lit(">=") > macro_bitshift_expr_pm
        [ qi::_a = ast::binary_( qi::_a, qi::_1, ast_::macro_expressions::greaterEqual ) ] ]
            | qi::hold[ qi::lit('<') > macro_bitshift_expr_pm
        [ qi::_a = ast::binary_( qi::_a, qi::_1, ast_::macro_expressions::lesserThan ) ] ]
            | qi::lit('>') > macro_bitshift_expr_pm
        [ qi::_a = ast::binary_( qi::_a, qi::_1, ast_::macro_expressions::greaterThan ) ] )
         > qi::eps[ qi::_val = qi::_a ];

        macro_equality_expr_pm = macro_relational_expr_pm[ qi::_a = qi::_1 ]
         > *( qi::hold[ qi::lit("==") > macro_relational_expr_pm
        [ qi::_a = ast::binary_( qi::_a, qi::_1, ast_::macro_expressions::equal ) ] ]
            | qi::lit("!=") > macro_relational_expr_pm
        [ qi::_a = ast::binary_( qi::_a, qi::_1, ast_::macro_expressions::notEqual ) ] )
         > qi::eps[ qi::_val = qi::_a ];

        /* // code snipet for reuse ( for binary expressions ):
        _expr_pm = _expr_pm[ qi::_a = qi::_1 ]
         > *( qi::hold[ qi::lit() > _expr_pm
        [ qi::_a = ast::binary_( qi::_a, qi::_1, ast_::macro_expressions:: ) ] ]
            | qi::hold[ qi::lit() > _expr_pm
        [ qi::_a = ast::binary_( qi::_a, qi::_1, ast_::macro_expressions:: ) ] ]
            | qi::lit() > _expr_pm
        [ qi::_a = ast::binary_( qi::_a, qi::_1, ast_::macro_expressions:: ) ] )
         > qi::eps[ qi::_val = qi::_a ];
        */

        macro_bitwiseAnd_expr_pm = macro_equality_expr_pm[ qi::_a = qi::_1 ]
          > *( qi::lit('&') > macro_equality_expr_pm
        [ qi::_a = ast::binary_( qi::_a, qi::_1, ast_::macro_expressions::bitwiseAnd ) ] )
          > qi::eps[ qi::_val = qi::_a ];

        macro_bitwiseXor_expr_pm = macro_bitwiseAnd_expr_pm[ qi::_a = qi::_1 ]
          > *( qi::lit('^') > macro_bitwiseAnd_expr_pm
        [ qi::_a = ast::binary_( qi::_a, qi::_1, ast_::macro_expressions::bitwiseXor ) ] )
          > qi::eps[ qi::_val = qi::_a ];

        macro_bitwiseOr_expr_pm = macro_bitwiseXor_expr_pm[ qi::_a = qi::_1 ]
          > *( qi::lit('|') > macro_bitwiseXor_expr_pm
        [ qi::_a = ast::binary_( qi::_a, qi::_1, ast_::macro_expressions::bitwiseOr ) ] )
          > qi::eps[ qi::_val = qi::_a ];

        macro_logicalAnd_expr_pm = macro_bitwiseOr_expr_pm[ qi::_a = qi::_1 ]
          > *( qi::lit("&&") > macro_bitwiseOr_expr_pm
        [ qi::_a = ast::binary_( qi::_a, qi::_1, ast_::macro_expressions::logicalAnd ) ] )
          > qi::eps[ qi::_val = qi::_a ];

        macro_logicalXor_expr_pm = macro_logicalAnd_expr_pm[ qi::_a = qi::_1 ]
          > *( qi::lit("^^") > macro_logicalAnd_expr_pm
        [ qi::_a = ast::binary_( qi::_a, qi::_1, ast_::macro_expressions::logicalXor ) ] )
          > qi::eps[ qi::_val = qi::_a ];

        macro_logicalOr_expr_pm = macro_logicalXor_expr_pm[ qi::_a = qi::_1 ]
          > *( qi::lit("||") > macro_logicalXor_expr_pm
        [ qi::_a = ast::binary_( qi::_a, qi::_1, ast_::macro_expressions::logicalOr ) ] )
          > qi::eps[ qi::_val = qi::_a ];

        /* // code snipet for reuse ( for binary expressions ):
         _expr_pm = _expr_pm[ qi::_a = qi::_1 ]
          > *( qi::lit() > _expr_pm
        [ qi::_a = ast::binary_( qi::_a, qi::_1, ast_::macro_expressions:: ) ] )
          > qi::eps[ qi::_val = qi::_a ];
        */

        macro_conditional_expr_pm = qi::eps[ qi::_a = phoenix::construct< container_t >() ]
         > macro_logicalOr_expr_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > -( ( qi::hold[ qi::lit('?') > macro_assignment_expr_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
              | qi::eps[ phoenix::push_back( qi::_a, ast::tag_( ast_::ws::sepperator ) ) ] )
            > ( qi::hold[ qi::lit(':') > macro_assignment_expr_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
              | qi::eps[ phoenix::push_back( qi::_a, ast::tag_( ast_::ws::sepperator ) ) ] ) )
         > qi::eps
         [ qi::_val = ast::nary_( qi::_a, ast_::macro_expressions::conditional ) ];

        macro_assignment_expr_pm = qi::hold[ ( macro_logicalOr_expr_pm[ qi::_a = qi::_1 ]
          > *( macro_assignment_expr_sym_pm[ qi::_b = qi::_1 ] > macro_conditional_expr_pm
        [ qi::_a = ast::binary_( qi::_a, qi::_1, qi::_b ) ] )
          > qi::eps[ qi::_val = qi::_a ] ) ]
          | qi::hold[ macro_conditional_expr_pm[ qi::_val = qi::_1 ] ]
          | macro_throw_expr_pm[ qi::_val = qi::_1 ] ;

        macro_sequence_expr_pm = macro_assignment_expr_pm[ qi::_a = qi::_1 ]
          > *( qi::lit(',') > macro_assignment_expr_pm
        [ qi::_a = ast::binary_( qi::_a, qi::_1, ast_::macro_expressions::sequence ) ] )
          > qi::eps[ qi::_val = qi::_a ];

        macro_throw_expr_pm = keyword_parser_pm("#throw")
         > macro_sequence_expr_pm[ qi::_val = ast::unary_( qi::_1, ast_::macro_expressions::throw_ ) ];

        macro_condition_expr_pm = ( qi::hold[ macro_assignment_expr_pm[ qi::_a = qi::_1 ] ]
                                  | macro_single_var_decl_pm [ qi::_a = qi::_1 ] )
         > *( qi::lit(',') > ( qi::hold[ macro_assignment_expr_pm
         [ qi::_a = ast::binary_( qi::_a, qi::_1, ast_::macro_expressions::condition ) ] ]
                                  | macro_single_var_decl_pm
         [ qi::_a = ast::binary_( qi::_a, qi::_1, ast_::macro_expressions::condition ) ] )
          > qi::eps[ qi::_val = qi::_a ];

        // macro_statements:
        macro_expression_state_pm = ( qi::hold[ macro_sequence_expr_pm
        [ qi::_val = ast::unary_( qi::_1, ast_::macro_statements::expression ) ] ]
         | qi::eps[ qi::_val = ast::tag_( ast_::macro_statements::expression ) ] )
         > qi::lit(';');

        macro_switch_state_pm = keyword_parser_pm("#switch")
         > qi::lit('(') > macro_condition_expr_pm[ qi::_a = qi::_1 ] > qi::lit(')')
         > macro_statement_pm
        [ qi::_val = ast::binary_( qi::_a, qi::_1, ast_::macro_statements::switch_ ) ];

        macro_if_state_pm = keyword_parser_pm("#if")
         > qi::lit('(') > macro_condition_expr_pm[ qi::_a = qi::_1 ] > qi::lit(')')
         > macro_statement_pm
        [ qi::_val = ast::binary_( qi::_a, qi::_1, ast_::macro_statements::if_ ) ];

        macro_elif_state_pm = keyword_parser_pm("#elif")
         > qi::lit('(') > macro_condition_expr_pm[ qi::_a = qi::_1 ] > qi::lit(')')
         > macro_statement_pm
        [ qi::_val = ast::binary_( qi::_a, qi::_1, ast_::macro_statements::elif ) ];

        macro_else_state_pm = keyword_parser_pm("#else")
         > macro_statement_pm
        [ qi::_val = ast::unary_( qi::_1, ast_::macro_statements::else_ ) ] ;

        macro_select_state_pm = qi::hold[ macro_switch_state_pm[ qi::_val = qi::_1 ] ]
                              | ( qi::eps[ qi::_a = phoenix::construct< container_t >() ]
                                > macro_if_state_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
                                > *( macro_elif_state_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
                                > -( macro_else_state_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
                                > qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::macro_statements::select_ ) ] );

        macro_label_state_pm = qi::hold[ keyword_parser_pm("#case") > macro_sequence_expr_pm[ qi::_a = qi::_1 ]
         > qi::lit(':') > macro_statement_pm
        [ qi::_val = ast::binary_( qi::_a, qi::_1, ast_::macro_statements::case_ ) ] ]
                             | keyword_parser_pm("#default") > qi::lit(':')
         > macro_statement_pm[ qi::_val = ast::unary_( qi::_1, ast_::macro_statements::default_ ) ];

        macro_jump_state_pm = qi::hold[ keyword_parser_pm("#break") > qi::lit(';')
                        [ qi::_val = ast::tag_( ast_::macro_statements::break_ ) ] ]
                            | qi::hold[ keyword_parser_pm("#continue") > qi::lit(';')
                        [ qi::_val = ast::tag_( ast_::macro_statements::continue_ ) ] ]
                            | keyword_parser_pm("#return") > macro_sequence_expr_pm
                        [ qi::_val = ast::unary_( qi::_1, ast_::macro_statements::return_ ) ] > qi::lit(';') ;

        macro_compound_state_pm = keyword_parser_pm("#begin")[ qi::_a = phoenix::construct< container_t >() ]
         > *( macro_statement_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
         > keyword_parser_pm("#end")[ qi::_val = ast::nary_( qi::_a, ast_::macro_statements::compound ) ];

        macro_catch_state_pm = keyword_parser_pm("#catch") > qi::lit('(')
         > macro_single_var_decl_pm[ qi::_a = qi::_1 ]
         > qi::lit(')') > macro_compound_state_pm
        [ qi::_val = ast::binary_( qi::_a, qi::_1, ast_::macro_statements::catch_ ) ];

        macro_finally_state_pm = keyword_parser_pm("#finally")
         > macro_compound_state_pm[ qi::_val = ast::unary_( qi::_1, ast_::macro_statements::finally_ ) ];

        macro_exception_state_pm = keyword_parser_pm("#try")[ qi::_a = phoenix::construct< container_t >() ]
         > macro_compound_state_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > *( macro_catch_state_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
         > -( macro_finally_state_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
         > qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::macro_statements::exception ) ];

        macro_for_loop_state_pm = keyword_parser_pm("#for")[ qi::_a = phoenix::construct< container_t >() ]
         > qi::lit('(') > -( macro_condition_expr_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
         > qi::lit(';') > -( macro_condition_expr_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
         > qi::lit(';') > -( macro_sequence_expr_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
         > qi::lit(')') > macro_statement_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::macro_statements::for_ ) ];

        macro_while_loop_state_pm = keyword_parser_pm("#while")
         > qi::lit('(') > macro_condition_expr_pm[ qi::_a = qi::_1 ] > qi::lit(')')
         > macro_statement_pm
        [ qi::_val = ast::binary_( qi::_a, qi::_1, ast_::macro_statements::while_ ) ] ;

        macro_do_loop_state_pm = keyword_parser_pm("#do")
         > macro_statement_pm[ qi::_a = qi::_1 ]
         > keyword_parser_pm("#while") > qi::lit('(')
         > macro_logicalOr_expr_pm > qi::lit(')')
        [ qi::_val = ast::binary_( qi::_a, qi::_1, ast_::macro_statements::do_ ) ]
         > qi::lit(';');

        macro_loop_state_pm %= qi::hold[ macro_for_loop_state_pm ]
                             | qi::hold[ macro_while_loop_state_pm ]
                             | macro_do_loop_state_pm;

        macro_using_state_pm = keyword_parser_pm("#using")[ qi::_a = phoenix::construct< container_t >() ]
         > macro_using_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] % qi::lit(',')
         > qi::lit(';')[ qi::_val = ast::nary_( qi::_a, ast_::macro_statements::using_ ) ] ;

        macro_namespace_alias_state_pm = keyword_parser_pm("#namespace")
         [ qi::_a = phoenix::construct< container_t >() ]
         > macro_basic_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > qi::lit('=')
         > -( qi::hold[ macro_import_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
              > qi::lit("::") ]
            | qi::lit("::")[ phoenix::push_back( qi::_a, ast::tag_( ast_::ws::sepperator ) ) ] )
         > -( qi::hold[ macro_namespace_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
            | qi::eps[ phoenix::push_back( qi::_a, ast::tag_( ast_::ws::sepperator ) ) ] )
            % qi::lit("::")
         > macro_namespace_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > qi::lit(';')[ qi::_val = ast::nary_( qi::_a, ast_::macro_statements::namespace_alias ) ] ;

        macro_statement_pm %= qi::hold[ macro_expression_state_pm ]
                            | qi::hold[ macro_var_decl_pm ]
                            | qi::hold[ macro_type_decl_pm ]
                            | qi::hold[ macro_select_state_pm ]
                            | qi::hold[ macro_label_state_pm ]
                            | qi::hold[ macro_jump_state_pm ]
                            | qi::hold[ macro_compound_state_pm ]
                            | qi::hold[ macro_exception_state_pm ]
                            | qi::hold[ macro_loop_state_pm ]
                            | qi::hold[ macro_namespace_alias_state_pm ]
                            | macro_using_state_pm ;


        // macro_declarations:
        macro_single_var_decl_pm = keyword_parser_pm("#var")[ qi::_a = phoenix::construct< container_t >() ]
         > *( macro_data_decl_mods_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
         > macro_data_decl_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > macro_basic_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > -( qi::hold[ qi::lit('=') > macro_assignment_expr_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
            | macro_func_call_expr_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
         > qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::macro_declarations::var_ ) ];

        macro_var_decl_pm = keyword_parser_pm("#var")[ qi::_a = phoenix::construct< container_t >() ]
         > *( macro_data_decl_mods_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
         > macro_data_decl_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > ( macro_basic_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
           > -( qi::hold[ qi::lit('=') > macro_assignment_expr_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
              | macro_func_call_expr_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ) )
         % qi::lit(',')
         > qi::lit(';')[ qi::_val = ast::nary_( qi::_a, ast_::macro_declarations::var_ ) ];

        macro_single_type_decl_pm = keyword_parser_pm("#type")[ qi::_a = phoenix::construct< container_t >() ]
         > macro_data_decl_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > macro_basic_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > *( macro_type_postfix_mod_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
         > qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::macro_declarations::single_type_ ) ];

        macro_type_decl_pm = keyword_parser_pm("#type")[ qi::_a = phoenix::construct< container_t >() ]
         > macro_data_decl_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > ( macro_basic_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > *( macro_type_postfix_mod_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ) )
         % qi::lit(',')
         > qi::lit(';')[ qi::_val = ast::nary_( qi::_a, ast_::macro_declarations::type_ ) ];

        macro_import_decl_pm = keyword_parser_pm("#import")[ qi::_a = phoenix::construct< container_t >() ]
         > macro_basic_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > qi::lit('=')
         > ( qi::hold[ import_sys_str_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
           | str_lit_pm(char_used::value)[ phoenix::push_back( qi::_a, qi::_1 ) ] )
           % qi::lit(',')
         > qi::lit(';')[ qi::_val = ast::nary_( qi::_a, ast_::macro_declarations::import_ ) ];

        macro_compound_decl_pm = keyword_parser_pm("#begin")[ qi::_a = phoenix::construct< container_t >() ]
         > *( macro_member_decl_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
         > keyword_parser_pm("#end")
         > qi::lit(';')[ qi::_val = ast::nary_( qi::_a, ast_::macro_declarations::compound ) ];

        macro_namespace_fwd_decl_pm = qi::eps[ qi::_a = phoenix::construct< container_t >() ]
         > -( qi::hold[ macro_import_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
              > qi::lit("::") ]
            | qi::lit("::")[ phoenix::push_back( qi::_a, ast::tag_( ast_::ws::sepperator ) ) ] )
         > -( qi::hold[ macro_namespace_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
            | qi::eps[ phoenix::push_back( qi::_a, ast::tag_( ast_::ws::sepperator ) ) ] )
            % qi::lit("::")
         > keyword_parser_pm("#namespace")
         > *( qi::hold[ macro_attribute_mod_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
            | macro_gen_decl_mod_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
         > macro_basic_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::macro_declarations::namespace_fwd ) ];

        macro_namespace_decl_pm = macro_namespace_fwd_decl_pm[ qi::_a = qi::_1 ]
         > macro_compound_decl_pm
        [ qi::_val = ast::binary_( qi::_a, qi::_1, ast_::macro_declarations::namespace_ ) ];

        macro_func_fwd_decl_pm = qi::eps[ qi::_a = phoenix::construct< container_t >() ]
         > -( macro_nested_name_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
         > keyword_parser_pm("#function")
         > macro_func_decl_mod_pm(template_type::null_)[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > macro_basic_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > qi::lit('(') > macro_func_decl_args_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > qi::lit(')') > qi::lit(':')
         > macro_single_var_decl_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::macro_declarations::function_fwd ) ];

        macro_function_decl_pm = macro_func_fwd_decl_pm[ qi::_a = qi::_1 ]
         > ( qi::hold[ macro_exception_state_pm
        [ qi::_val = ast::binary_( qi::_a, qi::_1, ast_::macro_declarations::function ) ] ]
           | macro_compound_state_pm
        [ qi::_val = ast::binary_( qi::_a, qi::_1, ast_::macro_declarations::function ) ] );

        macro_oper_fwd_decl_pm = qi::eps[ qi::_a = phoenix::construct< container_t >() ]
         > -( macro_nested_name_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
         > keyword_parser_pm("#operator")
         > macro_func_decl_mod_pm(template_type::oper)[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > macro_operators_sym_pm[ phoenix::push_back( qi::_a, ast::tag_( qi::_1 ) ) ]
         > qi::lit('(') > macro_func_decl_args_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > qi::lit(')') > qi::lit(':')
         > macro_single_var_decl_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::macro_declarations::operator_fwd ) ];

        macro_oper_decl_pm = macro_oper_fwd_decl_pm[ qi::_a = qi::_1 ]
         > ( qi::hold[ macro_exception_state_pm
        [ qi::_val = ast::binary_( qi::_a, qi::_1, ast_::macro_declarations::operator_ ) ] ]
           | macro_compound_state_pm
        [ qi::_val = ast::binary_( qi::_a, qi::_1, ast_::macro_declarations::operator_ ) ] ) ;

        macro_ctor_fwd_decl_pm = qi::eps[ qi::_a = phoenix::construct< container_t >() ]
         > -( macro_nested_name_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
         > keyword_parser_pm("#constructor")
         > macro_ctor_decl_mod_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > qi::lit('(') > macro_func_decl_args_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > qi::lit(')')
         > qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::macro_declarations::ctor_fwd ) ];

        macro_ctor_decl_pm = qi::eps[ qi::_a = phoenix::construct< container_t >() ]
         > macro_ctor_fwd_decl_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > -( macro_ctor_init_list_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
         > ( qi::hold[ macro_exception_state_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] ]
           | macro_compound_state_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
         > qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::macro_declarations::ctor ) ];

        macro_dtor_fwd_decl_pm = qi::eps[ qi::_a = phoenix::construct< container_t >() ]
         > -( macro_nested_name_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
         > keyword_parser_pm("#destructor")
         > macro_dtor_decl_mod_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > qi::lit('(') > qi::lit(')')
         > qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::macro_declarations::dtor_fwd ) ];

        macro_dtor_decl_pm = macro_dtor_fwd_decl_pm[ qi::_a = qi::_1 ]
         > macro_compound_state_pm
        [ qi::_val = ast::binary_( qi::_a, qi::_1, ast_::macro_declarations::dtor ) ];

        macro_object_fwd_decl_pm = qi::eps[ qi::_a = phoenix::construct< container_t >() ]
         > -( macro_nested_name_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
         > keyword_parser_pm("#object")
         > macro_type_infix_decl_mods_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > macro_basic_ident_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::macro_declarations::object_fwd ) ];

        macro_object_decl_pm = qi::eps[ qi::_a = phoenix::construct< container_t >() ]
         > macro_object_fwd_decl_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > -( macro_inherit_list_pm[ phoenix::push_back( qi::_a, qi::_1 ) ] )
         > macro_compound_decl_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
         > qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::macro_declarations::object_ ) ];

        macro_forward_decl_pm = ( qi::hold[ macro_namespace_fwd_decl_pm[ qi::_a = qi::_1 ] ]
                               | qi::hold[ macro_func_fwd_decl_pm[ qi::_a = qi::_1 ] ]
                               | qi::hold[ macro_oper_fwd_decl_pm[ qi::_a = qi::_1 ] ]
                               | qi::hold[ macro_ctor_fwd_decl_pm[ qi::_a = qi::_1 ] ]
                               | qi::hold[ macro_dtor_fwd_decl_pm[ qi::_a = qi::_1 ] ]
                               | macro_object_fwd_decl_pm[ qi::_a = qi::_1 ] )
         > qi::lit(';')[ qi::_val = ast::unary_( qi::_a , ast_::macro_declarations::forward_ ) ];

        macro_friend_decl_pm = keyword_parser_pm("#friend")
         > macro_forward_decl_pm[ qi::_val = ast::unary_( qi::_1 , ast_::macro_declarations::friend_ ) ];

        macro_member_decl_pm = qi::hold[ macro_friend_decl_pm[ qi::_val = qi::_1 ] ]
                             | qi::hold[ macro_forward_decl_pm[ qi::_val = qi::_1 ] ]
                             | qi::hold[ macro_using_state_pm[ qi::_val = qi::_1 ] ]
                             | ( macro_access_mod_pm[ qi::_a = qi::_1 ]
                               > macro_declaration_pm
         [ qi::_val = ast::binary_( qi::_a, qi::_1, ast_::macro_declarations::member_ ) ] ) ;

        macro_declaration_pm %= qi::hold[ macro_var_decl_pm ]
                              | qi::hold[ macro_type_decl_pm ]
                              | qi::hold[ macro_import_decl_pm ]
                              | qi::hold[ macro_namespace_decl_pm ]
                              | qi::hold[ macro_function_decl_pm ]
                              | qi::hold[ macro_oper_decl_pm ]
                              | qi::hold[ macro_ctor_decl_pm ]
                              | qi::hold[ macro_dtor_decl_pm ]
                              | qi::hold[ macro_object_decl_pm ]
                              | qi::hold[ macro_using_state_pm ]
                              | qi::hold[ macro_namespace_alias_state_pm ]
                              | macro_forward_decl_pm ;


        // the Grammar Start rule:
        start = qi::eps[ qi::_a = phoenix::construct< container_t >() ]
              >> + declaration_pm[ phoenix::push_back( qi::_a, qi::_1 ) ]
              >> qi::eps[ qi::_val = ast::nary_( qi::_a, ast_::module_ ) ];
     } // end of gsvm_parser ctor;

     // "general utility" ( rule types ) typedefs
     typedef qi::rule< IteratorT, node_ptr( void ), skipper_type >
                                                  pass_rule_type;

     typedef qi::rule< IteratorT, node_ptr( void ), skipper_type,
               qi::locals<container_t>  > grammar_sig_rule_type;
     // "grammar_sig_rule_type" - aka: "nary_rule_type"

     typedef qi::rule< IteratorT, node_ptr( void ), skipper_type,
                       qi::locals<node_ptr>  > unary_rule_type;

     typedef qi::rule< IteratorT, node_ptr( void ), skipper_type,
                       qi::locals<node_ptr, types::uint32 >  > unary_plus_rule_type;

     typedef qi::rule< IteratorT, node_ptr( void ), skipper_type,
                       qi::locals<node_ptr, node_ptr>  > binary_rule_type;

     typedef qi::rule< IteratorT, node_ptr( types::uint8 ), skipper_type,
              qi::locals<container_t>  > nary_rule_with_options_type;
     // the start rule
     grammar_sig_rule_type start;

     // macros:

     // macro_declarations:
     pass_rule_type macro_declaration_pm, macro_friend_decl_pm;
     grammar_sig_rule_type macro_var_decl_pm, macro_type_decl_pm, macro_import_decl_pm;
     grammar_sig_rule_type macro_single_var_decl_pm, macro_single_type_decl_pm;
     grammar_sig_rule_type macro_compound_decl_pm, macro_namespace_fwd_decl_pm;
     grammar_sig_rule_type macro_func_fwd_decl_pm, macro_oper_fwd_decl_pm;
     grammar_sig_rule_type macro_ctor_fwd_decl_pm, macro_ctor_decl_pm;
     grammar_sig_rule_type macro_dtor_fwd_decl_pm, macro_object_fwd_decl_pm;
     grammar_sig_rule_type macro_object_decl_pm, macro_namespace_alias_state_pm;
     unary_rule_type macro_namespace_decl_pm, macro_function_decl_pm;
     unary_rule_type macro_oper_decl_pm, macro_dtor_decl_pm, macro_forward_decl_pm;
     unary_rule_type macro_member_decl_pm;


     // macro_statements:
     pass_rule_type macro_expression_state_pm, macro_else_state_pm, macro_finally_state_pm;
     pass_rule_type macro_loop_state_pm, macro_statement_pm;
     unary_rule_type macro_label_state_pm, macro_jump_state_pm, macro_catch_state_pm;
     unary_rule_type macro_switch_state_pm, macro_if_state_pm, macro_elif_state_pm;
     unary_rule_type macro_while_loop_state_pm, macro_do_loop_state_pm;
     grammar_sig_rule_type macro_select_state_pm, macro_compound_state_pm;
     grammar_sig_rule_type macro_exception_state_pm, macro_for_loop_state_pm;
     grammar_sig_rule_type macro_using_state_pm;


     // macro expressions:
     pass_rule_type macro_this_expr_pm, macro_prime_expr_pm, macro_array_expr_pm;
     pass_rule_type macro_ctor_expr_pm, macro_member_expr_pm, macro_dtor_expr_pm;
     pass_rule_type macro_delete_expr_pm, macro_throw_expr_pm;
     unary_rule_type macro_multiply_expr_pm, macro_addition_expr_pm;
     unary_rule_type macro_bitshift_expr_pm, macro_relational_expr_pm;
     unary_rule_type macro_equality_expr_pm, macro_bitwiseAnd_expr_pm;
     unary_rule_type macro_bitwiseXor_expr_pm, macro_bitwiseOr_expr_pm;
     unary_rule_type macro_logicalAnd_expr_pm, macro_logicalXor_expr_pm;
     unary_rule_type macro_logicalOr_expr_pm, macro_sequence_expr_pm;
     unary_rule_type macro_condition_expr_pm;
     unary_plus_rule_type macro_assignment_expr_pm;

     grammar_sig_rule_type macro_func_call_expr_pm, macro_unary_expr_pm;
     grammar_sig_rule_type macro_identifier_expr_pm, macro_postfix_expr_pm;
     grammar_sig_rule_type macro_new_expr_pm, macro_conditional_expr_pm;

     // macro modifiers:
     nary_rule_with_options_type macro_tempid_mod_pm, macro_func_mod_pm;
     nary_rule_with_options_type macro_temp_decl_mod_pm, macro_func_decl_mod_pm;
     unary_rule_type macro_type_postfix_mod_pm;
     grammar_sig_rule_type macro_throw_mod_pm, macro_data_decl_mods_pm;
     grammar_sig_rule_type macro_func_decl_args_pm, macro_ctor_init_list_pm;
     grammar_sig_rule_type macro_ctor_decl_mod_pm, macro_dtor_decl_mod_pm;
     grammar_sig_rule_type macro_inherit_list_pm, macro_inherit_mem_pm;
     grammar_sig_rule_type macro_access_mod_pm, macro_type_infix_mods_pm;
     grammar_sig_rule_type macro_type_infix_decl_mods_pm;
     pass_rule_type macro_static_mod_pm, macro_const_mod_pm, macro_volatile_mod_pm;
     pass_rule_type macro_gen_decl_mod_pm, macro_abstract_mod_pm, macro_virtual_mod_pm;
     pass_rule_type macro_explicit_mod_pm;
     unary_rule_type macro_ctor_init_mem_pm, macro_attribute_mod_pm;

     // macro identifiers
     grammar_sig_rule_type macro_data_decl_pm;
     pass_rule_type macro_namespace_pm, macro_import_pm;
     grammar_sig_rule_type macro_type_ident_pm, macro_nested_name_pm;
     unary_rule_type macro_func_ident_pm, macro_oper_ident_pm;
     unary_rule_type macro_using_ident_pm;
     pass_rule_type macro_var_ident_pm, macro_identifier_pm;
     qi::rule< IteratorT, node_ptr( void ), skipper_type,
               qi::locals< string_type >
             > macro_basic_ident_pm;

     // the Script Language elements:
     // declarations:
     pass_rule_type declaration_pm, forward_decl_pm, friend_decl_pm;
     grammar_sig_rule_type union_decl_pm, enumerator_pm, namespace_alias_pm;
     grammar_sig_rule_type object_fwd_decl_pm, object_decl_pm, union_fwd_decl_pm;
     unary_rule_type destructor_decl_pm, member_decl_pm;
     grammar_sig_rule_type bitfield_decl_pm, enum_fwd_decl_pm, enum_decl_pm;
     grammar_sig_rule_type destructor_fwd_decl_pm, bitfield_fwd_decl_pm;
     grammar_sig_rule_type constructor_fwd_decl_pm, constructor_decl_pm;
     unary_rule_type namespace_decl_pm, function_decl_pm, operator_decl_pm;
     grammar_sig_rule_type function_fwd_decl_pm, operator_fwd_decl_pm;
     grammar_sig_rule_type namespace_fwd_decl_pm, nested_name_for_namespace_pm;
     qi::rule< IteratorT, node_ptr( void ), skipper_type,
               qi::locals< string_type >
             >  import_sys_str_pm;
     grammar_sig_rule_type import_decl_pm, type_decl_pm, compound_decl_pm;
     unary_rule_type single_var_decl_pm, single_const_decl_pm, single_type_decl_pm;
     grammar_sig_rule_type data_instance_decl_pm, var_decl_pm, const_decl_pm;

     // statements:
     pass_rule_type expression_state_pm, declaration_state_pm, else_state_pm;
     pass_rule_type default_state_pm, destination_state_pm, jump_state_pm;
     pass_rule_type break_state_pm, continue_state_pm, return_state_pm;
     pass_rule_type try_state_pm, finally_state_pm, loop_state_pm;
     unary_rule_type switch_state_pm, if_state_pm, elif_state_pm;
     unary_rule_type label_state_pm, case_state_pm, catch_state_pm;
     grammar_sig_rule_type select_state_pm, compound_state_pm, exception_state_pm;
     grammar_sig_rule_type for_state_pm, using_state_pm;
     unary_rule_type while_state_pm, do_state_pm;
     pass_rule_type statements_pm;

     // expressions:
     pass_rule_type throw_expr_pm;
     unary_rule_type sequence_expr_pm;
     unary_plus_rule_type assignment_expr_pm;
     grammar_sig_rule_type conditional_expr_pm, condition_expr_pm;
     unary_rule_type logicalAnd_expr_pm, logicalXor_expr_pm, logicalOr_expr_pm;
     unary_rule_type bitwiseAnd_expr_pm, bitwiseXor_expr_pm, bitwiseOr_expr_pm;
     unary_rule_type bitshift_expr_pm, relational_expr_pm, equality_expr_pm;
     unary_rule_type ptrMem_expr_pm, multiply_expr_pm, addition_expr_pm;
     grammar_sig_rule_type del_expr_pm;
     grammar_sig_rule_type postfix_expr_pm, unary_expr_pm, new_expr_pm;
     pass_rule_type memberRef_expr_pm, memberPtr_expr_pm, expl_dtor_expr_pm;
     pass_rule_type this_expr_pm, prime_expr_pm, array_expr_pm, expl_ctor_expr_pm;
     grammar_sig_rule_type identifier_pm, funcCall_expr_pm;
     // modifiers:

     // these parsers have not been init-ed in the ctor and do not apear there:




     // these parsers have been init-ed in the ctor
     unary_rule_type access_pm;
     binary_rule_type inherit_mem_pm;
     grammar_sig_rule_type dtor_decl_mods_pm, type_infix_decl_mods_pm;
     grammar_sig_rule_type inherit_list_pm, data_decl_pm, ctor_decl_mods_pm;
     grammar_sig_rule_type throw_pm, ctor_init_list_pm, ctor_init_mem_pm;
     pass_rule_type explicit_pm;
     nary_rule_with_options_type func_decl_mods_pm, func_ident_mods_pm;
     grammar_sig_rule_type func_decl_args_pm;
     unary_rule_type func_decl_arg_pm;
     pass_rule_type mutable_pm, abstract_pm, virtual_pm, callconv_pm;
     pass_rule_type genDecl_mod_pm, lang_pm, static_pm, const_pm, volatile_pm;
     grammar_sig_rule_type type_prefix_pm, type_infix_pm, type_postfix_pm;

     nary_rule_with_options_type template_ident_pm, template_decl_pm;
     unary_rule_type attribute_pm, template_decl_arg_pm;

     // identifiers
     pass_rule_type qualified_ident_pm, unqualified_ident_pm;
     grammar_sig_rule_type data_type_pm;
     grammar_sig_rule_type nested_name_ident_pm, using_ident_pm;
     pass_rule_type namespace_ident_pm, import_ident_pm;
     unary_rule_type function_ident_pm, operator_ident_pm;
     pass_rule_type label_ident_pm;
     grammar_sig_rule_type qual_typeid_ident_pm, unqual_typeid_ident_pm;
     pass_rule_type typeof_ident_pm;
     grammar_sig_rule_type function_type_arg_ident_pm, operator_type_ident_pm;
     grammar_sig_rule_type function_type_ident_pm, function_type_args_ident_pm;
     unary_rule_type union_ident_pm;
     unary_rule_type bitfield_ident_pm, enum_ident_pm, object_ident_pm;
     pass_rule_type var_ident_pm, const_ident_pm, typename_ident_pm;
     qi::rule< IteratorT, node_ptr( void ), skipper_type,
               qi::locals< string_type >
             > basic_ident_pm;

     //litterals
     qi::rule< IteratorT, node_ptr( types::uint8 ),
               skipper_type > litteral_pm; // literral group parser
     pass_rule_type number_lit_pm, bool_lit_pm;
     pass_rule_type uint_lit_pm, float_lit_pm;
     qi::rule< IteratorT, node_ptr( void ),
               skipper_type, qi::locals< types::boolean > > sint_lit_pm;
     qi::rule< IteratorT, node_ptr( types::uint8 ),
               skipper_type, qi::locals< string_type,
                                         wstring_type
                                       >
             > char_lit_pm, str_lit_pm;


   }; // end of struct gsvm_parser;

    } // end of namespace gsvm;
   } // end of namespace parsers;
  }// end of namespace frontends;
 } // end of namespace compilers;
} // end of namespace SK;

#endif
