#ifndef SK_COMPILERS_FRONTENDS_PARSERS_GSVM_SYMBOL_TABLES_HPP
#define SK_COMPILERS_FRONTENDS_PARSERS_GSVM_SYMBOL_TABLES_HPP

// #include <>

#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/qi_symbols.hpp>

// #include ""

#include "SK/utilities/types.hpp"
#include "SK/compilers/front_ends/AST/gsvm/lang_consts.hpp"
#include �SK/compilers/front_ends/AST/internals.hpp�
#include �SK/compilers/front_ends/parsers/internals.hpp�


namespace SK   
{   
 namespace compilers   
 {
  namespace parsers
  {   
   namespace gsvm   
   {   
    namespace spirit = boost::spirit;   
    namespace qi = boost::spirit::qi;  
    namespace phoenix = boost::phoenix;
    
    namespace types = SK::utility::types;
    namespace internals = SK::compilers::parsers::internals;
    namespace ast = SK::compilers::frontends::ast::internals;   
    namespace fe_ast = SK::compilers::frontends::ast;
    namespace id_c = fe_ast::gsvm::consts::IDs;
    // symbol tables:
    template< typename CharT >
    struct keyword_symbols;
    
    template<>
    struct keyword_symbols<char> : public qi::symbols<char>
    {
     keyword_symbols()
     {
      add("abstract")("addressof")("bitfield")("boolean")("break")("byte")
         ("case")("cast")("callconv")("catch")("char")("const")
         ( "constructor")("continue")("default")("delete")
         ("destructor")("do")("double")("doubleword")
         ("dynamic_cast")("elif")("else")("enum")("explicit")
         ("export")( "false")("finaly")("float")("for")("friend")
         ("function")("goto")("halfword")("if")("int")("intern")
         ("import")("lang")("long")("label")("mbchar")("namespace")
         ("new")("object")("octet")("private")("protected")("public")("return")
         ("reinterpret_cast")("self")("short")("signed")("sizeof")("static_cast")
         ("static")("switch")("template")("this")("throw")("true")("try")
         ("typedef")("typeid")("typeof")("typename")("typeinfo")("union")
         ("unsigned")("typename")("using")("var")("virtual")("vararg")("void")
         ("volatile")("vlchar")("wchar")("while")("word");
     }//end of ...
    };    
    
    template<>
    struct keyword_symbols<wchar_t> 
    : public qi::symbols<wchar_t>
    {
     keyword_symbols()
     {
      add(L"abstract")(L"addressof")(L"bitfield")(L"boolean")(L"break")(L"byte")
         (L"case")(L"cast")(L"callconv")(L"catch")(L"char")(L"const")
         (L"constructor")(L"continue")(L"default")(L"delete")
         (L"destructor")(L"do")(L"double")(L"doubleword")
         (L"dynamic_cast")(L"elif")(L"else")(L"enum")(L"explicit")
         (L"export")(L"false")(L"finaly")(L"float")(L"for")(L"friend")
         (L"function")(L"goto")(L"halfword")(L"if")(L"int")(L"intern")
         (L"import")(L"lang")(L"long")(L"label")(L"mbchar")(L"namespace")
         (L"new")(L"object")(L"octet")(L"private")(L"protected")(L"public")(L"return")
         (L"reinterpret_cast")(L"self")(L"short")(L"signed")(L"sizeof")(L"static_cast")
         (L"static")(L"switch")(L"template")(L"this")(L"throw")(L"true")(L"try")
         (L"typedef")(L"typeid")(L"typeof")(L"typename")(L"typeinfo")(L"union")
         (L"unsigned")(L"typename")(L"using")(L"var")(L"virtual")(L"vararg")(L"void")
         (L"volatile")(L"vlchar")(L"wchar")(L"while")(L"word");
     }//end of ...
    };
              
    template< typename CharT >
    struct operator_sym_table;
     
    template<>
    struct operator_sym_table<char> 
    : public qi::symbols< char, types::uint32 >
    {
     operator_sym_table()
     { // id_c::operators::
      add( "()", id_c::operators::functionCall  )// FUNC_CALL_OP_ID )
         ( "[]", id_c::operators::arrayIndex  )// ARRAY_INDEX_OP_ID )
         ( "->", id_c::operators::memberViaPtr  )// MEMBER_ACCESS_VIA_PTR_OP )
         ( "++", id_c::operators::Increment  )// INCREMENT_OP ) // postIncrement, preIncrement, // increment
         ( "--", id_c::operators::Decrement  )// DECREMENT_OP ) // postDecrement, preDecrement, // decrement
         ( "addressof",  id_c::operators::addressof_ ) // should this be a builtin function instead?
         ( "+", id_c::operators::Plus )// ADDITION_OP ) // addition // unaryPlus // plus
         ( "-", id_c::operators::Minus )// SUBTRACTION_OP ) // subtraction // unaryMinus // minus
         ( "!", id_c::operators::logicalNot  )//  LOGICAL_NOT_OP )
         ( "~", id_c::operators::bitwiseNot  )//BITWISE_NOT_OP ) 
         ( "&", id_c::operators::Ampersand )// BITWISE_AND_OP ) // bitwiseAnd // getAddress, // "&" - GETADDRESS_OP // ampersand
         ( "*",  id_c::operators::Star )// MULTIPLY_OP )// multiply // dereference, // "*" - DEREFERENCE_OP // star
         ( "cast", id_c::operators::cast )// TYPE_CAST_OP )
         ( "new", id_c::operators::dynamicAlloc )// DYNAMIC_ALLOCATE_OP )
         ( "delete", id_c::operators::dynamicDealloc )// DYNAMIC_DEALLOCATE_OP )
         ( "->*", id_c::operators::memberViaOffset )// MEMBER_ACCESS_VIA_PTR_TO_MEMBER_OP )
         ( "/", id_c::operators::division )// DIVISION_OP )
         ( "%", id_c::operators::modulus )// MODULUS_OP )
         ( "<<", id_c::operators::bitshiftLeft )// BITSHIFT_LEFT_OP )
         ( ">>", id_c::operators::bitshiftRightArith )// BITSHIFT_RIGHT_ARITHMETIC_OP )
         ( ">>>", id_c::operators::bitshiftRightLogic  )// BITSHIFT_RIGHT_LOGICAL_OP )
         ( "<", id_c::operators::lessThan )// LESS_THAN_OP )
         ( "<=", id_c::operators::lessThanEqual )// LESS_THAN_EQUAL_OP )
         ( ">", id_c::operators::greaterThan )// GREATER_THAN_OP )
         ( ">=",  id_c::operators::greaterThanEqual )// GREATER_THAN_EUQUAL_OP )
         ( "==",  id_c::operators::equals )// EQUALS_OP )
         ( "!=",  id_c::operators::notEquals )// NOT_EQUALS_OP )
         ( "^",  id_c::operators::bitwiseXor )// BITWISE_XOR_OP )
         ( "|",  id_c::operators::bitwiseOr ) // BITWISE_OR_OP )
         ( "&&",  id_c::operators::logicalAnd ) // LOGICAL_AND_OP )
         ( "^^",  id_c::operators::logicalXor )// LOGICAL_XOR_OP )
         ( "||",  id_c::operators::logicalOr )// LOGICAL_OR_OP )
         ( "=",  id_c::operators::assignment )// ASSIGNMENT_OP )
         ( "+=",  id_c::operators::assignmentAdd )// ASSIGNMENT_ADDITION_OP )
         ( "-=",  id_c::operators::assignmentSub )// ASSIGNMENT_SUBTRACTION_OP )
         ( "*=",  id_c::operators::assignmentMul )// ASSIGNMENT_MULTIPLY_OP )
         ( "/=",  id_c::operators::assignmentDiv )// ASSIGNMENT_DIVISION_OP )
         ( "%=",  id_c::operators::assignmentMod ) // ASSIGNMENT_MODULUS_OP )
         ( "&=",  id_c::operators::assignmentBitAnd )// ASSIGNMENT_BITWISE_AND_OP )
         ( "^=",  id_c::operators::assignmentBitXor )// ASSIGNMENT_BITWISE_XOR_OP )
         ( "|=", id_c::operators::assignmentBitOr ) // ASSIGNMENT_BITWISE_OR_OP
         ( "<<=", id_c::operators::assignmentBitSHL )// ASSIGNMENT_BITSHIFT_LEFT_OP )
         ( ">>=",  id_c::operators::assignmentBitSAR )// ASSIGNMENT_BITSHIFT_RIGHT_ARITHMETIC_OP )
         ( ">>>=", id_c::operators::assignmentBitSLR )// ASSIGNMENT_BITSHIFT_RIGHT_LOGICAL_OP )
         ( ",", id_c::operators::sequence ); //  SEQUENCE_OP 
     }// end of ...
    };
     
    template<>
    struct operator_sym_table< wchar_t > 
    : public qi::symbols< wchar_t, types::uint32 >
    {
     operator_sym_table()
     { // id_c::operators::
      add( L"()", id_c::operators::functionCall  )// FUNC_CALL_OP_ID )
         ( L"[]", id_c::operators::arrayIndex  )// ARRAY_INDEX_OP_ID )
         ( L"->", id_c::operators::memberViaPtr  )// MEMBER_ACCESS_VIA_PTR_OP )
         ( L"++", id_c::operators::Increment  )// INCREMENT_OP ) // postIncrement, preIncrement, // increment
         ( L"--", id_c::operators::Decrement  )// DECREMENT_OP ) // postDecrement, preDecrement, // decrement
         ( L"addressof",  id_c::operators::addressof_ ) // should this be a builtin function instead?
         ( L"+", id_c::operators::Plus )// ADDITION_OP ) // addition // unaryPlus // plus
         ( L"-", id_c::operators::Minus )// SUBTRACTION_OP ) // subtraction // unaryMinus // minus
         ( L"!", id_c::operators::logicalNot  )// LOGICAL_NOT_OP ) 
         ( L"~", id_c::operators::bitwiseNot  )// BITWISE_NOT_OP ) 
         ( L"&", id_c::operators::Ampersand )// BITWISE_AND_OP ) // bitwiseAnd // getAddress, // "&" - GETADDRESS_OP // ampersand
         ( L"*", id_c::operators::Star )//MULTIPLY_OP )  // multiply // dereference, // "*" - DEREFERENCE_OP // star
         ( L"cast", id_c::operators::cast )// TYPE_CAST_OP )
         ( L"new", id_c::operators::dynamicAlloc )// DYNAMIC_ALLOCATE_OP )
         ( L"delete", id_c::operators::dynamicDealloc )// DYNAMIC_DEALLOCATE_OP )
         ( L"->*", id_c::operators::memberViaOffset )// MEMBER_ACCESS_VIA_PTR_TO_MEMBER_OP )
         ( L"/", id_c::operators::division )// DIVISION_OP )
         ( L"%", id_c::operators::modulus )// MODULUS_OP )
         ( L"<<", id_c::operators::bitshiftLeft )// BITSHIFT_LEFT_OP )
         ( L">>", id_c::operators::bitshiftRightArith )// BITSHIFT_RIGHT_ARITHMETIC_OP )
         ( L">>>", id_c::operators::bitshiftRightLogic  )// BITSHIFT_RIGHT_LOGICAL_OP )
         ( L"<", id_c::operators::lessThan )// LESS_THAN_OP )
         ( L"<=", id_c::operators::lessThanEqual )// LESS_THAN_EQUAL_OP )
         ( L">", id_c::operators::greaterThan )// GREATER_THAN_OP )
         ( L">=",  id_c::operators::greaterThanEqual )// GREATER_THAN_EUQUAL_OP )
         ( L"==",  id_c::operators::equals )// EQUALS_OP )
         ( L"!=",  id_c::operators::notEquals )// NOT_EQUALS_OP )
         ( L"^",  id_c::operators::bitwiseXor )// BITWISE_XOR_OP )
         ( L"|",  id_c::operators::bitwiseOr ) // BITWISE_OR_OP )
         ( L"&&",  id_c::operators::logicalAnd ) // LOGICAL_AND_OP )
         ( L"^^",  id_c::operators::logicalXor )// LOGICAL_XOR_OP )
         ( L"||",  id_c::operators::logicalOr )// LOGICAL_OR_OP )
         ( L"=",  id_c::operators::assignment )// ASSIGNMENT_OP )
         ( L"+=",  id_c::operators::assignmentAdd )// ASSIGNMENT_ADDITION_OP )
         ( L"-=",  id_c::operators::assignmentSub )// ASSIGNMENT_SUBTRACTION_OP )
         ( L"*=",  id_c::operators::assignmentMul )// ASSIGNMENT_MULTIPLY_OP )
         ( L"/=",  id_c::operators::assignmentDiv )// ASSIGNMENT_DIVISION_OP )
         ( L"%=",  id_c::operators::assignmentMod ) // ASSIGNMENT_MODULUS_OP )
         ( L"&=",  id_c::operators::assignmentBitAnd )// ASSIGNMENT_BITWISE_AND_OP )
         ( L"^=",  id_c::operators::assignmentBitXor )// ASSIGNMENT_BITWISE_XOR_OP )
         ( L"|=", id_c::operators::assignmentBitOr ) // ASSIGNMENT_BITWISE_OR_OP
         ( L"<<=", id_c::operators::assignmentBitSHL )// ASSIGNMENT_BITSHIFT_LEFT_OP )
         ( L">>=",  id_c::operators::assignmentBitSAR )// ASSIGNMENT_BITSHIFT_RIGHT_ARITHMETIC_OP )
         ( L">>>=", id_c::operators::assignmentBitSLR )// ASSIGNMENT_BITSHIFT_RIGHT_LOGICAL_OP )
         ( L",", id_c::operators::sequence ); //  SEQUENCE_OP 
     }// end of ...
    };
        
    template< typename CharT >
    struct builtin_function_sym_table;
    
    template<>
    struct builtin_function_sym_table< char >
    : public qi::symbols< char, types::uint32 > 
    {
     builtin_function_sym_table()
     {
      add( "dynamic_cast", id_c::builtin_funcs::dynamicCast )
         ( "reinterpret_cast", id_c::builtin_funcs::reinterpretCast )
         ( "sizeof", id_c::builtin_funcs::sizeof_ )
         ( "static_cast", id_c::builtin_funcs::staticCast )
         ( "typeid", id_c::builtin_funcs::typeid_ );
     }// end of...
    };
    template<>
    struct builtin_function_sym_table< wchar_t >
    : public qi::symbols< wchar_t, types::uint32 > 
    {
     builtin_function_sym_table()
     {
      add( L"dynamic_cast", id_c::builtin_funcs::dynamicCast )
         ( L"reinterpret_cast", id_c::builtin_funcs::reinterpretCast )
         ( L"sizeof", id_c::builtin_funcs::sizeof_ )
         ( L"static_cast", id_c::builtin_funcs::staticCast )
         ( L"typeid", id_c::builtin_funcs::typeid_ );
     }// end of...
    };    
    
    template< typename CharT >    
    struct builtin_type_sym_table;
    
    template<>    
    struct builtin_type_sym_table< char >
    : public qi::symbols< char, types::uint32 > 
    {
     builtin_type_sym_table()
     {
      add( "boolean", id_c::builtin_types::boolean )
         ( "byte", id_c::builtin_types::byte )
         ( "char", id_c::builtin_types::char_ )
         ( "double", id_c::builtin_types::double_ )
         ( "doubleword", id_c::builtin_types::doubleword )
         ( "float", id_c::builtin_types::float_ )
         ( "halfword", id_c::builtin_types::halfword )
         ( "int", id_c::builtin_types::int_ )
         ( "long", id_c::builtin_types::long_ )
         ( "mbchar", id_c::builtin_types::mbchar_ )
         ( "short", id_c::builtin_types::short_ )
         ( "signed", id_c::builtin_types::signed_ )
         ( "typeinfo", id_c::builtin_types::typeinfo )
         ( "unsigned", id_c::builtin_types::unsigned_ )
         ( "void", id_c::builtin_types::void_ )
         ( "vlchar", id_c::builtin_types::vlchar )
         ( "vararg", id_c::builtin_types::vararg )
         ( "wchar", id_c::builtin_types::wchar )
         ( "word", id_c::builtin_types::word )
         ( "octet", id_c::builtin_types::octet );
     }// end of ...
    };
    
    template<>    
    struct builtin_type_sym_table< wchar_t >
    : public qi::symbols< wchar_t, types::uint32 > 
    {
     builtin_type_sym_table()
     {
      add( L"boolean", id_c::builtin_types::boolean )
         ( L"byte", id_c::builtin_types::byte )
         ( L"char", id_c::builtin_types::char_ )
         ( L"double", id_c::builtin_types::double_ )
         ( L"doubleword", id_c::builtin_types::doubleword )
         ( L"float", id_c::builtin_types::float_ )
         ( L"halfword", id_c::builtin_types::halfword )
         ( L"int", id_c::builtin_types::int_ )
         ( L"long", id_c::builtin_types::long_ )
         ( L"mbchar", id_c::builtin_types::mbchar_ )
         ( L"short", id_c::builtin_types::short_ )
         ( L"signed", id_c::builtin_types::signed_ )
         ( L"typeinfo", id_c::builtin_types::typeinfo )
         ( L"unsigned", id_c::builtin_types::unsigned_ )
         ( L"void", id_c::builtin_types::void_ )
         ( L"vlchar", id_c::builtin_types::vlchar )
         ( L"vararg", id_c::builtin_types::vararg )
         ( L"wchar", id_c::builtin_types::wchar )
         ( L"word", id_c::builtin_types::word )
         ( L"octet", id_c::builtin_types::octet );
     }// end of ...
    };
    
    template< typename CharT >
    struct unary_expr_sym_table;
     
    template<>
    struct unary_expr_sym_table< char > 
    : public qi::symbols< char, types::uint32 > 
    {
     unary_expr_sym_table()
     {
      add( "++", id_c::expressions::preIncrement )
         ( "--", id_c::expressions::preDecrement )
         ( "+", id_c::expressions::unaryPlus )
         ( "-", id_c::expressions::unaryMinus )
         ( "!", id_c::expressions::logicalNot )
         ( "~", id_c::expressions::bitwiseNot )
         ( "&", id_c::expressions::getAddress )
         ( "addressof", id_c::expressions::addressof_ )
         ( "*", id_c::expressions::dereference );
     }// end of ...
    };
    
    template<>
    struct unary_expr_sym_table< wchar_t > 
    : public qi::symbols< wchar_t, types::uint32 > 
    {
     unary_expr_sym_table()
     {
      add( L"++", id_c::expressions::preIncrement )
         ( L"--", id_c::expressions::preDecrement )
         ( L"+", id_c::expressions::unaryPlus )
         ( L"-", id_c::expressions::unaryMinus )
         ( L"!", id_c::expressions::logicalNot )
         ( L"~", id_c::expressions::bitwiseNot )
         ( L"&", id_c::expressions::getAddress )
         ( L"addressof", id_c::expressions::addressof_ )
         ( L"*", id_c::expressions::dereference );
     }// end of ...
    };
    
    template< typename CharT >
    struct assignment_sym_table; 
     
    template<>
    struct assignment_sym_table< char > 
    : public qi::symbols< char, types::uint32 > 
    {
     assignment_sym_table()
     {
      add( "=" , id_c::expressions::assign )
       ( "*=" , id_c::expressions::assignMult )
       ( "/=" , id_c::expressions::assignDiv )
       ( "%=" , id_c::expressions::assignMod )
       ( "+=" , id_c::expressions::assignAdd )
       ( "-=" , id_c::expressions::assignSub )
       ( "<<=" , id_c::expressions::assignShL )
       ( ">>=" , id_c::expressions::assignSAR )
       ( ">>>=" , id_c::expressions::assignSLR )
       ( "&=" , id_c::expressions::assignBitAnd )
       ( "^=" , id_c::expressions::assignBitXor )
       ( "|=" , id_c::expressions::assignBitOr );
     }// end of ...
    };
    
    template<>
    struct assignment_sym_table< wchar_t > 
    : public qi::symbols< wchar_t, types::uint32 > 
    {
     assignment_sym_table()
     {
      add( L"=" , id_c::expressions::assign )
       ( L"*=" , id_c::expressions::assignMult )
       ( L"/=" , id_c::expressions::assignDiv )
       ( L"%=" , id_c::expressions::assignMod )
       ( L"+=" , id_c::expressions::assignAdd )
       ( L"-=" , id_c::expressions::assignSub )
       ( L"<<=" , id_c::expressions::assignShL )
       ( L">>=" , id_c::expressions::assignSAR )
       ( L">>>=" , id_c::expressions::assignSLR )
       ( L"&=" , id_c::expressions::assignBitAnd )
       ( L"^=" , id_c::expressions::assignBitXor )
       ( L"|=" , id_c::expressions::assignBitOr );
     }// end of ...
    };
    
    template< typename CharT >
    struct builtin_macro_func_sym_table;
     
    template<>
    struct builtin_macro_func_sym_table< char > 
    : public qi::symbols< char, types::uint32 > 
    {
     builtin_macro_func_sym_table() // id_c::macro_builtin_funcs::
     { 
      add( "is_const" , id_c::macro_builtin_funcs::is_const )
         ( "is_func" , id_c::macro_builtin_funcs::is_func )
         ( "is_float" , id_c::macro_builtin_funcs::is_float )
         ( "is_int" , id_c::macro_builtin_funcs::is_int )
         ( "is_oper" , id_c::macro_builtin_funcs::is_oper )
         ( "is_str" , id_c::macro_builtin_funcs::is_str )
         ( "is_type" , id_c::macro_builtin_funcs::is_type )
         ( "is_var" , id_c::macro_builtin_funcs::is_var )
         ( "length" , id_c::macro_builtin_funcs::length )
         ( "typeof" , id_c::macro_builtin_funcs::typeof_ )
         ( "dynamic_cast" , id_c::macro_builtin_funcs::dynamic_cast_ )
         ( "reinterpret_cast" , id_c::macro_builtin_funcs::reinterpret_cast_ )
         ( "sizeof" , id_c::macro_builtin_funcs::sizeof_ )
         ( "static_cast" , id_c::macro_builtin_funcs::static_cast_ )
         ( "typeid" , id_c::macro_builtin_funcs::typeid_ );
     }// end of ...
    };
    
    template<>
    struct builtin_macro_func_sym_table< wchar_t > 
    : public qi::symbols< wchar_t, types::uint32 > 
    {
     builtin_macro_func_sym_table() // id_c::macro_builtin_funcs::
     {
      add( L"is_const" , id_c::macro_builtin_funcs::is_const )
         ( L"is_func" , id_c::macro_builtin_funcs::is_func )
         ( L"is_float" , id_c::macro_builtin_funcs::is_float )
         ( L"is_int" , id_c::macro_builtin_funcs::is_int )
         ( L"is_oper" , id_c::macro_builtin_funcs::is_oper )
         ( L"is_str" , id_c::macro_builtin_funcs::is_str )
         ( L"is_type" , id_c::macro_builtin_funcs::is_type )
         ( L"is_var" , id_c::macro_builtin_funcs::is_var )
         ( L"length" , id_c::macro_builtin_funcs::length )
         ( L"typeof" , id_c::macro_builtin_funcs::typeof_ )
         ( L"dynamic_cast" , id_c::macro_builtin_funcs::dynamic_cast_ )
         ( L"reinterpret_cast" , id_c::macro_builtin_funcs::reinterpret_cast_ )
         ( L"sizeof" , id_c::macro_builtin_funcs::sizeof_ )
         ( L"static_cast" , id_c::macro_builtin_funcs::static_cast_ )
         ( L"typeid" , id_c::macro_builtin_funcs::typeid_ )
     }// end of ...
    };
    
    template< typename CharT >
    struct builtin_macro_type_sym_table;
     
    template<>
    struct builtin_macro_type_sym_table< char > 
    : public qi::symbols< char, types::uint32 > 
    {
     builtin_macro_type_sym_table() // id_c::macro_builtin_types::
     {
      add( "boolean" , id_c::macro_builtin_types::bool_ )
         ( "char" , id_c::macro_builtin_types::char_ )
         ( "int" , id_c::macro_builtin_types::int_ )
         ( "float" , id_c::macro_builtin_types::float_ )
         ( "string" , id_c::macro_builtin_types::string_ )
         ( "typeinfo" , id_c::macro_builtin_types::typeinfo_ )
         ( "var" , id_c::macro_builtin_types::var_ )
         ( "vararg" , id_c::macro_builtin_types::vararg_ )
         ( "void" , id_c::macro_builtin_types::void_ )
         ( "doc_type" , id_c::macro_builtin_types::doc_ )
         ( "code_type" , id_c::macro_builtin_types::code_ )
         ( "error_type" , id_c::macro_builtin_types::error_ )
         ( "call_type" , id_c::macro_builtin_types::call_ )
         ( "system_type" , id_c::macro_builtin_types::system_ );
     }// end of ...
    };
    
    template<>
    struct builtin_macro_type_sym_table< wchar_t > 
    : public qi::symbols< wchar_t, types::uint32 > 
    {
     builtin_macro_type_sym_table() // id_c::macro_builtin_types::
     {
      add( L"boolean" , id_c::macro_builtin_types::bool_ )
         ( L"char" , id_c::macro_builtin_types::char_ )
         ( L"int" , id_c::macro_builtin_types::int_ )
         ( L"float" , id_c::macro_builtin_types::float_ )
         ( L"string" , id_c::macro_builtin_types::string_ )
         ( L"typeinfo" , id_c::macro_builtin_types::typeinfo_ )
         ( L"var" , id_c::macro_builtin_types::var_ )
         ( L"vararg" , id_c::macro_builtin_types::vararg_ )
         ( L"void" , id_c::macro_builtin_types::void_ )
         ( L"doc_type" , id_c::macro_builtin_types::doc_ )
         ( L"code_type" , id_c::macro_builtin_types::code_ )
         ( L"error_type" , id_c::macro_builtin_types::error_ )
         ( L"call_type" , id_c::macro_builtin_types::call_ )
         ( L"system_type" , id_c::macro_builtin_types::system_ );
     }// end of ...
    };
    
    template< typename CharT >
    struct macro_operators_sym_table;
     
    template<>
    struct macro_operators_sym_table< char > 
    : public qi::symbols< char, types::uint32 > 
    { 
     macro_operators_sym_table() // id_c::_::
     {
      add( "()", id_c::macro_operators::functionCall  )// FUNC_CALL_OP_ID )
         ( "[]", id_c::macro_operators::arrayIndex  )// ARRAY_INDEX_OP_ID )
         ( "->", id_c::macro_operators::memberViaPtr  )// MEMBER_ACCESS_VIA_PTR_OP )
         ( "++", id_c::macro_operators::Increment  )// INCREMENT_OP ) // postIncrement, preIncrement, // increment
         ( "--", id_c::macro_operators::Decrement  )// DECREMENT_OP ) // postDecrement, preDecrement, // decrement
         ( "+", id_c::macro_operators::Plus )// ADDITION_OP ) // addition // unaryPlus // plus
         ( "-", id_c::macro_operators::Minus )// SUBTRACTION_OP ) // subtraction // unaryMinus // minus
         ( "!", id_c::macro_operators::logicalNot  )//  LOGICAL_NOT_OP )
         ( "~", id_c::macro_operators::bitwiseNot  )//BITWISE_NOT_OP ) 
         ( "&", id_c::macro_operators::Ampersand )// BITWISE_AND_OP ) // bitwiseAnd // getAddress, // "&" - GETADDRESS_OP // ampersand
         ( "*",  id_c::macro_operators::Star )// MULTIPLY_OP )// multiply // dereference, // "*" - DEREFERENCE_OP // star
         ( "cast", id_c::macro_operators::cast )// TYPE_CAST_OP )
         ( "/", id_c::macro_operators::division )// DIVISION_OP )
         ( "%", id_c::macro_operators::modulus )// MODULUS_OP )
         ( "<<", id_c::macro_operators::bitshiftLeft )// BITSHIFT_LEFT_OP )
         ( ">>", id_c::macro_operators::bitshiftRightArith )// BITSHIFT_RIGHT_ARITHMETIC_OP )
         ( ">>>", id_c::macro_operators::bitshiftRightLogic  )// BITSHIFT_RIGHT_LOGICAL_OP )
         ( "<", id_c::macro_operators::lessThan )// LESS_THAN_OP )
         ( "<=", id_c::macro_operators::lessThanEqual )// LESS_THAN_EQUAL_OP )
         ( ">", id_c::macro_operators::greaterThan )// GREATER_THAN_OP )
         ( ">=",  id_c::macro_operators::greaterThanEqual )// GREATER_THAN_EUQUAL_OP )
         ( "==",  id_c::macro_operators::equals )// EQUALS_OP )
         ( "!=",  id_c::macro_operators::notEquals )// NOT_EQUALS_OP )
         ( "^",  id_c::macro_operators::bitwiseXor )// BITWISE_XOR_OP )
         ( "|",  id_c::macro_operators::bitwiseOr ) // BITWISE_OR_OP )
         ( "&&",  id_c::macro_operators::logicalAnd ) // LOGICAL_AND_OP )
         ( "^^",  id_c::macro_operators::logicalXor )// LOGICAL_XOR_OP )
         ( "||",  id_c::macro_operators::logicalOr )// LOGICAL_OR_OP )
         ( "=",  id_c::macro_operators::assignment )// ASSIGNMENT_OP )
         ( "+=",  id_c::macro_operators::assignmentAdd )// ASSIGNMENT_ADDITION_OP )
         ( "-=",  id_c::macro_operators::assignmentSub )// ASSIGNMENT_SUBTRACTION_OP )
         ( "*=",  id_c::macro_operators::assignmentMul )// ASSIGNMENT_MULTIPLY_OP )
         ( "/=",  id_c::macro_operators::assignmentDiv )// ASSIGNMENT_DIVISION_OP )
         ( "%=",  id_c::macro_operators::assignmentMod ) // ASSIGNMENT_MODULUS_OP )
         ( "&=",  id_c::macro_operators::assignmentBitAnd )// ASSIGNMENT_BITWISE_AND_OP )
         ( "^=",  id_c::macro_operators::assignmentBitXor )// ASSIGNMENT_BITWISE_XOR_OP )
         ( "|=", id_c::macro_operators::assignmentBitOr ) // ASSIGNMENT_BITWISE_OR_OP
         ( "<<=", id_c::macro_operators::assignmentBitSHL )// ASSIGNMENT_BITSHIFT_LEFT_OP )
         ( ">>=",  id_c::macro_operators::assignmentBitSAR )// ASSIGNMENT_BITSHIFT_RIGHT_ARITHMETIC_OP )
         ( ">>>=", id_c::macro_operators::assignmentBitSLR )// ASSIGNMENT_BITSHIFT_RIGHT_LOGICAL_OP )
         ( ",", id_c::macro_operators::sequence );
     }// end of ...
    };
    
    template<>
    struct macro_operators_sym_table< wchar_t > 
    : public qi::symbols< wchar_t, types::uint32 > 
    {
     macro_operators_sym_table() // id_c::_::
     {
      add( L"()", id_c::macro_operators::functionCall  )// FUNC_CALL_OP_ID )
         ( L"[]", id_c::macro_operators::arrayIndex  )// ARRAY_INDEX_OP_ID )
         ( L"->", id_c::macro_operators::memberViaPtr  )// MEMBER_ACCESS_VIA_PTR_OP )
         ( L"++", id_c::macro_operators::Increment  )// INCREMENT_OP ) // postIncrement, preIncrement, // increment
         ( L"--", id_c::macro_operators::Decrement  )// DECREMENT_OP ) // postDecrement, preDecrement, // decrement
         ( L"+", id_c::macro_operators::Plus )// ADDITION_OP ) // addition // unaryPlus // plus
         ( L"-", id_c::macro_operators::Minus )// SUBTRACTION_OP ) // subtraction // unaryMinus // minus
         ( L"!", id_c::macro_operators::logicalNot  )//  LOGICAL_NOT_OP )
         ( L"~", id_c::macro_operators::bitwiseNot  )//BITWISE_NOT_OP ) 
         ( L"&", id_c::macro_operators::Ampersand )// BITWISE_AND_OP ) // bitwiseAnd // getAddress, // "&" - GETADDRESS_OP // ampersand
         ( L"*",  id_c::macro_operators::Star )// MULTIPLY_OP )// multiply // dereference, // "*" - DEREFERENCE_OP // star
         ( L"cast", id_c::macro_operators::cast )// TYPE_CAST_OP )
         ( L"/", id_c::macro_operators::division )// DIVISION_OP )
         ( L"%", id_c::macro_operators::modulus )// MODULUS_OP )
         ( L"<<", id_c::macro_operators::bitshiftLeft )// BITSHIFT_LEFT_OP )
         ( L">>", id_c::macro_operators::bitshiftRightArith )// BITSHIFT_RIGHT_ARITHMETIC_OP )
         ( L">>>", id_c::macro_operators::bitshiftRightLogic  )// BITSHIFT_RIGHT_LOGICAL_OP )
         ( L"<", id_c::macro_operators::lessThan )// LESS_THAN_OP )
         ( L"<=", id_c::macro_operators::lessThanEqual )// LESS_THAN_EQUAL_OP )
         ( L">", id_c::macro_operators::greaterThan )// GREATER_THAN_OP )
         ( L">=",  id_c::macro_operators::greaterThanEqual )// GREATER_THAN_EUQUAL_OP )
         ( L"==",  id_c::macro_operators::equals )// EQUALS_OP )
         ( L"!=",  id_c::macro_operators::notEquals )// NOT_EQUALS_OP )
         ( L"^",  id_c::macro_operators::bitwiseXor )// BITWISE_XOR_OP )
         ( L"|",  id_c::macro_operators::bitwiseOr ) // BITWISE_OR_OP )
         ( L"&&",  id_c::macro_operators::logicalAnd ) // LOGICAL_AND_OP )
         ( L"^^",  id_c::macro_operators::logicalXor )// LOGICAL_XOR_OP )
         ( L"||",  id_c::macro_operators::logicalOr )// LOGICAL_OR_OP )
         ( L"=",  id_c::macro_operators::assignment )// ASSIGNMENT_OP )
         ( L"+=",  id_c::macro_operators::assignmentAdd )// ASSIGNMENT_ADDITION_OP )
         ( L"-=",  id_c::macro_operators::assignmentSub )// ASSIGNMENT_SUBTRACTION_OP )
         ( L"*=",  id_c::macro_operators::assignmentMul )// ASSIGNMENT_MULTIPLY_OP )
         ( L"/=",  id_c::macro_operators::assignmentDiv )// ASSIGNMENT_DIVISION_OP )
         ( L"%=",  id_c::macro_operators::assignmentMod ) // ASSIGNMENT_MODULUS_OP )
         ( L"&=",  id_c::macro_operators::assignmentBitAnd )// ASSIGNMENT_BITWISE_AND_OP )
         ( L"^=",  id_c::macro_operators::assignmentBitXor )// ASSIGNMENT_BITWISE_XOR_OP )
         ( L"|=", id_c::macro_operators::assignmentBitOr ) // ASSIGNMENT_BITWISE_OR_OP
         ( L"<<=", id_c::macro_operators::assignmentBitSHL )// ASSIGNMENT_BITSHIFT_LEFT_OP )
         ( L">>=",  id_c::macro_operators::assignmentBitSAR )// ASSIGNMENT_BITSHIFT_RIGHT_ARITHMETIC_OP )
         ( L">>>=", id_c::macro_operators::assignmentBitSLR )// ASSIGNMENT_BITSHIFT_RIGHT_LOGICAL_OP )
         ( L",", id_c::macro_operators::sequence );
     }// end of ...
    };
    
    template< typename CharT >
    struct macro_unary_expr_sym_table;
     
    template<>
    struct macro_unary_expr_sym_table< char > 
    : public qi::symbols< char, types::uint32 > 
    { 
     macro_unary_expr_sym_table() // id_c::_::
     {
      add( "++", id_c::macro_expressions::preIncrement )
         ( "--", id_c::macro_expressions::preDecrement )
         ( "+", id_c::macro_expressions::unaryPlus )
         ( "-", id_c::macro_expressions::unaryMinus )
         ( "!", id_c::macro_expressions::logicalNot )
         ( "~", id_c::macro_expressions::bitwiseNot )
         ( "&", id_c::macro_expressions::getAddress_ )
         ( "*", id_c::macro_expressions::deref );
     }// end of ...
    };
    
    template<>
    struct macro_unary_expr_sym_table< wchar_t > 
    : public qi::symbols< wchar_t, types::uint32 > 
    {
     macro_unary_expr_sym_table() // id_c::_::
     {
      add( L"++", id_c::macro_expressions::preIncrement )
         ( L"--", id_c::macro_expressions::preDecrement )
         ( L"+", id_c::macro_expressions::unaryPlus )
         ( L"-", id_c::macro_expressions::unaryMinus )
         ( L"!", id_c::macro_expressions::logicalNot )
         ( L"~", id_c::macro_expressions::bitwiseNot )
         ( L"&", id_c::macro_expressions::getAddress_ )
         ( L"*", id_c::macro_expressions::deref );
     }// end of ...
    };
    
    // macro_assignment_expr
    
    template< typename CharT >
    struct macro_assignment_expr_sym_table;
     
    template<>
    struct macro_assignment_expr_sym_table< char > 
    : public qi::symbols< char, types::uint32 > 
    { 
     macro_assignment_expr_sym_table() // id_c::_::
     {
      add( "=" , id_c::macro_expressions::assign )
       ( "*=" , id_c::macro_expressions::assignMult )
       ( "/=" , id_c::macro_expressions::assignDiv )
       ( "%=" , id_c::macro_expressions::assignMod )
       ( "+=" , id_c::macro_expressions::assignAdd )
       ( "-=" , id_c::macro_expressions::assignSub )
       ( "<<=" , id_c::macro_expressions::assignShL )
       ( ">>=" , id_c::macro_expressions::assignSAR )
       ( ">>>=" , id_c::macro_expressions::assignSLR )
       ( "&=" , id_c::macro_expressions::assignBitAnd )
       ( "^=" , id_c::macro_expressions::assignBitXor )
       ( "|=" , id_c::macro_expressions::assignBitOr );
     }// end of ...
    };
    
    /*
    
    ( "=" , id_c::macro_expressions::assign )
       ( "*=" , id_c::macro_expressions::assignMult )
       ( "/=" , id_c::macro_expressions::assignDiv )
       ( "%=" , id_c::macro_expressions::assignMod )
       ( "+=" , id_c::macro_expressions::assignAdd )
       ( "-=" , id_c::macro_expressions::assignSub )
       ( "<<=" , id_c::macro_expressions::assignShL )
       ( ">>=" , id_c::macro_expressions::assignSAR )
       ( ">>>=" , id_c::macro_expressions::assignSLR )
       ( "&=" , id_c::macro_expressions::assignBitAnd )
       ( "^=" , id_c::macro_expressions::assignBitXor )
       ( "|=" , id_c::macro_expressions::assignBitOr )
    
    */
    
    template<>
    struct macro_assignment_expr_sym_table< wchar_t > 
    : public qi::symbols< wchar_t, types::uint32 > 
    {
     macro_assignment_expr_sym_table() // id_c::_::
     {
      add( L"=" , id_c::macro_expressions::assign )
       ( L"*=" , id_c::macro_expressions::assignMult )
       ( L"/=" , id_c::macro_expressions::assignDiv )
       ( L"%=" , id_c::macro_expressions::assignMod )
       ( L"+=" , id_c::macro_expressions::assignAdd )
       ( L"-=" , id_c::macro_expressions::assignSub )
       ( L"<<=" , id_c::macro_expressions::assignShL )
       ( L">>=" , id_c::macro_expressions::assignSAR )
       ( L">>>=" , id_c::macro_expressions::assignSLR )
       ( L"&=" , id_c::macro_expressions::assignBitAnd )
       ( L"^=" , id_c::macro_expressions::assignBitXor )
       ( L"|=" , id_c::macro_expressions::assignBitOr );
     }// end of ...
    };
    
    
    
    /*
    template< typename CharT >
    struct _sym_table;
     
    template<>
    struct _sym_table< char > 
    : public qi::symbols< char, types::uint32 > 
    { 
     _sym_table() // id_c::_::
     {
      add( "" , );
     }// end of ...
    };
    
    template<>
    struct _sym_table< wchar_t > 
    : public qi::symbols< wchar_t, types::uint32 > 
    {
     _sym_table() // id_c::_::
     {
      add( L"" , );
     }// end of ...
    };
        
    
  
    
    struct builtin_macro_var_symbols : public symbols<std::size_t,char>
    {
     builtin_macro_var_symbols()
     {
      add( "system" , SYSTEM_MBV )
     }//end of ...
     
    };
    
    */
   } // end of namespace gsvm;   
  } // end of namespace parsers;  
 } // end of namespace compilers;   
} // end of namespace SK;   

#endif
