#include <cstdlib>
#include <iostream>
#include <string>
#include <vector>
#include <iomanip>

#define BOOST_SPIRIT_USE_PHOENIX_V3

#include <boost/spirit/include/qi.hpp>
#include <boost/phoenix/core.hpp> 
#include <boost/phoenix/function.hpp> 
#include <boost/phoenix/operator.hpp> 
//#include <boost/phoenix/statement.hpp>
//#include <boost/phoenix/object.hpp> 
//#include <boost/phoenix/scope.hpp> 
#include <boost/phoenix/stl/container.hpp>


#include <SK/utility/types/basic_types.hpp>
#include <SK/compilers/front_ends/parsers/internals.hpp>

namespace pi = SK::compilers::frontends::parsers::internals;
namespace types = SK::utility::types;
namespace qi = boost::spirit::qi;
namespace phoenix = boost::phoenix;

int main(int argc, char *argv[])
{
   // first we will test the parser`s internal parsers 
   std::cout << "Begining test of SK.Compilers.FrontEnds.Parsers.Internals" << std::endl;
   {
    // first among the internals we will test the c_lang_escape_parser
    pi::c_lang_escape_parser< std::string::const_iterator > esc_p;
    std::string test_string1 = "\n\b\t\r\\\'\"\n\0";
    std::string::const_iterator beg( test_string1.begin() ), 
                                end_( test_string1.end() );
    std::vector< char > vc;          
    std::cout << "Begining test 1: " << std::endl
              << "test of C-Lang-Escape-Parser with std::string and char" 
              << std::endl;
    bool b = qi::parse( beg, end_, esc_p[ phoenix::push_back( phoenix::ref(vc) , qi::_1 ) ] );    
    if( b )
    {
     std::cout << "Parse Test Successfull!" << std::endl
               << "parsed: " << vc.size() << " characters out of "
               << test_string1.size() << "." << std::endl
               << "Begining dump of vector: " << std::endl;
     for( std::vector< char >::iterator i = vc.begin(); i != vc.end(); i++ )
     {
      std::cout << i - vc.begin() << ": " << std::hex << static_cast<int>(*i) 
                << std::dec << " , " << *i << " ." << std::endl;
     }
    }
    else
    {
     std::cout << "Parse Test Failed!" << std::endl;
    }         
   }
    
    
      
    
    // end of internal parser`s test
    
    std::system("PAUSE");
    return EXIT_SUCCESS;
}
