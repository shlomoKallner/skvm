#ifndef SK_COMPILERS_FRONTENDS_PARSERS_INTERNALS_HPP
#define SK_COMPILERS_FRONTENDS_PARSERS_INTERNALS_HPP

//#include ""
//#include "internals/"

#include "internals/c_lang_escape_parsers.hpp"
#include "internals/number_parsers.hpp"
#include "internals/skip_parser.hpp"
#include "internals/keyword_parser.hpp"

#endif
