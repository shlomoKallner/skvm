#ifndef SK_COMPILERS_FRONTENDS_PARSERS_INTERNALS_C_LANG_ESCAPE_PARSERS_HPP
#define SK_COMPILERS_FRONTENDS_PARSERS_INTERNALS_C_LANG_ESCAPE_PARSERS_HPP

#include <iterator>
#include <boost/spirit/include/qi.hpp>   
#include <boost/spirit/include/qi_symbols.hpp>
#include <boost/phoenix/object/construct.hpp>

#include <SK/utility/types/basic_types.hpp>

namespace SK
{
 namespace compilers
 {
  namespace frontends
  {
   namespace parsers
   {
    namespace internals
    {
       
     namespace types = SK::utility::types;
     namespace qi = boost::spirit::qi;
     namespace phoenix = boost::phoenix;
     
     template< typename CharT >
     struct c_lang_escape_sym_parser;
     
     template<>
     struct c_lang_escape_sym_parser<char> 
     : public qi::symbols< char, char >
     {
        c_lang_escape_sym_parser()
         {
           add ("\\b", '\b') ("\\f", '\f') ("\\n", '\n')
               ("\\r", '\r') ("\\t", '\t') ("\\\"", '\"') 
               ("\\\'", '\'') ("\\\\", '\\') ("\\v", '\v')
               ("\\a", '\a') ("\\?", '\?') ("\\0", '\0');
         }
     };
     
     template<>
     struct c_lang_escape_sym_parser<wchar_t> 
     : public qi::symbols< wchar_t, wchar_t >
     {
        c_lang_escape_sym_parser()
         {
           add (L"\\b", L'\b') (L"\\f", L'\f') (L"\\n", L'\n')
               (L"\\r", L'\r') (L"\\t", L'\t') (L"\\\"", L'\"') 
               (L"\\\'", L'\'') (L"\\\\", L'\\') (L"\\v", L'\v')
               (L"\\a", L'\a') (L"\\?", L'\?') (L"\\0", L'\0');
         }
     };
     
     template< typename IteratorT , typename CharT = std::iterator_traits<IteratorT>::value_type >  
     struct c_lang_escape_parser;
     // the default arg of CharT :
     //  typename CharT = typename std::iterator_traits<IteratorT>::value_type 
   
     template< typename IteratorT >
     struct c_lang_escape_parser< IteratorT, char > 
     : public qi::grammar< IteratorT, char(void) >
     {
       typedef char    char_type;
       
       qi::rule< IteratorT, char_type(void) >  start_m;
       c_lang_escape_sym_parser<char_type>     sym_m;
       
       
       c_lang_escape_parser() 
       :  c_lang_escape_parser::base_type( start_m, 
            "C language character escape constants - char parser")
       {
         start_m = sym_m[ qi::_val = phoenix::construct<char_type>(qi::_1) ]
          | qi::standard::char_[ qi::_val = phoenix::construct<char_type>(qi::_1) ] 
            ;
       }// end of
     };
     
     template< typename IteratorT >
     struct c_lang_escape_parser< IteratorT, wchar_t > 
     : public qi::grammar< IteratorT, wchar_t(void) >
     {
       typedef wchar_t    char_type;
       
       qi::rule< IteratorT, char_type(void) >  start_m;
       c_lang_escape_sym_parser<char_type>     sym_m;
       
       
       c_lang_escape_parser() 
       :  c_lang_escape_parser::base_type( start_m, 
            "C language character escape constants - wchar_t parser")
       {
         start_m = sym_m[ qi::_val = phoenix::construct<char_type>(qi::_1) ]
          | qi::standard_wide::char_[ qi::_val = phoenix::construct<char_type>(qi::_1) ] 
            ;
       }// end of
     };
     
     
     namespace obsolete
     {
      template< typename CharT = char, typename IntT = types::sint64 >
     struct esc_char
     { // a generic type for holding the result of 
       // c_lang_escape_parser below
      typedef CharT char_type;
      typedef IntT  int_type;
      explicit esc_char( char_type c )
      : is_char_m(true), char_m( c ), int_m( int_type() ){}//end of...
      explicit esc_char( int_type i )
      : is_char_m(false), char_m( char_type() ), int_m( i ){}//end of...
      esc_char( const esc_char& e )
      : is_char_m(e.is_char_m), char_m( e.char_m ),
        int_m( e.int_m ){}//end of...
      virtual ~esc_char(){}// end of ...
      
      const esc_char& operator=( const esc_char& e )
      {
        is_char_m = e.is_char_m; 
        char_m =  e.char_m;
        int_m = e.int_m;
        return *this;
      }// end of...
      
      types::boolean is_char()
      {
       return is_char_m;
      }// end of ...
      
      char_type char_()
      {
       return char_m;
      }//end of..
      
      int_type int_()
      {
       return int_m;
      }// end of ...
      
      protected:
                types::boolean  is_char_m;
                char_type       char_m;
                int_type        int_m;
     };
     
     template< typename IteratorT , typename IntT = types::sint64,
       typename CharT = typename std::iterator_traits<IteratorT>::value_type >
     struct c_lang_escape_parser 
     : public qi::grammar< IteratorT, esc_char< CharT, IntT >(void) >
     {
       typedef esc_char< CharT, IntT > data_type;
       typedef data_type::int_type     int_type;
       typedef data_type::char_type    char_type;
       
       qi::rule< IteratorT, data_type(void) >  start_m;
       c_lang_escape_sym_parser<char_type>     sym_m;
       
       
       c_lang_escape_parser() 
       :  c_lang_escape_parser::base_type( start_m, "C language character escape constants")
       {
         start_m = sym_m[ qi::_val = phoenix::construct<data_type>(qi::_1) ]
         | qi::lexeme[ "\\" >> 
           ( qi::oct[ qi::_val = phoenix::construct<data_type>(qi::_1) ] 
             | ( qi::lit("x") | qi::lit("X") ) 
                >> qi::hex[ qi::_val = phoenix::construct<data_type>(qi::_1) ] 
           ) 
                     ] ;
       }// end of

     };  
     }// end of namespace obsolete;
     

     

    }// end of namespace internals;
   }// end of namespace parsers;
  }// end of namespace frontends;
 }// end of namespace compilers;
}// end of namespace SK; 
#endif
