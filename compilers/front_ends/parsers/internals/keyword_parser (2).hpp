#ifndef SK_COMPILERS_PARSERS_INTERNALS_KEYWORD_PARSER_HPP
#define SK_COMPILERS_PARSERS_INTERNALS_KEYWORD_PARSER_HPP

#include <iterator>
#include <string>

#include <boost/spirit/include/qi.hpp>   

#include "SK/utilities/types.hpp"

namespace SK
{
 namespace compilers
 {
  namespace parsers
  {
   namespace internals
   {
    namespace types = SK::utility::types;
    namespace qi = boost::spirit::qi;
    
    template< typename IteratorT, typename CharT >
    struct keyword_parser_type;
    
    template< typename IteratorT >
     struct keyword_parser_type<IteratorT,char> 
     : qi:grammar< IteratorT, void(std::basic_string<char>) >
     {
      keyword_parser_type() 
      : base_type(start, "SK.Compilers.parser internals.keyword_parser_type<char>" )
      {
       start = qi::lexeme[ qi::lit(qi::_r1) 
             >> !( qi::ascii::alnum | qi::lit('_') ) ]; 
      }// end of...
      qi::rule< IteratorT, void(std::basic_string<char>) > start;
      
     }; 
     
     template< typename IteratorT >
     struct keyword_parser_type<IteratorT,wchar_t> 
     : qi:grammar< IteratorT, void(std::basic_string<wchar_t>) >
     {
      keyword_parser_type() 
      : base_type(start, "SK.Compilers.parser internals.keyword_parser_type<wchar_t>" )
      {
       start = qi::lexeme[ qi::lit(qi::_r1) 
             >> !( qi::standard_wide::alnum | qi::lit(L'_') ) ]; 
      }// end of...
      qi::rule< IteratorT, void(std::basic_string<wchar_t>) > start;
      
     };
    
   }// end of namespace internals;
  }// end of namespace parsers;
 }// end of namespace compilers;
}// end of namespace SK; 
#endif
 
