#ifndef SK_COMPILERS_PARSERS_INTERNALS_NUMERIC_PARSERS_HPP
#define SK_COMPILERS_PARSERS_INTERNALS_NUMERIC_PARSERS_HPP

#include <boost/spirit/include/qi_numeric.hpp>
#include "SK/utility/types.hpp

namespace SK
{
 namespace compilers
 {
  namespace parsers
  {
   namespace internals
   {
    namespace types = SK::utility::types;
    namespace qi = boost::spirit::qi;
    
    /*
    template < typename T, unsigned Radix, 
               unsigned MinDigits = 1, int MaxDigits = -1 >
    struct uint_parser;
    */
    typedef qi::uint_parser< types::uint64, 2 > ubin_p;
    typedef qi::uint_parser< types::uint64, 8 > uoct_p;
    typedef qi::uint_parser< types::uint64, 10> udec_p;
    typedef qi::uint_parser< types::uint64, 16> uhex_p;
    
    /*
    template < typename T, unsigned Radix,
               unsigned MinDigits = 1, int MaxDigits = -1 >
    struct int_parser;
    */
    typedef qi::int_parser< types::sint64, 2 > sbin_p;
    typedef qi::int_parser< types::sint64, 8 > soct_p;
    typedef qi::int_parser< types::sint64, 10> sdec_p;
    typedef qi::int_parser< types::sint64, 16> shex_p;
    
     /*
     template <typename T, typename RealPolicies>
     struct real_parser;
     */
   
    typedef qi::strict_ureal_policies< types::float64 > ufloat_pol;
    typedef qi::strict_real_policies< types::float64 > sfloat_pol;
    
    typedef qi::real_parser< types::float64, ufloat_pol > ufloat_p;
    typedef qi::real_parser< types::float64, sfloat_pol > sfloat_p;
    
   } // end  of  namespace internals;
  } // end of namespace parsers;
 } // end of namespace compilers;
} // end of namespace SK;

#endif


