#ifndef SK_COMPILERS_FRONTENDS_PARSERS_INTERNALS_MULTIPASS_LINECOUNTER_HPP
#define SK_COMPILERS_FRONTENDS_PARSERS_INTERNALS_MULTIPASS_LINECOUNTER_HPP

// based on the implementation of spirit 2.5 line_pos_iterators
// for copyrights see %BOOST_ROOT%/spirit/home/support/iterators/line_pos_iterator.hpp

#include <boost/iterator/iterator_adaptor.hpp>
#include <boost/range/iterator_range.hpp>

#include <boost/spirit/home/support/multi_pass.hpp>
#include <SK/utility/types/basic_types.hpp>

namespace SK
{
 namespace compilers
 {
  namespace frontends
  {
   namespace parsers
   {
    namespace internals
    {
     template< typename IteratorT >
     class position_iterator 
	 : public boost::iterator_adaptor<
             position_iterator<IteratorT>  // Derived
           , boost::spirit::multi_pass< IteratorT >  // Base
           , boost::use_default           // Value
           , boost::forward_traversal_tag // CategoryOrTraversal 
                                     >
	 {
	   public:
	   	typedef typename boost::spirit::multi_pass< IteratorT > base_typeT;
        typedef typename position_iterator<IteratorT> this_typeT;
        typedef typename boost::iterator_adaptor< this_typeT, base_typeT,
                 boost::use_default, boost::forward_traversal_tag > adaptor_t;
        
        position_iterator(){}
        explicit position_iterator( IteratorT& i /*, SK::utility::types::uint8 t = 4*/  ) 
		: adaptor_t( boost::spirit::make_multi_pass(i) ), line_pos_m(0),
		  col_pos_m(0), prev() /* , tabs_m(t)*/
		{}//end of ....
        
        SK::utility::types::uint64 column()
        {
        	return col_pos_m;
        }// end of ...
        
        SK::utility::types::uint64 line()
        {
        	return line_pos_m;
        }// end of ...
        
       private:
      	friend class boost::iterator_core_access;
      	
      	void increment(void)
      	{
      	  typename std::iterator_traits<Iterator>::reference
          ref = *(this->base());
      
          switch (ref)
		  {
           case '\r':
            if (prev != '\n')
              ++line_pos_m;
              col_pos_m = 0;
            break;
           case '\n':
            if (prev != '\r')
              ++line_pos_m;
              col_pos_m = 0;
            break;
           //case '\t':
		   //  col_pos_m += tabs_m;
		   // break;
           default:
           	++col_pos_m;
            break;
         }
      
        prev = ref;
        ++this->base_reference();
			
      	}// end of ...
      	
        /*mutable*/ 
		SK::utility::types::uint64 line_pos_m, col_pos_m;
        typename std::iterator_traits<IteratorT>::value_type prev;
        //SK::utility::types::uint8 tabs_m;
       
     };
    }
   }
  }
 }
}

#endif
