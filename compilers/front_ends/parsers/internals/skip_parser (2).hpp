#ifndef SK_COMPILERS_PARSERS_INTERNALS_SKIP_PARSER_HPP
#define SK_COMPILERS_PARSERS_INTERNALS_SKIP_PARSER_HPP

#include <iterator>
#include <string>

#include <boost/spirit/include/qi.hpp>   

#include "SK/utilities/types.hpp"

namespace SK
{
 namespace compilers
 {
  namespace parsers
  {
   namespace internals
   {
       
     namespace types = SK::utility::types;
     namespace qi = boost::spirit::qi;
     //namespace phoenix = boost::phoenix;
     
     template< typename IteratorT, typename CharT >
     struct c_lang_skipper_type;
     
     template< typename IteratorT >
     struct c_lang_skipper_type<IteratorT,char> 
     : qi:grammar< IteratorT, std::basic_string<char>(void) >
     {
      skipper_type() 
      : base_type(start, "SK.Compilers.parser internals.skipper_type<char>" )
      {
       comment %= ( qi::lit("//") >> *(qi::ascii::char_ - qi::eol )
                                  >> qi::eol )
                | ( qi::lit("/*") >> *(qi::ascii::char_ - "*/") 
                                  >> "*/");
       start %= *( qi::ascii::space | comment ); // qi::ascii::
      }// end of...
      qi::rule< IteratorT, std::basic_string<char>(void) > start;
      qi::rule< IteratorT, std::basic_string<char>(void) > comment;      
     }; 
     
     template< typename IteratorT >
     struct c_lang_skipper_type<IteratorT,wchar_t> 
     : qi:grammar< IteratorT, std::basic_string<wchar_t>(void) >
     {
      skipper_type() 
      : base_type(start, "SK.Compilers.parser internals.skipper_type<wchar_t>" )
      {
       comment %= ( qi::lit(L"//") >> *(qi::standard_wide::char_ - qi::eol )
                                  >> qi::eol )
                | ( qi::lit(L"/*") >> *(qi::standard_wide::char_ - L"*/") 
                                  >> L"*/");
       start %= *( qi::standard_wide::space | comment ); // qi::ascii::
      }// end of...
      qi::rule< IteratorT, std::basic_string<wchar_t>(void) > start;
      qi::rule< IteratorT, std::basic_string<wchar_t>(void) > comment;      
     };    

   }// end of namespace internals;
  }// end of namespace parsers;
 }// end of namespace compilers;
}// end of namespace SK; 
#endif
 
