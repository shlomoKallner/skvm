#ifndef SK_COMPILERS_PARSERS_INTERNALS_TREE_NODE_HPP
#define SK_COMPILERS_PARSERS_INTERNALS_TREE_NODE_HPP


#inlcude <boost/shared_ptr.hpp>
#include <boost/type_traits/is_same.hpp>
#include "SK/utilities/types/SK_types.h"
#include "SK/compilers/utilities/types.hpp"

namespace SK
{
 namespace compilers
 {
  namespace parsers
  {
   namespace internals
   {
    using SK::utility::types::uint32;
    using SK::compilers::utilities::types::nil_t;
    using SK::compilers::utilities::types::nil; // this should be changed.

    class ast_node_base // formerly just "ast_node"
    {
 // Note: 
 // this type is on purpose a non templated type; that way we can derive many types that can
 // contained in the tree by simply defining pointers to derived types that are up-casted. 
 // end of note.
 // private data members: 
         ast_node_base * parent_m;
         uint32 tag_m;
        public:
          typedef ast_node_base * node_cPtr;
          //typedef uint32 tag_type;
          typedef boost::shared_ptr<ast_node_base> node_ptr;
          typedef nil_t value_type;
          // the constructors:
          ast_node_base(void) 
          : parent_m(0), tag_m(0) {}
          ast_node_base( node_cPtr p = 0, uint32 t = 0 ) 
          : parent_m(p), tag_m(t) {}
          // the copy constructor:
          ast_node_base( const ast_node& n ) 
          : parent_m( n.parent_m), tag_m( n.tag_m) {}
          // the destructor:
          virtual ~ast_node_base(){}
          // the member functions
          virtual boolean is_leaf() { return false; }
          virtual boolean is_root() { return false; }
          virtual boolean has_value(){ return false; }
          virtual boolean has_children(){ return false; } // may not be equivelent to !is_leaf() or to is_root().
          virtual uint32 num_children(){ return 0; } // if is_leaf() == true then return 0.
          node_cPtr parent() { return parent_m; }
          void parent( node_cPtr p ) { parent_m = p } 
          uint32 tag() { return tag_m; }
          void tag( uint32 t ) { tag_m = t; }
          virtual value_type value( void ) { return value_type(); }
          virtual void value( value_type ) {}
          virtual node_ptr child( uint32 = 0) { return node_ptr(); }
          virtual void child( uint32 = 0, node_ptr = node_ptr() ) {}
    };

      

#define SK_TREE_NODE_USE_VECTOR 1

     template< typename ValueT = nil_t >
     class ast_node : public ast_node_base 
    { // formerly "nary_ast_node"
      // typedef ast_node_base::node_ptr  node_ptr;
#ifdef SK_TREE_NODE_USE_LIST 
       typedef std::list<node_ptr>      ContainerT;  
#elif defined(SK_TREE_NODE_USE_VECTOR)
       typedef std::vector<node_ptr>    ContainerT;  
#elif defined(SK_TREE_NODE_USE_DEQUE)
       typedef std::deque<node_ptr>     ContainerT;  
#endif
       ValueT value_m;
       ContainerT children_m;
      public:
        typedef ValueT value_type;
        typedef ContainerT container_t;
        typedef container_t::iterator iterator;
        typedef container_t::const_iterator const_iterator;
        // the constructor:
        ast_node(void) 
        : ast_node_base(), value_m(), children_m() {}
        ast_node( container_t& c, value_type v = value_type(),
                  node_cPtr p = 0, uint32 t = 0 )
        : ast_node_base( p, t ), value_m(v), children_m(c) {}
        // the copy constructor:
        ast_node( const n_ary_ast_node& n ) 
        : ast_node_base( n.parent(), n.tag() ), value_m( n.value_m ), 
         children_m( n.chilren_m) {}
        // the destructor
        virtual ~ast_node(){}
        // the member functions:
         virtual boolean is_leaf() { return chilren_m.size() == 0; }
         virtual boolean is_root() { return !this->is_leaf(); }
         virtual boolean has_value() { return ! boost::is_same< ValueT , nil_t >::value ;  }
         virtual boolean has_children() { return children_m.size() != 0; } // the same as is_root()!!!
         virtual uint32 num_children() { return children_m.size(); }
         value_type value() { return value_m; }
         void value( value_type v ) { value_m = v; }
      //  container_t children() { return children_m; }
     //   void children( container_t c ) { children_m = c; }
        iterator first_child() { return children_m.begin(); }
        iterator last_child() { return --children_m.end(); }
        virtual node_ptr child( uint32 i ) 
        { 
          if( i > -1 && i < children_m.size() )
          {
#ifdef SK_TREE_NODE_USE_LIST 
           iterator j = children_m.begin()
           while( i-- != 0 && j != (--children_m.end()) )
           {
             j++;
           }
           return *j;
#else
           return children_m.at(i);
#endif
          }
          else
           return node_ptr(); 
        } // end of function node_ptr child(uint32);

        virtual void child( uint32 i, node_ptr n ) 
        {
          if( i > -1 && i < children_m.size() )
          {
#ifdef SK_TREE_NODE_USE_LIST               
           iterator j = children_m.begin()
           /*while( i != 0 )
           {
             j++;
             i--;
           }*/
           while( i-- != 0 && j != (--children_m.end()) )
           {
             j++;
           }
           *j = n;
#else
           children_m.at(i) = n;            
#endif           
          }
        }  // end of function void child(node_ptr, uint32);
    };

   } // end  of  namespace internals;
  } // end of namespace parsers;
 } // end of namespace compilers;
} // end of namespace SK;

#endif
