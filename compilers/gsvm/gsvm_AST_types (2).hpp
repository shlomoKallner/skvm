#ifndef SK_COMPILERS_GSVM_AST_TYPES_HPP 
#define SK_COMPILERS_GSVM_AST_TYPES_HPP 

#include "SK/utilities/types.hpp"
#include "SK/compilers/utilities/types.hpp"
#include �SK/compilers/parsers/internal/tree_node.h� // fix this include!!!
#include "SK/compilers/parsers/front_ends/ /GSSL_lang_const.hpp"

namespace SK   
{   
 namespace compilers   
 {
  namespace parsers
  {
   namespace gsvm   
   {
    namespace types = SK::utility::types;
    namespace cut = SK::compilers::utilities::types;             
    namespace pi = SK::compilers::parsers::internals;
    
   
    template< typename T, types::uint64 i >
    struct value_node : public pi::ast_node<T>
    {
     value_node( T& t, node_Cptr p = 0 );
     value_node( value_node<T> const & v );
     virtual ~value_node(){}
    };

    
    
    
    
   }
  }
 }
} 
  

#endif
