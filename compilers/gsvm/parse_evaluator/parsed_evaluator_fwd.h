#ifndef SK_GSVM_EVAL_PARSER_EVALUATOR_FWD_H
#define SK_GSVM_EVAL_PARSER_EVALUATOR_FWD_H

#include "SK_types.h"
#include <boost/spirit/include/classic_spirit.hpp>
#include <boost/shared_ptr.hpp>
#include <string>
#include <map>
#include <list>
#include <set>
#include "gsvm_ddl_types.h"

namespace SK
{
 namespace gsvm
 {
  namespace eval
  {
   using namespace boost::spirit::classic;
   using namespace SK::types;
   
   template< typename CharT = char, typename FactoryT = node_val_data_factory<> 
             >
   class parsed_module;
   
   template< typename CharT = char, typename FactoryT = node_val_data_factory<> 
             >
   class parsed_namespace;
    
   template< typename CharT = char, typename FactoryT = node_val_data_factory<> 
             >
   class parsed_types;
    
   template< typename CharT = char, typename FactoryT = node_val_data_factory<> 
             >
   class parsed_ctor;
    
   template< typename CharT = char, typename FactoryT = node_val_data_factory<> 
             >
   class parsed_dtor;
    
   template< typename CharT = char, typename FactoryT = node_val_data_factory<> 
             >
   class parsed_function;
    
   template< typename CharT = char, typename FactoryT = node_val_data_factory<> 
             >
   class parsed_operator;
    
   template< typename CharT = char, typename FactoryT = node_val_data_factory<> 
             >
   class parsed_signature;
    
   template< typename CharT = char, typename FactoryT = node_val_data_factory<> 
             >
   class parsed_data;
    
   template< typename CharT = char, typename FactoryT = node_val_data_factory<> 
             >
   class parsed_code;
    
   template< typename CharT = char, typename FactoryT = node_val_data_factory<> 
             >
   class parsed_insr;
    
   template< typename CharT = char, typename FactoryT = node_val_data_factory<> 
             >
   class parsed_modifier;
    
   template< typename CharT = char, typename FactoryT = node_val_data_factory<> 
             >
   class parsed_import;
    
   template< typename CharT = char, typename FactoryT = node_val_data_factory<> 
             >
   class parsed_project;
    
   template< typename CharT = char, typename FactoryT = node_val_data_factory<> 
             >
   class parsed_obj_file;
    
   template< typename CharT = char, typename FactoryT = node_val_data_factory<> 
             >
   class parsed_value;
    
   template< typename CharT = char, typename FactoryT = node_val_data_factory<> 
             >
   class parsed_param;
    
  }
 }
}

#endif
