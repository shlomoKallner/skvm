#ifndef SK_GSVM_EVAL_PARSED_IMPORT_H
#define SK_GSVM_EVAL_PARSED_IMPORT_H

namespace SK
{
 namespace gsvm
 {
  namespace eval
  { 
   template< typename CharT >
    class parsed_import
    {
     public:
            typedef typename parsed_import< CharT >          import_t;
            typedef typename boost::shared_ptr< import_t >   import_ptr;
            typedef typename std::basic_string< CharT >      string_t;
     protected:
               std::set< string_t > file_set_m;
    };
  } 
 } 
}

#endif
