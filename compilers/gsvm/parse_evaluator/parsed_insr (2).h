#ifndef SK_SGVM_EVAL_PARSED_INSR_H
#define SK_SGVM_EVAL_PARSED_INSR_H

namespace SK
{
 namespace gsvm
 {
  namespace eval
  { 
   template< typename CharT >
    class insr_decr
    {
     public:
            typedef typename insr_decr< CharT >                 insr_decr_t;
            typedef typename boost::shared_ptr< insr_decr_t >   insr_decr_ptr;
            typedef typename std::basic_string< CharT >         string_t;
            //
            insr_decr_t( string_t& str, uint64& num )
            : insr_str_m( str ), insr_num_m( num ) {}//end of ...
            
            string_t& get_str( void )
            { return insr_str_m; }// end of ...
            
            uint64& get_num( void )
            { return insr_num_m; }//end of ...
            
     protected:
               string_t insr_str_m;
               uint64   insr_num_m;
    };
    
    template< typename CharT >
    class parsed_insr
    { 
     public:
         typedef typename parsed_insr< CharT >               insr_t;
         typedef typename boost::shared_ptr< insr_t >        insr_ptr;
         typedef typename std::basic_string< CharT >         string_t;
         typedef typename insr_decr< CharT >::insr_decr_t    insr_decr_t;
         typedef typename insr_decr_t::insr_decr_ptr         insr_decr_ptr;
         //
         typedef typename parsed_code< CharT >::code_t         code_t;
         typedef typename code_t::code_ptr                     code_ptr;
         typedef typename parsed_types< CharT >::types_t       types_t;
         typedef typename types_t::types_ptr                   types_ptr;
         typedef typename parsed_constant< CharT >::constant_t constant_t;
         typedef typename constant_t::constant_ptr             constant_ptr;
         typedef typename parsed_variable< CharT >::variable_t variable_t;
         typedef typename variable_t::variable_ptr             variable_ptr;
     protected:
               code_ptr                           label_ptr_m;
               std::map< uint64 , constant_ptr >  const_args_m;
               std::map< uint64 , variable_ptr >  var_args_m;
               types_ptr                          type_arg_m;
               // type_arg_m is only here to indicate where the type comes into 
               // scope.
               insr_decr_ptr                      insr_decr_m;
               enum arg_pos{ DEST = 0, ARG1, ARG2, ARG3, MAX_ARG };
    };
  }
 }
}

#endif
