#ifndef SK_SGVM_EVAL_PARSED_MODULE_H
#define SK_SGVM_EVAL_PARSED_MODULE_H

namespace SK
{
 namespace gsvm
 {
  namespace eval
  { 
   template< typename CharT >
    class parsed_module
    { 
     public:
      typedef typename parsed_module< CharT >         module_t;
      typedef typename boost::shared_ptr< module_t >  module_ptr;
      typedef typename std::basic_string< CharT >     string_t;
      // 
      typedef typename parsed_modifier< CharT >::modifier_t     modifier_t;
      typedef typename modifier_t::modifier_ptr                 modifier_ptr;
      typedef typename parsed_import< CharT >::import_t         import_t;
      typedef typename import_t::import_ptr                     import_ptr;
      typedef typename parsed_namespace< CharT >::namespace_t   namespace_t;
      typedef typename namespace_t::namespace_ptr               namespace_ptr;
      typedef typename parsed_types< CharT >::types_t           types_t;
      typedef typename types_t::types_ptr                       types_ptr;
      typedef typename parsed_data< CharT >::data_t             data_t;
      typedef typename data_t::data_ptr                         data_ptr;
      typedef typename parsed_function< CharT >::function_t     function_t;
      typedef typename function_t::function_ptr                 function_ptr;
      typedef typename parsed_operator< CharT >::operator_t     operator_t;
      typedef typename operator_t::operator_ptr                 operator_ptr;
      //
      typedef typename std::map< string_t , import_ptr >::iterator 
                                                             imprtIter_t;
      typedef typename std::map< string_t , namespace_ptr >::iterator
                                                             mnspcIter_t;  
      typedef typename std::multimap< string_t , types_ptr >::iterator  
                                                             typeIter_t;   
      typedef typename std::multimap< string_t , data_ptr >::iterator     
                                                             dataIter_t;
      typedef typename std::multimap< string_t , function_ptr >::iterator 
                                                             funcIter_t;
      typedef typename std::multimap< string_t , operator_ptr >::iterator 
                                                             operIter_t;
      //
      /*
      void add_X ( X_ptr x );
      X_ptr find_X();
      X_ptr getfirst_X();
      X_ptr getlast_X();
      */
      
      void add_import( import_ptr imp_ );
      import_ptr find_import( string_t& s );
      void add_namespace ( namespace_ptr x );
      namespace_ptr find_namespace( string_t& s );
      void add_types ( types_ptr x );
      types_ptr find_types( string_t& );
      void add_data ( data_ptr x );
      data_ptr find_data( string_t& );
      void add_function ( function_ptr x );
      function_ptr find_function( string_t& );
      void add_operator ( operator_ptr x );
      operator_ptr find_operator( string_t& );
      
     protected:
      string_t                                      name_m;
      std::list< modifier_ptr >                     modifier_list_m;
      //
      std::map< string_t , import_ptr >             cont_imprt_map_m;
      std::map< imprtIter_t , uint64 >              contImprtLayout_m;
      //
      std::map< string_t , namespace_ptr >          cont_nmspc_map_m; 
      std::map< mnspcIter_t , uint64 >              contNmspcLayout_m;
      //
      std::multimap< string_t , types_ptr >         cont_type_map_m;
      std::map< typeIter_t , uint64 >               contTypeLayout_m;
      //
      std::multimap< string_t , data_ptr >          cont_data_map_m;
      std::map< dataIter_t , uint64 >               contDataLayout_m;
      //
      std::multimap< string_t , function_ptr >      cont_func_map_m;
      std::map< funcIter_t , uint64 >               contFuncLayout_m;
      //
      std::multimap< string_t , operator_ptr >      cont_oper_map_m;
      std::map< operIter_t , uint64 >               contOperLayout_m;
    };
  } 
 } 
}

#endif
