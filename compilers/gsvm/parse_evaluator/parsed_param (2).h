#ifndef SK_SGVM_EVAL_PARSED_PARAM_H
#define SK_SGVM_EVAL_PARSED_PARAM_H

namespace SK
{
 namespace gsvm
 {
  namespace eval
  { 
    template< typename CharT >
    class parsed_param
    {
     public:
      typedef typename parsed_param< CharT >                 param_t;
      typedef typename boost::shared_ptr< param_t >          param_ptr;
      typedef typename std::basic_string< CharT >            string_t;
      //
      typedef typename parsed_types< CharT >::types_t        types_t;
      typedef typename types_t::types_ptr                    types_ptr;
      typedef typename parsed_data< CharT >::data_t          data_t;
      typedef typename data_t::data_ptr                      data_ptr;
      
      enum param_type_t{ NULL = 0, CONST_T, VAR_T, TYPE_T };
      
     protected:
               string_t      name_m;
               param_type_t  param_type_m;
               data_ptr      data_param_m, data_deflt_m;
               // NOTE: even if param_type_m == VAR_T const_deflt_m will 
               // contain the default value.
               types_ptr     type_param_m, type_deflt_m;
               
    };
  }
 }
}


#endif
