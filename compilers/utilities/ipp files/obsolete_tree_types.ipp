#ifndef SK_COMPILERS_PARSERS_INTERNALS_TREE_NODE_OBSOLETE_IPP
#define SK_COMPILERS_PARSERS_INTERNALS_TREE_NODE_OBSOLETE_IPP


namespace SK
{
 namespace compilers
 {
  namespace parsers
  {
   namespace internals
   {
    namespace obsolete
    {
              
     template< typename ValueT = nil_t >
      class nullary_ast_node : public ast_node
      {
         ValueT value_m;
        public:
           typedef ValueT value_type;
          // the constructor:
          nullary_ast_node( ValueT v = ValueT(), node_cPtr n = 0, uint32 t = 0 ) 
          : ast_node( n, t ), value_m(v) {}
          // the copy constructor:
          nullary_ast_node( const nullary_ast_node& n ) 
          : ast_node( n.parent(), n.tag() ), value_m(n.value_m){}
          // the destructor:
          virtual ~nullary_ast_node(){}
          // the *overiden* member functions:
          virtual boolean is_leaf() { return true; }
          virtual boolean has_value()
          { return  ! boost::is_same< ValueT , nil_t >::value ; }
          value_type value() { return value_m; }  
          void value( value_type v ) { value_m = v; }
       };     

     template< typename ValueT = nil_t > 
     class unary_ast_node : public ast_node
      {
         ValueT value_m;
         node_ptr child_m;
        public:
          typedef ValueT value_type;
         unary_ast_node( ValueT v = ValueT(), node_ptr c = node_ptr() ,node_cPtr p = 0, uint32 t = 0 ) 
              : ast_node( p, t ), value_m(v), child_m(c) {}
         unary_ast_node( const unary_ast_node& u ): ast_node( u.parent(), u.tag() ), value_m( u.value_m ), 
                                           child_m( u.child_m ) {}
         virtual ~unary_ast_node(){}
        // the member functions:
         virtual boolean is_root() { return true; }
         virtual boolean has_value() { return ! boost::is_same< ValueT , nil_t >::value ;  }
         virtual boolean has_children() { return true; }
         virtual uint32 num_children() { return 1; }
         value_type value() { return value_m; }
         void value( value_type v ) { value_m = v; }
        // node_ptr child() { return child_m; }
        // void child( node_ptr n ){ child_m = n; }
         virtual node_ptr child( uint32 i = 0) 
         { 
           if( i == 0) 
            return child_m; 
           else
             return node_ptr(); 
          }
          virtual void child( node_ptr n = node_ptr() ,  uint32 i  = 0 ) 
          {
            if( i == 0 )
             child_m = n;
          }	
      };
      
     template< typename ValueT = nil_t >
     class binary_ast_node : public ast_node
     {
        // private data members:
        ValueT value_m;
        node_ptr rChild_m, lChild_m;
       public:
          typedef ValueT value_type;
         // the constructor:
         binary_ast_node( value_type v = value_type, node_ptr r = node_ptr(), node_ptr l = node_ptr(), node_cPtr p = 0, 
                                      uint32 t = 0 ) :  ast_node(p,t), value_m(v), rChild_m(r), lChild_m(l) {}
         // the copy constructor:
         binary_ast_node( const  binary_ast_node& b ) : ast_node(b.parent(),b.tag()), value_m( b.value_m),
                                             rChild_m( b.rChild_m ),  lChild_m( b.lChild_m ) {}
         // the destructor:
         virtual ~ binary_ast_node(){}
         // the member functions:
         virtual boolean is_root() { return true;}
         virtual boolean has_value() { return ! boost::is_same< value_type , nil_t >::value ;  }
         virtual boolean has_children() { return true; } 
         virtual uint32 num_children() { return 2; }
         value_type value() { return value_m; }
         void value( value_type v ) { value_m = v; }
         node_ptr right(){ return rChild_m; }
         void right( node_ptr n ) { rChild_m = n; }
         node_ptr left() { return lChild_m; }
         void left( node_ptr n ) { lChild_m = n; }
         virtual node_ptr child( uint32 i ) 
         {
           if( i == 0 ) 
             return lChild_m; 
           else if( i == 1 )
             return rChild_m;
           else 
             return node_ptr(); 
          } // end of function node_ptr child(uint32);
          virtual void child( node_ptr n, uint32 i  ) 
         {
           if( i == 0 )
            lChild_m = n;
           else if( i == 1 )
            rChild_m = n;
         } // end of function void child(node_ptr, uint32);
        };
         
              
    }// end of namespace obsolete;
   } // end  of  namespace internals;
  } // end of namespace parsers;
 } // end of namespace compilers;
} // end of namespace SK;

#endif
    


