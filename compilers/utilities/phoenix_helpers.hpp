#ifndef SK_COMPILERS_UTILITIES_PHOENIX_HELPERS_HPP_JAN_16_2012
#define SK_COMPILERS_UTILITIES_PHOENIX_HELPERS_HPP_JAN_16_2012

#include <boost/utility/result_of.hpp>
#include <boost/tuples/tuples.hpp>
#include "SK/compilers/utilities/types.hpp"

namespace SK
{
 namespace compilers
 {
  namespace utilities
  {
   namespace phoenix_helpers 
   {

    using  SK::compilers::utilities::types::nil_t

    struct tuple_at_impl
    {
      template< typename ContainerT, std::size_t >
      struct result;
    /*  
    /  template< typename ContainerT, std::size_t N , typename A0 = nil_t , typename A1 = nil_t, 
    /                       typename A2 = nil_t, typename A3 = nil_t, typename A4 = nil_t, typename A5 = nil_t, 
    /                       typename A6 = nil_t, typename A7 = nil_t, typename A8 = nil_t, typename A9 = nil_t >
    /  struct result< ContainerT<A0,A1,A2,A3,A4,A5,A6,A7,A8,A9>, N  >
    /  { typedef A_ type;}; // fill in "_" with arg number. 
    /*
      
        template< typename ContainerT, std::size_t N , typename A0 = nil_t , typename A1 = nil_t, 
                           typename A2 = nil_t, typename A3 = nil_t, typename A4 = nil_t, typename A5 = nil_t, 
                           typename A6 = nil_t, typename A7 = nil_t, typename A8 = nil_t, typename A9 = nil_t >
      struct result< ContainerT<A0,A1,A2,A3,A4,A5,A6,A7,A8,A9>, 0  >
      { typedef A0 type;};
      
       template< typename ContainerT, std::size_t N , typename A0 = nil_t , typename A1 = nil_t, 
                           typename A2 = nil_t, typename A3 = nil_t, typename A4 = nil_t, typename A5 = nil_t, 
                           typename A6 = nil_t, typename A7 = nil_t, typename A8 = nil_t, typename A9 = nil_t >
      struct result< ContainerT<A0,A1,A2,A3,A4,A5,A6,A7,A8,A9>, 1  >
      { typedef A1 type;};

       template< typename ContainerT, std::size_t N , typename A0 = nil_t , typename A1 = nil_t, 
                           typename A2 = nil_t, typename A3 = nil_t, typename A4 = nil_t, typename A5 = nil_t, 
                           typename A6 = nil_t, typename A7 = nil_t, typename A8 = nil_t, typename A9 = nil_t >
      struct result< ContainerT<A0,A1,A2,A3,A4,A5,A6,A7,A8,A9>, N  >; 
      
      template< typename ContainerT >
      void operator()( Container c )
      { return; }//end of operator N;
      
       template< typename ContainerT, std::size_t N , typename A0 = nil_t , typename A1 = nil_t, 
                           typename A2 = nil_t, typename A3 = nil_t, typename A4 = nil_t, typename A5 = nil_t, 
                           typename A6 = nil_t, typename A7 = nil_t, typename A8 = nil_t, typename A9 = nil_t >
       typename result<>::type
       operator()( ContainerT c )
       {
         return boost::get<N>(c);
       }// end of operator N;

    };

   } // end of namespace phoenix_helpers;
  } // end of namespace utilities;
 } // end of namespace compilers;
} // end of namespace SK;

#endif