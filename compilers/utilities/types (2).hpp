#ifndef SK_COMPILERS_UTILITIES_TYPES_HPP_JAN_16_2012
#define SK_COMPILERS_UTILITIES_TYPES_HPP_JAN_16_2012

#include <boost/mpl/mpl.hpp>

namespace SK
{ 
  namespace compilers
  {
    namespace utilities
    {
      namespace types
      {
        // nil_ type - basicly Boost.Fusion & Boost.Spirit.V2 
        // unused_type. Copied almost verbatim from 
        // Boost.Spirit.V2.Support. 
        struct nil_type
        { 
         typedef nil_type type;
         nil_type(){}
         nil_type( const nil_type& ) {}
         ~nil_type(){}
         template< typename T >
         nil_type( T const & ){}
         nil_type const & operator=( nil_type const & )
         { return *this; }
         nil_type const & operator=( nil_type const & ) const
         { return *this; }
         template< typename T >
         nil_type const & operator=( T const & )
         { return *this; }
         template< typename T >
         nil_type const & operator=( T const & )const
         { return *this; }
        };
        namespace _impl
        {
         struct nil_only
         {
          nil_only( nil_type const& ){}
         };
        }
        static const nil_type nil = {};
        
        template< typename Ostream >
        Ostream& operator<<( Ostream& out, _impl::nil_only const & )
        { return out; }
        
        template< typename Istream >
        Istream& operator>>( Istream& in, _impl::nil_only const & )
        { return in; }
        
        namespace traits
        {
         template< typename T >
         struct is_nil : mpl::false_
         {};
         
         template<>
         struct is_nil< nil_type > : mpl::true_
         {};
        } // end of namespace traits;
        
      } // end of namespace types;
    } // end of namespace utilities;
  } // end of namespace compilers;
} // end of namespace SK;
#endif
