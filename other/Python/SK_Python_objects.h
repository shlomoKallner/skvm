

#ifndef SK_PYTHON_OBJECTS_H
#define SK_PYTHON_OBJECTS_H

#include "SK_types.h"
#include <boost/shared_ptr.hpp>
#include "SK_Python_types.h"


namespace SK
{
          
  namespace python
  {
   
   namespace _impl 
   {
     
     class object_base
     {
     }; // end of class object_base
     
     class object 
     {
      public:  
       typedef boost::shared_ptr< object > object_ptr;       
       virtual bool is_builtin( void )
       {
         return false;      
       } // end of function is_builtin;
       
      private:
    
     };  // end of class object
   
     static void object_deleter( object * o )
     {
      if ( !o->is_builtin() )
      {
          delete o;
      }
     } //end of function object_deleter   
     
     typedef object::object_ptr object_ptr;
     
     object_ptr make_new_object( void )
     {
                
      return object_ptr( new object, object_deleter );
     
     } //end of function make_new_object
             
   } // end of namespace  _impl            
      
   
   
  } // end of namespace python
          
} // end of namespace SK

#endif
