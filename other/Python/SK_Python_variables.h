

#ifndef SK_PYTHON_VARIABLES_H
#define SK_PYTHON_VARIABLES_H


#include "SK_types.h"
#include <boost/shared_ptr.hpp>
#include "SK_Python_objects.h"

namespace SK
{
          
  namespace python
  {
   
   namespace _impl 
   {
    // the variable sized object:
    class variable_sized : public object
    {
     public:
      variable_sized( std::size_t s = 0 ) : size_m(s) 
      {} // end of constructor.
      
      typedef boost::shared_ptr< variable_sized >  variable_sized_ptr;
      virtual bool is_builtin( void )
      {
        return false;
      } // end of function is_builtin;
      
      std::size_t get_size()
      {
       return this->size_m;
      } //end of function get_size;
     private:
      std::size_t size_m;    
    }; // end of class variable;
   
   static void variable_deleter( variable_sized * v )
     {
      if ( !v->is_builtin() )
      {
          delete v;
      }
     } // end of function type_deleter
     
     typedef variable_sized::variable_sized_ptr  variable_sized_ptr;
     
     type_ptr make_variable_sized( void )
     {
      return variable_ptr( new variable , variable_deleter );
     } // end of function make_type;
      
   
   } // end of namespace _impl
   
   typedef _impl::variable     variable;
   typedef _impl::variable_ptr variable_ptr;
   
   variable_ptr make_variable( void )
   {
    return _impl::make_variable();
   } // end of function make_type;
   
   
  } // end of namespace python
          
} // end of namespace SK
  

#endif
