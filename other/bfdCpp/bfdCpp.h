#ifndef SK_UTILITIES_BFDCPP_H
#define SK_UTILITIES_BFDCPP_H

#include <fstream>
#include "SK/common/SK_types.h"
#include "endianess.h"

namespace SK
{
 namespace utilities
 {
  
  
  template< typename EndianT >
  class bfd : public EndianT
  {
   typedef EndianT         base_t;
   typedef base_t::uword_t uvma_t;
   typedef base_t::sword_t svma_t;
   typedef base_t::uword_t size_type;
   typedef base_t::uword_t symvalue_t;
   typedef uint32          flagword;	/* 32 bits of flags */
   typedef uint8           byte;
   
   bfd(){}//end of ...
   bfd( bfd const& ){}//end of ...
   virtual ~bfd(){}//end of ...
   
  };
  
  
 }
}

#endif
