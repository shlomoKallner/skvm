#ifndef SK_UTILITIES_TYPES_H
#define SK_UTILITIES_TYPES_H

#include <stdint.h>
#include "SK/common/value_ptr.h"

namespace SK
{
 namespace types
 {
   typedef bool         boolean; // boolean type.
   typedef ::int8_t     sint8;   // signed 8-bit integer.
   typedef ::uint8_t    uint8;   // unsigned 8-bit integer.
   typedef ::int16_t    sint16;  // signed 16-bit integer.
   typedef ::uint16_t   uint16;  // unsigned 16-bit integer.
   typedef ::int32_t    sint32;  // signed 32-bit integer.
   typedef ::uint32_t   uint32;  // unsigned 32-bit integer.
   typedef ::int64_t    sint64;  // signed 64-bit integer.
   typedef ::uint64_t   uint64;  // unsigned 64-bit integer.
   typedef ::intptr_t   ptrsint; // pointer holding signed integer.
   typedef ::uintptr_t  prtuint; // pointer holding unsigned integer.
   typedef void *       anyptr;  // pointer type that can point to anithing.
   typedef float        float32; // 32 - bit floating-point type.
   typedef double       float64; // 64 - bit floating-point type.
   typedef long double  floatn;
   
 }
}

#endif 
