#ifndef SK_SGVM_EVAL_IDENTIFIER_EVALUATOR_H
#define SK_SGVM_EVAL_IDENTIFIER_EVALUATOR_H

// you include the file of the base type here:
#include "gsvm/evaluator/framework/rule_eval.h"
#include "gsvm/general/gsvm_dll_const.h"
// your defines go here:

namespace SK
{
 namespace gsvm
 {
  namespace eval
  { 
   
   struct ident_eval : public rule_eval< ident_eval >
   {
    typedef typename ident_eval                            self_t;
    typedef typename boost::shared_ptr< self_t >           self_ptr;
    typedef typename base_eval< ident_eval >               base_t;
    
    // end of required typedefs
    
    // end of typedefs
    
    template< typename InputT >
    struct result
    { 
     typedef evaluator_result< identifier_object<InputT> > type;
     typedef std::list< type > container_t; 
    };
    
    template< typename InputT >
    inline typename eval_result<self_t,InputT>::type
    operator()( InputT::tree_iter const& node_ )const
    {
     return this->evaluate(node_);
    }//end of ...
    
    template< typename InputT >
    typename eval_result<self_t,InputT>::type
    evaluate( InputT::tree_iter const& node_ )const
    {
     using namespace SK::gsvm::ddl::consts;
     typedef typename eval_result<self_t,InputT>::type return_type;
     boolean s = is_eval_able( node_ );
     return_type ret(s);
     if( s )
     {
      InputT::string_t str = "";
      std::size_t node_id = node_->value.id().to_long();
      
      switch( node_id )
      {
       using namespace SK::gsvm::ddl::consts;
       case BASIC_IDENT: // basic_ident_r
            str.assign(node_->value.begin(), node_->value.end() );
            typedef return_type::value_type basic_val_t;
            ret.value( new basic_val_t(str) );
            ret.id( node_id );
            break;
       case VAR_IDENT: // qualified_var_r
            str.assign(node_->value.begin(), node_->value.end() );
            if( str == "var" )
            {
             return_type r = this->evaluate( node_->children.begin() );
             if( r )
             {
              ret.id( node_id );
              
             }
             
            }
            else
            { assert(0); }
            break;           
       case CONST_IDENT: // qualified_const_r
                        
       case TYPENAME_IDENT: // qualified_typename_r
                           
       case BITFIELD_IDENT: // qualified_bitfield_r
      
       case ENUM_IDENT: // qualified_enum_r
      
       case OBJECT_IDENT: // qualified_object_r
      
       case UNION_IDENT: // qualified_union_r
      
       case FUNCTION_TYPE_IDENT: // qualified_func_type_r
      
       case OPERATOR_TYPE_IDENT: // qualified_oper_type_r
      
       case QUALIFIED_TYPEID: // qualified_typeid_r
       
       case QUALIFIED_TYPEOF_IDENT: // qualified_typeof_expr_r
      
       case UNQUALIFIED_TYPEID: // unqualified_typeid_r
      
       case LABEL_IDENT: // qualified_label_r
      
       case FUNCTION_IDENT: // qualified_function_r
      
       case OPERATOR_IDENT: // qualified_operator_r
      
       case BUILTIN_TYPES_IDENT: // builtin_types_r
      
       case BUILTIN_FUNC_IDENT: // builtin_function_r
      
       case NESTED_NAME_IDENT: // nested_namespace_r
       
             break;
       default:
             assert(0);
      }
         
     }// end of if
      
     return ret;  
    }//end of ...
    
    template< typename InputT >
    inline typename eval_result<self_t,InputT>::container_t
    operator()( InputT::tree_iter const& first_, 
                InputT::tree_iter const& last_ )const
    {
     return this->evaluate(first_,last_);
    }//end of ...
    
    template< typename InputT >
    typename eval_result<self_t,InputT>::container_t
    evaluate( InputT::tree_iter const& first_, 
              InputT::tree_iter const& last_ )const
    {
     typedef typename eval_result<self_t,InputT>::container_t return_type;
     return_type ret();
     for( InputT::tree_iter i = first_ ; i != last_; i++)
     {
      ret.push_back( this->evaluate(i) );
     }
     return ret;
    }//end of ...
    
    static boolean is_eval_able( InputT::tree_iter const& node_ )const
    {
     switch( node_->value.id().to_long() )
     {
      using namespace SK::gsvm::ddl::consts;
      case BASIC_IDENT: // basic_ident_r
      case VAR_IDENT: // qualified_var_r
      case CONST_IDENT: // qualified_const_r
      case TYPENAME_IDENT: // qualified_typename_r
      case BITFIELD_IDENT: // qualified_bitfield_r
      case ENUM_IDENT: // qualified_enum_r
      case OBJECT_IDENT: // qualified_object_r
      case UNION_IDENT: // qualified_union_r
      case FUNCTION_TYPE_IDENT: // qualified_func_type_r
      case OPERATOR_TYPE_IDENT: // qualified_oper_type_r
      case QUALIFIED_TYPEID: // qualified_typeid_r
      case UNQUALIFIED_TYPEID: // unqualified_typeid_r
      case LABEL_IDENT: // qualified_label_r
      case FUNCTION_IDENT: // qualified_function_r
      case OPERATOR_IDENT: // qualified_operator_r
      case BUILTIN_TYPES_IDENT: // builtin_types_r
      case BUILTIN_FUNC_IDENT: // builtin_function_r
      case NESTED_NAME_IDENT: // nested_namespace_r
      case USING_IDENT: // using_ident_r
            return true;
      default:
       return false;
     }
    }// end of ...
   
   
   };
   
  } 
 } 
}



#endif
