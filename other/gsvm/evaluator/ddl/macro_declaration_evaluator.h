#ifndef SK_SGVM_EVAL_DECLARATION_EVALUATOR_H
#define SK_SGVM_EVAL_DECLARATION_EVALUATOR_H

#include "rule_eval.h"
#include "gsvm_ddl_const.h"
#include "macro_statement_evaluator.h"

namespace SK
{
 namespace gsvm
 {
  namespace eval
  { 
   
   struct macro_decl_eval : public rule_eval<macro_decl_eval>
   {
    typedef typename macro_decl_eval                         self_t;
    typedef typename boost::shared_ptr< self_t >             self_ptr;
    typedef typename rule_eval<macro_decl_eval>              base_t;
                     
    template< typename T >
    struct result
    { 
     typedef evaluator_result< nil_t > type;
     typedef std::list< type > container_t;
    };
    
    
    template< typename T >
    inline typename eval_result<self_t,T>::type
    operator()(tree_iter const& node_ )const
    {
     return this->evaluate(node_);
    }//end of ...
    
    template< typename T >
    typename eval_result<self_t,T>::type
    evaluate( tree_iter const& node_ )const
    {
     /* - the possible declarations to be found in macro_declaration_groop_r:
        macro_declaration_state_r , macro_using_state_r , macro_import_decl_r , 
        macro_namespace_decl_r , macro_func_decl_r , macro_oper_decl_r , 
        macro_ctor_decl_r , macro_dtor_decl_r , macro_object_decl_r , 
        macro_forward_decl_r  
     */
     using namespace SK::gsvm::ddl::consts;
     typedef typename typename eval_result<self_t,T>::type return_type;
     return_type ret();
     switch( node_->value.id() )
     {
      case DECL_MSTATE: // macro_declaration_state_r
       break;
      case USING_MSTATE: // macro_using_state_r
       break;
      case IMPORT_MDECL: // macro_import_decl_r
       break;
      case NMSPC_MDECL: // macro_namespace_decl_r
       break;
      case FUNC_MDECL: // macro_func_decl_r
       break;
      case OPER_MDECL: // macro_oper_decl_r
       break;
      case CTOR_MDECL: // macro_ctor_decl_r
       break;
      case DTOR_MDECL: // macro_dtor_decl_r
       break;
      case OBJECT_MDECL: // macro_object_decl_r
       break;
      case FORWARD_MDECL: // macro_forward_decl_r
       break;
      /*
      case : // 
       break;
      */
      default: // maybe this should be a throw of some error type instead of assert() ?
              assert(0);
     }
     return ret;
    }//end of 
    
     template< typename T >
     inline typename eval_result<self_t,T>::container_t
     operator()( tree_iter const& first_, tree_iter const& last_ )const
     {
      return this->evaluate(first_,last_);
     }//end of ...
     
     template< typename T >
     typename eval_result<self_t,T>::container_t
     evaluate( tree_iter const& first_, tree_iter const& last_ )const
     {
      typedef typename typename eval_result<self_t,T>::container_t return_type;
      return_type ret();
      for(tree_iter i = first_ ; i != last_; i++)
      {
       ret.push_back( this->evaluate(i) );
      }
      return ret;
     }//end of ...
     
     static boolean is_eval_able( tree_iter const& node_ )const
     {
      using namespace SK::gsvm::ddl::consts;
      switch( node_->value.id() )
      {
       /*
        case : // 
       */
       case DECL_MSTATE: // macro_declaration_state_r
       case USING_MSTATE: // macro_using_state_r
       case IMPORT_MDECL: // macro_import_decl_r
       case NMSPC_MDECL: // macro_namespace_decl_r
       case FUNC_MDECL: // macro_func_decl_r
       case OPER_MDECL: // macro_oper_decl_r
       case CTOR_MDECL: // macro_ctor_decl_r
       case DTOR_MDECL: // macro_dtor_decl_r
       case OBJECT_MDECL: // macro_object_decl_r
       case FORWARD_MDECL: // macro_forward_decl_r
        return true;
       default:
        return false;
      }
     }//end of ...
    
    
   };
   
  } 
 } 
}


#endif
