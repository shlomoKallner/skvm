#ifndef SK_SGVM_EVAL_PARSED_PROJECT_H
#define SK_SGVM_EVAL_PARSED_PROJECT_H



namespace SK
{
 namespace gsvm
 {
  namespace eval
  { 
   
   struct project_eval : public rule_eval<project_eval>
   {
    typedef typename project_eval                       self_t;
    typedef typename boost::shared_ptr< self_t >        self_ptr;
    typedef typename rule_eval<project_eval>            base_t;
    
    
    template< typename T >
    struct result
    { 
     typedef evaluator_result< nil_t > type;
     typedef std::list< type > container_t; 
    };
    
    
    template< typename T >
    inline typename eval_result<self_t,T>::type
    operator()(tree_iter const& node_ )const
    {
     return this->evaluate(node_);
    }//end of ...
    
    template< typename T >
    typename eval_result<self_t,T>::type
    evaluate( tree_iter const& node_ )const
    {
     typedef typename eval_result<self_t,T>::type return_type;
     return return_type();
    }//end of 
    
    template< typename T >
     inline typename eval_result<self_t,T>::container_t
     operator()( tree_iter const& first_, tree_iter const& last_ )const
     {
      return this->evaluate(first_,last_);
     }//end of ...
     
     template< typename T >
     typename eval_result<self_t,T>::container_t
     evaluate( tree_iter const& first_, tree_iter const& last_ )const
     {
      typedef typename eval_result<self_t,T>::container_t return_type;
      return_type ret();// rewrite with real ctor!!!!
      for(tree_iter i = first_ ; i != last_; i++)
      {
       ret.push_back( this->evaluate(i) );
      }
      return ret;
     }//end of ...
     
     static boolean is_eval_able( tree_iter const& node_ )const
     {
      switch( node_->value.id().to_long() )
      {
       default:
        return false;
      }
     }//end of ...
    
   };
   
   
  } 
 } 
}

#endif
