#ifndef SK_GSVM_EVAL_CUSTOM_EVALUATOR_H
#define SK_GSVM_EVAL_CUSTOM_EVALUATOR_H

#include <boost/spirit/home/classic/namespace.hpp>
#include <boost/spirit/home/classic/core/non_terminal/impl/rule.ipp>

#include "evaluator_base.h"

namespace SK
{
 namespace gsvm
 {
  namespace eval
  { 
   using BOOST_SPIRIT_CLASSIC_NS::impl::get_param;
   
   template < typename CheckerT, typename EvalT, 
              typename ContextT = evaluator_context >
   struct custom_evaluator 
          : public evaluator<custom_evaluator<CheckerT,EvalT, ContextT > > , 
            public evaluator_context_aux< ContextT, custom_evaluator<CheckerT,
                   EvalT , ContextT > >
   {
    typedef typename custom_evaluator< CheckerT, EvalT, ContextT >     
                                                 self_t;
    typedef typename value_ptr< self_t >         self_ptr;
    typedef typename self_ptr                    embed_t;
    typedef typename evaluator< self_t >         base_t;
    typedef typename CheckerT                    check_type;
    typedef typename EvalT                       eval_type;
    typedef typename ContextT                    context_t;
    typedef typename context_t::linked_evaluator_context_t
                                                 linked_evaluator_context_t;
    // end of required typedefs
    
    // end of typedefs
    
    template< typename InputT >
    struct result
    { 
     typedef EvalT::result<InputT>::type type; 
     typedef EvalT::result<InputT>::container_t container_t;
    };
    
    custom_evaluator( CheckerT& c, EvalT& e ) : checker_m(c),
              evaluator_m(e) {}//end of ...
    custom_evaluator( self_t const& s) : checker_m(s.checker_m),
              evaluator_m(s.evaluator_m) {}//end of ...
    
    template< typename InputT >
    inline typename eval_result<self_t,InputT>::type
    operator()( InputT::tree_iter const& node_ )const
    {
     return this->evaluate(node_);
    }//end of ...
    
    template< typename InputT >
    typename eval_result<self_t,InputT>::type
    evaluate( InputT::tree_iter const& node_ )const
    {
     typedef typename eval_result<self_t,InputT>::type return_type;
     boolean s = checker_m(node_);
     return_type ret(s);
     linked_evaluator_context_t ctx( *this );
     ctx.pre_eval( *this, node_ );
     if( s )
     {
      ret = evaluator_m(node_);
     }
     return ctx.post_eval( ret, *this, node_ );
    }//end of ...
    
    template< typename InputT >
    inline typename eval_result<self_t,InputT>::container_t
    operator()( InputT::tree_iter const& first_, 
                InputT::tree_iter const& last_ )const
    {
     return this->evaluate(first_,last_);
    }//end of ...
    
    template< typename InputT >
    typename eval_result<self_t,InputT>::container_t
    evaluate( InputT::tree_iter const& first_, 
              InputT::tree_iter const& last_ )const
    {
     typedef typename eval_result<self_t,InputT>::container_t return_type;
     return_type ret();
     linked_evaluator_context_t ctx( *this );
     ctx.pre_eval( *this, first_ , last_ );
     for( InputT::tree_iter i = first_ ; i != last_; i++)
     {
      ret.push_back( this->evaluate(i) );
     }
     return ctx.post_eval( ret, *this, first_ , last_ );
    }//end of ...
    
    template< typename InputT >
    static boolean is_eval_able( InputT::tree_iter const& node_ )const
    {
     CheckerT check_();
     return check_(node_);
    }// end of ...
    
    static embed_t make_embed( self_t& s )
    {
     return embed_t(s);
    }//end of ...
    
    protected:
              CheckerT checker_m;
              EvalT    evaluator_m;
   
   };
   
   template < typename CheckerT, typename EvalT, 
              typename ContextT = evaluator_context >
   typename custom_evaluator<CheckerT,EvalT,ContextT>::embed_t
   eval( CheckerT const& c, EvalT const& e, ContextT = ContextT() )
   {
    typename custom_evaluator<CheckerT,EvalT,ContextT> ret_type;
    ret_type ret( c, e );
    return make_embed( ret );
   }//end of ...
   
   
   
  } 
 } 
}


#endif
// 
