#ifndef SK_GSVM_EVAL_PARSER_EVALUATOR_BASE_H
#define SK_GSVM_EVAL_PARSER_EVALUATOR_BASE_H

#include "evaluator_fwd.h"
#include "evaluator_result.h"

namespace SK
{
 namespace gsvm
 {
  namespace eval
  {
   using boost::spirit::classic::tree_match;
   using namespace SK::types;
   
   
   
   template< typename IteratorT, typename T, typename FactoryT >
   struct input_policy
   {
     typedef typename IteratorT                                iterator_t;
     typedef typename FactoryT                                 factory_t;
     typedef typename T                                        value_t;
     typedef typename 
        boost::detail::iterator_traits<IteratorT>::value_type  char_t;
     typedef typename std::basic_string< char_t >              string_t;
     typedef typename tree_match< iterator_t, factory_t, value_t >      
                                                               match_t;
     typedef typename match_t::node_t                          node_t;
     typedef typename match_t::tree_iterator                   tree_iter;
   };
   
   template< typename EvalT, typename InputT >
   struct eval_result
   {
    typedef typename EvalT::template result<InputT>::type type;
    typedef typename EvalT::template result<InputT>::container_t container_t;
   };
   
   template< typename EvalT >
   class evaluator
   {
    public:
     typedef typename evaluator< EvalT >                       self_t;
     typedef typename boost::shared_ptr< self_t >              self_ptr;
     typedef typename EvalT::embed_t                           embed_t;
     
     
     
     template< typename InputT >
     struct result
     {
      typedef typename EvalT::template result<InputT>::type type;
      typedef typename EvalT::template result<InputT>::container_t container_t;
     };
     
     evaluator( void ){ }// end of ...
     evaluator( const self_t& ) {} //end of ...
     virtual ~evaluator() {}//end of ...
     
     EvalT& derived(void)
     {
      return *static_cast<EvalT*>(this);
     }//end of ...
     EvalT const& derived(void)
     {
      return *static_cast<EvalT const*>(this);
     }//end of ...
     
     template< typename InputT >
     inline typename eval_result<self_t,InputT>::type
     operator()( InputT::tree_iter const& node_ )const
     {
      return this->evaluate(node_);
     }//end of ...
     
     template< typename InputT >
     typename eval_result<self_t,InputT>::type
     evaluate( InputT::tree_iter const& node_ )const
     {
      return this->derived().evaluate(node_);
     }//end of ...
     
     template< typename InputT >
     inline typename eval_result<self_t,InputT>::container_t
     operator()( InputT::tree_iter const& first_, 
                 InputT::tree_iter const& last_ )const
     {
      return this->evaluate(first_,last_);
     }//end of ...
     
     template< typename InputT >
     typename eval_result<self_t,InputT>::container_t
     evaluate( InputT::tree_iter const& first_, 
               InputT::tree_iter const& last_ )const
     {
      return this->derived().evaluate(first_,last_);
     }//end of ...
     
     template< typename InputT >
     static boolean is_eval_able( InputT::tree_iter const& node_ )const
     {
      return EvalT::is_eval_able( node_ );
     }//end of ...
     
     static embed_t make_embed( self_t& s )
     {
      return EvalT::make_embed( s );
     }//end of ...
     
   };   
   
   
  }
 }
}

#endif
