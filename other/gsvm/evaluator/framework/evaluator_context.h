#ifndef SK_SGVM_EVAL_EVALUATOR_CONTEXT_EVALUATOR_H
#define SK_SGVM_EVAL_EVALUATOR_CONTEXT_EVALUATOR_H

// you include the file of the base type here:
#include "evaluator_fwd.h"
// your defines go here:

namespace SK
{
 namespace gsvm
 {
  namespace eval
  { 
   
   struct evaluator_context_base
   {
    template< EvalT >
    struct aux {};
   };
   
   struct evaluator_context
   {
    typedef evaluator_context self_t;
    typedef evaluator_context_base eval_base_t;
    typedef evaluator_context_linker<self_t> linked_evaluator_context_t;
    
    template< typename EvalT >      
    evaluator_context( EvalT const& ){}//end of ...
    
    template< typename EvalT, typename InputT >
    void pre_eval( EvalT const&, InputT::tree_iter const& )
    {}//end of ...
    
    template< typename EvalT, typename InputT >
    void pre_eval( EvalT const&, InputT::tree_iter const&,
                   InputT::tree_iter const& )
    {}//end of ...
    
    template< typename EvalT, typename InputT, typename ReturnT >
    ReturnT& post_eval( ReturnT& ret, EvalT const&, InputT::tree_iter const& )
    { return ret; }// end of ...
    
    template< typename EvalT, typename InputT, typename ReturnT >
    ReturnT& post_eval( ReturnT& ret, EvalT const&, InputT::tree_iter const&,
                        InputT::tree_iter const& )
    { return ret; }// end of ...
   };
   
   template< typename ContextT, typename EvalT >
   struct evaluator_context_aux 
   : public ContextT::eval_base_t::template aux<EvalT>
   {};
   
   template< typename ContextT >
   struct evaluator_context_linker : public ContextT
   {
    typedef typename ContextT::eval_base_t eval_base_t;
    
    template< typename EvalT >      
    evaluator_context_linker( EvalT const& e ) : ContextT( e )
    {}//end of ...
    
    template< typename EvalT, typename InputT >
    void pre_eval( EvalT const& e, InputT::tree_iter const& t )
    {
      ContextT::pre_eval( e , t );
    }//end of ...
    
    template< typename EvalT, typename InputT >
    void pre_eval( EvalT const& e, InputT::tree_iter const& first_,
                   InputT::tree_iter const& last_ )
    {
      ContextT::pre_eval( e , first_ , last_ );
    }//end of ...
    
    template< typename EvalT, typename InputT, typename ReturnT >
    ReturnT& post_eval( ReturnT& ret, EvalT const& e, 
                        InputT::tree_iter const& t )
    {
     return ContextT::post_eval( ret, e , t );
    }// end of ...
    
    template< typename EvalT, typename InputT, typename ReturnT >
    ReturnT& post_eval( ReturnT& ret, EvalT const& e, 
                        InputT::tree_iter const& first_,
                        InputT::tree_iter const& last_ )
    {
     return ContextT::post_eval( ret, e , first_ , last_ );
    }// end of ...
     
   };
   
  } 
 } 
}



#endif
