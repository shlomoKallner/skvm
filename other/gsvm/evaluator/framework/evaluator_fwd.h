#ifndef SK_GSVM_EVAL_EVALUATOR_FWD_H
#define SK_GSVM_EVAL_EVALUATOR_FWD_H

#include <string>
#include <map>
#include <list>
#include <vector>
#include <set>
#include <cassert>

#include <boost/spirit/include/classic_spirit.hpp>
#include <boost/spirit/home/classic/namespace.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/detail/iterator.hpp>

#include "SK_common/SK_types.h"
#include "gsvm_ddl_types.h"

namespace SK
{
 namespace gsvm
 {
  namespace eval
  {
   using BOOST_SPIRIT_CLASSIC_NS::node_val_data_factory;
   using BOOST_SPIRIT_CLASSIC_NS::node_iter_data_factory;
   using namespace SK::types;
   
   template< typename IteratorT = char const *, 
             typename T = nil_t,
             typename FactoryT = node_iter_data_factory<T> >
   struct input_policy;
   
   template< typename EvalT, typename InputT = input_policy<> >
   struct eval_result;
   
   template< typename EvalT >
   typename EvalT::embed_t make_embed( EvalT & t )
   {
    return EvalT::make_embed(t);
   }//end of ...
   
   template< typename EvalT >
   class evaluator;      
   
   template< typename EvalT >
   struct rule_eval;
   
   template< typename EvalT >
   struct value_eval;
   
   struct id_checker;
   
   struct root_checker;
   
   struct text_checker;
   
   template< typename T >
   struct value_checker;
   
   // the evaluator result type
   template< typename T >
   struct evaluator_result;
      
  }
 }
}

#endif
