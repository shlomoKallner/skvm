#ifndef SK_SGVM_EVAL_EVALUATOR_RESULT_H
#define SK_SGVM_EVAL_EVALUATOR_RESULT_H

// you include the file of the base type here:
#include "evaluator_fwd.h"
// your defines go here:

namespace SK
{
 namespace gsvm
 {
  namespace eval
  { 
   
   template< typename T >
   struct evaluator_result
   {
    typedef T value_type;
    
    evaluator_result(): success_m(false), val_(), id_m()
    {}//end of ...
    
    explicit evaluator_result( boolean s ): success_m(s), val_(), id_m()
    {}//end of ...
    
    evaluator_result( boolean s , value_type& v, std::size_t i )
    : success_m(s), val_(v), id_m(i)
    {}//end of ...
    
    virtual ~ evaluator_result(){}//END OF ...
    
    std::size_t id(void)
    {
     return id_m;
    }//end of ...
    
    void id( std::size_t i )
    {
     id_m = i;
    }//end of ...
    
    value_type value(void)
    {
     return val_m;
    }//end of ...
    
    void value( value_type& v )
    {
     val_m = v;
    }//end of 
    
    boolean operator boolean( void )
    {
     return success_m;
    }//end of ...
    
    protected:
              boolean success_m;
              value_type val_m;
              std::size_t id_m;
   };
   
   
   
  } 
 } 
}



#endif
