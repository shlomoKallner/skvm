#ifdef  GSVM_DDL_CONST_H
#define GSVM_DDL_CONST_H

namespace SK
{
 namespace gsvm
 {
  namespace ddl
  {
   namespace consts
   {
                      // the IDs of all parser nodes:
     enum Id_consts { NULL_ID = 0, WHITESPACE_ID, COMMENT_ID, 
                      // the IDs of operators:
                      FUNC_CALL_OP, ARRAY_INDEX_OP, MEMBER_ACCESS_VIA_PTR_OP,
                      INCREMENT_OP, DECREMENT_OP, ADDITION_OP, SUBTRACTION_OP,
                      ADDRESSOF_OP, UNARY_PLUS_OP, UNARY_MINUS_OP, 
                      LOGICAL_NOT_OP, BITWISE_NOT_OP, GETADDRESS_OP,
                      DEREFERENCE_OP,
                      BITWISE_AND_OP, MULTIPLY_OP, TYPE_CAST_OP, 
                      DYNAMIC_ALLOCATE_OP, DYNAMIC_DEALLOCATE_OP, 
                      MEMBER_ACCESS_VIA_PTR_TO_MEMBER_OP,
                      DIVISION_OP, MODULUS_OP, BITSHIFT_LEFT_OP, 
                      BITSHIFT_RIGHT_ARITHMETIC_OP, BITSHIFT_RIGHT_LOGICAL_OP,
                      LESS_THAN_OP, LESS_THAN_EQUAL_OP, GREATER_THAN_OP,
                      GREATER_THAN_EUQUAL_OP, EQUALS_OP, NOT_EQUALS_OP,
                      BITWISE_XOR_OP, BITWISE_OR_OP, LOGICAL_AND_OP, 
                      LOGICAL_XOR_OP, LOGICAL_OR_OP, ASSIGNMENT_OP, 
                      ASSIGNMENT_ADDITION_OP, ASSIGNMENT_SUBTRACTION_OP,
                      ASSIGNMENT_MULTIPLY_OP, ASSIGNMENT_DIVISION_OP, 
                      ASSIGNMENT_MODULUS_OP, ASSIGNMENT_BITWISE_AND_OP,
                      ASSIGNMENT_BITWISE_XOR_OP, ASSIGNMENT_BITWISE_OR_OP,
                      ASSIGNMENT_BITSHIFT_LEFT_OP, 
                      ASSIGNMENT_BITSHIFT_RIGHT_ARITHMETIC_OP, 
                      ASSIGNMENT_BITSHIFT_RIGHT_LOGICAL_OP, SEQUENCE_OP,
                      // the IDs of litterals:
                      CHAR_LIT, WCHAR_LIT, MBCHAR_LIT, VLCHAR_LIT,
                      STR_LIT, WSTR_LIT, MBSTR_LIT, VLSTR_LIT, 
                      SINT8_LIT, SINT10_LIT, SINT16_LIT, 
                      UINT8_LIT, UINT10_LIT, UINT16_LIT,
                      FLOAT8_LIT, FLOAT10_LIT, FLOAT16_LIT, 
                      BOOL_LIT,
                      
                      // the ddl IDs:
                      // the IDs of builtin fucs:
                      DYNAMIC_CAST_BF, REINTERPRET_CAST_BF, SIZEOF_BF, 
                      STATIC_CAST_BF, TYPEID_BF,
                      
                      // the ID of builtin types:
                      BOOLEAN_BT, BYTE_BT, CHAR_BT, DOUBLE_BT,
                      DOUBLEWORD_BT, FLOAT_BT, HALFWORD_BT, INT_BT,
                      LONG_BT, MBCHAR_BT, SHORT_BT, SIGNED_BT, 
                      TYPEINFO_BT, UNSIGNED_BT, VOID_BT, VLCHAR_BT, 
                      VARARG_BT, WCHAR_BT, WORD_BT,
                      
                      // the IDs of identifiers:
                      BASIC_IDENT, VAR_IDENT, CONST_IDENT, TYPENAME_IDENT,
                      BITFIELD_IDENT, ENUM_IDENT, OBJECT_IDENT, UNION_IDENT,
                      FUNCTION_TYPE_IDENT, FUNCTION_IDENT_ARGS, 
                      FUNCTION_IDENT_ARG, QUALIFIED_TYPEOF_IDENT, 
                      QUALIFIED_TYPEID, UNQUALIFIED_TYPEID,
                      LABEL_IDENT, FUNCTION_IDENT, OPERATOR_IDENT, 
                      NAMESPACE_IDENT, IMPORT_IDENT, BUILTIN_TYPES_IDENT,
                      BUILTIN_FUNC_IDENT, OPERATOR_HELPER, OPERATOR_TYPE_IDENT,
                      NESTED_NAME_IDENT, USING_IDENT, 
                      
                      // the IDs of modifiers:
                      TEMP_ID_MOD, TEMP_OPERID_MOD,  TEMP_MOD_ARGS,
                      TEMP_MOD_ARG, VAR_PTR_MOD, VAR_REF_MOD, CONST_PTR_MOD,
                      CONST_REF_MOD, TYPE_POSTFIX_MOD, CALLCONV_MOD, LANG_MOD, 
                      THROW_MOD, THROW_ARGS_MOD, THROW_ARG_MOD, STATIC_MOD,
                      CONST_MOD, GENDEC_MOD, TEMP_DECL_MOD, TEMP_OPERDECL_MOD,
                      TEMP_DECL_ARGS, TEMP_DECL_ARG, VOLATILE_MOD, 
                      INITIALIZER_MOD, MUTABLE_MOD, ABSTRACT_MOD, VIRTUAL_MOD,
                      FUNC_DECL_ARGS, FUNC_DECL_ARG, EXPLICIT_MOD, 
                      CTOR_INIT_LIST, CTOR_INIT_MEM, INHERIT_LIST, INHERIT_MEM,
                      TYPEDECL_INFIX_MOD, ENUMERATOR_MOD, ACCESS_MOD,
                      
                      // the IDs of expressions:
                      IDENT_EXPR, PRIMARY_EXPR, POSTFIX_EXPR, ARRAY_INDEX_EXPR,
                      FUNC_CALL_EXPR, EXPLICIT_CTOR_EXPR, MEMBER_REF_EXPR,
                      MEMBER_PTR_EXPR, EXPLICIT_DTOR_EXPR, INC_DEC_EXPR, 
                      EXPR_LIST, NEW_EXPR, DEL_EXPR, DEL_ARRAY_EXPR, UNARY_EXPR,
                      UNARY_OPERS, PTR_MEM_EXPR, MULTIPLY_EXPR, ADDITION_EXPR,
                      BITSHFT_EXPR, RELATIONAL_EXPR, EQUALITY_EXPR, 
                      BITWISEAND_EXPR, BITWISEXOR_EXPR, BITWISEOR_EXPR,
                      LOGICALAND_EXPR, LOGICALXOR_EXPR, LOGICALOR_EXPR, 
                      CONDITIONAL_EXPR, ASSIGNMENT_EXPR, ASSIGNMENT_OPERS, 
                      SEQUENCE_EXPR, SEQUENCE_EXPR, CONDITION_EXPR, 
                      // the IDs of statements:
                      EXPR_STATE, DECL_STATE, SELECT_STATE, IF_STATE,
                      ELIF_STATE, ELSE_STATE, SWITCH_STATE, LABEL_STATE,
                      JUMP_STATE, CMPND_STATE, EXCPTN_STATE, CATCH_STATE, 
                      FINALLY_STATE, LOOP_STATE, USING_STATE,
                      // the IDs of declarations:
                      CONST_DECL, VAR_DECL, DATA_DECL, TYPEDEF_DECL, 
                      TYPEDEF_DEF_DECL, CONSTANT_DECL, VARIABLE_DECL, 
                      IMPORT_DECL, CMPND_DECL, NMSPC_FWD_DECL, NMSPC_DECL,
                      FUNC_FWD_DECL, FUNC_DECL, OPER_FWD_DECL, OPER_DECL,
                      CTOR_FWD_DECL, CTOR_DECL, DTOR_FWD_DECL, DTOR_DECL, 
                      BITFIELD_FWD_DECL, BITFIELD_DECL, ENUM_FWD_DECL, 
                      ENUM_DECL, OBJECT_FWD_DECL, OBJECT_DECL, UNION_FWD_DECL,
                      UNION_DECL, FORWARD_DECL, FRIEND_DECL, MEMBER_DECL, 
                      
                      // the macro IDs:
                      // the IDs of macro builtin funcs:
                      IS_CONST_MBF, IS_FUNC_MBF, IS_FLOAT_MBF, IS_INT_MBF,
                      IS_OPER_MBF, IS_STR_MBF, IS_TYPE_MBF, IS_VAR_MBF, 
                      LENGTH_MBF, TYPEOF_MBF, DYNAMIC_CAST_MBF, 
                      REINTERPRET_CAST_MBF, SIZEOF_MBF, STATIC_CAST_MBF, 
                      TYPEID_MBF,
                      
                      // the IDs of builtin macro types:
                      BOOLEAN_MBT, CHAR_MBT, INT_MBT, FLOAT_MBT, STRING_MBT,
                      TYPEINFO_MBT, VAR_MBT, VARARG_MBT, VOID_MBT, DOC_MBT, 
                      CODE_MBT, ERROR_MBT, CALL_MBT, SYSTEM_MBT, 
                      
                      // the IDs of builtin macro vars:
                      SYSTEM_MBV,
                      // the IDs of macro identifiers:
                      BASIC_MID, VAR_MID, FUNCTION_MID, OPERATOR_MID, 
                      TYPE_MID, NMSPC_MID, IMPORT_MID, NNAME_MID, USING_MID,
                      BFUNC_MID, BOPER_MID, BTYPE_MID, BVAR_MID, 
                      // the IDs of macro modifiers:
                      TEMPID_MMOD, TEMPID_OPER_MMOD, TEMPID_ARGS_MMOD, 
                      TYPE_POSTFIX_MMOD, THROW_MMOD, THROW_ARGS_MMOD, 
                      THROW_ARG_MMOD, STATIC_MMOD, CONST_MMOD, VOLATILE_MMOD,
                      INITIALIZER_MMOD, GEN_DECL_MMOD, ABSTRACT_MMOD, 
                      VIRTUAL_MMOD, TEMPDECL_MMOD, TEMPDECL_OPER_MMOD, 
                      TEMPDECL_ARGS_MMOD, TEMPDECL_ARG_MMOD, FUNCDECL_ARGS_MMOD,
                      FUNCDECL_ARG_MMOD, EXPLICIT_MMOD, CTOR_INITLIST_MMOD,
                      CTOR_INITMEM_MMOD, INHERITLIST_MMOD, INHERITMEM_MMOD,
                      ACCESS_MMOD,
                      // the IDs of macro expressions:
                      IDENT_MEXPR, PRRIME_MEXPR, POSTFIX_MEXPR, ARRAY_MEXPR,
                      EXPRLIST_MEXPR, FUNCCALL_MEXPR, EXPLCTCTOR_MEXPR,
                      MEMBER_MEXPR, EXPLCTDTOR_MEXPR, UNARY_MEXPR, UNARY_MOPERS,
                      NEW_MEXPR, NEWCTOR_MEXPR, DELETE_MEXPR, MULTIPLY_MEXPR,
                      ADDITION_MEXPR, BITSHIFT_MEXPR, RELATIONAL_MEXPR,
                      EQUALITY_MEXPR, BITWISEAND_MEXPR, BITWISEXOR_MEXPR,
                      BITWISEOR_MEXPR, LOGICALAND_MEXPR, LOGICALXOR_MEXPR,
                      LOGICALOR_MEXPR, CONDITIONAL_MEXPR, ASSIGNMENT_MEXPR,
                      THROW_MEXPR, SEQUENCE_MEXPR, CONDITION_MEXPR,
                      // the IDs of macro statements:
                      EXPR_MSTATE, DECL_MSTATE, SWITCH_MSTATE, IF_MSTATE,
                      ELIF_MSTATE, ELSE_MSTATE, SELECT_MSTATE, LABEL_MSTATE,
                      JUMP_MSTATE, CMPND_MSTATE, EXCPTN_MSTATE, CATCH_MSTATE,
                      FINALLY_MSTATE, LOOP_MSTATE, USING_MSTATE, 
                      // the IDs of macro declarations:
                      VAR_MDECL, DATA_MDECL, VARIABLE_MDECL, IMPORT_MDECL,
                      CMPND_MDECL, NMSPC_FWD_MDECL, NMSPC_MDECL, FUNC_FWD_MDECL,
                      FUNC_MDECL, OPER_FWD_MDECL, OPER_MDECL, CTOR_FWD_MDECL,
                      CTOR_MDECL, DTOR_FWD_MDECL, DTOR_MDECL, OBJECT_FWD_MDECL,
                      OBJECT_MDECL, FORWARD_MDECL, FRIEND_MDECL, MEMBER_MDECL,
                      
                      //the highest ID ( not used ):
                      MAX_ID };
       
   }
  }
 }
}



#endif
