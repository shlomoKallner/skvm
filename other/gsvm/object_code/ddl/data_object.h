#ifndef SK_GSVM_OBJECT_CODE_DATA_OBJECT_H
#define SK_GSVM_OBJECT_CODE_DATA_OBJECT_H

// framework includes here:
#include "declaration_object.h"

// your includes here

// namespace begin
namespace SK
{
 namespace gsvm
 {
  namespace object_code
  {
   
   template< typename InputT, typename T = void_t >
   struct data_object : public declaration_object< InputT,T >
   {
    
   };
   
  }
 }
}


#endif
