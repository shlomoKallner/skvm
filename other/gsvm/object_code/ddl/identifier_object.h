#ifndef SK_GSVM_OBJECT_CODE_XXX_OBJECT_H
#define SK_GSVM_OBJECT_CODE_XXX_OBJECT_H

// framework includes here:
#include "declaration_object.h"

// your includes here

// namespace begin
namespace SK
{
 namespace gsvm
 {
  namespace object_code
  {
   
   template< typename InputT, typename T = void_t >
   struct identifier_object : public declaration_object< InputT,T >
   {
    // BASIC_IDENT
    typedef typename declaration_object< InputT,T >    base_t;
    typedef typename identifier_object< InputT,T >     self_t;
    typedef typename boost::shared_ptr<self_t>         self_ptr;
    
    identifier_object( string_t const& s ) : name(s) {} //end of ...
    virtual ~identifier_object() {} //end of ...
    
    string_t get_name( void )
    {
     return this->name;
    } // end of ...
    
    void set_name( string_t const& s )
    {
     this->name = s; 
    } // end of ...
    
    protected:
              string_t name;
   };
   
   template< typename InputT, typename T = void_t >
   struct _identifier : public identifier_object< InputT,T >
   {
    //  
    typedef typename identifier_object< InputT,T >    base_t;
    typedef typename _identifier< InputT,T >          self_t;
    typedef typename boost::shared_ptr<self_t>        self_ptr;
    
    _identifier( string_t const& s ) : base_t(s) {} //end of ...
    virtual ~_identifier() {} //end of ...
    
    protected:
              
   };
   
  }
 }
}


#endif
