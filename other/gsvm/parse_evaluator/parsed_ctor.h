#ifndef SK_SGVM_EVAL_PARSED_CTOR_H
#define SK_SGVM_EVAL_PARSED_CTOR_H

namespace SK
{
 namespace gsvm
 {
  namespace eval
  { 
   template< typename CharT, typename FactoryT >
    class parsed_ctor : evaluator_base< parsed_code< CharT, FactoryT >,
                                        CharT, FactoryT > 
    {
     public:
         typedef typename parsed_ctor< CharT, FactoryT >  self_t;
         typedef typename boost::shared_ptr< self_t >     self_ptr;
         
    };
  } 
 } 
}

#endif
