#ifndef SK_SGVM_EVAL_PARSED_DATA_H
#define SK_SGVM_EVAL_PARSED_DATA_H

namespace SK
{
 namespace gsvm
 {
  namespace eval
  {
   template< typename CharT, typename FactoryT >
    class parsed_data : evaluator_base< parsed_code< CharT, FactoryT >,
                                        CharT, FactoryT > 
    { 
      public:
         typedef typename parsed_data< CharT, FactoryT >       self_t;
         typedef typename boost::shared_ptr< self_t >          self_ptr;
         //
         typedef typename parsed_types< CharT >::types_t       types_t;
         typedef typename types_t::types_ptr                   types_ptr;
         typedef typename parsed_value< CharT >::value_t       value_t;
         typedef typename value_t::value_ptr                   value_ptr;
         typedef typename parsed_code< CharT >::code_t         code_t;
         typedef typename code_t::code_ptr                     code_ptr;
         typedef typename code_t::insr_iter_t                  insr_iter_t;
         
         enum data_types_t { NULL_T = 0, VAR_T, CONST_T, MAX_T }; 
         
      protected:
                types_ptr     type_m;
                data_types_t  type_of_data_m;
                string_t      name_m;
                value_ptr     init_val_m; // may be null if is initialized by
                // explicit instructions. 
                code_ptr      block_where_init_m;// will contain ptr to block 
                // in which this is init-ed, if init_val_m is null.
                insr_iter_t   init_beg_m, init_end_m;// will contain iters to 
                // the begining of the init-ing and to the first past the end 
                // of init-ing, if init_val_m is null.
    }; 
  } 
 }
}

#endif
