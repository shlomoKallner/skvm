#ifndef SK_SGVM_EVAL_PARSED_DTOR_H
#define SK_SGVM_EVAL_PARSED_DTOR_H

namespace SK
{
 namespace gsvm
 {
  namespace eval
  { 
   template< typename CharT >
    class parsed_dtor
    {
     public:
         typedef typename parsed_dtor< CharT >         dtor_t;
         typedef typename boost::shared_ptr< dtor_t >  dtor_ptr;
         typedef typename std::basic_string< CharT >   string_t;
    };
  }
 }
}

#endif
