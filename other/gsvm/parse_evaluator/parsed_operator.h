#ifndef SK_SGVM_EVAL_PARSED_OPERATOR_H
#define SK_SGVM_EVAL_PARSED_OPERATOR_H

namespace SK
{
 namespace gsvm
 {
  namespace eval
  {
    template< typename CharT >
    class parsed_operator
    { 
     public:
      typedef typename parsed_operator< CharT >                operator_t;
      typedef typename boost::shared_ptr< operator_t >         operator_ptr;
      typedef typename std::basic_string< CharT >              string_t;
      //
      typedef typename parsed_modifier< CharT >::modifier_t    modifier_t;
      typedef typename modifier_t::modifier_ptr                modifier_ptr;
      typedef typename parsed_code< CharT >::code_t            code_t;
      typedef typename code_t::code_ptr                        code_ptr;
      typedef typename parsed_signature< CharT >::signature_t  signature_t;
      typedef typename signature_t::signature_ptr              signature_ptr;
     protected:
               std::list< modifier_ptr >                     modifier_list_m;
               code_ptr                                      code_block_m;
               signature_ptr                                 oper_sig_m;
    };
  }
 }
}
#endif
