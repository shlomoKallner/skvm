#ifndef SK_GSVM_EVAL_PARSED_VALUE_H
#define SK_GSVM_EVAL_PARSED_VALUE_H

namespace SK
{
 namespace gsvm
 {
  namespace eval
  {
            
   template< typename CharT  >
    class parsed_value
    {
     public:
      typedef typename parsed_value< CharT >            value_t;
      typedef typename boost::shared_ptr< value_t >     value_ptr;
      typedef typename std::basic_string< CharT >       string_t;      
      enum value_type { null_type = 0, bool_type, uint_type, sint_type,
                        float_type, ptr_type, max_type };
      //
      void evaluate( tree_iter node )
      {
           if( node.id() == parser_id( CHAR_LIT ) )
           {  }
           else if( node.id() == parser_id( WCHAR_LIT ) )
           {}
           else if( node.id() == parser_id( NULL_ID ) )
           {
               assert(0);
           }
      }
     protected:
               union value_impl
               {
                using namespace SK::types;
                boolean b_val;
                sint64  s64_val;
                uint64  u64_val;
                anyptr  ap_val;
                float64 f64_val;
               };
               value_impl val_m;
               value_type typeof_m;
               std::list< value_ptr > cont_vals_m; 
    };
  }
 }
}

#endif
