#ifndef GSVM_DDL_TYPES_H
#define GSVM_DDL_TYPES_H

#include "SK_types.h"
#include <boost/spirit/include/classic_spirit.hpp>
#include "gsvm_tree_context.h" // includes gsvm_node_val.h for us.
#include "gsvm_numeral_prefix.h"
#include "gsvm_real_parser.h"

namespace SK
{
 namespace gsvm
 {
  namespace ddl
  {
    using namespace boost::spirit::classic;
    using namespace SK::types;
            
    typedef  uint_parser<uint64,10,1,-1> uint10_p; //parses base 10 integers.
    typedef  int_parser<sint64,10,1,-1>  sint10_p;
    
    typedef  uint_parser<uint64,8,1,-1> uint8_p;   //parses base 8 integers.
    typedef  int_parser<sint64,8,1,-1>  sint8_p;
    
    typedef  uint_parser<uint64,16,1,-1> uint16_p; //parses base 16 integers.
    typedef  int_parser<sint64,16,1,-1>  sint16_p;
    
    typedef real_parser< float64, GSVM_real_parser_policies<float64,10> > 
            float10_p;  //parses base 10 floating-point numbers.
    typedef real_parser< float64, GSVM_real_parser_policies<float64,8> > 
            float8_p;   //parses base 8 floating-point numbers.
    typedef real_parser< float64, GSVM_real_parser_policies<float64,16> > 
            float16_p;  //parses base 16 floating-point numbers.
    
    // a pair of predefined factories for our use ( no longer needed ): 
    typedef node_val_data_factory<GSVM_node_val>  node_val_factory;
    typedef node_iter_data_factory<GSVM_node_val> node_iter_factory;
    
    
  }
 }
}  
#endif
