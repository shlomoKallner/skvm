#ifndef SK_GSVM_DDL_REAL_PARSER_H
#define SK_GSVM_DDL_REAL_PARSER_H

#include "SK_types.h"
#include <boost/spirit/include/classic_spirit.hpp>

namespace SK
{
 namespace gsvm
 {
  namespace ddl
  {
    using namespace boost::spirit::classic;
    using namespace SK::types;
    
    // a custom policie for parsing floating-point numbers based on 
    // a floating-point type and a radix base. 
    template< typename T = float64, const std::size_t N = 10 >
    struct GSVM_real_parser_policies : public strict_real_parser_policies<T>
    {
      typedef uint_parser<T,N,1,-1> uint_parser_t;
      typedef int_parser<T,N,1,-1>  int_parser_t;
      typedef numeral_prefix<N>     numeral_prefix_t;
           
      template <typename ScannerT>
      static typename parser_result<uint_parser_t, ScannerT>::type
      parse_n(ScannerT& scan)
      { numeral_prefix_t().parse(scan);
       return uint_parser_t().parse(scan); }
           
      template <typename ScannerT>
      static typename parser_result<uint_parser_t, ScannerT>::type
      parse_frac_n(ScannerT& scan)
      { numeral_prefix_t().parse(scan); 
       return uint_parser_t().parse(scan); }

      template <typename ScannerT>
      static typename parser_result<int_parser_t, ScannerT>::type
      parse_exp_n(ScannerT& scan)
      { numeral_prefix_t().parse(scan); 
       return int_parser_t().parse(scan); }
       
    };//end of GSVM_real_parser_policies;
    
  }
 }
}

#endif
