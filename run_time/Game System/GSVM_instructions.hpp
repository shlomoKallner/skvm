#ifndef SK_RUNTIME_GAMESYSTEM_VIRTUALMACHINE_INSTRUCTION_SET_HPP
#define SK_RUNTIME_GAMESYSTEM_VIRTUALMACHINE_INSTRUCTION_SET_HPP

#include "SK/utility/types.hpp"

namespace SK
{
 namespace runtime
 {
  namespace gameSystem
  {
   namespace reg_ids
   {
    namespace types = SK::Utilities::types;
    const types::uint32 nil_ = 0;
    const types::uint32 reg1 = 1;// for all types...
     
   }// end of reg_ids...
   namespace insr_consts
   {
    namespace types = SK::Utilities::types;
    /* this is the instruction set list:
       nil_  = invalid instrution
       nop_  = no - op
       // data movement: op = scr, dest
       load  = scr( reg, imm, reg[ptr] ), dest( reg )
       store = scr( reg ), dest( reg, reg[ptr] )
       
       push  = scr( reg[ptr], reg, imm )[, n( imm, reg )]
       pop   = dest( reg[ptr], reg )[, n(reg)]
       
       in    = scr( reg[port], imm[port] ), dest( reg, reg[ptr] )[, n( reg, reg[ptr], imm ) ]
       out   = scr( reg, reg[ptr] ), dest( reg[port], imm[port] )[, n( reg, reg[ptr], imm ) ]
       
       // alb ops: implicit dest is accumulator of type.
       // unary op: op = scr( reg, imm )[, dest( reg )]
       // unary op list: plus, neg, inc, dec, not
       // binary op: op = scr1( reg, imm ), scr2( reg, imm )[, dest( reg)]
       // binary op list: add, addc, sub, subc, div, mod, and, ior, xor,
       //                 shl, sar( signed shift ), shr( unsigned shift ),
       //                 shld, shrd( unsigned shift ), rol, ror, rcl, rcr,
       //                 bt, bts, btr, btc, test
                 
       // control flow:
       // non conditional: op = address( reg, imm )
       // non conditional op list: jmp, call, ret, int, iret
       // conditional: op = scr( reg, imm ), address( reg, imm )
       // conditional op list: jt, jnt, jfs, jnfs
       
     
    */
    const types::uint32 nil_ = 0;
    const types::uint32 nop_ = 1;
    
    //enum instr_e{ nil_ = 0, nop_ ,
    //              load_reg, load_address, max };
   }// end of namespace insr_consts;
   namespace object_code_ops
   {
    namespace types = SK::Utilities::types;
    const types::uint32 nil_ = 0;
    const types::uint32 nop_ = 1;
    /* this is the generic object code op_codes that are compiled
       into the instruction set ops above by the loader.
       
       load // a; *a; a[]; &a;
       store // a = ; *a = ; a[] = ; 
       binary ops // a op b // op: + - * / % && || ^^ & | ^  
                  // << >> >>> < > == != <= >= ,
       unary ops // op a; // op: + - ++ -- ! ~ & * 
                 // a op; // op: ++ --
    
    */
             
    enum opcodes_e{ nil_ = 0, max};         
   }// end of namespace object_code_ops; 
  }// end of namespace gameSystem;
 }// end of namespace runtime;
}//end of namespace SK;

#endif
            
