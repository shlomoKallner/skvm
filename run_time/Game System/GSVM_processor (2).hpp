#ifndef SK_RUNTIME_GAMESYSTEM_VIRTUALMACHINE_PROCESSOR_HPP
#define SK_RUNTIME_GAMESYSTEM_VIRTUALMACHINE_PROCESSOR_HPP

#include <vector>
#include "SK/utility/libGC.hpp"

namespace SK
{
 namespace runtime
 {
  namespace gameSystem
  {
    class processor
    {
     typedef void gcCoord_type;
     
     typedef void ireg_type;
     const SK::utilities::types::uint32 num_iregs = 16;
     typedef std::vector< ireg_type,
                  gcCoord_type::allocator<ireg_type> 
                        > ireg_vector;
     
     typedef void freg_type;
     const SK::utilities::types::uint32 num_fregs = 16;
     typedef std::vector< freg_type,
                  gcCoord_type::allocator<freg_type> 
                        > freg_vector;
     
     typedef void preg_type;
     const SK::utilities::types::uint32 num_pregs = 16;
     typedef std::vector< preg_type,
                  gcCoord_type::allocator<preg_type> 
                        > preg_vector;
                        
     typedef void  ExecStatData_type;
     protected:
               // registers:
               ireg_type   iAccumulator_m;
               ireg_vector iRegs_m;
               
               freg_type   fAccumulator_m;
               freg_vector fRegs_m;
               
               preg_type   pAccumulator_m;
               preg_vector pRegs_m;
               
               preg_type begin_stack_m, current_stack_m, end_stack_m;
               preg_type begin_args_m, current_args_m, end_args_m;
               
               SK::utilities::types::uint64 flags_m;
               ExecStatData_type ExecStatData_m;
               SK::utilities::types::uint16 execMode_m, 
                                            basicDataTypeMode_m;
               // end of registers. from here type specific data.
               
    };
  }// end of namespace gameSystem
 }// end of namespace runtime  
}//end of namespace SK

#endif
