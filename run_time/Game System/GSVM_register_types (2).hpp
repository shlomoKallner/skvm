#ifndef SK_RUNTIME_GAMESYSTEM_VIRTUALMACHINE_REGISTERS_HPP
#define SK_RUNTIME_GAMESYSTEM_VIRTUALMACHINE_REGISTERS_HPP

#include <boost/shared_ptr.hpp>
#include "SK/utility/types.hpp"

#include "integer_register_types.hpp"
#include "float_register_types.hpp"

namespace SK
{
 namespace runtime
 {
  namespace gameSystem
  {
   namespace SK_Util = SK::Utilities::types;
   

   
   struct pregV1a
   {
    union
    {
     SK_Util::uint64 p64;
     SK_Util::uint32 p32[2];
    };
   };
   
   struct pidV1a
   {
    SK_Util::uint32 proc_id_m, thrd_id_m, access_code_m;
   };
   
   typedef boost::shared_ptr<pidV1a> pid_ptrV1a;
   
   struct pidV2a
   {
    SK_Util::uint64 proc_id_m, thrd_id_m, access_code_m;
   };
   
   typedef boost::shared_ptr<pidV2a> pid_ptrV2a;
   
   class pidV1b
   {
    SK_Util::uint32 proc_id_m, thrd_id_m, access_code_m;
    
    public:
           pidV1b( SK_Util::uint32 p, SK_Util::uint32 t, SK_Util::uint32 a )
           : proc_id_m(p), thrd_id_m(t), access_code_m(a)
           {}// end of
           pidV1b( const pidV1b& pid )
           : proc_id_m(pid.proc_id_m), thrd_id_m(pid.thrd_id_m),
             access_code_m(pid.access_code_m)
           {}//end of ...
           virtual ~pidV1b(){};
           
           SK_Util::uint32 procID()
           {
            return proc_id_m;
           }// end of ...
           SK_Util::uint32 thrdID()
           {
            return thrd_id_m;
           }// end of ...
           SK_Util::uint32 access()
           {
            return access_code_m;
           }// end of ...
           
           SK_Util::boolean operator==( const pidV1b& pid )const
           {
            return (proc_id_m == pid.proc_id_m)
                 &&(thrd_id_m == pid.thrd_id_m)
                 &&(access_code_m == pid.access_code_m);
           }// end of ...                    
           
           SK_Util::boolean operator!=( const pidV1b& pid )const
           {
            return !(*this == pid );
           }//end of ...
   };
   
   typedef boost::shared_ptr<pidV1b> pid_ptrV1b;
   
   class pidV2b
   {
    SK_Util::uint64 proc_id_m, thrd_id_m, access_code_m;
    
    public:
           pidV1b( SK_Util::uint64 p, SK_Util::uint64 t, SK_Util::uint64 a )
           : proc_id_m(p), thrd_id_m(t), access_code_m(a)
           {}// end of
           pidV1b( const pidV2b& pid )
           : proc_id_m(pid.proc_id_m), thrd_id_m(pid.thrd_id_m),
             access_code_m(pid.access_code_m)
           {}//end of ...
           virtual ~pidV2b(){};
           
           SK_Util::uint64 procID()
           {
            return proc_id_m;
           }// end of ...
           SK_Util::uint64 thrdID()
           {
            return thrd_id_m;
           }// end of ...
           SK_Util::uint64 access()
           {
            return access_code_m;
           }// end of ...
           
           SK_Util::boolean operator==( const pidV2b& pid )const
           {
            return (proc_id_m == pid.proc_id_m)
                 &&(thrd_id_m == pid.thrd_id_m)
                 &&(access_code_m == pid.access_code_m);
           }// end of ...                    
           
           SK_Util::boolean operator!=( const pidV2b& pid )const
           {
            return !(*this == pid );
           }//end of ...
   };
   
   typedef boost::shared_ptr<pidV2b> pid_ptrV2b;
   
  }// end of namespace gameSystem
 }// end of namespace runtime  
}//end of namespace SK



#endif
