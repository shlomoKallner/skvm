#ifndef SK_RUNTIME_GAMESYSTEM_VIRTUALMACHINE_FLOAT_REGISTERS_HPP
#define SK_RUNTIME_GAMESYSTEM_VIRTUALMACHINE_FLOAT_REGISTERS_HPP

#include <exception>
#include <sstream>
#include <iosfwd>
#include <boost/shared_ptr.hpp>
#include "SK/utility/types.hpp"

namespace SK
{
 namespace runtime
 {
  namespace gameSystem
  {
   namespace SK_Util = SK::Utilities::types;


   
   class float_reg_exception : public std::exception
   {
     const char * _m_Str;
    public:
     float_reg_exception( const char * str ) throw()
     : std::exception(),  _m_Str( str ) {}//end of ..
    
     virtual ~float_reg_exception() throw()
     {};

    /** Returns a C-style character string describing the general cause
     *  of the current error.  */
     virtual const char* what() const throw()
     {
      return _m_Str;   
     }
   };
   
   class fregV2a
   {
    union internal_type
    {
     SK_Util::float64 f64;
     SK_Util::float32 f32[2];
    };
    internal_type data_m;
    public:
     fregV2a(): data_m() {}// end of ...
     fregV2a( const fregV2a& f ) : data_m( f.data_m ) {}// end of ...
     virtual ~fregV2a(){}// end of ...
     // accessor functions:
     SK_Util::float64 regF64( void )
     { return data_m.f64; }// end of ...
     void regF64( SK_Util::float64 val )
     { data_m.f64 = val; }// end of ...
     
     SK_Util::float32 regF32( SK_Util::uint8 indx )
     {
      if( indx > -1 && indx < 2)
       return data_m.f32[indx];
      else
      {
       throw float_reg_exception("ERROR in \"float32 regF32( uint8 indx );\" \n"
                                 "register index is Out of range!");
      }
     }// end of ...
     void regF32( SK_Util::uint8 indx, SK_Util::float32 val )
     {
      if( indx > -1 && indx < 2)
       data_m.f32[indx] = val;
      else
      {
       throw float_reg_exception("ERROR in \"float32 regF32( uint8 indx );\" \n"
                                 "register index is Out of range!");
      }
     }// end of ...
     
   };
   
   
   
  }// end of namespace gameSystem
 }// end of namespace runtime  
}//end of namespace SK




#endif
