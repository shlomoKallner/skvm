#ifndef SK_RUNTIME_GAMESYSTEM_VIRTUALMACHINE_INTEGER_REGISTERS_HPP
#define SK_RUNTIME_GAMESYSTEM_VIRTUALMACHINE_INTEGER_REGISTERS_HPP

#include <exception>
#include <sstream>
#include <iosfwd>
#include <boost/shared_ptr.hpp>
#include "SK/utility/types.hpp"


namespace SK
{
 namespace runtime
 {
  namespace gameSystem
  {
   namespace SK_Util = SK::Utilities::types;

#undef SK_GSVM_REG_TYPES_USE_POINTER_TO_STRING
   
   class int_reg_exception : public std::exception
   {
     const char * _mStr;
    public:
     int_reg_exception( const char* str ) throw()
     : std::exception(),  _m_Str( str ) {}

     virtual ~int_reg_exception() throw()
     {}

    /** Returns a C-style character string describing the general cause
     *  of the current error.  */
     virtual const char* what() const throw()
     {
      return _m_Str;
     }
   };
   
   
   
   class iregV2a
   {
     union internal_type
     {
      SK_Util::uint64 u64;
      SK_Util::sint64 s64;
      SK_Util::uint32 u32[2];
      SK_Util::sint32 s32[2];
      SK_Util::uint16 u16[4];
      SK_Util::sint16 s16[4];
      SK_Util::uint8  u8[8];
      SK_Util::sint8  s8[8];
     };
     internal_type internal_m;
    public:
     iregV2a(): internal_m(){} // end of ...
     iregV2a( const iregV2a& i ): internal_m(i.internal_m){} // end of ...
     virtual ~iregV2a(){} // end of ...
     // access methods:
     SK_Util::uint64 regU64( void )
     { return internal_m.u64; }// end of ...
     void regU64( SK_Util::uint64 u )
     { internal_m.u64 = u; }// end of ...
     SK_Util::sint64 regS64( void )
     { return internal_m.s64; }// end of ...
     void regS64( SK_Util::sint64 s )
     { internal_m.s64 = s; }// end of ...
     
     SK_Util::uint32 regU32( SK_Util::uint32 num_reg )
     {
      if( num_reg > -1 && num_reg < 2 ) 
       return internal_m.u32[num_reg]; 
      else
      {
       throw int_reg_exception("Error In \"uint32 iregV2a::regU32( uint32 num_reg );\" \n"
                               "Register index is out of range!\n");
      } 
     } // end of ...
     void regU32( SK_Util::uint32 num_reg, SK_Util::uint32 u )
     {
      if( num_reg > -1 && num_reg < 2 ) 
       internal_m.u32[num_reg] = u; 
      else
      {
       throw int_reg_exception("Error In \"void iregV2a::regU32( uint32 num_reg, uint32 val );\" \n"
                               "Register index is out of range!\n");
      }
     } // end of ...
     SK_Util::sint32 regS32( SK_Util::uint32 num_reg )
     {
      if( num_reg > -1 && num_reg < 2 ) 
       return internal_m.s32[num_reg]; //s32
      else
      {
       throw int_reg_exception("Error In \"sint32 iregV2a::regS32( uint32 num_reg );\" \n"
                               "Register index is out of range!\n");
      }
     } // end of ...
     void regS32( SK_Util::uint32 num_reg, SK_Util::sint32 s )
     {
      if( num_reg > -1 && num_reg < 2 ) 
       internal_m.s32[num_reg] = s; //s32
      else
      {
       throw int_reg_exception("Error In \"void iregV2a::regS32( uint32 num_reg, uint32 val );\" \n"
                               "Register index is out of range!\n");
      }
     } // end of ...
     
     SK_Util::uint16 regU16( SK_Util::uint32 num_reg )
     {
      if( num_reg > -1 && num_reg < 4 ) 
       return internal_m.u16[num_reg]; //u16[4];
      else
      {
       throw int_reg_exception("Error In \"uint16 iregV2a::regU16( uint32 num_reg );\" \n"
       "Register index is out of range!\n");
       
      }
     } // end of ...
     void regU16( SK_Util::uint32 num_reg, SK_Util::uint16 u )
     {
      if( num_reg > -1 && num_reg < 4 ) 
       internal_m.u16[num_reg] = u; //u16[4];
      else
      {
       throw int_reg_exception("Error In \"void iregV2a::regU16( uint32 num_reg, uint16 val );\" \n"
       "Register index is out of range!\n");
      }
     } // end of ...
     SK_Util::sint16 regS16( SK_Util::uint32 num_reg )
     { 
      if( num_reg > -1 && num_reg < 4 ) 
       return internal_m.s16[num_reg]; // s16[4];
      else
      {
       throw int_reg_exception("Error In \"sint16 iregV2a::regS16( uint32 num_reg );\" \n"
       "Register index is out of range!\n");
      }
     } // end of ...
     void regS16( SK_Util::uint32 num_reg, SK_Util::sint16 s )
     {
      if( num_reg > -1 && num_reg < 4 ) 
       internal_m.s16[num_reg] = s; // s16[4];
      else
      {
       throw int_reg_exception("Error In \"void iregV2a::regS16( uint32 num_reg, sint16 val );\" \n" 
                               "Register index is out of range!\n");
      }
     } // end of ...
     SK_Util::uint8 regU8( SK_Util::uint32 num_reg )
     {
      if( num_reg > -1 && num_reg < 8 ) // u8[8];
       return internal_m.u8[num_reg];
      else
      {
       throw int_reg_exception("Error In \"uint8 iregV2a::regU8( uint32 num_reg );\" \n"
       "Register index is out of range!\n");
      } 
     } // end of ...
     void regU8( SK_Util::uint32 num_reg, SK_Util::uint8 u )
     {
      if( num_reg > -1 && num_reg < 8 ) // u8[8];
       internal_m.u8[num_reg] = u;
      else
      {
       throw int_reg_exception("Error In \"void iregV2a::regU8( uint32 num_reg, uint8 val );\" \n"
       "Register index is out of range!\n");
      }
     } // end of ...
     SK_Util::sint8 regS8( SK_Util::uint32 num_reg )
     {
      if( num_reg > -1 && num_reg < 8 ) // s8[8];
       return internal_m.s8[num_reg];
      else
      {
       throw int_reg_exception("Error In \"sint8 iregV2a::regS8( uint32 num_reg );\" \n"
       "Register index is out of range!\n");
      }
     } // end of ...
     void regS8( SK_Util::uint32 num_reg, SK_Util::sint8 s )
     {
      if( num_reg > -1 && num_reg < 8 ) // s8[8];
       internal_m.s8[num_reg] = s;
      else
      {
       throw int_reg_exception("Error In \"void iregV2a::regS8( uint32 num_reg, sint8 val );\" \n"
       "Register index is out of range!\n");
      }
     } // end of ...
   };
    
  }// end of namespace gameSystem
 }// end of namespace runtime  
}//end of namespace SK


#endif
