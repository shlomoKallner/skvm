#ifndef SK_RUNTIME_GAMESYSTEM_VIRTUALMACHINE_OBSOLETE_FLOAT_REGISTERS_HPP
#define SK_RUNTIME_GAMESYSTEM_VIRTUALMACHINE_OBSOLETE_FLOAT_REGISTERS_HPP

#include "SK/utility/types.hpp"

namespace SK
{
 namespace runtime
 {
  namespace gameSystem
  {
   namespace SK_Util = SK::Utilities::types;
   
   struct fregV1a
   {
    union
    {
     SK_Util::float64 f64;
     SK_Util::float32 f32[2];
    };
   };
   
   struct single_prec_regs
   {
    SK_Util::float32 f1, f2;//f32[2];
   };
   
   struct fregV1b
   {
    union
    {
     SK_Util::float64 f64;
     single_prec_regs f32;
    };
   };
   
   class fregV2b
   {
    union internal_type
    {
     SK_Util::float64 f64;
     single_prec_regs f32;
    };
    internal_type data_m;
    public:
   };
