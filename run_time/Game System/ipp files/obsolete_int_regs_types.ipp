#ifndef SK_RUNTIME_GAMESYSTEM_VIRTUALMACHINE_OBSOLETE_INTEGER_REGISTERS_HPP
#define SK_RUNTIME_GAMESYSTEM_VIRTUALMACHINE_OBSOLETE_INTEGER_REGISTERS_HPP

#include "SK/utility/types.hpp"

namespace SK
{
 namespace runtime
 {
  namespace gameSystem
  {
   namespace SK_Util = SK::Utilities::types;
   
   struct iregV1a
   {
    union
    {
     SK_Util::uint64 u64;
     SK_Util::sint64 s64;
     SK_Util::uint32 u32[2];
     SK_Util::sint32 s32[2];
     SK_Util::uint16 u16[4];
     SK_Util::sint16 s16[4];
     SK_Util::uint8  u8[8];
     SK_Util::sint8  s8[8];
    };
   };
   
   struct octet_regs
   {
    SK_Util::sint8  s1, s2, s3, s4, s5, s6, s7, s8;// s8[8];
   };
   struct byte_regs
   {
    SK_Util::uint8  u1, u2, u3, u4, u5, u6, u7, u8; //u8[8];
   };
   
   struct short_regs
   {
    SK_Util::sint16  s1, s2, s3, s4;//s16[4];
   };
   struct halfword_regs
   {
    SK_Util::uint16  u1, u2, u3, u4;//u16[4];
   };
   
   struct int_regs
   {
    SK_Util::sint32 s1, s2;//s32[2];
   };
   struct word_regs
   {
    SK_Util::uint32 u1, u2;//u32[2];
   };
   
   struct iregV1b
   {
    union
    {
     SK_Util::uint64 u64;
     SK_Util::sint64 s64;
     word_regs       u32;
     int_regs        s32;
     halfword_regs   u16;
     short_regs      s16;
     byte_regs       u8;
     octet_regs      s8;
    };
   };

   class iregV2b
   {
     union internal_type
     {
      SK_Util::uint64 u64;
      SK_Util::sint64 s64;
      word_regs       u32;
      int_regs        s32;
      halfword_regs   u16;
      short_regs      s16;
      byte_regs       u8;
      octet_regs      s8;
     };
     internal_type internal_m;
    public:
           iregV2b(){} // end of ...
           iregV2b( const iregV2b& i ){} // end of ...
           virtual ~iregV2b(){} // end of ...
           // access methods:
           SK_Util::uint64 regU64( void );
           void regU64( SK_Util::uint64 u );
           SK_Util::sint64 regS64( void );
           void regS64( SK_Util::sint64 s );
           SK_Util::uint32 regU32( SK_Util::uint32 num_reg );
           void regU32( SK_Util::uint32 u, SK_Util::uint32 num_reg );
           SK_Util::sint32 regS32( SK_Util::uint32 num_reg );
           void regS32( SK_Util::sint32 s, SK_Util::uint32 num_reg );
           SK_Util::uint16 regU16( SK_Util::uint32 num_reg );
           void regU16( SK_Util::uint16 u, SK_Util::uint32 num_reg );
           SK_Util::sint16 regS16( SK_Util::uint32 num_reg );
           void regS16( SK_Util::sint16 s, SK_Util::uint32 num_reg );
           SK_Util::uint8 regU8( SK_Util::uint32 num_reg );
           void regU8( SK_Util::uint8 u, SK_Util::uint32 num_reg );
           SK_Util::sint8 regS8( SK_Util::uint32 num_reg );
           void regS8( SK_Util::sint8 s, SK_Util::uint32 num_reg );
   };   
   
   
  }// end of namespace gameSystem
 }// end of namespace runtime  
}//end of namespace SK


#endif
