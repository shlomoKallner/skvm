

#ifndef SK_PYTHON_H
#define SK_PYTHON_H

namespace SK
{
          
  namespace python
  {
    
    namespace ver_2_7_1;
    namespace ver_3_1_3;
    
    namespace ver_2x = SK::python::ver_2_7_1;
    namespace ver_3x = SK::python::ver_3_1_3;
   // namespace ver_current = SK::python::ver_3x;
   // namespace ver_legacy = SK::python::ver_2x;
             
  }
          
}

#endif
