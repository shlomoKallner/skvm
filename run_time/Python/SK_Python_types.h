

#ifndef SK_PYTHON_TYPES_H
#define SK_PYTHON_TYPES_H

#include <boost/shared_ptr.hpp>
#include <string>


namespace SK
{
          
  namespace python
  {
   
   namespace _impl
   {
             
     class type_base
     {
     }; // end of class type_base
   
     class type 
     {
      public:     
       typedef boost::shared_ptr< type >  type_ptr;
       virtual bool is_builtin( void )
       {
        return false;
       } // end of function is_builtin;
       std::string get_name()
       {
        return this->name_m;
       } // end of function get_name;
      private:
       std::string name_m;
     }; // end of class type
   
     static void type_deleter( type * t )
     {
      if ( !t->is_builtin() )
      {
          delete t;
      }
     } // end of function type_deleter
     
     typedef type::type_ptr  type_ptr;
     
     type_ptr make_type( void )
     {
      return type_ptr( new type , type_deleter );
     } // end of function make_type;
     
   } // end of namespace _impl
   
   typedef _impl::type     type;
   typedef _impl::type_ptr type_ptr;
   
   type_ptr make_type( void )
   {
    return _impl::make_type();
   } // end of function make_type;
   
  } // end of namespace python
          
} // end of namespace SK

#endif
