#ifndef SK_JIT_TYPES_HPP
#define SK_JIT_TYPES_HPP

#include <boost\shared_ptr.h>
#include <vector>
#include <list>
#include <deque>
#include <map>
#include <string>
#include "SK\common\SK_types.h"
namespace SK
{
 namespace jit
 {
  namespace types
  {
   using namespace SK::types;         
   enum tag_type{ null_tag = 0, builtin_tag, pointer_tag, struct_tag, union_tag, signature_tag};
   
   template< typename CharT = char, const uint32 tag = tag_type::null_tag >
   class basic_type
   {
    public:
           typedef typename CharT char_t;
           typedef typename basic_type<char_t,tag> self_t;
           typedef typename boost::shared_ptr<self_t> self_ptr;
           typedef typename std::basic_string<char_t> string_t

    protected:
             uint32 tag_m;
             string_t name_m;  
   };
   
   template< typename CharT = char >
   class builtin_type : public basic_type< CharT, tag_type::builtin_tag>
   {};
   
   template< typename CharT = char >
   class pointer_type : public basic_type< CharT, tag_type::pointer_tag>
   {};

/*

#ifdef SK_JIT_USE_VECTOR_AS_COMPONENT_CONTAINER           
           typedef typename std::vector<self_ptr> component_container_t;
#elif  SK_JIT_USE_LIST_AS_COMPONENT_CONTAINER
           typedef typename std::list<self_ptr> component_container_t;
#elif  SK_JIT_USE_DEQUE_AS_COMPONENT_CONTAINER
           typedef typename std::deque<self_ptr> component_container_t;
#endif
           typedef typename component_container_t::const_iterator component_const_iterator;
           typedef typename component_container_t::iterator component_iterator;
           typedef typename std::map<string_t,component_iterator> component_name_map_t;


*/
   
   template< typename CharT = char, const uint32 tag >
   class aggragate_type : public basic_type< CharT, tag>
   {
    public:
           typedef typename aggragate_type< CharT, tag> self_t;
           typedef typename boost::shared_ptr<self_t> self_ptr;
           typedef typename basic_type< CharT, tag> base_t;
#ifdef SK_JIT_USE_VECTOR_AS_COMPONENT_CONTAINER           
           typedef typename std::vector<self_ptr> component_container_t;
#elif  SK_JIT_USE_LIST_AS_COMPONENT_CONTAINER
           typedef typename std::list<self_ptr> component_container_t;
#elif  SK_JIT_USE_DEQUE_AS_COMPONENT_CONTAINER
           typedef typename std::deque<self_ptr> component_container_t;
#endif
           typedef typename component_container_t::const_iterator component_const_iterator;
           typedef typename component_container_t::iterator component_iterator;
           typedef typename std::map<string_t,component_iterator> component_name_map_t;


    protected:
           component_container_t component_container_m;
           component_name_map_t  component_name_map_m;   
    private:     
   };
   
   template< typename CharT = char >
   class struct_type : public aggragate_type< CharT, tag_type::struct_tag>
   {};
   
   template< typename CharT = char >
   class union_type : public aggragate_type< CharT, tag_type::union_tag>
   {};
   
   template< typename CharT = char >
   class signature_type : public aggragate_type< CharT, tag_type::signature_tag>
   {
   };
   
  }//end of namespace types
 }//end of namespace jit
}//end of namespace SK

#endif
