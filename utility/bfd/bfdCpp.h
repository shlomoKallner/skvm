#ifndef SK_UTILITIES_BFDCPP_H
#define SK_UTILITIES_BFDCPP_H

#include <fstream>
#include "SK/common/SK_types.h"
#include "endianess.h"

namespace SK
{
 namespace utilities
 {
  
  
  template< typename EndianT >
  struct bfd : public EndianT
  {
    // some important public typedefs:
      typedef EndianT         base_t;
      typedef base_t::uword_t uvma_t;
      typedef base_t::sword_t svma_t;
      typedef base_t::uword_t size_type;
      typedef base_t::uword_t symvalue_t;
      typedef types::uint32          flagword;	/* 32 bits of flags */
      typedef types::uint8           byte;
      typedef uvma_t                   ufile_ptr; // generic file offset type.
      typedef svma_t                   file_ptr;   // generic file offset type.
      enum format {unknown = 0, object, archive, core };
   //  the constructor:
      bfd(){}//end of ...
   //  the copy constructor: 
      bfd( bfd const& ){}//end of ...
   //  the destructor:
      virtual ~bfd(){}//end of ...
   //  the member functions:
      
   
  };
  
  
 }
}

#endif
