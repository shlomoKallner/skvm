#ifndef SK_UTILITIES_ENDIANESS_H
#define SK_UTILITIES_ENDIANESS_H

#include <exception>
#include "SK/common/SK_types.h"
//  #include <Boost/static_assert.hpp> // - no need

namespace SK
{
 namespace utilities
 {
  enum endianess { unknown_endianess = 0, big_endianess, little_endianess };
  
  endianess find_local_endianess(void)
  {
   int i = 1;
   char * c = reinterpret_cast<char*>(&i);
   if( c[0] == 1 )
   { return little_endianess; }
   else if( c[sizeof(int)-1] = 1 )
   { return big_endianess; }
   else 
   { return unknown_endianess; }
  }//end of ...
  
  static const endianess local_endianess = find_local_endianess();
  
  class unknown_endian_exception : public std::exception
  {
   public:
          unknown_endian_exception(){}
          virtual ~ unknown_endian_exception(){}
          virtual const char* what() const throw()
          {
           return "Unknown Endianess - Unable to byteswap."
          }
   
  }
  
  
  struct endian
  {
   
   sint16 byteswap( sint16 i )
   {
    sint16 x = ( (i & 0x00ff) << 8 );
    sint16 y = ( (i & 0xff00 ) >> 8 );
    return x | y ;
   }//end of ... 
   sint32 byteswap( sint32 i )
   {
    sint16 a = byteswap( static_cast<sint16>(i & 0x0000ffff) );
    sint32 x = static_cast<sint32>(a) << 16;
    a = byteswap( static_cast<sint16>( (i & 0xffff0000 ) >> 16 ) );
    return x | static_cast<sint32>(a); 
   }//end of ...
   sint64 byteswap( sint64 i )
   {
    sint32 a = byteswap( static_cast<sint32>(i & 0x00000000ffffffff) );
    sint64 x = static_cast<sint64>(a) << 32;
    a = byteswap( static_cast<sint32>( (i & 0xffffffff00000000 ) >> 32 ) );
    return x | static_cast<sint64>(a); 
   }//end of ...
   
   uint16 byteswap( uint16 i )
   {
    uint16 x = ( (i & 0x00ff) << 8 );
    uint16 y = ( (i & 0xff00 ) >> 8 );
    return x | y ;
   }//end of ... 
   uint32 byteswap( uint32 i )
   {
    uint16 a = byteswap( static_cast<uint16>(i & 0x0000ffff) );
    uint32 x = static_cast<uint32>(a) << 16;
    a = byteswap( static_cast<uint16>( (i & 0xffff0000 ) >> 16 ) );
    return x | static_cast<uint32>(a); 
   }//end of ...
   uint64 byteswap( uint64 i )
   {
    uint32 a = byteswap( static_cast<uint32>(i & 0x00000000ffffffff) );
    uint64 x = static_cast<uint64>(a) << 32;
    a = byteswap( static_cast<uint32>( (i & 0xffffffff00000000 ) >> 32 ) );
    return x | static_cast<uint64>(a); 
   }//end of ...
   
   sint8  toLocal( sint8 i)
   {
     return i;
   }//end of ...
   virtual sint16 toLocal( sint16 i)=0;
   virtual sint32 toLocal( sint32 i)=0;
   virtual sint64 toLocal( sint64 i)=0;
   
   uint8  toLocal( uint8 i)
   {
    return i;
   }//end of ...
   virtual uint16 toLocal( uint16 i)=0;
   virtual uint32 toLocal( uint32 i)=0;
   virtual uint64 toLocal( uint64 i)=0;
   
   
   sint8  fromLocal( sint8 i)
   {
    return i;
   }//end of ...
   virtual sint16 fromLocal( sint16 i)=0;
   virtual sint32 fromLocal( sint32 i)=0;
   virtual sint64 fromLocal( sint64 i)=0;
   
   uint8  fromLocal( uint8 i)
   {
    return i;
   }//end of ...
   virtual uint16 fromLocal( uint16 i)=0;
   virtual uint32 fromLocal( uint32 i)=0;
   virtual uint64 fromLocal( uint64 i)=0;
   
  };
  
  struct bigEndian : public endian
  {
   virtual sint16 toLocal( sint16 i)
   {
    if( local_endianess == little_endianess )
    {  
     return byteswap(i);
    }
    else if( local_endianess == big_endianess )
    { return i; }
    else if( local_endianess == unknown_endianess )
    { throw unknown_endian_exception(); }
   }//end of ...
   virtual sint32 toLocal( sint32 i)
   {
    if( local_endianess == little_endianess )
    { 
     return byteswap(i);
    }
    else if( local_endianess == big_endianess )
    { return i; }
    else if( local_endianess == unknown_endianess )
    { throw unknown_endian_exception(); }
   }//end of ...
   virtual sint64 toLocal( sint64 i)
   {
    if( local_endianess == little_endianess )
    {  
     return byteswap(i);
    }
    else if( local_endianess == big_endianess )
    { return i; }
    else if( local_endianess == unknown_endianess )
    { throw unknown_endian_exception(); }  
   }//end of ...
   
   virtual uint16 toLocal( uint16 i)
   {
    if( local_endianess == little_endianess )
    {
     return byteswap(i);  
    }
    else if( local_endianess == big_endianess )
    { return i; }
    else if( local_endianess == unknown_endianess )
    { throw unknown_endian_exception(); }
   }//end of ...
   virtual uint32 toLocal( uint32 i)
   {
    if( local_endianess == little_endianess )
    {
     return byteswap(i);
    }
    else if( local_endianess == big_endianess )
    { return i; }
    else if( local_endianess == unknown_endianess )
    { throw unknown_endian_exception(); }
   }//end of ...
   virtual uint64 toLocal( uint64 i)
   {
    if( local_endianess == little_endianess )
    {  
     return byteswap(i);
    }
    else if( local_endianess == big_endianess )
    { return i; }
    else if( local_endianess == unknown_endianess )
    { throw unknown_endian_exception(); }
   }//end of ...
   
   virtual sint16 fromLocal( sint16 i)
   {
    if( local_endianess == little_endianess )
    {
     return byteswap(i);  
    }
    else if( local_endianess == big_endianess )
    { return i; }
    else if( local_endianess == unknown_endianess )
    { throw unknown_endian_exception(); }
   }//end of ...
   virtual sint32 fromLocal( sint32 i)
   {
    if( local_endianess == little_endianess )
    {  
     return byteswap(i);
    }
    else if( local_endianess == big_endianess )
    { return i; }
    else if( local_endianess == unknown_endianess )
    { throw unknown_endian_exception(); }
   }//end of ...
   virtual sint64 fromLocal( sint64 i)
   {
    if( local_endianess == little_endianess )
    {  
     return byteswap(i);
    }
    else if( local_endianess == big_endianess )
    { return i; }
    else if( local_endianess == unknown_endianess )
    { throw unknown_endian_exception(); }
   }//end of ...
   
   virtual uint16 fromLocal( uint16 i)
   {
    if( local_endianess == little_endianess )
    {  
     return byteswap(i);
    }
    else if( local_endianess == big_endianess )
    { return i; }
    else if( local_endianess == unknown_endianess )
    { throw unknown_endian_exception(); }
   }//end of ...
   virtual uint32 fromLocal( uint32 i)
   {
    if( local_endianess == little_endianess )
    {  
     return byteswap(i);
    }
    else if( local_endianess == big_endianess )
    { return i; }
    else if( local_endianess == unknown_endianess )
    { throw unknown_endian_exception(); }
   }//end of ...
   virtual uint64 fromLocal( uint64 i)
   {
    if( local_endianess == little_endianess )
    {
     return byteswap(i);
    }
    else if( local_endianess == big_endianess )
    { return i; }
    else if( local_endianess == unknown_endianess )
    { throw unknown_endian_exception(); }
   }//end of ...
  };
  
  
  
  struct littleEndian : public endian
  {
   virtual sint16 toLocal( sint16 i)
   {
    if( local_endianess == little_endianess )
    { return i; }
    else if( local_endianess == big_endianess )
    {
     return byteswap(i);
    }
    else if( local_endianess == unknown_endianess )
    { throw unknown_endian_exception(); }  
   }//end of ...
   virtual sint32 toLocal( sint32 i)
   {
    if( local_endianess == little_endianess )
    { return i; }
    else if( local_endianess == big_endianess )
    {
     return byteswap(i); 
    }
    else if( local_endianess == unknown_endianess )
    { throw unknown_endian_exception(); }  
   }//end of ...
   virtual sint64 toLocal( sint64 i)
   {
    if( local_endianess == little_endianess )
    { return i; }
    else if( local_endianess == big_endianess )
    {
     return byteswap(i); 
    }
    else if( local_endianess == unknown_endianess )
    { throw unknown_endian_exception(); }    
   }//end of ...
   
   virtual uint16 toLocal( uint16 i)
   {
    if( local_endianess == little_endianess )
    { return i; }
    else if( local_endianess == big_endianess )
    {
     return byteswap(i); 
    }
    else if( local_endianess == unknown_endianess )
    { throw unknown_endian_exception(); }    
   }//end of ...
   virtual uint32 toLocal( uint32 i)
   {
    if( local_endianess == little_endianess )
    { return i; }
    else if( local_endianess == big_endianess )
    {
     return byteswap(i); 
    }
    else if( local_endianess == unknown_endianess )
    { throw unknown_endian_exception(); }    
   }//end of ...
   virtual uint64 toLocal( uint64 i)
   {
    if( local_endianess == little_endianess )
    { return i; }
    else if( local_endianess == big_endianess )
    {
     return byteswap(i); 
    }
    else if( local_endianess == unknown_endianess )
    { throw unknown_endian_exception(); }    
   }//end of ...
   
   virtual sint16 fromLocal( sint16 i)
   {
    if( local_endianess == little_endianess )
    { return i; }
    else if( local_endianess == big_endianess )
    {
     return byteswap(i); 
    }
    else if( local_endianess == unknown_endianess )
    { throw unknown_endian_exception(); }    
   }//end of ...
   virtual sint32 fromLocal( sint32 i)
   {
    if( local_endianess == little_endianess )
    { return i; }
    else if( local_endianess == big_endianess )
    {
     return byteswap(i); 
    }
    else if( local_endianess == unknown_endianess )
    { throw unknown_endian_exception(); }    
   }//end of ...
   virtual sint64 fromLocal( sint64 i)
   {
    if( local_endianess == little_endianess )
    { return i; }
    else if( local_endianess == big_endianess )
    {
     return byteswap(i); 
    }
    else if( local_endianess == unknown_endianess )
    { throw unknown_endian_exception(); }    
   }//end of ...
   
   virtual uint16 fromLocal( uint16 i)
   {
    if( local_endianess == little_endianess )
    { return i; }
    else if( local_endianess == big_endianess )
    {
     return byteswap(i); 
    }
    else if( local_endianess == unknown_endianess )
    { throw unknown_endian_exception(); }    
   }//end of ...
   virtual uint32 fromLocal( uint32 i)
   {
    if( local_endianess == little_endianess )
    { return i; }
    else if( local_endianess == big_endianess )
    {
     return byteswap(i); 
    }
    else if( local_endianess == unknown_endianess )
    { throw unknown_endian_exception(); }    
   }//end of ...
   virtual uint64 fromLocal( uint64 i)
   {
    if( local_endianess == little_endianess )
    { return i; }
    else if( local_endianess == big_endianess )
    {
     return byteswap(i); 
    }
    else if( local_endianess == unknown_endianess )
    { throw unknown_endian_exception(); }    
   }//end of ...
  };
  
  
  struct mixxedEndian  : public endian
  { }; // unimplemented! byteswap won`t work on such a platform!
 }
}

#endif
