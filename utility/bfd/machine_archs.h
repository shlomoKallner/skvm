#ifndef SK_UTILITIES_MACHINE_ARCHS_H
#define SK_UTILITIES_MACHINE_ARCHS_H

#include "endianess.h"

namespace SK
{
 namespace utilities
 {
   
  template< const std::size_t ARCH_SIZE >
  struct machine_arch
  {
   std::size_t get_arch_size(void)
   { return ARCH_SIZE; }//end of ...
   
  };
  
  struct bigEndian_32 : public bigEndian, public machine_arch<32>
  {
   typedef sint32 sword_t;
   typedef uint32 uword_t;
  };
  
  struct bigEndian_64 : public bigEndian, public machine_arch<64>
  {
   typedef sint64 sword_t;
   typedef uint64 uword_t;
  };
  
  
  struct littleEndian_32 : public littleEndian, public machine_arch<32>
  {
   typedef sint32 sword_t;
   typedef uint32 uword_t;
  };
  
  struct littleEndian_64 : public littleEndian, public machine_arch<64>
  {
   typedef sint64 sword_t;
   typedef uint64 uword_t;
  };
     
  
 }
}




#endif
