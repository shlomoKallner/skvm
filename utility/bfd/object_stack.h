/* object_stack.h - classes and subroutines to implement 
   object stacks in c++ based on ( in more ways than one )
   obstack from the GNU libiberty project found in the 
   Binutils ditribution version 2.11.94 which is copyrighted to 
   the Free Software Foundation, Inc.
   
   Copyright (C) 2010 Shlomo Kallner .
   
   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 2, or (at your option) any
   later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA.  */

#ifndef SK_UTILITY_OBJECT_STACK_H
#define SK_UTILITY_OBJECT_STACK_H

#include <vector>
#include <cstdlib>
#include <new>
#include <boost/shared_ptr.hpp>

#ifdef __GNUC__ 

#undef offsetof
#define offsetof(TYPE, MEMBER) __builtin_offsetof ( (TYPE), (MEMBER) )

#endif

namespace SK
{
 namespace utility
 {
  namespace _impl
  { 
   // this is copied and modified from obstack.c in the GNU libiberty project.
   /* Determine default alignment.  */
   struct align {char x; double d;};

   std::ptrdiff_t get_default_alignment(void)
   { 
    return static_cast<std::ptrdiff_t>( offsetof ( align, d ) );
   }//end of ...
   
   /* If malloc were really smart, it would round addresses to 
     DEFAULT_ALIGNMENT.
   But in fact it might be less smart and round addresses to as much as
   DEFAULT_ROUNDING.  So we prepare for it to do that.  */
   union round {long x; double d;};

   std::size_t get_default_rounding(void)
   { 
    return sizeof(union round); 
   }//end of ...
   
  } // end of namespace _impl ;
  
  const std::size_t    CHUNCK_SIZE = 4096;
  const std::ptrdiff_t DEFAULT_ALIGNMENT = _impl::get_default_alignment();
  const std::size_t    DEFAULT_ROUNDING =  _impl::get_default_rounding();
  
  class bad_dealloc : public exception
  {
   bad_dealloc(){}// end of ...
   virtual~ bad_dealloc(){}// end of ...
  };
  
  class chunck_allocator
    {
    public:
      typedef std::size_t     size_type;
      typedef std::ptrdiff_t  difference_type;
      typedef void*           pointer;
      typedef const void*     const_pointer;
      
      chunck_allocator() throw() { }
      
      chunck_allocator(const chunck_allocator&) throw() { }
      virtual ~chunck_allocator() throw() { }

      // NB: __n is permitted to be 0.  The C++ standard says nothing
      // about what the return value is when __n == 0.
      // SK Notes: p is expected to be of (n) size ... ( due memcpy .... )
      pointer
      allocate(size_type n, const void* p = 0)
      {
       if( n == 0 ) return 0;
       pointer temp = ::operator new(n);
       if( p != 0 )
         { std::memcpy( temp, p, n); } 
       return temp; 
      }

      // __p is not permitted to be a null pointer.
      void
      deallocate(pointer __p, size_type)
      {
       if( __p != 0 ) 
       {
        ::operator delete(__p); 
       }
      }//end of ...

    };

  template< typename Alloc = chunck_allocator >
  class object_stack_chunck
  {
   public:
      typedef typename Alloc::size_type         size_type;
      typedef typename Alloc::difference_type   difference_type;
      typedef typename Alloc::pointer           pointer;
      typedef typename Alloc::const_pointer     const_pointer;
      typedef typename Alloc                    allocator_type;
      
      
      object_stack_chunck() : alloc_m(), begin_p(0), current_p(0),end_p(0)
      {
        try
       {
        begin_p = alloc_m.allocate(CHUNCK_SIZE);
        end_p = begin_p + CHUNCK_SIZE;
        current_p = begin_p;
       }
       catch(std::bad_alloc a)
       {
        throw;
       }                     
      }// end of ...
      
      explicit object_stack_chunck( Alloc a = Alloc() ) : alloc_m(a),
                                    begin_p(0), current_p(0),end_p(0)
      {
       try
       {
        begin_p = alloc_m.allocate(CHUNCK_SIZE);
        end_p = begin_p + CHUNCK_SIZE;
        current_p = begin_p;
       }
       catch(std::bad_alloc a)
       {
        throw;
       }
      }//end of ...
      
      object_stack_chunck( size_type i , Alloc a = Alloc() ) : alloc_m(a), 
                           begin_p(0), current_p(0), end_p(0)
      {
       try
       {
        begin_p = alloc_m.allocate(i);
        end_p = begin_p + i;
        current_p = begin_p;
       }
       catch(std::bad_alloc a)
       {
        throw;
       }
      }//end of ...
      
      ~object_stack_chunck()
      {
       alloc_m.deallocate(begin_p);
       begin_p = current_p = end_p = 0;
      }//end of ...
      
      // SK Notes: if n != 0 && size_left >= n then allocate.
      // if p != 0 the data it points to MUST be of (n) size!!! 
      //  --preforms memcpy!!!
      pointer allocate(size_type n, const void* p = 0)
      {
       if( n == 0 ) return 0; 
       if( this->size_left() >= n )
       {
        pointer temp = current_p;
        current_p = current_p + n;
        if( p != 0 )
        {
         std::memcpy( temp, p, n);
        }
        return temp;
       }
       else
       { 
        throw std::bad_alloc(); 
       } 
      }//end of ...

      // p is not permitted to be a null pointer.
      void deallocate(pointer p, size_type n )
      { 
       if( __p != 0 ) 
       {
        if( this->is_in_chunck(p) )
        {
         if( current_p == p + n )
         current_p = p;
         else 
         { 
          goto error;
         }
        }  
       }
       else
       {
        error:
        throw bad_dealloc(); 
       }
      }//end of ...
      
      size_type size_left()
      {
       return end_p - current_p;
      }//end of ...
      
      pointer get_begin();
      size_type get_size();
      
      boolean is_in_chunck( pointer p )
      {
       return p >= begin_p || p < end_p ;
      }
      
      protected:
                allocator_type alloc_m;
                pointer begin_p, current_p, end_p;
      private:
           object_stack_chunck( ) : alloc_m(), begin_p(), current_p(), end_p()
           {}
           const object_stack_chunck& operator=( const object_stack_chunck& r )   
              { return r; }
  };
  
  template< typename chunck_t = object_stack_chunck<chunck_allocator> >
  class object_stack
  {
   public:
      typedef typename chunck_t::size_type        size_type;
      typedef typename chunck_t::difference_type  difference_type;
      typedef typename chunck_t::pointer          pointer;
      typedef typename chunck_t::const_pointer    const_pointer;
      typedef typename chunck_t                   chunck_type;
      typedef void        value_type; // is value_type needed?????
      typedef boost::shared_ptr<chunck_type> chunck_ptr;
      typedef std::list<chunck_ptr>               chunck_list;
      
      pointer allocate(size_type n, const void* p = 0)
      { 
       return ; 
      }//end of ...

      // __p is not permitted to be a null pointer.
      void deallocate(pointer p, size_type n )
      { 
       if( __p != 0 ) 
       {
           
       }
       else
       {
        error:
        throw bad_dealloc(); 
       }
      }//end of ...

      

      protected:
                std::vector<chunck_ptr> chuncks_m;
  };
  
  template < typename T >
  void construct(void* __p, const T& __val) 
  { 
   ::new(__p) T(__val); 
  }//end of ...

  template < typename T >  
  void destroy(void * p) 
  { 
   T* t = static_cast<T*>(p);
   t->~T(); 
  }//end of ...
  
 }
}

#endif
