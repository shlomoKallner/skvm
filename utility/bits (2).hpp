#ifndef SK_UTILITIES_BITS_H
#define SK_UTILITIES_BITS_H

#include <exception>
#include <ostream>
#include <sstream>
#include <boost/static_assert.hpp> 
#include <boost/mpl/mpl.hpp>
#include "SK/utility/types.hpp"

// the previous contents of this file included from thier own files:
#include "bits/get_highlow.hpp"
#include "bits/set_highlow.hpp"
#include "bits/byteswap.hpp" 
#include "bits/bitmask.hpp"
#include "bits/endianess.hpp"

/*
namespace SK
{
 namespace utility
 {
  namespace bits
  {
    
//   namespace mpl = boost::mpl;
//   namespace types = SK::utility::types;


  } // end of namespace bits; 
 } // end of namespace utility;
} // end of namespace SK;
*/

#endif
