// alignment.hpp 
// based on alignment computing macros in
// the libJIT project as found in jit/jit-internal.h
// libJIT is copyrighted to Southern Storm Software, Pty Ltd.

#ifndef SK_UTILITY_BITS_ALIGNMENT_HPP
#define SK_UTILITY_BITS_ALIGNMENT_HPP

#include <boost/utility.hpp>

namespace SK
{
 namespace utility
 {
  namespace bits
  {
   
   template< typename T >
  int get_alignment( void )
  {
   struct aligner
   {
    char pad;
    T field;
   };
   return static_cast< int > ( 
    boost::addressof( 
     reinterpret_cast< aligner * >(0)->field 
                    )        );
  }// end of ...
  
   
  } // end of namespace bits  
 } // end of namespace utility
} // end of namespace SK

#endif
