#ifndef SK_UTILITIES_BITS_BYTESWAP_H
#define SK_UTILITIES_BITS_BYTESWAP_H

#include <exception>
#include <ostream>
#include <sstream>
#include <boost/static_assert.hpp> 
#include <boost/mpl/mpl.hpp>
#include "SK/utility/types.hpp"

namespace SK
{
 namespace utility
 {
  namespace bits
  {
   namespace types = SK::utility::types;       
   // functions that return a bitmask according to a bit number or range or range set:
   template <std::size_t N ,typename T = types::uint_of_size<N> >
   T bitmask( std::size_t i);
   {
     if( i >= 0 && i < N )
      return T(1 << i );
     else
      return T(0);
   }// end of ...
 
   template<>
   uint8 bitmask< 8, types::uint8 >( std::size_t i )
   {
     if( i >= 0 && i < 8 )
      return 1 << i ;
     else
      return 0;
   }// end of  ....

   template<>
   uint16 bitmask< 16, types::uint16 >( std::size_t i )
   {
     if( i >= 0 && i < 16 )
      return 1 << i ;
     else
      return 0;
   }// end of  ....
   
   template<>
   uint32 bitmask< 32, types::uint32 >( std::size_t i )
   {
     if( i >= 0 && i < 32 )
      return 1 << i ;
     else
      return 0;
   }// end of  ....
  
   template<>
   uint64 bitmask< 64, types::uint64 >( std::size_t i )
   {
     if( i >= 0 && i < 64 )
      return 1 << i ;
     else
      return 0;
   }// end of  ....

 template< std::size_t N, typename ContainerT, 
           typename T = types::uint_of_size<N> >
   T bitmask( ContainerT c )
   {
    T t(0);
     for( typename ContainerT::iterator i = c.begin() ; i != c.end(); c++ )
     {  t |= bitmask<N,T>(*i); }
     return t;
   }//end of ...
   
   // masks all bits in the bit range 
   template< std::size_t N, 
      typename T = types::uint_of_size<N> >
   T bitmask( std::size_t begin_, std::size_t end_ );
   
   
  }// end of namespace bits;
 }// end of namespace utility;
}// end of namespace SK;  
  
