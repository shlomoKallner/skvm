#ifndef SK_UTILITIES_BITS_BYTESWAP_H
#define SK_UTILITIES_BITS_BYTESWAP_H


#include "SK/utility/types.hpp"
#include "get_highlow.hpp"
#include "set_highlow.hpp"

namespace SK
{
 namespace utility
 {
  namespace bits
  {
   namespace types = SK::utility::types;         
   // byte swap ( endianess ) functions:
           
   // signed byteswap functions:
   types::sint16 byteswap( types::sint16 i )
   {
    return make_sint( get_low(i) , get_high(i) ); 
   }//end of ... 

   types::sint32 byteswap( types::sint32 i )
   {
    return make_sint( get_low(i), get_high(i) ); 
   }//end of ...

   types::sint64 byteswap( types::sint64 i )
   {
    return make_sint( get_low(i), get_high(i) );
   }//end of ...

   // unsigned byteswap functions:   
   types::uint16 byteswap( types::uint16 i )
   {
    return make_uint( get_low(i), get_high(i) ); 
   }//end of ... 

   types::uint32 byteswap( types::uint32 i )
   {
    return make_uint( get_low(i), get_high(i) ); 
   }//end of ...

   types::uint64 byteswap( types::uint64 i )
   {
    return make_uint( get_low(i), get_high(i) );
   }//end of ...
   
   
  }// end of namespace bits;
 }// end of namespace utility;
}// end of namespace SK;  
  
#endif
