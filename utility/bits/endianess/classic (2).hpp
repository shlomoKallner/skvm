#ifndef SK_UTILITIES_BITS_ENDIANESS_CLASSIC_H
#define SK_UTILITIES_BITS_ENDIANESS_CLASSIC_H

#include <exception>
#include <ostream>
#include <sstream>
#include <boost/static_assert.hpp> 
#include <boost/mpl/mpl.hpp>

#include "SK/utility/types.hpp"
#include "endianess_fwd.hpp"
#include "local_endianess.hpp"

namespace SK
{
 namespace utility
 {
  namespace bits
  {
   namespace endianess
   {
    namespace classic
    {
     namespace mpl   = boost::mpl;
     namespace types = SK::utility::types;
     namespace bits  = SK::utility::bits;

         
    // endianess meta-data types with functions for changing the byte order of their argument:
  
  template< endianess e /*= null_endianess */ > 
  struct endian
  {
   typedef mpl::integral_c<endianess,e> endianess_tag;  
   
   template< typename T >
   static typename T change_byte_order( typename T i, endianess  scr_e, 
                                        endianess dest_e ) const
   {
    if( ( scr_e ==  big_endianess && dest_e ==  little_endianess )
     || ( scr_e ==  little_endianess && dest_e == big_endianess  ) )
      return bits::byteswap(i); // these 2 endianesses are opposites 
                         //  so just swap the byte order.
    else if( scr_e == dest_e )
     return i;
    else  
      throw  bad_endianess_exception( scr_e , dest_e );
   } //end of .... 
  
   template<>
   static types::sint8 change_byte_order<types::sint8>( types::sint8 i, 
                          endianess = null_endianess ,
                          endianess = null_endianess )
   {
    return i;
   } // end of...
   
   template< typename T >
   T toLocal( T i ) const
   {
     return change_byte_order<T>(i, 
                   endianess_tag::value , 
                   local_endianess);
   }// end of ...
  
   template< typename T >
   T fromLocal( T i ) const
   {
     return change_byte_order<T>(i, 
                   local_endianess, 
                   endianess_tag::value );           
   }// end of ...
   
   
  };

  
    
    }// end of namespace classic;
   }// end of namespace endianess;
  }// end of namespace bits;
 }// end of namespace utility;
}// end of namespace SK;
