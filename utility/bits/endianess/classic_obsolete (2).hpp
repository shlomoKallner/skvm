#ifndef SK_UTILITY_BITS_ENDIANESS_CLASSIC_OBSOLETE_HPP
#define SK_UTILITY_BITS_ENDIANESS_CLASSIC_OBSOLETE_HPP


static sint16 change_byte_order( sint16 i, endianess  scr_e, endianess dest_e )
  {
   if( ( scr_e ==  big_endianess && dest_e ==  little_endianess )
    || ( scr_e ==  little_endianess && dest_e == big_endianess  ) )
     return byteswap(i); // these 2 endianesses are opposites so just swap the byte order.
   else if( scr_e == dest_e )
    return i;
   else  
     throw  bad_endianess_exception( scr_e , dest_e );
  } //end of ....

 static uint16 change_byte_order( uint16 i, endianess  scr_e, endianess dest_e )
  {
   if( ( scr_e ==  big_endianess && dest_e ==  little_endianess )
    || ( scr_e ==  little_endianess && dest_e == big_endianess  ) )
     return byteswap(i); // these 2 endianesses are opposites so just swap the byte order.
   else if( scr_e == dest_e )
    return i;
   else  
     throw  bad_endianess_exception(  scr_e, dest_e );
  } //end of ....

 static sint32 change_byte_order( sint32 i, endianess  scr_e, endianess dest_e )
  {
   if( ( scr_e ==  big_endianess && dest_e ==  little_endianess )
    || ( scr_e ==  little_endianess && dest_e == big_endianess  ) )
     return byteswap(i); // these 2 endianesses are opposites so just swap the byte order.
   else if( scr_e == dest_e )
    return i;
   else  
     throw  bad_endianess_exception(  scr_e, dest_e );
  } //end of ....

 static uint32 change_byte_order( uint32 i, endianess  scr_e, endianess dest_e )
  {
   if( ( scr_e ==  big_endianess && dest_e ==  little_endianess )
    || ( scr_e ==  little_endianess && dest_e == big_endianess  ) )
     return byteswap(i); // these 2 endianesses are opposites so just swap the byte order.
   else if( scr_e == dest_e )
    return i;
   else  
     throw  bad_endianess_exception(  scr_e, dest_e );
  } //end of ....

static sint64 change_byte_order( sint64 i, endianess  scr_e, endianess dest_e )
  {
   if( ( scr_e ==  big_endianess && dest_e ==  little_endianess )
    || ( scr_e ==  little_endianess && dest_e == big_endianess  ) )
     return byteswap(i); // these 2 endianesses are opposites so just swap the byte order.
   else if( scr_e == dest_e )
    return i;
   else  
     throw  bad_endianess_exception(  scr_e, dest_e );
  } //end of ....

 static uint64 change_byte_order( uint64 i, endianess  scr_e, endianess dest_e )
  {
   if( ( scr_e ==  big_endianess && dest_e ==  little_endianess )
    || ( scr_e ==  little_endianess && dest_e == big_endianess  ) )
     return byteswap(i); // these 2 endianesses are opposites so just swap the byte order.
   else if( scr_e == dest_e )
    return i;
   else  
     throw  bad_endianess_exception(  scr_e, dest_e );
  } //end of ....


    sint8  toLocal( sint8 i )
   {
     return change_byte_order(i, endianess_tag::value , local_endianess);
   }//end of ...

   sint16 toLocal( sint16 i)
   {
     return change_byte_order(i, endianess_tag::value , local_endianess);
   }//end of ...

   sint32 toLocal( sint32 i)
   {
     return change_byte_order(i, endianess_tag::value , local_endianess);
   }//end of ...
   sint64 toLocal( sint64 i)
   {
     return change_byte_order(i, endianess_tag::value , local_endianess);
   }//end of ...
   
   uint8  toLocal( uint8 i)
    {
     return change_byte_order(i, endianess_tag::value , local_endianess);
   }//end of ...

   uint16 toLocal( uint16 i)
   {
     return change_byte_order(i, endianess_tag::value , local_endianess);
   }//end of ...

   uint32 toLocal( uint32 i)
   {
     return change_byte_order(i, endianess_tag::value , local_endianess);
   }//end of ...
   uint64 toLocal( uint64 i)
   {
     return change_byte_order(i, endianess_tag::value , local_endianess);
   }//end of ...
   
   
   sint8  fromLocal( sint8 i)
   {
     return change_byte_order(i, local_endianess, endianess_tag::value );
   }//end of ...
   sint16 fromLocal( sint16 i)
   {
     return change_byte_order(i, local_endianess, endianess_tag::value );
   }//end of ...

   sint32 fromLocal( sint32 i)
   {
     return change_byte_order(i, local_endianess, endianess_tag::value );
   }//end of ...

   sint64 fromLocal( sint64 i)
   {
     return change_byte_order(i, local_endianess, endianess_tag::value );
   }//end of ...
   
   uint8  fromLocal( uint8 i)
   {
     return change_byte_order(i, local_endianess, endianess_tag::value );
   }//end of ...

   uint16 fromLocal( uint16 i)
   {
     return change_byte_order(i, local_endianess, endianess_tag::value );
   }//end of ...

   uint32 fromLocal( uint32 i)
   {
     return change_byte_order(i, local_endianess, endianess_tag::value );
   }//end of ...

   uint64 fromLocal( uint64 i)
   {
     return change_byte_order(i, local_endianess, endianess_tag::value );
   }//end of ...
   


#endif
