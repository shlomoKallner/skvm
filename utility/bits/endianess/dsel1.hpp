#ifndef SK_UTILITY_BITS_ENDIANESS_DSEL_V1_HPP
#define SK_UTILITY_BITS_ENDIANESS_DSEL_V1_HPP

#include <boost/proto/proto.hpp>
#include <boost/mpl/assert.hpp>
#include <boost/mpl/if.hpp>
#include <boost/type_traits.hpp>
#include <boost/utility/result_of.hpp>

#include <boost/fusion/container/list/cons.hpp>
#include <boost/fusion/include/cons.hpp>
#include <boost/fusion/algorithm/transformation/push_back.hpp>
#include <boost/fusion/include/push_back.hpp>

#include <boost/mpl/plus.hpp>

#include <cstdlib>
#include <vector>
#include <map>
#include <string>
#include <sstream>
#include <ostream>
#include <typeinfo>

#include "SK/utility/types.hpp"
#include "SK/utility/bits/get_setBytes.hpp"
#include "endianess_fwd.hpp"
#include "local_endianess.hpp"


namespace SK
{
 namespace utility
 {
  namespace bits
  {
   namespace endianess
   {
    namespace dsel1  // fromerly v1
    {
     namespace proto  =  boost::proto;
     namespace mpl    =  boost::mpl;
     namespace fusion =  boost::fusion;
     namespace types  =  SK::utility::types;
     namespace bits   =  SK::utility::bits;
     
     
     struct converter_domain;
     struct converter_grammar;
     template< typename Expr >
     struct converter_expr;
    
     struct converter_domain
     : proto::domain< 
          proto::pod_generator< converter_expr >,
          converter_grammar 
                    >
     {};
     
     template< types::sint32 N  > 
     struct placeholder : mpl::integral_c<types::sint32,N>
     {
      typedef placeholder< N > this_type;
      
      template< typename CharT, typename TraitsT, typename Alloc >
      std::basic_ostream<CharT,TraitsT,Alloc>& 
      operator<<( std::basic_ostream<CharT,TraitsT,,Alloc>& out, 
                                              this_type const & )
      {
       out << "placeholder< " << N << " >";
       return out;
      }
      
     }; // v5
     
     template< types::sint32 N  > 
     struct _n : converter_expr< 
                   proto::terminal< placeholder< N > >::type 
                               >
     {};
    
    // the placeholders:
     _n< 1 > const _0 = {{}};
     _n< 2 > const _1 = {{}};  
     _n< 3 > const _2 = {{}};
     _n< 4 > const _3 = {{}};
     _n< 5 > const _4 = {{}};
     _n< 6 > const _5 = {{}};
     _n< 7 > const _6 = {{}};
     _n< 8 > const _7 = {{}};
     _n< 9 > const _8 = {{}};
     _n< 10 > const _9 = {{}};
     _n< 11 > const _10 = {{}};
     _n< 12 > const _11 = {{}};  
     _n< 13 > const _12 = {{}};
     _n< 14 > const _13 = {{}};
     _n< 15 > const _14 = {{}};
     _n< 16 > const _15 = {{}};
     _n< 17 > const _16 = {{}};
     _n< 18 > const _17 = {{}};
     _n< 19 > const _18 = {{}};
     _n< 20 > const _19 = {{}};
     _n< 21 > const _20 = {{}};
     _n< 22 > const _21 = {{}};  
     _n< 23 > const _22 = {{}};
     _n< 24 > const _23 = {{}};
     _n< 25 > const _24 = {{}};
     _n< 26 > const _25 = {{}};
     _n< 27 > const _26 = {{}};
     _n< 28 > const _27 = {{}};
     _n< 29 > const _28 = {{}};
     _n< 30 > const _29 = {{}};
     _n< 31 > const _30 = {{}};
     _n< 32 > const _31 = {{}};  
     _n< 33 > const _32 = {{}};
     _n< 34 > const _33 = {{}};
     _n< 35 > const _34 = {{}};
     _n< 36 > const _35 = {{}};
     _n< 37 > const _36 = {{}};
     _n< 38 > const _37 = {{}};
     _n< 39 > const _38 = {{}};
     _n< 40 > const _39 = {{}};

   
     
  // proto::_value()
     
     struct push_back_ : proto::callable
     { // v5
       typedef byteOrderRep_type & return_type;
       
       return_type operator()( types::sint32 i, return_type stat_, 
                               types::sint32 s = -1 )
       {
         if( ( s == -1 ) 
          || ( stat_.size() >= s - 1 ) 
          || ( i >= s ) 
          || ( i <= 0 )  ) 
         { return stat_; }
         stat_.push_back(i); 
         return stat_;
       }// end of ...
     };     
     
     struct converter_cases // v1
     {
       // The primary template matches nothing:
       template<typename Tag>
       struct case_
        : proto::not_<proto::_>
       {};
     };
     
     // Terminal expressions are handled here
     template<types::sint32 N>
     struct converter_cases::case_<proto::tag::terminal>
     : proto::when<
         proto::terminal< placeholder< N > >,
         push_back_( N, proto::_state, proto::_data )
                  >
     {};
      
     template<>
     struct converter_cases::case_<proto::tag::shift_left>
     : proto::when<
         proto::shift_left< converter_grammar, converter_grammar >,
         proto::fold<proto::_, proto::_state, converter_grammar>
                  >
     {}; 
     // old code for tag:  proto::tag::shift_left 
      /* 
     converter_grammar(
          proto::_right,
          converter_grammar(proto::_left, proto::_state, proto::_data),
          proto::_data )
      */

     struct converter_grammar 
     : proto::switch_<converter_cases>
     {};
     
     
     
     template< typename Expr,
               class Dummy = proto::is_proto_expr >
     struct converter_expr
     {
        BOOST_PROTO_BASIC_EXTENDS(Expr, converter_expr<Expr>, converter_domain)
      //  BOOST_PROTO_EXTENDS_SUBSCRIPT()
      //  BOOST_PROTO_EXTENDS_FUNCTION()
       /* 
        typedef byteOrderRep_type data_type; 
        typedef data_type result_type;
       */ 
       // result_type operator()( IntegerT i );
        
     };
     
     enum conversion_direction { null_ = 0, To_ = 1, From_ };
     
     template< typename IntegralT = void >
     class converter
     {
           BOOST_MPL_ASSERT( boost::is_integral<IntegralT>::value );
           BOOST_MPL_ASSERT_NOT(( boost::is_same<IntegralT,void> ));
       
      public:
           typedef typename IntegralT int_type;
           typedef byteOrderRep_type  data_type;
           typedef data_type::iterator data_iter;
           typedef data_type::size_type data_size;
           typedef typename converter<int_type> converter_type;
           
      protected:
           data_type to_code_m, from_code_m;    
      
      public:
           static const types::uint32 int_size 
                   = sizeof(int_type)/sizeof(char);

           // { null_ = 0, To_, From_  
           template< typename Expr >
           converter( Expr const & expr, conversion_direction dir_ = null_ ,
             types::boolean u = false )
           : to_code_m( dir_ == To_ ? 
                    converter_grammar()( expr, data_type(), int_size ):
                    ( dir_ == From_ ? get_local_encoding<int_type>(): 
                    data_type() )  ),
             from_code_m( dir_ == From_ ?
                    converter_grammar()( expr, data_type(), int_size ): 
                    ( dir_ == To_ ? get_local_encoding<int_type>():
                    data_type() ) ) 
           {
            if( ( to_code_m.empty() && dir_ == To_ )||
                ( from_code_m.empty() && dir_ == From_ ) )
             throw std::invalid_argument("ERROR! Invalid Argument passed for grammar.");
            if( dir_ == null_ )
             throw std::invalid_argument("ERROR! Invalid Argument passed for conversion direction.");
             
            if(u)
             {
              this->unique(to_code_m);
              this->unique(from_code_m);
             }
           }// end of ...
           
           template< typename Expr1, typename Expr2 >
           converter( Expr1 const & from_code, Expr2 const & to_code, 
                      types::boolean u = false )
           : to_code_m(converter_grammar()( to_code, data_type(), int_size )),
             from_code_m( converter_grammar()( from_code, data_type(), int_size ) )
           {
             if(u)
             {
              this->unique(to_code_m);
              this->unique(from_code_m);
             }
           }// end of ...
           
           converter( const converter_type & ct ) 
           : to_code_m( ct.to_code_m ), from_code_m( ct.from_code_m )
           {}// end of ...
 
           virtual ~converter(){}
           
           static void unique( data_type& d ) throw( std::out_of_range )
           {
            if( d.empty() ) return;
            if( d.size() != int_size ) 
              throw std::out_of_range("");
            data_type temp( int_size, 0 );// this is 0 to int_size non-inclusive
            // the last time I checked ...
            for( data_iter i = d.begin(); i != d.end(); i++ )
            { temp.at( absolute(*i) ) += 1; }// if we throw here we have a problem.
            for( data_iter i = temp.begin(); i != temp.end(); i++ ) 
            { if( *i != 1 ) throw std::out_of_range(""); }
           }// end of ...
           
           static types::uint8 indexof( const data_type& d, 
                    types::sint32 val ) throw( std::out_of_range )
           {
            types::uint8 temp = 0;
            types::boolean bol = false;
            for( types::uint8 iter = 0; 
                 iter < d.size() && iter < int_size ;
                 iter++  )
            {
             if( d.at(iter) == val ) // should this be using abs()???
             {
              temp = iter;
              bol = true;
              break; // we found the index, even if it`s the last.
             }
            }
            if( d.at(iter) == val && bol ) // double checking ...
             return temp;
            else
             throw std::out_of_range(""); 
           }// end of ...
           
           static types::uint8 byte( types::uint8 index, int_type i__ )
                                      throw( std::out_of_range )
           {
             if( index >= int_size ) // index is 0 based...
              throw std::out_of_range("");
#ifdef SK_UTILITY_BITS_DSEL1_USE_BITS_GETSETBYTE
             return bits::getUByte<int_type>( index, i__);
#else
             types::uint8* ptr = reinterpret_cast< types::uint8* >
                    ( boost::adressof(i__));
             return ptr[index]; 
#endif            
           }// end of
           
           static void byte( types::uint8 index, int_type& i__, 
                             types::uint8 val ) 
                             throw( std::out_of_range )
           {
             if( index >= int_size ) // index is 0 based...
              throw std::out_of_range("");
#ifdef SK_UTILITY_BITS_DSEL1_USE_BITS_GETSETBYTE
             bits::setUByte2<int_type>(  index, val, i__);
#else
             types::uint8* ptr = reinterpret_cast< types::uint8* >
                    ( boost::adressof(i__));
             ptr[index] = val; 
#endif                 
           }// end of...

           
           int_type operator()( int_type i__ )  throw( std::out_of_range )
           {
            int_type ret();
            // the basic idea is that we`ed use SK.Bits.Get/SetByte 
            //  if it`s available... no worry about that now as
            //  static functions byte() cover the details.
            
            // the convertion algorithm is as fallows:
            // *] each index of data_type (from 0 to int_size )
            //     contains the "byte index" for the address
            //    (that is index into the data_type + base_address);
            // *] signed indexes indicate that this byte contains 
            //    the sign bit, although this isn`t currently fully 
            //    implemented.
            // so, 
            // 1] iterate over the from_code and extract the corresponding
            //     to_code index for insertion of value.
            
            // -note: each vector index contains
            // which byte index is at this address
            // therefore the address of a given byte is:
            // (base_address + abs(vector[index])) 
            // where vector is the data_type representation
            // of the type and index is the index in data_type
            // containing the byte`s position ...
            // the sign bit`s byte`s address is encoded as
            // a negative vector_index ....
            
            for( types::uint8 j = 0; j < int_size; j++ )
            {
             // attemp 2 ... uses indexof() to find the index ...
             try
             {
              // types::uint8 index = 0, temp = 0;
              // index = indexof( from_code_m, from_code_m.at(j) );
              // the above just converts the sint32 to a uint8 and does some 
              // bounds checking. so ... we change it to ... 
              // "index = j;" ? - no way, that just is extranious.
              // "temp = byte( index, i__);" is now old code too ...
              // and the new code is:
              types::uint8 temp = byte( j, i__);   // extract byte`s value        
              types::uint8 index = indexof( to_code_m, from_code_m.at(j) );
              // this gets the corresponding byte index in to_code...
              byte( index, ret, temp); // set the new byte`s value.
             }
             catch( std::out_of_range e )
             { 
               throw; 
              // I really don`t know yet what to do if we threw till now...
             }                        
            }
            return ret;
           }//end of...
           
           
           boolean operator==( const converter_type & );
                
     };
     
     
     
    } // end of namespace dsel1; // fromerly v1
   } // end of namespace endianess;
  } // end of namespace bits;
 } // end of namespace utility;
} // end of namespace SK;
 

#endif
