#ifndef SK_UTILITY_BITS_ENDIANESS_DSEL_V1_OBSOLETE_HPP
#define SK_UTILITY_BITS_ENDIANESS_DSEL_V1_OBSOLETE_HPP

#include <boost/proto/proto.hpp>
#include <boost/mpl/assert.hpp>
#include <boost/mpl/if.hpp>
#include <boost/type_traits.hpp>
#include <boost/utility/result_of.hpp>

#include <boost/preprocessor/arithmetic/add.hpp>
#include <boost/preprocessor/arithmetic/mul.hpp>
#include <boost/preprocessor/cat.hpp>
#include <boost/preprocessor/repetition/repeat_from_to.hpp>



namespace SK
{
 namespace utility
 {
  namespace bits
  {
   namespace endianess
   {
    namespace v1_obsolete
    {
     namespace proto = boost::proto;
     namespace mpl = boost::mpl;
     
     template< types::sint32 N  > 
     struct placeholder // v1    
     {};              
       
     struct placeholder_n
     {};

/*   // -- forget the zeroth placeholder it`s not important.  
#ifdef SK_UTILITY_BITS_FORMAT_USE_ZEROTH_PLACEHOLDER     
     proto::terminal<placeholder<0> >::type const _0 = {{}};
     proto::terminal<placeholder<1> >::type const _1 = {{}};
#else
     proto::terminal<placeholder<0> >::type const _1 = {{}};
     proto::terminal<placeholder<1> >::type const _2 = {{}};
#endif
*/

// #define SK_UTILITY_BITS_FORMAT_V1_USE_MACROS
#undef SK_UTILITY_BITS_FORMAT_V1_USE_MACROS
#ifdef SK_UTILITY_BITS_FORMAT_V1_USE_MACROS

#define MAKE_PLACEHOLDER( n )  \
  converter_expr< proto::terminal< placeholder< (n) > >::type \
                > const BOOST_PP_CAT(_, n) = {{}};

#ifdef SK_UTILITY_BITS_FORMAT_V1_USE_MACROS_PROTO

#define M2( n ) BOOST_PP_MUL( BOOST_PROTO_MAX_ARITY, (n) )

#define M1 0
        
#define M0(N, typename_A, A_const_ref, A_const_ref_a, ref_a)  \
    MAKE_PLACEHOLDER( BOOST_PP_ADD(N , M1) )

#define M1 0
BOOST_PROTO_REPEAT_FROM_TO(1, BOOST_PROTO_MAX_ARITY, M0)
#define M1 M2( 1 )
BOOST_PROTO_REPEAT_FROM_TO(1, BOOST_PROTO_MAX_ARITY, M0)
#define M1 M2( 2 )
BOOST_PROTO_REPEAT_FROM_TO(1, BOOST_PROTO_MAX_ARITY, M0)
#define M1 M2( 3 )
BOOST_PROTO_REPEAT_FROM_TO(1, BOOST_PROTO_MAX_ARITY, M0)
#define M1 M2( 4 )
BOOST_PROTO_REPEAT_FROM_TO(1, BOOST_PROTO_MAX_ARITY, M0)
#define M1 M2( 5 )
BOOST_PROTO_REPEAT_FROM_TO(1, BOOST_PROTO_MAX_ARITY, M0)
#define M1 M2( 6 )
BOOST_PROTO_REPEAT_FROM_TO(1, BOOST_PROTO_MAX_ARITY, M0)
#define M1 M2( 7 )
BOOST_PROTO_REPEAT_FROM_TO(1, BOOST_PROTO_MAX_ARITY, M0)

#undef M2
#undef M1
#undef MO

#else

#define MY_PP_MACRO( z, n, data ) MAKE_PLACEHOLDER(n)

BOOST_PP_REPEAT_FROM_TO(0, 32, MY_PP_MACRO, 0 )

#undef MY_PP_MACRO

#endif

#undef MAKE_PLACEHOLDER  
#else
/* use this syntax to create directly placeholders, redefining n as you go.
 converter_expr< proto::terminal< placeholder< (n) > >::type 
               > const _ ## n = {{}}; 
 // we used this syntax for creating _0 to _32, use it for creating more.
*/
               
#endif
  


     template< typename F >
     struct get_integer_type
     {
      using boost::is_integral;
      using boost::is_function;
      using boost::remove_pointer;
      using boost::is_pointer;
      using boost::function_traits;
      using boost::result_of;
      //using boost:: ;
      //using boost:: ;
      //using boost:: ;
      
      struct default_empty_t{};
      
      typedef mpl::if_c< 
               is_integral<F>::value || 
                is_integral< result_of< F() >::type >::value, 
               F, 
               mpl::if_c< 
                is_function<F>::value && function_traits<F>::arity == 0
                 && is_integral< function_traits<F>::return_type >::value, 
                F,              
                mpl::if_c< 
                 is_pointer<F>::value && 
                  is_function<remove_pointer<F>::type>::value &&
                  function_traits<remove_pointer<F>::type>::arity == 0 &&
                  is_integral< function_traits< remove_pointer<F>::type 
                                              >::return_type >::value,  
                 F,             
                 default_empty_t 
                         >::type
                        >::type
                       >::type type;
             
       BOOST_MPL_ASSERT_NOT(( boost::is_same<type,default_empty_t> ));
     };
     
     template< typename F >
     struct litteral_t
     {
      typedef converter_expr< 
                proto::terminal< 
                       get_integer_type<F>::type // calling custom metafunction.
                               >::type 
                       > type;
     };
     
     template< typename IntegralT = void >
     class converter
     {
       BOOST_MPL_ASSERT( boost::is_integral<IntegralT>::value );
       BOOST_MPL_ASSERT_NOT(( boost::is_same<IntegralT,void> ));
       
       conversion_context<IntegralT> ctx_m;
      
      public:
             
           typedef typename IntegralT int_type;
           typedef typename converter<int_type> type;
           // static int_type To = 1;
             
           template< typename Expr >
           explicit converter( Expr const & expr )
           {
                   reset( expr );
           }// end of ...
           
          // template< typename Expr1, typename Expr2 >
          // converter( Expr1 const & from_code, Expr2 const & to_code );
          //template< typename Expr >
          // converter( Expr const & expr, conversion_direction dir_)
          // converter( const converter& )
            
           virtual ~converter_x(){}
           template< typename Expr >
           void
           reset( Expr const & expr )
           {
            ctx_m.res_m.clear();
            proto::eval( expr , ctx_m );
           }
           
           types::boolean 
           check()const;
           
           int_type
           convert( int_type i__, 
            conversion_direction dir_ = conversion_direction::null )const;
           inline int_type // for use as a functor
           operator() ( int_type i__, 
            conversion_direction dir_ = conversion_direction::null ) const
            {
             return this->convert(i__,dir_ );
            }// end of ....
            
            // int_type operator()( int_type i__ );
           
     };
     
     
     template< typename IntegralT = void >
     struct conversion_context
     { 
       // std::pair< std::size_t, std::size_t > pair_type;
       // typedef void result_type;
       
       typedef std::vector< std::size_t > data_type; 
       typedef data_type result_type;
       //data_type res_m;
       
       template<
        typename Expr
         // defaulted template parameters, so we can
         // specialize on the expressions that need
         // special handling.
        , typename Tag = typename proto::tag_of<Expr>::type
        , typename Arg0 = typename proto::child_c<Expr, 0>::type
        , typename Arg1 = typename proto::child_c<Expr, 1>::type
        >
       struct eval;

       // Handle placeholder terminals here...
       template<typename Expr, std::size_t I>
       struct eval<Expr, proto::tag::terminal, placeholder<I> >
       {
         typedef void result_type;

         result_type operator()(Expr &, conversion_context &ctx) const
         {
             ctx.res_m.push_back(I+1);
         }
       };

       template<typename Expr >
       struct eval<Expr, proto::tag::terminal, std::size_t >
       {
         typedef void result_type;

         result_type operator()(Expr & expr, conversion_context &ctx) const
         {
             ctx.res_m.push_back( proto::value(expr) );
         }
       };
       
       template<typename Expr, typename Arg0, typename Arg1>
       struct eval<Expr, proto::tag::subscript, Arg0>
       {
         typedef void result_type;

         result_type operator()(Expr &expr, MyContext &ctx) const
         {
             proto::eval(proto::right(expr), ctx);
         }
       };

       template<typename Expr, typename Arg0, typename Arg1>
       struct eval<Expr, proto::tag::shift_left, Arg0, Arg1>
       {
         typedef void result_type;

         result_type operator()(Expr &expr, MyContext &ctx) const
         {
             proto::eval(proto::left(expr), ctx);
             proto::eval(proto::right(expr), ctx);
         }
       };

       
       template<std::size_t I>
       result_type operator()(proto::tag::terminal, placeholder<I>) const
       {
        res_m.push_back( I + 1 );
       }//end of...
       
       // I hope this is right ...
       result_type operator()( proto::tag::terminal, 
                               std::size_t i )const
       {
        res_m.push_back(i + 1);
       }//end of ...
       
       
     };
     
     
     
     //      -- signed/unsigned placeholder usage.
//     proto::terminal< signed_pl >::type > const _s = {{}};
//     proto::terminal< unsigned_pl >::type > const _u = {{}}; 
// proto::function< placeholders_t, proto::terminal< signed_pl > >,
       // proto::function< placeholders_t, proto::terminal< unsigned_pl > >,
//    disabled for now - use negate instead. 

     
    } // end of namespace v1_obsolete;
   } // end of namespace endianess;
  } // end of namespace bits;
 } // end of namespace utility;
} // end of namespace SK;
 
namespace SK
{
 namespace utility
 {
  namespace bits
  {
   namespace endianess
   {
    namespace v1_obsolete
    {
     namespace proto = boost::proto;
     namespace mpl = boost::mpl;

 
 template< typename T >
     struct evaluator
     {
      typedef std::vector< int > data_type; 
      typedef data_type result_type;
      
       template<
        typename Expr
         // defaulted template parameters, so we can
         // specialize on the expressions that need
         // special handling.
        , typename Tag = typename proto::tag_of<Expr>::type
        , typename Arg0 = typename proto::child_c<Expr, 0>::type
        , typename Arg1 = typename proto::child_c<Expr, 1>::type
        >
       struct eval;
       
       // Handle placeholder terminals here...
       template<typename Expr, int I>
       struct eval<Expr, proto::tag::terminal, placeholder<I> >
       {
         typedef data_type result_type;

         result_type operator()(Expr &, evaluator & ctx) const
         {
             data_type dat();
             dat.push_back(I+1);
             return dat;
         }
       };
       
       
       template<typename Expr >
       struct eval<Expr, proto::tag::terminal, int >
       {
         typedef data_type result_type;

         result_type operator()(Expr & expr, evaluator & ctx) const
         {
             data_type dat();
             dat.push_back( proto::value(expr) + 1 );
             return dat;
         }
       };
       
       template<typename Expr >
       struct eval<Expr, proto::tag::negate, typename Arg0 >
       {
         typedef data_type result_type;

         result_type operator()(Expr & expr, evaluator & ctx) const
         {
             data_type dat(proto::child(expr));
             assert( dat.size() == 1);
             dat.at(0) = - dat.at(0);
             return dat;
         }
       };
       
       template<typename Expr, typename Arg0>
       struct eval<Expr, proto::tag::subscript, Arg0>
       {
         typedef data_type result_type;

         result_type operator()(Expr &expr, evaluator & ctx) const
         {
            return proto::eval(proto::right(expr), ctx);
         }
       };

       template<typename Expr, typename Arg0>
       struct eval<Expr, proto::tag::shift_left, Arg0>
       {
         typedef data_type result_type;

         result_type operator()(Expr & expr, evaluator & ctx) const
         {   
             data_type dat(proto::eval(proto::left(expr), ctx));
             data_type dat2(proto::eval(proto::right(expr), ctx));
             for( int i = 0; i < dat2.size(); i++)
             {  dat.push_back(dat2.at(i));}
             return dat;  
         }
       };

       
       
     };
     
     template< types::sint32 N  > // formerly std::size_t or int 
     struct placeholderV1   // had ", typename T1 = void" as second
     {}; // v1              // template argument.
       
     template< types::sint32 N  > 
     struct placeholderV2 : mpl::integral_c<types::sint32,N>
     {}; // v2
    
     template< types::sint32 N  > 
     struct placeholderV3 : proto::integral_c<types::sint32,N>
     {}; // v3
     
     template< typename T >
     struct placeholderV4 : T
     {}; // v4
       
     struct placeholder_n
     {};
     
     struct signed_pl
     {};
     
     struct unsigned_pl
     {};  
     
     // the latest version used:
     template< types::sint32 N  > 
     struct placeholder // v1    
     {};              
       
     struct placeholder_n
     {};
     
     typedef std::vector<types::sint32> byteOrderRep_type;
  //   struct signed_pl{};
  //   struct unsigned_pl{};
     
     struct converter_domain;
     struct converter_grammar;
     template< typename Expr >
     struct converter_expr;
    
     struct converter_domain
     : proto::domain< 
          proto::pod_generator< converter_expr >,
          converter_grammar 
                    >
     {};
     
     converter_expr< proto::terminal< placeholderV3< 1 > >::type 
               > const _0 = {{}};
     converter_expr< proto::terminal< placeholderV3< 2 > >::type 
               > const _1 = {{}};  
     converter_expr< proto::terminal< placeholderV3< 3 > >::type 
               > const _2 = {{}};
     converter_expr< proto::terminal< placeholderV3< 4 > >::type 
               > const _3 = {{}};
     converter_expr< proto::terminal< placeholderV3< 5 > >::type 
               > const _4 = {{}};
     converter_expr< proto::terminal< placeholderV3< 6 > >::type 
               > const _5 = {{}};
     converter_expr< proto::terminal< placeholderV3< 7 > >::type 
               > const _6 = {{}};
     converter_expr< proto::terminal< placeholderV3< 8 > >::type 
               > const _7 = {{}};
     converter_expr< proto::terminal< placeholderV3< 9 > >::type 
               > const _8 = {{}};
     converter_expr< proto::terminal< placeholderV3< 10 > >::type 
               > const _9 = {{}};
     converter_expr< proto::terminal< placeholderV3< 11 > >::type 
               > const _10 = {{}};
     converter_expr< proto::terminal< placeholderV3< 12 > >::type 
               > const _11 = {{}};  
     converter_expr< proto::terminal< placeholderV3< 13 > >::type 
               > const _12 = {{}};
     converter_expr< proto::terminal< placeholderV3< 14 > >::type 
               > const _13 = {{}};
     converter_expr< proto::terminal< placeholderV3< 15 > >::type 
               > const _14 = {{}};
     converter_expr< proto::terminal< placeholderV3< 16 > >::type 
               > const _15 = {{}};
     converter_expr< proto::terminal< placeholderV3< 17 > >::type 
               > const _16 = {{}};
     converter_expr< proto::terminal< placeholderV3< 18 > >::type 
               > const _17 = {{}};
     converter_expr< proto::terminal< placeholderV3< 19 > >::type 
               > const _18 = {{}};
     converter_expr< proto::terminal< placeholderV3< 20 > >::type 
               > const _19 = {{}};
     converter_expr< proto::terminal< placeholderV3< 21 > >::type 
               > const _20 = {{}};
     converter_expr< proto::terminal< placeholderV3< 22 > >::type 
               > const _21 = {{}};  
     converter_expr< proto::terminal< placeholderV3< 23 > >::type 
               > const _22 = {{}};
     converter_expr< proto::terminal< placeholderV3< 24 > >::type 
               > const _23 = {{}};
     converter_expr< proto::terminal< placeholderV3< 25 > >::type 
               > const _24 = {{}};
     converter_expr< proto::terminal< placeholderV3< 26 > >::type 
               > const _25 = {{}};
     converter_expr< proto::terminal< placeholderV3< 27 > >::type 
               > const _26 = {{}};
     converter_expr< proto::terminal< placeholderV3< 28 > >::type 
               > const _27 = {{}};
     converter_expr< proto::terminal< placeholderV3< 29 > >::type 
               > const _28 = {{}};
     converter_expr< proto::terminal< placeholderV3< 30 > >::type 
               > const _29 = {{}};
     converter_expr< proto::terminal< placeholderV3< 31 > >::type 
               > const _30 = {{}};
     converter_expr< proto::terminal< placeholderV3< 32 > >::type 
               > const _31 = {{}};  
     converter_expr< proto::terminal< placeholderV3< 33 > >::type 
               > const _32 = {{}};
     converter_expr< proto::terminal< placeholderV3< 34 > >::type 
               > const _33 = {{}};
     converter_expr< proto::terminal< placeholderV3< 35 > >::type 
               > const _34 = {{}};
     converter_expr< proto::terminal< placeholderV3< 36 > >::type 
               > const _35 = {{}};
     converter_expr< proto::terminal< placeholderV3< 37 > >::type 
               > const _36 = {{}};
     converter_expr< proto::terminal< placeholderV3< 38 > >::type 
               > const _37 = {{}};
     converter_expr< proto::terminal< placeholderV3< 39 > >::type 
               > const _38 = {{}};
     converter_expr< proto::terminal< placeholderV3< 40 > >::type 
               > const _39 = {{}};

   
     converter_expr< proto::terminal< placeholder_n >::type 
               > const _n = {{}};

     
     converter_expr< proto::terminal< signed_pl >::type 
               > const _s = {{}};
     converter_expr< proto::terminal< unsigned_pl >::type 
               > const _u = {{}};


 converter_expr< proto::terminal< placeholder< 37 > >::type 
               > const _36 = {{}};
     converter_expr< proto::terminal< placeholder< 38 > >::type 
               > const _37 = {{}};
     converter_expr< proto::terminal< placeholder< 39 > >::type 
               > const _38 = {{}};
     converter_expr< proto::terminal< placeholder< 40 > >::type 
               > const _39 = {{}};
               
 converter_expr< proto::terminal< placeholder_n >::type 
               > const _n = {{}};
                    
     // several function objects for use in the DSEL Grammar.
#define SK_UTILITY_BITS_DSEL1_USE_STDC_ABS_FUNC 1
     
     inline types::sint32 absolute( types::sint32 val )
     {
#ifndef SK_UTILITY_BITS_DSEL1_USE_STDC_ABS_FUNC                   
      return (val >= 0) ? val : -val ;
      //if( val >= 0 )
      // return val;
      //else
      // return -val;  //val * -1; 
#else
      return std::abs(val);   
#endif         
     }// end of ...
     
  
     // push back obsoletes:
     template< types::sint32 I >
     byteOrderRep_type push_back_( mpl::integral_c<types::sint32,I> val )
     { // v1
       byteOrderRep_type rep();
       rep.push_back(val::value);
       return rep;
     }//enf of ...
     
     template< types::sint32 I >
     byteOrderRep_type push_back_( byteOrderRep_type rep, 
                          mpl::integral_c<types::sint32,I> val )
     { // v2
       rep.push_back(val::value);
       return rep;
     }//enf of ...
     
     struct push_back_ : proto::callable
     { // v3
       typedef byteOrderRep_type & return_type;
       template< types::sint32 I >
       return_type operator()( mpl::integral_c<types::sint32,I>,
                               return_type stat_, types::sint32 s )
       {
         /* if( stat_.size() < s ) */
         stat_.push_back(I);
         return stat_;
       }// end of ...
     };
     
     struct push_back_ : proto::callable
     { // v4
       typedef byteOrderRep_type & return_type;
       
       return_type operator()( types::sint32 i, return_type stat_, 
                               types::sint32 s )
       {
         if( stat_.size() < s - 1 && (i <= s && i > 0 )  ) 
         { stat_.push_back(i); }
         return stat_;
       }// end of ...
     };
     
     // negate obsoletes:
     byteOrderRep_type negate_( byteOrderRep_type rep )
     { // v1
      if( rep.size() > 0 )
      {
       rep.at( rep.size() - 1 ) = - rep.at( rep.size() - 1 );
      }
      return rep;
     }//end of ...
     
      struct negate_ : proto::callable
     { // v2
       typedef byteOrderRep_type & return_type;
       
       return_type operator()( return_type val_, return_type stat_, 
                               types::sint32 s )
       {
        if( val_.size() > 0 /* && stat_.size() < s */ )
        {
         stat_.push_back( - val_.at( val_.size()-1 ) );
        }
        return stat_;
       }// end of ..
     };
     
     struct negate_ : proto::callable
     { // v3
       typedef byteOrderRep_type & return_type;
       
       return_type operator()( types::sint32 val_, return_type stat_, 
                               types::sint32 s )
       {
        if( stat_.size() < s - 1 && 
            ( absolute(val_) <= s && 
              absolute(val_) > 0 ) 
          )
        {
         stat_.push_back( - absolute(val_) );
        }
        return stat_;
       }// end of ..
     };
     
     // merge obsoletes:
     byteOrderRep_type merge_( byteOrderRep_type rep1,  byteOrderRep_type rep2 )
     { // v1
      for( byteOrderRep_type::size_type i = 0; i < rep2.size(); i++)
      { rep1.push_back( rep2.at(i); }
      return rep1;
     }// end of ...
     
     struct merge_ : proto::callable
     { // v2
       typedef byteOrderRep_type & return_type;
       
       return_type operator()( return_type val, return_type stat, 
                               types::sint32 s )
       {              
        //  if( stat_.size() + val.size() < s )       
        for( return_type::size_type i = 0; i < val.size(); i++)
        { stat.push_back( val.at(i); }
        return stat;
       }//end of...
     };
     
     struct merge_ : proto::callable
     { // v3
       typedef byteOrderRep_type & return_type;
       
       return_type operator()( return_type left, return_type right, 
                               types::sint32 s )
       {              
        if( left.size() + right.size() < s )       
        {
         for( return_type::size_type i = 0; i < right.size(); i++)
         { left.push_back( right.at(i); }
        }
        return left;
       }//end of...
     };
     
     
     struct placeholders_1_8 // v1
     : proto::or_<  
       proto::when<  
         proto::terminal< placeholder<1> >,
         push_back_( mpl::integral_c<types::sint32,1>(), proto::_state, proto::_data ) 
                  >,
       proto::when< 
         proto::terminal< placeholder<2> >,
         push_back_( mpl::integral_c<types::sint32,2>(), proto::_state, proto::_data ) 
                  >,         
       proto::when< 
         proto::terminal< placeholder<3> >,          
         push_back_( mpl::integral_c<types::sint32,3>(), proto::_state, proto::_data ) 
                  >,          
       proto::when< 
         proto::terminal< placeholder<4> >,            
         push_back_( mpl::integral_c<types::sint32,4>(), proto::_state, proto::_data) 
                  >,         
       proto::when< 
         proto::terminal< placeholder<5> >,            
         push_back_( mpl::integral_c<types::sint32,5>(), proto::_state, proto::_data) 
                  >,          
       proto::when< 
         proto::terminal< placeholder<6> >,            
         push_back_( mpl::integral_c<types::sint32,6>(), proto::_state, proto::_data ) 
                  >,          
       proto::when< 
         proto::terminal< placeholder<7> >,            
         push_back_( mpl::integral_c<types::sint32,7>(), proto::_state, proto::_data ) 
                  >,          
       proto::when< 
         proto::terminal< placeholder<8> >,            
         push_back_( mpl::integral_c<types::sint32,8>(), proto::_state, proto::_data) 
                  >          
              >
     {};
     
     /* // v1
       proto::when< 
                    
        push_back_( mpl::integral_c<types::sint32,X>() ) 
                  >,
     */             
     struct placeholders_9_16
     : proto::or_< 
         proto::when< 
           proto::terminal< placeholder<9> >,         
           push_back_( mpl::integral_c<types::sint32,9>(), proto::_state, proto::_data ) 
                  >,
         proto::when< 
           proto::terminal< placeholder<10> >,         
           push_back_( mpl::integral_c<types::sint32,10>(), proto::_state, proto::_data ) 
                     >,
         proto::when< 
           proto::terminal< placeholder<11> >,         
           push_back_( mpl::integral_c<types::sint32,11>(), proto::_state, proto::_data ) 
                     >,      
         proto::when< 
           proto::terminal< placeholder<12> >,         
           push_back_( mpl::integral_c<types::sint32,12>(), proto::_state, proto::_data ) 
                     >,     
         proto::when< 
           proto::terminal< placeholder<13> >,         
           push_back_( mpl::integral_c<types::sint32,13>(), proto::_state, proto::_data ) 
                     >,     
         proto::when< 
           proto::terminal< placeholder<14> >,         
           push_back_( mpl::integral_c<types::sint32,14>(), proto::_state, proto::_data ) 
                     >,     
         proto::when< 
           proto::terminal< placeholder<15> >,         
           push_back_( mpl::integral_c<types::sint32,15>(), proto::_state, proto::_data ) 
                     >,     
         proto::when< 
           proto::terminal< placeholder<16> >,         
           push_back_( mpl::integral_c<types::sint32,16>(), proto::_state, proto::_data ) 
                    >   
              
                 >
     {};
     /*
          proto::when< 
                    
           push_back_( proto::_state,
                       mpl::integral_c<types::sint32,1>() ) 
                     >,
     */ 
     struct placeholders_17_24
     : proto::or_< 
         proto::when< 
           proto::terminal< placeholder<17> >,         
           push_back_( mpl::integral_c<types::sint32,17>(), proto::_state, proto::_data ) 
                     >,
         proto::when< 
           proto::terminal< placeholder<18> >,         
           push_back_( mpl::integral_c<types::sint32,18>(), proto::_state, proto::_data ) 
                     >,     
         proto::when< 
           proto::terminal< placeholder<19> >,         
           push_back_( mpl::integral_c<types::sint32,19>(), proto::_state, proto::_data ) 
                     >,     
         proto::when< 
           proto::terminal< placeholder<20> >,        
           push_back_( mpl::integral_c<types::sint32,20>(), proto::_state, proto::_data ) 
                     >,     
         proto::when< 
           proto::terminal< placeholder<21> >,         
           push_back_( mpl::integral_c<types::sint32,21>(), proto::_state, proto::_data ) 
                     >,     
         proto::when< 
           proto::terminal< placeholder<22> >,         
           push_back_( mpl::integral_c<types::sint32,22>(), proto::_state, proto::_data ) 
                     >,     
         proto::when< 
           proto::terminal< placeholder<23> >,         
           push_back_( mpl::integral_c<types::sint32,23>(), proto::_state, proto::_data ) 
                     >,     
         proto::when< 
           proto::terminal< placeholder<24> >,         
           push_back_( mpl::integral_c<types::sint32,24>(), proto::_state, proto::_data ) 
                     >     
               >
     {};
     /*
          proto::when< 
                    
           push_back_( mpl::integral_c<types::sint32,2X>() ) 
                     >,
     */ 
     struct placeholders_25_32
     : proto::or_< 
          proto::when< 
           proto::terminal< placeholder<25> >,         
           push_back_( mpl::integral_c<types::sint32,25>(), proto::_state, proto::_data ) 
                     >,
          proto::when< 
           proto::terminal< placeholder<26> >,         
           push_back_( mpl::integral_c<types::sint32,26>(), proto::_state, proto::_data ) 
                     >,    
          proto::when< 
           proto::terminal< placeholder<27> >,         
           push_back_( mpl::integral_c<types::sint32,27>(), proto::_state, proto::_data ) 
                     >,    
          proto::when< 
           proto::terminal< placeholder<28> >,         
           push_back_( mpl::integral_c<types::sint32,28>(), proto::_state, proto::_data ) 
                     >,    
          proto::when< 
           proto::terminal< placeholder<29> >,         
           push_back_( mpl::integral_c<types::sint32,29>(), proto::_state, proto::_data ) 
                     >,    
          proto::when< 
           proto::terminal< placeholder<30> >,         
           push_back_( mpl::integral_c<types::sint32,30>(), proto::_state, proto::_data ) 
                     >,
          proto::when< 
           proto::terminal< placeholder<31> >,         
           push_back_( mpl::integral_c<types::sint32,31>(), proto::_state, proto::_data ) 
                     >,    
          proto::when< 
           proto::terminal< placeholder<32> >,         
           push_back_( mpl::integral_c<types::sint32,32>(), proto::_state, proto::_data ) 
                     >,    
                 >
     {};
     /*
          proto::when< 
                    
           push_back_( mpl::integral_c<types::sint32,3X>() ) 
                     >,
     */ 
     struct placeholders_33_40
     : proto::or_< 
          proto::when< 
           proto::terminal< placeholder<33> >,         
           push_back_( mpl::integral_c<types::sint32,33>(), proto::_state, proto::_data )
                     >,
          proto::when< 
           proto::terminal< placeholder<34> >,         
           push_back_( mpl::integral_c<types::sint32,34>(), proto::_state, proto::_data )
                     >,    
          proto::when< 
           proto::terminal< placeholder<35> >,         
           push_back_( mpl::integral_c<types::sint32,35>(), proto::_state, proto::_data )
                     >,    
          proto::when< 
           proto::terminal< placeholder<36> >,         
           push_back_( mpl::integral_c<types::sint32,36>(), proto::_state, proto::_data )
                     >,    
          proto::when< 
           proto::terminal< placeholder<37> >,         
           push_back_( mpl::integral_c<types::sint32,37>(), proto::_state, proto::_data )
                     >,    
          proto::when< 
           proto::terminal< placeholder<38> >,         
           push_back_( mpl::integral_c<types::sint32,38>(), proto::_state, proto::_data )
                     >,    
          proto::when< 
           proto::terminal< placeholder<39> >,         
           push_back_( mpl::integral_c<types::sint32,39>(), proto::_state, proto::_data )
                     >,    
          proto::when< 
           proto::terminal< placeholder<40> >,         
           push_back_( mpl::integral_c<types::sint32,40>(), proto::_state, proto::_data )
                     >    
                >
     {};
     /*
          proto::when< 
                    
           push_back_( proto::_state,
                       mpl::integral_c<types::sint32,3>() ) 
                     >,
                     
                     mpl::integral_c< types::sint32,
                              proto::_value(proto::_right) >()
     */
     
     /*
          proto::when< 
                    
             proto::_state 
                     >,
     */
     /*
      struct placeholder_usage
     : proto::or_< 
          proto::when< 
           placeholders_t         
           byteOrderRep_type(proto::_child)
                     >,
         proto::when< 
           proto::negate< placeholders_t >,         
           negate_( proto::_child, proto::_state, proto::_data ) 
                    >
                 >
     {};
     */
     
     struct placeholders_1_8 // v2
     : proto::or_<  
       proto::when<  
         proto::terminal< placeholder<1> >,
         push_back_( types::sint32(1), proto::_state, proto::_data ) 
                  >,
       proto::when< 
         proto::terminal< placeholder<2> >,
         push_back_( types::sint32(2), proto::_state, proto::_data ) 
                  >,         
       proto::when< 
         proto::terminal< placeholder<3> >,          
         push_back_( types::sint32(3), proto::_state, proto::_data ) 
                  >,          
       proto::when< 
         proto::terminal< placeholder<4> >,            
         push_back_( types::sint32(4), proto::_state, proto::_data) 
                  >,         
       proto::when< 
         proto::terminal< placeholder<5> >,            
         push_back_( types::sint32(5), proto::_state, proto::_data) 
                  >,          
       proto::when< 
         proto::terminal< placeholder<6> >,            
         push_back_( types::sint32(6), proto::_state, proto::_data ) 
                  >,          
       proto::when< 
         proto::terminal< placeholder<7> >,            
         push_back_( types::sint32(7), proto::_state, proto::_data ) 
                  >,          
       proto::when< 
         proto::terminal< placeholder<8> >,            
         push_back_( types::sint32(8), proto::_state, proto::_data) 
                  >          
              >
     {};
     
      struct placeholders_1_8 // v3
     : proto::or_<  
       proto::when<  
         proto::terminal< placeholder<1> >,
         types::sint32(1) 
                  >,
       proto::when< 
         proto::terminal< placeholder<2> >,
         types::sint32(2) 
                  >,         
       proto::when< 
         proto::terminal< placeholder<3> >,          
         types::sint32(3) 
                  >,          
       proto::when< 
         proto::terminal< placeholder<4> >,            
         types::sint32(4) 
                  >,         
       proto::when< 
         proto::terminal< placeholder<5> >,            
         types::sint32(5) 
                  >,          
       proto::when< 
         proto::terminal< placeholder<6> >,            
         types::sint32(6) 
                  >,          
       proto::when< 
         proto::terminal< placeholder<7> >,            
         types::sint32(7) 
                  >,          
       proto::when< 
         proto::terminal< placeholder<8> >,            
         types::sint32(8) 
                  >          
              >
     {};
     
     /* // v1
       proto::when< 
                    
        push_back_( types::sint32(X) ) 
                  >,
     */             
     struct placeholders_9_16 // v2
     : proto::or_< 
         proto::when< 
           proto::terminal< placeholder<9> >,         
           push_back_( types::sint32(9), proto::_state, proto::_data ) 
                  >,
         proto::when< 
           proto::terminal< placeholder<10> >,         
           push_back_( types::sint32(10), proto::_state, proto::_data ) 
                     >,
         proto::when< 
           proto::terminal< placeholder<11> >,         
           push_back_( types::sint32(11), proto::_state, proto::_data ) 
                     >,      
         proto::when< 
           proto::terminal< placeholder<12> >,         
           push_back_( types::sint32(12), proto::_state, proto::_data ) 
                     >,     
         proto::when< 
           proto::terminal< placeholder<13> >,         
           push_back_( types::sint32(13), proto::_state, proto::_data ) 
                     >,     
         proto::when< 
           proto::terminal< placeholder<14> >,         
           push_back_( types::sint32(14), proto::_state, proto::_data ) 
                     >,     
         proto::when< 
           proto::terminal< placeholder<15> >,         
           push_back_( types::sint32(15), proto::_state, proto::_data ) 
                     >,     
         proto::when< 
           proto::terminal< placeholder<16> >,         
           push_back_( types::sint32(16), proto::_state, proto::_data ) 
                    >   
              
                 >
     {};
     
     struct placeholders_9_16 // v3
     : proto::or_< 
         proto::when< 
           proto::terminal< placeholder<9> >,         
           types::sint32(9) 
                  >,
         proto::when< 
           proto::terminal< placeholder<10> >,         
           types::sint32(10)
                     >,
         proto::when< 
           proto::terminal< placeholder<11> >,         
          types::sint32(11) 
                     >,      
         proto::when< 
           proto::terminal< placeholder<12> >,         
           types::sint32(12)
                     >,     
         proto::when< 
           proto::terminal< placeholder<13> >,         
           types::sint32(13)
                     >,     
         proto::when< 
           proto::terminal< placeholder<14> >,         
           types::sint32(14) 
                     >,     
         proto::when< 
           proto::terminal< placeholder<15> >,         
           types::sint32(15) 
                     >,     
         proto::when< 
           proto::terminal< placeholder<16> >,         
           types::sint32(16)
                    >   
              
                 >
     {};
     /*
          proto::when< 
                    
           push_back_( proto::_state,
                       types::sint32(X) ) 
                     >,
     */ 
     struct placeholders_17_24 // v2
     : proto::or_< 
         proto::when< 
           proto::terminal< placeholder<17> >,         
           push_back_( types::sint32(17), proto::_state, proto::_data ) 
                     >,
         proto::when< 
           proto::terminal< placeholder<18> >,         
           push_back_( types::sint32(18), proto::_state, proto::_data ) 
                     >,     
         proto::when< 
           proto::terminal< placeholder<19> >,         
           push_back_( types::sint32(19), proto::_state, proto::_data ) 
                     >,     
         proto::when< 
           proto::terminal< placeholder<20> >,        
           push_back_( types::sint32(20), proto::_state, proto::_data ) 
                     >,     
         proto::when< 
           proto::terminal< placeholder<21> >,         
           push_back_( types::sint32(21), proto::_state, proto::_data ) 
                     >,     
         proto::when< 
           proto::terminal< placeholder<22> >,         
           push_back_( types::sint32(22), proto::_state, proto::_data ) 
                     >,     
         proto::when< 
           proto::terminal< placeholder<23> >,         
           push_back_( types::sint32(23), proto::_state, proto::_data ) 
                     >,     
         proto::when< 
           proto::terminal< placeholder<24> >,         
           push_back_( types::sint32(24), proto::_state, proto::_data ) 
                     >     
               >
     {};
     
     struct placeholders_17_24 // v3
     : proto::or_< 
         proto::when< 
           proto::terminal< placeholder<17> >,         
           types::sint32(17)
                     >,
         proto::when< 
           proto::terminal< placeholder<18> >,         
           types::sint32(18) 
                     >,     
         proto::when< 
           proto::terminal< placeholder<19> >,         
           types::sint32(19) 
                     >,     
         proto::when< 
           proto::terminal< placeholder<20> >,        
           types::sint32(20) 
                     >,     
         proto::when< 
           proto::terminal< placeholder<21> >,         
           types::sint32(21) 
                     >,     
         proto::when< 
           proto::terminal< placeholder<22> >,         
           types::sint32(22) 
                     >,     
         proto::when< 
           proto::terminal< placeholder<23> >,         
           types::sint32(23) 
                     >,     
         proto::when< 
           proto::terminal< placeholder<24> >,         
           types::sint32(24)
                     >     
               >
     {};
     
     /*
          proto::when< 
                    
           push_back_( types::sint32(2X) ) 
                     >,
     */ 
     struct placeholders_25_32 // v2
     : proto::or_< 
          proto::when< 
           proto::terminal< placeholder<25> >,         
           push_back_( types::sint32(25), proto::_state, proto::_data ) 
                     >,
          proto::when< 
           proto::terminal< placeholder<26> >,         
           push_back_( types::sint32(26), proto::_state, proto::_data ) 
                     >,    
          proto::when< 
           proto::terminal< placeholder<27> >,         
           push_back_( types::sint32(27), proto::_state, proto::_data ) 
                     >,    
          proto::when< 
           proto::terminal< placeholder<28> >,         
           push_back_( types::sint32(28), proto::_state, proto::_data ) 
                     >,    
          proto::when< 
           proto::terminal< placeholder<29> >,         
           push_back_( types::sint32(29), proto::_state, proto::_data ) 
                     >,    
          proto::when< 
           proto::terminal< placeholder<30> >,         
           push_back_( types::sint32(30), proto::_state, proto::_data ) 
                     >,
          proto::when< 
           proto::terminal< placeholder<31> >,         
           push_back_( types::sint32(31), proto::_state, proto::_data ) 
                     >,    
          proto::when< 
           proto::terminal< placeholder<32> >,         
           push_back_( types::sint32(32), proto::_state, proto::_data ) 
                     >,    
                 >
     {};
     
     struct placeholders_25_32 // v3
     : proto::or_< 
          proto::when< 
           proto::terminal< placeholder<25> >,         
           types::sint32(25) 
                     >,
          proto::when< 
           proto::terminal< placeholder<26> >,         
           types::sint32(26) 
                     >,    
          proto::when< 
           proto::terminal< placeholder<27> >,         
           types::sint32(27)
                     >,    
          proto::when< 
           proto::terminal< placeholder<28> >,         
           types::sint32(28) 
                     >,    
          proto::when< 
           proto::terminal< placeholder<29> >,         
           types::sint32(29) 
                     >,    
          proto::when< 
           proto::terminal< placeholder<30> >,         
           types::sint32(30) 
                     >,
          proto::when< 
           proto::terminal< placeholder<31> >,         
           types::sint32(31)
                     >,    
          proto::when< 
           proto::terminal< placeholder<32> >,         
           types::sint32(32) 
                     >,    
                 >
     {};
     
     
     /*
          proto::when< 
                    
           push_back_( mpl::integral_c<types::sint32,3X>() ) 
                     >,
     */ 
     
     struct placeholders_33_40 // v2
     : proto::or_< 
          proto::when< 
           proto::terminal< placeholder<33> >,         
           push_back_( types::sint32(33), proto::_state, proto::_data )
                     >,
          proto::when< 
           proto::terminal< placeholder<34> >,         
           push_back_( types::sint32(34), proto::_state, proto::_data )
                     >,    
          proto::when< 
           proto::terminal< placeholder<35> >,         
           push_back_( types::sint32(35), proto::_state, proto::_data )
                     >,    
          proto::when< 
           proto::terminal< placeholder<36> >,         
           push_back_( types::sint32(36), proto::_state, proto::_data )
                     >,    
          proto::when< 
           proto::terminal< placeholder<37> >,         
           push_back_( types::sint32(37), proto::_state, proto::_data )
                     >,    
          proto::when< 
           proto::terminal< placeholder<38> >,         
           push_back_( types::sint32(38), proto::_state, proto::_data )
                     >,    
          proto::when< 
           proto::terminal< placeholder<39> >,         
           push_back_( types::sint32(39), proto::_state, proto::_data )
                     >,    
          proto::when< 
           proto::terminal< placeholder<40> >,         
           push_back_( types::sint32(40), proto::_state, proto::_data )
                     >    
                >
     {};
     
     struct placeholders_33_40 // v3
     : proto::or_< 
          proto::when< 
           proto::terminal< placeholder<33> >,         
           types::sint32(33)
                     >,
          proto::when< 
           proto::terminal< placeholder<34> >,         
           types::sint32(34)
                     >,    
          proto::when< 
           proto::terminal< placeholder<35> >,         
           types::sint32(35)
                     >,    
          proto::when< 
           proto::terminal< placeholder<36> >,         
           types::sint32(36)
                     >,    
          proto::when< 
           proto::terminal< placeholder<37> >,         
           types::sint32(37)
                     >,    
          proto::when< 
           proto::terminal< placeholder<38> >,         
           types::sint32(38)
                     >,    
          proto::when< 
           proto::terminal< placeholder<39> >,         
           types::sint32(39)
                     >,    
          proto::when< 
           proto::terminal< placeholder<40> >,         
           types::sint32(40)
                     >    
                >
     {};
     
     
     /*
          proto::when< 
                    
           push_back_( proto::_state,
                       mpl::integral_c<types::sint32,3>() ) 
                     >,
                     
                     mpl::integral_c< types::sint32,
                              proto::_value(proto::_right) >()
     */
     
     struct litteral  // for proto::subscript< placeholder_n, litteral >
     : proto::when< //v1
          proto::terminal< proto::convertible_to< types::sint32 > >,
          mpl::integral_c< types::sint32, proto::_value >()          
                  > 
     {}; 
     
     struct litteral  // for proto::subscript< placeholder_n, litteral >
     : proto::when< //v2
          proto::terminal< proto::convertible_to< types::sint32 > >,
          proto::integral_c< types::sint32, proto::_value >()          
                  > 
     {}; 
     
     struct placeholders_t
     : proto::or_<  // v1
          proto::when< 
            placeholders_1_8,        
            merge_( proto::_child, proto::_state, proto::_data )
                     >,
          proto::when< 
             placeholders_9_16,       
             merge_( proto::_child, proto::_state, proto::_data )
                     >,    
          proto::when< 
             placeholders_17_24,       
             merge_( proto::_child, proto::_state, proto::_data )
                     >,    
          proto::when< 
             placeholders_25_32,       
             merge_( proto::_child, proto::_state, proto::_data )
                     >,    
          proto::when< 
             placeholders_33_40,       
             merge_( proto::_child, proto::_state, proto::_data )
                     >,    
          proto::when< 
           proto::subscript< placeholder_n , litteral >,         
           push_back_( proto::_value(proto::_right), proto::_state, proto::_data ) 
                     >,    
              
                 >
     {};
     
     struct placeholders_t
     : proto::or_<  // v2
          proto::when< 
            placeholders_1_8,        
            proto::_value( proto::_child )
                     >,
          proto::when< 
            placeholders_9_16,       
            proto::_value( proto::_child )
                     >,    
          proto::when< 
            placeholders_17_24,       
            proto::_value( proto::_child )
                     >,    
          proto::when< 
            placeholders_25_32,       
            proto::_value( proto::_child )
                     >,    
          proto::when< 
            placeholders_33_40,       
            proto::_value( proto::_child )
                     >,    
          proto::when< 
            proto::subscript< placeholder_n , litteral >,         
            proto::_value(proto::_right)
                     >,    
              
                 >
     {};
     
     struct placeholders_t
     : proto::or_<  // v3
          proto::when< 
            placeholders_1_8,        
            proto::_value( proto::_child )
                     >,
          proto::when< 
            placeholders_9_16,       
            proto::_value( proto::_child )
                     >,    
          proto::when< 
            placeholders_17_24,       
            proto::_value( proto::_child )
                     >,    
          proto::when< 
            placeholders_25_32,       
            proto::_value( proto::_child )
                     >,    
          proto::when< 
            placeholders_33_40,       
            proto::_value( proto::_child )
                     >,    
          proto::when< 
            proto::subscript< 
              placeholder_n,
              proto::terminal< 
                proto::convertible_to< types::sint32 > 
                             > 
                            >,         
            proto::_value(proto::_right)
                     >,    
              
                 >
     {};
     
     struct converter_grammar 
     : proto::or_< // v1
          //  proto::when< 
          //   placeholder_usage,       
          //   byteOrderRep_type(proto::_child)
          //           >, // push_back_( proto::_value( proto::_child )
          
          proto::when< 
           placeholders_t         
           push_back_( proto::_child, proto::_state, proto::_data )
                     >,
          proto::when< 
           proto::negate< placeholders_t >,         
           negate__( converter_grammar(proto::_child), proto::_state, proto::_data ) 
                    >,
          
          proto::when< 
            proto::shift_left< converter_grammar, converter_grammar >,
            merge_( converter_grammar(proto::_left),
                    converter_grammar(proto::_right), proto::_data )
                       >
                 > 
     {};
     
     struct converter_grammar 
     : proto::or_< // v2
          //  proto::when< 
          //   placeholder_usage,       
          //   byteOrderRep_type(proto::_child)
          //           >, // push_back_( proto::_value( proto::_child )
          
          proto::when< 
           placeholders_t         
           push_back_( proto::_value( proto::_child ),
                       proto::_state, proto::_data )
                     >,
          proto::when< 
           proto::negate< placeholders_t >,         
           negate_( proto::_value(proto::_child), 
                     proto::_state, proto::_data ) 
                    >,
          
          proto::when< 
            proto::shift_left< converter_grammar, converter_grammar >,
            merge_( converter_grammar(proto::_left),
                    converter_grammar(proto::_right), proto::_data )
                       >
                 > 
     {};
     
     struct converter_grammar 
     : proto::or_< // v3
          //  proto::when< 
          //   placeholder_usage,       
          //   byteOrderRep_type(proto::_child)
          //           >, // push_back_( proto::_value( proto::_child )
          
          proto::when< 
           placeholders_t         
           push_back_( proto::_value( placeholders_t ),
                       proto::_state, proto::_data )
                     >,
          proto::when< 
           proto::negate< placeholders_t >,         
           negate_( proto::_value( placeholders_t ), 
                     proto::_state, proto::_data ) 
                    >,
          
          proto::when< 
            proto::shift_left< converter_grammar, converter_grammar >,
           merge_(  merge_( proto::_state, converter_grammar(proto::_left), 
                            proto::_data ),
                    merge_( proto::_state, converter_grammar(proto::_right), 
                            proto::_data ), 
                    proto::_data )
                       >
                 > 
     {};
     
     struct converter_grammar 
     : proto::or_< // v4
          proto::when< 
           placeholders_t         
           push_back_( proto::_value( placeholders_t ),
                       proto::_state, proto::_data )
                     >,
          proto::when< 
           proto::negate< placeholders_t >,         
           negate_( proto::_value( placeholders_t ), 
                     proto::_state, proto::_data ) 
                    >,
          // we dropped merge_ because we don`t need it! 
          proto::when< 
           proto::shift_left< converter_grammar, converter_grammar >,
           converter_grammar(
              proto::_right,
              converter_grammar(proto::_left, proto::_state, proto::_data),
              proto::_data )
                       >
                 > 
     {};
     // v5
     
        /* // v4
       proto::when< 
                    
        proto::integral_c< types::sint32, X > 
                  >,
     */ 
     struct placeholders_1_8 // v3
     : proto::or_<  
       proto::when<  
         proto::terminal< placeholder<1> >,
         proto::integral_c< types::sint32,1>
                  >,
       proto::when< 
         proto::terminal< placeholder<2> >,
         proto::integral_c< types::sint32,2>
                  >,         
       proto::when< 
         proto::terminal< placeholder<3> >,          
         proto::integral_c< types::sint32,3>
                  >,          
       proto::when< 
         proto::terminal< placeholder<4> >,            
         proto::integral_c< types::sint32,4>
                  >,         
       proto::when< 
         proto::terminal< placeholder<5> >,            
         proto::integral_c< types::sint32,5>
                  >,          
       proto::when< 
         proto::terminal< placeholder<6> >,            
         proto::integral_c< types::sint32,6>
                  >,          
       proto::when< 
         proto::terminal< placeholder<7> >,            
         proto::integral_c< types::sint32,7>
                  >,          
       proto::when< 
         proto::terminal< placeholder<8> >,            
         proto::integral_c< types::sint32,8>
                  >          
              >
     {};
     
     /* // v4
       proto::when< 
                    
        proto::integral_c< types::sint32,X> 
                  >,
     */             
     struct placeholders_9_16 // v3
     : proto::or_< 
         proto::when< 
           proto::terminal< placeholder<9> >,         
           proto::integral_c< types::sint32,9> 
                  >,
         proto::when< 
           proto::terminal< placeholder<10> >,         
           proto::integral_c< types::sint32,10>
                     >,
         proto::when< 
           proto::terminal< placeholder<11> >,         
           proto::integral_c< types::sint32,11> 
                     >,      
         proto::when< 
           proto::terminal< placeholder<12> >,         
           proto::integral_c< types::sint32,12>
                     >,     
         proto::when< 
           proto::terminal< placeholder<13> >,         
           proto::integral_c< types::sint32,13>
                     >,     
         proto::when< 
           proto::terminal< placeholder<14> >,         
           proto::integral_c< types::sint32,14> 
                     >,     
         proto::when< 
           proto::terminal< placeholder<15> >,         
           proto::integral_c< types::sint32,15> 
                     >,     
         proto::when< 
           proto::terminal< placeholder<16> >,         
           proto::integral_c< types::sint32,16>
                    >   
              
                 >
     {};
     /*
          proto::when< 
                    
           proto::integral_c< types::sint32,X> 
                     >,
     */ 
     struct placeholders_17_24 // v4
     : proto::or_< 
         proto::when< 
           proto::terminal< placeholder<17> >,         
           proto::integral_c< types::sint32,17>
                     >,
         proto::when< 
           proto::terminal< placeholder<18> >,         
           proto::integral_c< types::sint32,18> 
                     >,     
         proto::when< 
           proto::terminal< placeholder<19> >,         
           proto::integral_c< types::sint32,19> 
                     >,     
         proto::when< 
           proto::terminal< placeholder<20> >,        
           proto::integral_c< types::sint32,20> 
                     >,     
         proto::when< 
           proto::terminal< placeholder<21> >,         
           proto::integral_c< types::sint32,21> 
                     >,     
         proto::when< 
           proto::terminal< placeholder<22> >,         
           proto::integral_c< types::sint32,22> 
                     >,     
         proto::when< 
           proto::terminal< placeholder<23> >,         
           proto::integral_c< types::sint32,23> 
                     >,     
         proto::when< 
           proto::terminal< placeholder<24> >,         
           proto::integral_c< types::sint32,24>
                     >     
               >
     {};
     
     /*
          proto::when< 
                    
           proto::integral_c< types::sint32,X> 
                     >,
     */ 
     
     struct placeholders_25_32 // v3
     : proto::or_< 
          proto::when< 
           proto::terminal< placeholder<25> >,         
           proto::integral_c< types::sint32,25> 
                     >,
          proto::when< 
           proto::terminal< placeholder<26> >,         
           proto::integral_c< types::sint32,26> 
                     >,    
          proto::when< 
           proto::terminal< placeholder<27> >,         
           proto::integral_c< types::sint32,27>
                     >,    
          proto::when< 
           proto::terminal< placeholder<28> >,         
           proto::integral_c< types::sint32,28> 
                     >,    
          proto::when< 
           proto::terminal< placeholder<29> >,         
           proto::integral_c< types::sint32,29> 
                     >,    
          proto::when< 
           proto::terminal< placeholder<30> >,         
           proto::integral_c< types::sint32,30> 
                     >,
          proto::when< 
           proto::terminal< placeholder<31> >,         
           proto::integral_c< types::sint32,31>
                     >,    
          proto::when< 
           proto::terminal< placeholder<32> >,         
           proto::integral_c< types::sint32,32> 
                     >,    
                 >
     {};
     
     
     /*
          proto::when< 
                    
           proto::integral_c< types::sint32,X>
                     >,
     */ 
     
     struct placeholders_33_40 // v4
     : proto::or_< 
          proto::when< 
           proto::terminal< placeholder<33> >,         
           proto::integral_c< types::sint32,33>
                     >,
          proto::when< 
           proto::terminal< placeholder<34> >,         
           proto::integral_c< types::sint32,34>
                     >,    
          proto::when< 
           proto::terminal< placeholder<35> >,         
           proto::integral_c< types::sint32,35>
                     >,    
          proto::when< 
           proto::terminal< placeholder<36> >,         
           proto::integral_c< types::sint32,36>
                     >,    
          proto::when< 
           proto::terminal< placeholder<37> >,         
           proto::integral_c< types::sint32,37>
                     >,    
          proto::when< 
           proto::terminal< placeholder<38> >,         
           proto::integral_c< types::sint32,38>
                     >,    
          proto::when< 
           proto::terminal< placeholder<39> >,         
           proto::integral_c< types::sint32,39>
                     >,    
          proto::when< 
           proto::terminal< placeholder<40> >,         
           proto::integral_c< types::sint32,40>
                     >    
                >
     {};
     
     /*
          proto::when< 
                    
           push_back_( proto::_state,
                       mpl::integral_c<types::sint32,3>() ) 
                     >,
                     
                     proto::integral_c< types::sint32,
                              proto::_value(proto::_right) >()
     */
     struct placeholders_t
     : proto::or_<  // v4
          proto::when< 
            placeholders_1_8,        
            placeholders_1_8
                     >,
          proto::when< 
            placeholders_9_16,       
            placeholders_9_16
                     >,    
          proto::when< 
            placeholders_17_24,       
            placeholders_17_24
                     >,    
          proto::when< 
            placeholders_25_32,       
            placeholders_25_32
                     >,    
          proto::when< 
            placeholders_33_40,       
            placeholders_33_40
                     >,    
          proto::when< 
            proto::subscript< 
              placeholder_n,
              proto::terminal< 
                proto::convertible_to< types::sint32 > 
                             > 
                            >,         
            proto::_value(proto::_right) 
                     >    
              
                 >
     {};
  // several function objects for use in the DSEL Grammar.
#define SK_UTILITY_BITS_DSEL1_USE_STDC_ABS_FUNC 1
     
     inline types::sint32 absolute( types::sint32 val )
     {
#ifndef SK_UTILITY_BITS_DSEL1_USE_STDC_ABS_FUNC                   
      return (val >= 0) ? val : -val ;
#else
      return std::abs(val);   
#endif         
     }// end of ...
     
     struct negate_ : proto::callable
     { // v4
       typedef byteOrderRep_type & return_type;
       
       return_type operator()( types::sint32 val_, return_type stat_, 
                               types::sint32 s )
       {
        if( stat_.size() < s - 1 && 
            ( absolute(val_) <= s && 
              absolute(val_) != 0 ) 
          )
        {
         stat_.push_back( - absolute(val_) );// push the negated value
        }                   // of val_; val_ must not be 0.
        return stat_;
       }// end of ..
     };
     
      struct converter_grammar 
     : proto::or_< // v5
          proto::when< 
           placeholders_t         
           push_back_( 
            placeholders_t( proto::_expr, 0, 0 ),
            proto::_state, proto::_data )
                     >,
          proto::when< 
           proto::negate< placeholders_t >,         
           negate_( 
            placeholders_t( proto::_expr, 0, 0 ), 
            proto::_state, proto::_data ) 
                    >,
          
          proto::when< 
           proto::shift_left< converter_grammar, converter_grammar >,
           converter_grammar(
              proto::_right,
              converter_grammar(proto::_left, proto::_state, proto::_data),
              proto::_data )
                       >
                 > 
     {};
     
     
     
    
     
    
     struct push_back_ : proto::callable
     { // v4
       typedef byteOrderRep_type & return_type;
       
       return_type operator()( types::sint32 i, return_type stat_, 
                               types::sint32 s )
       {
         if( stat_.size() < s - 1 && 
            (absolute(i) <= s && 
             absolute(i) != 0 )  ) 
         { stat_.push_back(absolute(i)); }// push the absolute value
         return stat_;                // of i; i must not be 0.
       }// end of ...
     };
     
     // obsolete loacl_endiaess :

     
     namespace _impl
     {
//#define SK_UTILITY_BITS_ENDIANESS_DSEL1_LOCAL_USE_INTERNAL_MAP   1
#undef  SK_UTILITY_BITS_ENDIANESS_DSEL1_LOCAL_USE_INTERNAL_MAP
// by default, for now, we don`t make use of the internal map
// -- in order to save memory.
      class local_encodings
      {
       private: // just in case this isn`t private by default.
       using SK::utility::bits::endianess::dsel1::byteOrderRep_type;
       
#ifdef SK_UTILITY_BITS_ENDIANESS_DSEL1_LOCAL_USE_INTERNAL_MAP       
       typedef std::map< std::string, byteOrderRep_type > map_type;
       typedef map_type::iterator iterator;
       map_type map_m;
#endif       
       
#ifdef SK_UTILITY_BITS_ENDIANESS_DSEL1_LOCAL_USE_INTERNAL_MAP       
         local_encodings( const local_encodings& l ) 
         : map_m( l.map_m )
         {}// end of copy ctor ...
#else
         local_encodings( const local_encodings& )
         {}// end of copy ctor ...
#endif
       public:
         mpl::integer_c< types::sint32, 0xff > current_limit; 
         
         local_encodings( void )
#ifdef SK_UTILITY_BITS_ENDIANESS_DSEL1_LOCAL_USE_INTERNAL_MAP         
         : map_m() 
#endif         
         {
#ifdef SK_UTILITY_BITS_ENDIANESS_DSEL1_LOCAL_USE_INTERNAL_MAP
          /*
           // builtin 8 bits ...
          if(! map_m.insert( std::make_pair( typeid(sc).name(), rep ) ).second )
            {
             std::ostringstream str_m();
             str_m << "class local_encodings::ctor(): unable to"
                  << " insert type`s (" << typeid(c).name() 
                  << ") local encoding into typemap.";
            throw std::runtime_error( str_m.str() );
            }
            byteOrderRep_type rep();
            rep.resize(1,0x1);
            // char ...
            char c;
            // signed char ...
            signed char sc;
            // unsigned char ..
            unsigned char uc;
            // sk types ...
            // sk types uint8 ...
            types::uint8 u;
            // sk types sint8...
            types::sint8 s;
          
          */
          // check< >(); 
          check< char >();
          check< signed char >(); 
          check< unsigned char >(); 
          check< short >();
          check< signed short >();
          check< unsigned short >();
          check< int >();
          check< signed int >();
          check< unsigned int >();
          check< unsigned >();
          check< long >();
          check< signed long >();
          check< unsigned long >();
          check< long long >();
          check< signed long long >();
          check< unsigned long long >();
          // SK types ... 
          // check< >(); 
          check< types::uint8 >();
          check< types::sint8 >();
          check< types::uint16 >();
          check< types::sint16 >();
          check< types::uint32 >();
          check< types::sint32 >();
          check< types::uint64 >();
          check< types::sint64 >();
#endif
         }// end of ctor...;
         
         template< typename T >
         const byteOrderRep_type& check()
         {
          typename T t();
#ifdef SK_UTILITY_BITS_ENDIANESS_DSEL1_LOCAL_USE_INTERNAL_MAP
          iterator iter = map_m.find( std::string( typeid(t).name() ) );
          if( iter != map_m.end() )
            return iter->second;
          else
          {
#endif           
           // we use the bits::setUByte2<T>() function to set the
           //  bytes and then we determine via 
           // "types::uint8* ptr = reinterpret_cast< types::uint8* >
           // ( boost::adressof(i__));" where the bytes wound up,
           // by simplt assigning each byte`s value to the rep.
           // 
           byteOrderRep_type bor(sizeof(T),0);
           if( sizeof(T) > current_limit::value ) 
           {  // we don`t support bigger sized types!
            std::ostringstream str_m();
            str_m << "class local_encodings::check(): Type: " 
                  << typeid(t).name() << endl
                  << " is bigger than " << current_limit::value << " bytes" << endl
                  << " This function does not support checking types that are" << endl
                  << " bigger than " << current_limit::value << " bytes."
            throw std::runtime_error( str_m.str() );    
           }
           for( types::uint8 i = 0; i < sizeof(T) ; i++)
           { bits::setUByte2<T>( i,i,t ); }
           types::uint8* ptr = reinterpret_cast< types::uint8* >
           ( boost::adressof(t));
           for( types::uint8 i = 0; i < sizeof(T) ; i++)
           { bor.at(i) = ( ptr[i] + 1 ); } // plus 1 allows byte ident
            // to be from 1 to sizeof(T) inclusive 
            // - for conversion type below .
#ifdef SK_UTILITY_BITS_ENDIANESS_DSEL1_LOCAL_USE_INTERNAL_MAP
           if( !map_m.insert( std::make_pair( std::string( typeid(t).name() ), bor ) ).second )
           {
            std::ostringstream str_m();
            str_m << "class local_encodings::check(): unable to"
                  << " insert new type`s (" << typeid(t).name() 
                  << ") local encoding into typemap.";
            throw std::runtime_error( str_m.str() );      
           }
#endif
           return bor;
                       
          }
         }// end of ...    
      };
     }// end of namespace _impl;
                   
                 
     template< typename T >
     const byteOrderRep_type& get_local_encoding()
     {
      static _impl::local_encodings local_encoding_s = local_encodings();
      try
      {
       return local_encoding_s.check<T>();
      }
      catch( std::runtime_error )
      {
       throw;
      }  
     }// end of...
     
     /*
     template< typename T >
     int_type converter<T>::operator()( int_type i__ )
     {
       // attemp 1
       // this code (attemp 1) is fundumentally wrong. 
       // we need to use a find algorithm to find the 
       // i`th byte index as described by from_code_m 
       // and to_code_m in order to properly convert integers. 
       // see note above.             
     int_type ret();
     for( types::uint8 j = 0; j < int_size; j++ )
     {
     types::uint8 index = static_cast< types::uint8 >
                   ( absolute(from_code_m.at(j)) );
     types::uint8 temp = byte( (index - 1 ), i__);
     index = static_cast< types::uint8 >
                 ( absolute(to_code_m.at(j)) );
     byte( (index - 1 ), temp, ret); 
     }
     return ret;
     }// end of ....
     */
     
    } // namespace v1_obsolete
   } // namespace endianess
  } // namespace bits
 } // namespace utility
} // namespace SK
   

#endif
   
