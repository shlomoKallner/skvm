#ifndef SK_UTILITY_BITS_ENDIANESS_FORWARD_HPP
#define SK_UTILITY_BITS_ENDIANESS_FORWARD_HPP


#include <vector>
#include <string>
#include <sstream>
#include <ostream>
#include <typeinfo>

namespace SK
{
 namespace utility
 {
  namespace bits
  {
   namespace endianess
   {
    namespace dsel1  // fromerly v1
    {
     namespace proto  =  boost::proto;
     namespace mpl    =  boost::mpl;
     namespace types  =  SK::utility::types;
     namespace bits   =  SK::utility::bits;
     
     typedef std::vector<types::sint32> byteOrderRep_type;
     
    } // end of namespace dsel1; // fromerly v1
    namespace classic
    {
     namespace mpl = boost::mpl;
     namespace types = SK::utility::types;
     
     enum endianess { null_endianess = 0, big_endianess, 
                      little_endianess,  unknown_endianess };
    
     boolean is_endianess_valid( endianess e )
     {
      return ( e ==  big_endianess ) || ( e == little_endianess ) ;
     } // end of ...

     template< typename CharT, typename TraitsT >
     std::basic_ostream<CharT,TraitsT>& 
     operator<<( std::basic_ostream<CharT,TraitsT>& out, 
                                              endianess e )
     { 
      if( e ==  big_endianess )
        out << "Big " ;
      else if( e ==  little_endianess )
        out << "Little " ;
      else if( e == unknown_endianess )
        out << "Unknown " ;
      else
        out << "Null " ;
      out << "Endianness" ;
      return out;
     } // end of operator<<();
     
     class bad_endianess_exception : public std::exception
     {
       endianess scr_m, dest_m;
      public:
       bad_endian_exception( endianess s, endianess d ) : scr_m(s), dest_m(d){}
       virtual ~ unknown_endian_exception(){}
       virtual const char* what() const throw()
       {
        std::ostringstream o;
        o << " ERROR! Unable to  change the byte order from ( " 
          << scr_m << " ) to ( " << dest_m << " )!" ;
        return o.str().c_str();
       }// end of function();
     };//end of class;
     
     template< endianess e = null_endianess> 
     struct endian;
     
     // predefined endianess typedefs in the form of "typedef endian<E> E_endian;" :

     typedef endian<null_endianess>  nullEndian;

     typedef endian<big_endianess>  bigEndian;

     typedef endian< little_endianess>  littleEndian; 

     typedef endian<unknown_endianess> mixxedEndian; // unimplemented! byteswap won`t work on such a platform!

     typedef endian<unknown_endianess> unknownEndian;
 

     // a querying meta-functions:
     template<typename Endian>
     struct has_endianess_tag 
     : public mpl::false_
     {};
 
     template<>
     struct has_endianess_tag<bigEndian> 
     : public mpl::true_
     {};
     
     template<>
     struct has_endianess_tag<littleEndian> 
     : public mpl::true_
     {};

      template<typename Endian>
      struct is_valid_endianess 
      : public mpl::false_
      {};

template<>
struct is_valid_endianess<bigEndian> : public mpl::true_
{};

template<>
struct is_valid_endianess<littleEndian> : public mpl::true_
{};
  

    }// end of namespace classic;
   } // end of namespace endianess;
  } // end of namespace bits;
 } // end of namespace utility;
} // end of namespace SK;
 
  

#endif
