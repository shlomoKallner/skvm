#ifndef SK_UTILITY_BITS_LOCAL_ENDIANESS_HPP
#define SK_UTILITY_BITS_LOCAL_ENDIANESS_HPP

#include <boost/utility.hpp>

#include "SK/utility/types.hpp"
#include "SK/utility/bits/get_setBytes.hpp"
#include "endianess_fwd.hpp"

namespace SK
{
 namespace utility
 {
  namespace bits
  {
   namespace endianess
   {
    namespace dsel1  // fromerly v1
    {
     namespace proto  =  boost::proto;
     namespace mpl    =  boost::mpl;
     namespace types  =  SK::utility::types;
     namespace bits   =  SK::utility::bits;
     
     mpl::integer_c< types::sint32, 0xff > current_limit;
     
     template< typename T >
     struct get_local_helper : mpl::integer_c< std::size_t, sizeof(T) >
     {
      BOOST_MPL_ASSERT_RELATION( sizeof(T), <=, current_limit::value )
     };
     
     template< typename T >
     const byteOrderRep_type& get_local_encoding()
     {
      
      typename T t();
      get_local_helper< T > helper; 
      byteOrderRep_type bor(helper::value,0);
      try
      {
       // we use the bits::setUByte2<T>() function to set the
       //  bytes and then we determine via 
       // "types::uint8* ptr = reinterpret_cast< types::uint8* >
       // ( boost::adressof(i__));" where the bytes wound up,
       // by simply assigning each byte`s value to the rep.
       
       if( sizeof(T) >  ) 
       {  // we don`t support bigger sized types!
        std::ostringstream str_m();
        str_m << "function get_local_encoding< " << typeid(t).name() 
              << " >():" << endl << "Type: " << typeid(t).name() << endl
              << " is bigger than " << current_limit::value << " bytes" << endl
              << " This function does not support checking types that are" << endl
              << " bigger than " << current_limit::value << " bytes."
        throw std::runtime_error( str_m.str() );    
       }
       for( types::uint8 i = 0; i < sizeof(T) ; i++)
       { bits::setUByte2<T>( i,i,t ); }
       types::uint8* ptr = reinterpret_cast< types::uint8* >
       ( boost::adressof(t));
       for( types::uint8 i = 0; i < sizeof(T) ; i++)
       { bor.at(i) = ( ptr[i] + 1 ); } // plus 1 allows byte ident
        // to be from 1 to sizeof(T) inclusive 
        // - for the conversion type .
      }
      catch( std::runtime_error )
      {
       throw;
      }
      catch(...)
      {
       throw;
      } 
      return bor; 
     }// end of...
     
    } // end of namespace dsel1; // fromerly v1
    namespace classic
    {
     //namespace mpl = boost::mpl;
     //namespace types = SK::utility::types;
     
     
     endianess find_local_endianess(void)
     {
      int i = 1;
      char * c = reinterpret_cast<char*>(&i);
      if( c[0] == 1 )
        return little_endianess; 
      else if( c[sizeof(int)-1] = 1 )
        return big_endianess; 
      else 
        return unknown_endianess; 
     }//end of function();
  

     static const endianess local_endianess = 
#ifdef SK_RUNTIME_FIND_LOCAL_ENDIANESS     
       find_local_endianess();
#elif defined( SK_RUNTIME_BIG_ENDIAN )
       big_endianess ;
#elif defined( SK_RUNTIME_LITTLE_ENDIAN ) 
       little_endianess ;
#else 
       unknown_endianess ;
#endif
     
     
    }// end of namespace classic;
   } // end of namespace endianess;
  } // end of namespace bits;
 } // end of namespace utility;
} // end of namespace SK;
 
     


#endif
