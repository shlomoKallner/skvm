#include <cstdlib>
#include <iostream>
#include <iterator>

#include <boost/spirit/include/karma.hpp>

#include "SK/utility/types.hpp"
#include "dsel1.hpp"

//using namespace std;
namespace types = SK::utility::types;
namespace karma = boost::spirit::karma;
namespace dsel = SK::utility::bits::endianess::dsel1;

int main(int argc, char *argv[])
{
    dsel::byteOrderRep_type bor();
    
    std::system("PAUSE");
    return std::EXIT_SUCCESS;
}
