#ifndef SK_UTILITIES_BITS_GET_HIGHLOW_H
#define SK_UTILITIES_BITS_GET_HIGHLOW_H

#include <exception>
#include <ostream>
#include <sstream>
#include <boost/static_assert.hpp> 
#include <boost/mpl/mpl.hpp>
#include "SK/utility/types.hpp"

namespace SK
{
 namespace utility
 {
  namespace bits
  {
   namespace types = SK::utility::types;
   namespace emul = SK::utility::types::emulation;  
   namespace mpl = boost::mpl;
          
// get_high<X> and get_low<X> functions where X is 8, 16 or 32.

   template< std::size_t N, 
             typename T1 = types::uint<
                 mpl::plus< mpl::size_t< N >, 
                            mpl::size_t< N > 
                          >::type::value
                                      >::type, 
             typename T2 = types::uint<N>::type >
  T2 get_high( T1 t)
  {
   BOOST_STATIC_ASSERT( N == 8 || N == 16 || N == 32 || N == 64 );
   types::High<T1> H();
   return static_cast< T2 >( H(t) >> N );
  }// end of function get_high ( T1 );

  template< std::size_t N, 
             typename T1 = types::uint<
         mpl::plus< mpl::size_t< N >, mpl::size_t< N > >::type::value
                                      >::type,
             typename T2 = types::uint<N>::type >
  T2 get_low( T1 );
  {
   BOOST_STATIC_ASSERT( N == 8 || N == 16 || N == 32 || N == 64 );
   types::Low<T1> L();
   return static_cast< T2 >(  L(t) ); 
  }// end of function get_high ( T1 );


  template< std::size_t N, 
             typename T1 = types::sint<
         mpl::plus< mpl::size_t< N >, mpl::size_t< N > >::type::value
                                      >::type, 
             typename T2 = types::sint<N>::type >
  T2 get_high( T1 t)
  {
    types::High<T1> H();
    return static_cast< T2 >( H( t ) >> N );
  }// end of function get_high ( T1 );
  
  template< std::size_t N, 
             typename T1 = types::sint<
         mpl::plus< mpl::size_t< N >, mpl::size_t< N > >::type::value
                                      >::type,
             typename T2 = types::sint<N>::type >
  T2 get_low( T1 );
  {
    types::Low<T1> L();
    return static_cast< T2 >( L(t) ); 
  }// end of function get_high ( T1 );

   
   



   template<>
   types::uint8 get_high<8,types::uint16,types::uint8>
    ( types::uint16 i )
   {
    types::High< types::uint16 > H();
    return static_cast< types::uint8 >( H(i) >> 8 ); // & 0xff00
   }// end of function get_high ( uint16 );

   template<>
   types::uint8 get_low<8,types::uint16,types::uint8>
    ( types::uint16 i )
   {
    types::Low< types::uint16 > L();
    return static_cast< types::uint8 >( L(i) );& 0x00ff
   }// end of function get_high ( uint16 );

   template<>
   types::sint8 get_high<8,types::sint16,types::sint8>
   ( types::sint16 i )
   {
    types::High< types::sint16 > H();
    return static_cast< types::sint8 >( H(i) >> 8 ); // & 0xff00
   }// end of function get_high ( sint16 );

   template<>
   types::sint8 get_low<8,types::sint16,types::sint8>
   ( types::sint16 i )
   {
    types::Low< types::sint16 > L();
    return static_cast< types::sint8 >( L(i) ); // & 0x00ff
   }// end of function get_high ( sint16 );

   template<>
   types::uint16 get_high<16,types::uint32,types::uint16>
    ( types::uint32 i )
   {
    types::High< types::uint32 > H();
    return static_cast< types::uint16 >( H(i) >> 16 ); // & 0xffff0000
   }// end of function get_high( uint32 );

   template<>
   types::uint16 get_low<16,types::uint32,types::uint16>
    ( types::uint32 i )
   {
    types::Low< types::uint32 > L();
    return static_cast< types::uint16 >( L(i) );//  & 0x0000ffff
   }// end of function get_low( uint16 );

   template<>
   types::sint16 get_high<16,types::sint32,types::sint16>
    ( types::sint32 i )
   {
    types::High< types::sint32 > H();
    return static_cast< types::sint16 >( H(i) >> 16 );//  & 0xffff0000
   }// end of function get_high( sint32 );

   template<>
   types::sint16 get_low<16,types::sint32,types::sint16>
    ( types::sint32 i )
   {
    types::Low< types::sint32 > L();
    return static_cast< types::sint16 >( L(i) );// & 0x0000ffff
   }// end of function get_low( sint32 );

   template<>
   types::uint32 get_high<32,types::uint64,types::uint32>
    ( types::uint64 i )
   {
    types::High< types::uint64 > H();
    return static_cast< types::uint32 >( H(i) >> 32 ); 
     //  & 0xffffffff00000000
   }// end of function get_high( uint64 );

   template<>
   types::uint32 get_low<32,types::uint64,types::uint32>
    ( types::uint64 i )
   {
    types::Low< types::uint64 > L();
    return static_cast< types::uint32 >( L(i) );
    // & 0x00000000ffffffff
   }// end of function get_low( uint64 );

   template<>
   types::sint32 get_high<32,types::sint64,types::sint32>
    ( types::sint64 i )
   {
    types::High< types::sint64 > H();
    return static_cast< types::sint32 >( H(i) >> 32 );
    //  & 0xffffffff00000000
   }// end of function get_high( sint64 );

   template<>
   types::sint32 get_low<32,types::sint64,types::sint32>
    ( types::sint64 i )
   {
    types::Low< types::sint64 > L();
    return static_cast< types::sint32 >( L(i) ); 
    //  & 0x00000000ffffffff
   }// end of function get_low( sint64 );

            
  }// end of namespace bits;
 }// end of namespace utility;
}// end of namespace SK;  
