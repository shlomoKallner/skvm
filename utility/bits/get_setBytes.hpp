#ifndef SK_UTILITIES_BITS_GETSET_BYTES_HPP
#define SK_UTILITIES_BITS_GETSET_BYTES_HPP

#include <stdexcept>
#include <boost/type_traits/is_integral.hpp>
#include "SK/utility/types.hpp"

namespace SK
{
 namespace utility
 {
  namespace bits
  {
   namespace types = SK::utility::types; 
   // byte selection by index ( get the byte`s value )
   
// signed byte selection by index ( get the byte`s value )
   template< typename T /*, types::boolean */ >
   types::sint8 getSByte( types::uint8 byte_, T t )
   { // byte indexxes start from 0 and go to ( sizeof(T) - 1 )
     //  whereas it is believed that sizeof() returns 
     //  number of bytes to the passed type name.            
    if( boost::is_integral<T>::value && 
        sizeof(T)/sizeof(types::uint8) > byte_ &&
        byte_ >= 0 )
    {
     using SK::utility::bitsPerByte;
     types::sint8 i = byte_ == 0 ? 0xff : 
                       0xff << (bitsPerByte * byte_ );
     //types::sint8 i = 0xff;                        
     //i <<= (bitsPerByte * byte_ ); 
     return static_cast<types::sint8>( t & i );
    }
    else 
    { 
     return 0;   // return zero for out of bounds
    }
   }//end of...
   
   // unsigned byte selection by index( get the byte`s value )
   template< typename T /*, types::boolean */ >
   types::uint8 getUByte( types::uint8 byte_, T t )
   { // byte indexxes start from 0 and go to ( sizeof(T) - 1 )
     //  whereas it is believed that sizeof() returns 
     //  number of bytes to the passed type name.            
    if( boost::is_integral<T>::value && 
        sizeof(T)/sizeof(types::uint8) > byte_ &&
        byte_ >= 0 )
    {
     using SK::utility::bitsPerByte;
     types::uint8 i = byte_ == 0 ? 0xff : 
                      0xff << (bitsPerByte * byte_ );
     //types::uint8 i = 0xff;    
     //i <<= (bitsPerByte * byte_ ); 
     return static_cast< types::uint8 >( t & i );
    }
    else 
    { 
     return 0; // return zero for out of bounds
    }
   }//end of...
   
   
   // unsigned byte selection by index( set the byte`s value )
   template< typename T /*, types::boolean */ >
   typename T setUByte1( types::uint8 byte_, types::uint8 val, T t )
   { // byte indexxes start from 0 and go to ( sizeof(T) - 1 )
     //  whereas it is believed that sizeof() returns 
     //  number of bytes to the passed type name.            
    if( boost::is_integral<T>::value && 
        sizeof(T)/sizeof(types::uint8) > byte_ &&
        byte_ >= 0 )
    {
     using SK::utility::bitsPerByte;
     //typename T i( val << (bitsPerByte * byte_ )); 
     typename T i( byte_ == 0 ? val :
                   val << (bitsPerByte * byte_ ));
     return t | i;
    }
    else 
    { 
     return T(0); // return zero for out of bounds
    }
   }//end of...
   
   // signed byte selection by index( set the byte`s value )
   template< typename T /*, types::boolean */ >
   typename T setSByte1( types::uint8 byte_, types::sint8 val, T t )
   { // byte indexxes start from 0 and go to ( sizeof(T) - 1 )
     //  whereas it is believed that sizeof() returns 
     //  number of bytes to the passed type name.            
    if( boost::is_integral<T>::value && 
        sizeof(T)/sizeof(types::uint8) > byte_ &&
        byte_ >= 0 )
    {
     using SK::utility::bitsPerByte;
     //typename T i( val << (bitsPerByte * byte_ ));
     typename T i( static_cast< T >( byte_ == 0 ? val :
                   val << (bitsPerByte * byte_ ) ) );
     return t | i;
    }
    else 
    { 
     return T(0);   // return zero for out of bounds
    }
   }//end of...
   
   
   // set the (byte_) byte`s value in (t) with (val)
   // unsigned byte selection is by index (byte_)
   template< typename T /*, types::boolean */ >
   void setUByte2( types::uint8 byte_, types::uint8 val, T& t )
   { // byte indexxes start from 0 and go to ( sizeof(T) - 1 )
     //  whereas it is believed that sizeof() returns 
     //  number of bytes to the passed type name.            
    if( boost::is_integral<T>::value && 
        sizeof(T)/sizeof(types::uint8) > byte_ &&
        byte_ >= 0 )
    {
     using SK::utility::bitsPerByte;
     typename T i( static_cast< T >( byte_ == 0 ? val :
                   val << (bitsPerByte * byte_ ) ) );
     //typename T i( val << (bitsPerByte * byte_ )); 
     t |= i;
    }
    else 
    { 
      throw std::out_of_range(""); // return zero for out of bounds
    }
   }//end of...
   
   // set the (byte_) byte`s value in (t) with (val)
   // signed byte selection is by index (byte_)
   template< typename T /*, types::boolean */ >
   void setSByte2( types::uint8 byte_, types::sint8 val, T& t )
   { // byte indexxes start from 0 and go to ( sizeof(T) - 1 )
     //  whereas it is believed that sizeof() returns 
     //  number of bytes to the passed type name.            
    if( boost::is_integral<T>::value && 
        sizeof(T)/sizeof(types::uint8) > byte_ &&
        byte_ >= 0 )
    {
     using SK::utility::bitsPerByte;
     typename T i( static_cast< T >( byte_ == 0 ? val :
                   val << (bitsPerByte * byte_ ) ) );
     //typename T i( val << (bitsPerByte * byte_ )); 
     t |= i;
    }
    else 
    { 
     throw std::out_of_range("");   // return zero for out of bounds
    }
   }//end of...
   
  }// end of namespace bits;
 }// end of namespace utility;
}// end of namespace SK;  

#endif
