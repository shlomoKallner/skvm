#ifndef SK_UTILITIES_BITS_GET_HIGHLOW_H
#define SK_UTILITIES_BITS_GET_HIGHLOW_H

#include <exception>
#include <ostream>
#include <sstream>
#include <boost/static_assert.hpp> 
#include <boost/mpl/mpl.hpp>
#include "SK/utility/types.hpp"

namespace SK
{
 namespace utility
 {
  namespace bits
  {
            
   template< std::size_t N, typename T, typename TH, typename TL >
   struct Get
   {
    typedef typename TH high_type;
    typedef typename TL low_type;
    typedef typename T value_type;
    struct high
    {
     typedef high_type return_type;
     high_type operator()( value_type l )const
     {
      types::High<high_type> H();
      return static_cast< high_type >( H(l) >> (N/2) );
     }//end of ....
    }; 
    struct low
    {
     typedef low_type return_type;
     low_type operator()( value_type h )const
     {
      types::Low<low_type> L();
      return static_cast< low_type >( L(t) );
     }//end of ...
    }; 
   };  
   
   template< std::size_t N, typename T, typename TH, typename TL >
   struct Set
   {
    typedef typename TH high_type;
    typedef typename TL low_type;
    typedef typename T value_type;
    typedef value_type return_type;
     value_type operator()( high_type h, low_type l )const
     { // TO BE DONE!!!
      //types::High<high_type> H();
      //return static_cast< value_type >( H(l) >> (N/2) );
     }//end of ....
   };
            
  }// end of namespace bits;
 }// end of namespace utility;
}// end of namespace SK;  
