// byte swap ( endianess ) functions:

   sint16 byteswap( sint16 i )
   {
     sint16 x =  (i & 0x00ff) << 8 ;
     sint16 y =  (i & 0xff00 ) >> 8 ;
     return x | y ; 
   }//end of ... 
   
  sint16 byteswap1( sint16 i )
   {
     return  ( static_cast<sint16>(get_low(i))  << 8 ) | static_cast<sint16>(get_high(i) );
   }//end of ... 

  sint16  byteswap2( sint16 i )
  {
     return make_sint( get_low(i) , get_high(i) ); 
  }//end of ..

   sint32 byteswap( sint32 i )
   {
    sint16 a = byteswap( static_cast<sint16>(i & 0x0000ffff) );
    sint32 x = static_cast<sint32>( byteswap( a ) );
    a = byteswap(  static_cast<sint16>( (i & 0xffff0000 ) >> 16 ) );
    return x |  static_cast<sint32>( a ); 
   }//end of ...

sint32 byteswap1( sint32 i )
   {
    return ( static_cast<sint32>( byteswap( get_low(i) ) ) << 16 ) | static_cast<sint32>( byteswap( get_high(i) ) );
   }//end of ...

sint32 byteswap2( sint32 i )
   {
    return make_sint( get_low(i), get_high(i) ); 
   }//end of ...


   sint64 byteswap( sint64 i )
   {
    // sint32 a = byteswap( static_cast<sint32>(i & 0x00000000ffffffff) );
    // sint64 x = static_cast<sint64>(a) << 32;
    // a = byteswap( static_cast<sint32>( (i & 0xffffffff00000000 ) >> 32 ) );
    // return x | static_cast<sint64>(a);
    // return ( static_cast<sint64>(  byteswap( get_low(i) ) ) << 32 ) | static_cast<sint64>( byteswap( get_high(i) ) ) ;
    return  make_sint( get_low(i), get_high(i) );
   }//end of ...
   
   uint16 byteswap( uint16 i )
   {
    // uint16 x = ( (i & 0x00ff) << 8 );
    // uint16 y = ( (i & 0xff00 ) >> 8 );
    // return x | y ; // (  static_cast<uint16>(get_low(i))  << 8 ) | static_cast<uint16>(get_high(i) ) 
    return make_uint( get_low(i), get_high(i) ); 
   }//end of ... 

   uint32 byteswap( uint32 i )
   {
    // uint16 a = byteswap( static_cast<uint16>(i & 0x0000ffff) );
    // uint32 x = static_cast<uint32>(a) << 16;
    // a = byteswap( static_cast<uint16>( (i & 0xffff0000 ) >> 16 ) );
    // return x | static_cast<uint32>(a); // ( static_cast<uint32>( byteswap( get_low(i) ) ) << 16 ) | static_cast<uint32>( byteswap( get_high(i) ) )
    return make_uint( get_low(i), get_high(i) ); 
   }//end of ...

   uint64 byteswap( uint64 i )
   {
    // uint32 a = byteswap( static_cast<uint32>(i & 0x00000000ffffffff) );
    // uint64 x = static_cast<uint64>(a) << 32;
    // a = byteswap( static_cast<uint32>( (i & 0xffffffff00000000 ) >> 32 ) );
    // return x | static_cast<uint64>(a); //  ( static_cast<uint64>(  byteswap( get_low(i) ) ) << 32 ) | static_cast<uint64>( byteswap( get_high(i) ) ) 
    return make_uint( get_low(i), get_high(i) );
   }//end of ...