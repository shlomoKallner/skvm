#ifndef SK_UTILITIES_BITS_ENDIANESS_CLASSIC_H
#define SK_UTILITIES_BITS_ENDIANESS_CLASSIC_H

#include <exception>
#include <ostream>
#include <sstream>
#include <boost/static_assert.hpp> 
#include <boost/mpl/mpl.hpp>
#include "SK/utility/types.hpp"

namespace SK
{
 namespace utility
 {
  namespace bits
  {
   namespace classic
   {
     namespace mpl = boost::mpl;
     using namespace SK::utility::types; /// TODO: CHANGE THIS TO 
      // "namespace types = SK::utility::types;" and every type "X" to 
      // "types::X" where "X" is the type out of "SK::utility::types".
     
     enum endianess { null_endianess = 0, big_endianess, 
                      little_endianess,  unknown_endianess };
    
     boolean is_endianess_valid( endianess e )
     {
      return ( e ==  big_endianess ) || ( e == little_endianess ) ;
     } // end of ...

     template< typename CharT, typename TraitsT >
     std::ostream<CharT,TraitsT>& operator<<( std::ostream<CharT,TraitsT>& out, 
                                              endianess e )
     { 
      if( e ==  big_endianess )
        out << "Big " ;
      else if( e ==  little_endianess )
        out << "Little " ;
      else if( e == unknown_endianess )
        out << "Unknown " ;
      else
        out << "Null " ;
      out << "Endianness" ;
      return out;
     } // end of operator<<();

     endianess find_local_endianess(void)
     {
      int i = 1;
      char * c = reinterpret_cast<char*>(&i);
      if( c[0] == 1 )
        return little_endianess; 
      else if( c[sizeof(int)-1] = 1 )
        return big_endianess; 
      else 
        return unknown_endianess; 
     }//end of function();
  

     static const endianess local_endianess = 
#ifdef SK_RUNTIME_FIND_LOCAL_ENDIANESS     
       find_local_endianess();
#elif defined( SK_RUNTIME_BIG_ENDIAN )
       big_endianess ;
#elif defined( SK_RUNTIME_LITTLE_ENDIAN ) 
       little_endianess ;
#else 
       unknown_endianess ;
#endif

     class bad_endianess_exception : public std::exception
     {
       endianess scr_m, dest_m;
      public:
       bad_endian_exception( endianess s, endianess d ) : scr_m(s), dest_m(d){}
       virtual ~ unknown_endian_exception(){}
       virtual const char* what() const throw()
       {
        std::ostringstream o;
        o << " ERROR! Unable to  change the byte order from ( " 
          << scr_m << " ) to ( " << dest_m << " )!" ;
        return o.str().c_str();
       }// end of function();
     };//end of class;
    
    // endianess meta-data types with functions for changing the byte order of their argument:
  
  template< endianess e = null_endianess> 
  struct endian
  {
    
   typedef mpl::integral_c<endianess,e> endianess_tag;  
    
  
  static sint8 change_byte_order( sint8 i, endianess = null_endianess ,
                                  endianess = null_endianess )
  {
   return i;
  } // end of...
 
  static sint16 change_byte_order( sint16 i, endianess  scr_e, endianess dest_e )
  {
   if( ( scr_e ==  big_endianess && dest_e ==  little_endianess )
    || ( scr_e ==  little_endianess && dest_e == big_endianess  ) )
     return byteswap(i); // these 2 endianesses are opposites so just swap the byte order.
   else if( scr_e == dest_e )
    return i;
   else  
     throw  bad_endianess_exception( scr_e , dest_e );
  } //end of ....

 static uint16 change_byte_order( uint16 i, endianess  scr_e, endianess dest_e )
  {
   if( ( scr_e ==  big_endianess && dest_e ==  little_endianess )
    || ( scr_e ==  little_endianess && dest_e == big_endianess  ) )
     return byteswap(i); // these 2 endianesses are opposites so just swap the byte order.
   else if( scr_e == dest_e )
    return i;
   else  
     throw  bad_endianess_exception(  scr_e, dest_e );
  } //end of ....

 static sint32 change_byte_order( sint32 i, endianess  scr_e, endianess dest_e )
  {
   if( ( scr_e ==  big_endianess && dest_e ==  little_endianess )
    || ( scr_e ==  little_endianess && dest_e == big_endianess  ) )
     return byteswap(i); // these 2 endianesses are opposites so just swap the byte order.
   else if( scr_e == dest_e )
    return i;
   else  
     throw  bad_endianess_exception(  scr_e, dest_e );
  } //end of ....

 static uint32 change_byte_order( uint32 i, endianess  scr_e, endianess dest_e )
  {
   if( ( scr_e ==  big_endianess && dest_e ==  little_endianess )
    || ( scr_e ==  little_endianess && dest_e == big_endianess  ) )
     return byteswap(i); // these 2 endianesses are opposites so just swap the byte order.
   else if( scr_e == dest_e )
    return i;
   else  
     throw  bad_endianess_exception(  scr_e, dest_e );
  } //end of ....

static sint64 change_byte_order( sint64 i, endianess  scr_e, endianess dest_e )
  {
   if( ( scr_e ==  big_endianess && dest_e ==  little_endianess )
    || ( scr_e ==  little_endianess && dest_e == big_endianess  ) )
     return byteswap(i); // these 2 endianesses are opposites so just swap the byte order.
   else if( scr_e == dest_e )
    return i;
   else  
     throw  bad_endianess_exception(  scr_e, dest_e );
  } //end of ....

 static uint64 change_byte_order( uint64 i, endianess  scr_e, endianess dest_e )
  {
   if( ( scr_e ==  big_endianess && dest_e ==  little_endianess )
    || ( scr_e ==  little_endianess && dest_e == big_endianess  ) )
     return byteswap(i); // these 2 endianesses are opposites so just swap the byte order.
   else if( scr_e == dest_e )
    return i;
   else  
     throw  bad_endianess_exception(  scr_e, dest_e );
  } //end of ....

    sint8  toLocal( sint8 i )
   {
     return change_byte_order(i, endianess_tag::value , local_endianess);
   }//end of ...

   sint16 toLocal( sint16 i)
   {
     return change_byte_order(i, endianess_tag::value , local_endianess);
   }//end of ...

   sint32 toLocal( sint32 i)
   {
     return change_byte_order(i, endianess_tag::value , local_endianess);
   }//end of ...
   sint64 toLocal( sint64 i)
   {
     return change_byte_order(i, endianess_tag::value , local_endianess);
   }//end of ...
   
   uint8  toLocal( uint8 i)
    {
     return change_byte_order(i, endianess_tag::value , local_endianess);
   }//end of ...

   uint16 toLocal( uint16 i)
   {
     return change_byte_order(i, endianess_tag::value , local_endianess);
   }//end of ...

   uint32 toLocal( uint32 i)
   {
     return change_byte_order(i, endianess_tag::value , local_endianess);
   }//end of ...
   uint64 toLocal( uint64 i)
   {
     return change_byte_order(i, endianess_tag::value , local_endianess);
   }//end of ...
   
   
   sint8  fromLocal( sint8 i)
   {
     return change_byte_order(i, local_endianess, endianess_tag::value );
   }//end of ...
   sint16 fromLocal( sint16 i)
   {
     return change_byte_order(i, local_endianess, endianess_tag::value );
   }//end of ...

   sint32 fromLocal( sint32 i)
   {
     return change_byte_order(i, local_endianess, endianess_tag::value );
   }//end of ...

   sint64 fromLocal( sint64 i)
   {
     return change_byte_order(i, local_endianess, endianess_tag::value );
   }//end of ...
   
   uint8  fromLocal( uint8 i)
   {
     return change_byte_order(i, local_endianess, endianess_tag::value );
   }//end of ...

   uint16 fromLocal( uint16 i)
   {
     return change_byte_order(i, local_endianess, endianess_tag::value );
   }//end of ...

   uint32 fromLocal( uint32 i)
   {
     return change_byte_order(i, local_endianess, endianess_tag::value );
   }//end of ...

   uint64 fromLocal( uint64 i)
   {
     return change_byte_order(i, local_endianess, endianess_tag::value );
   }//end of ...
   
  };

// predefined endianess typedefs in the form of "typedef endian<E> E_endian;" :

  typedef endian<null_endianess>  nullEndian;

  typedef endian<big_endianess>  bigEndian;

  typedef endian< little_endianess>  littleEndian; 

  typedef endian<unknown_endianess> mixxedEndian; // unimplemented! byteswap won`t work on such a platform!

  typedef endian<unknown_endianess> unknownEndian;
 

 // a querying meta-functions:
 template<typename Endian>
 struct has_endianess_tag : public mpl::false_
 {};
 
  template<>
 struct has_endianess_tag<bigEndian> : public mpl::true_
 {};
  template<>
 struct has_endianess_tag<littleEndian> : public mpl::true_
 {};

template<typename Endian>
struct is_valid_endianess : public mpl::false_
{};

template<>
struct is_valid_endianess<bigEndian> : public mpl::true_
{};

template<>
struct is_valid_endianess<littleEndian> : public mpl::true_
{};
    
    
   }// end of namespace classic;
  }// end of namespace bits;
 }// end of namespace utility;
}// end of namespace SK;
