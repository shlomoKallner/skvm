#ifndef SK_UTILITY_BITS_ENDIANESS_DSEL_V1_HPP
#define SK_UTILITY_BITS_ENDIANESS_DSEL_V1_HPP

#include <boost/proto/proto.hpp>
#include <boost/mpl/assert.hpp>
#include <boost/mpl/if.hpp>
#include <boost/type_traits.hpp>
#include <boost/utility/result_of.hpp>
#include <boost/preprocessor/arithmetic/add.hpp>
#include <boost/preprocessor/arithmetic/mul.hpp>
#include <boost/preprocessor/cat.hpp>
#include <boost/preprocessor/repetition/repeat_from_to.hpp>


namespace SK
{
 namespace utility
 {
  namespace bits
  {
   namespace format
   {
    namespace v1
    {
     namespace proto = boost::proto;
     namespace mpl = boost::mpl;
     
     template< std::size_t N /*, typename T1 = void */ >
     struct placeholder
     {};
     struct placeholder_n
     {};
     
     struct converter_domain;
     struct converter_grammar;
     template< typename Expr >
     struct converter_expr;
    
     struct converter_domain
     : proto::domain< 
                proto::pod_generator< converter_expr >,
                converter_grammar 
                    >
     {};
     
    
     converter_expr< proto::terminal< placeholder< 0 > >::type 
               > const _0 = {{}};
     converter_expr< proto::terminal< placeholder< 1 > >::type 
               > const _1 = {{}};  
     converter_expr< proto::terminal< placeholder< 2 > >::type 
               > const _2 = {{}};
     converter_expr< proto::terminal< placeholder< 3 > >::type 
               > const _3 = {{}};
     converter_expr< proto::terminal< placeholder< 4 > >::type 
               > const _4 = {{}};
     converter_expr< proto::terminal< placeholder< 5 > >::type 
               > const _5 = {{}};
     converter_expr< proto::terminal< placeholder< 6 > >::type 
               > const _6 = {{}};
     converter_expr< proto::terminal< placeholder< 7 > >::type 
               > const _7 = {{}};
     converter_expr< proto::terminal< placeholder< 8 > >::type 
               > const _8 = {{}};
     converter_expr< proto::terminal< placeholder< 9 > >::type 
               > const _9 = {{}};
     converter_expr< proto::terminal< placeholder< 10 > >::type 
               > const _10 = {{}};
     converter_expr< proto::terminal< placeholder< 11 > >::type 
               > const _11 = {{}};  
     converter_expr< proto::terminal< placeholder< 12 > >::type 
               > const _12 = {{}};
     converter_expr< proto::terminal< placeholder< 13 > >::type 
               > const _13 = {{}};
     converter_expr< proto::terminal< placeholder< 14 > >::type 
               > const _14 = {{}};
     converter_expr< proto::terminal< placeholder< 15 > >::type 
               > const _15 = {{}};
     converter_expr< proto::terminal< placeholder< 16 > >::type 
               > const _16 = {{}};
     converter_expr< proto::terminal< placeholder< 17 > >::type 
               > const _17 = {{}};
     converter_expr< proto::terminal< placeholder< 18 > >::type 
               > const _18 = {{}};
     converter_expr< proto::terminal< placeholder< 19 > >::type 
               > const _19 = {{}};
     converter_expr< proto::terminal< placeholder< 20 > >::type 
               > const _20 = {{}};
     converter_expr< proto::terminal< placeholder< 21 > >::type 
               > const _21 = {{}};  
     converter_expr< proto::terminal< placeholder< 22 > >::type 
               > const _22 = {{}};
     converter_expr< proto::terminal< placeholder< 23 > >::type 
               > const _23 = {{}};
     converter_expr< proto::terminal< placeholder< 24 > >::type 
               > const _24 = {{}};
     converter_expr< proto::terminal< placeholder< 25 > >::type 
               > const _25 = {{}};
     converter_expr< proto::terminal< placeholder< 26 > >::type 
               > const _26 = {{}};
     converter_expr< proto::terminal< placeholder< 27 > >::type 
               > const _27 = {{}};
     converter_expr< proto::terminal< placeholder< 28 > >::type 
               > const _28 = {{}};
     converter_expr< proto::terminal< placeholder< 29 > >::type 
               > const _29 = {{}};
     converter_expr< proto::terminal< placeholder< 30 > >::type 
               > const _30 = {{}};
     converter_expr< proto::terminal< placeholder< 31 > >::type 
               > const _31 = {{}};  
     converter_expr< proto::terminal< placeholder< 32 > >::type 
               > const _32 = {{}};
     converter_expr< proto::terminal< placeholder< 33 > >::type 
               > const _33 = {{}};
     converter_expr< proto::terminal< placeholder< 34 > >::type 
               > const _34 = {{}};
     converter_expr< proto::terminal< placeholder< 35 > >::type 
               > const _35 = {{}};
     converter_expr< proto::terminal< placeholder< 36 > >::type 
               > const _36 = {{}};
     converter_expr< proto::terminal< placeholder< 37 > >::type 
               > const _37 = {{}};
     converter_expr< proto::terminal< placeholder< 38 > >::type 
               > const _38 = {{}};
     converter_expr< proto::terminal< placeholder< 39 > >::type 
               > const _39 = {{}};

   
     converter_expr< proto::terminal< placeholder_n >::type 
               > const _n = {{}};

  
     struct placeholders_0_7
     : proto::or_< 
              proto::terminal< placeholder<0> >,
              proto::terminal< placeholder<1> >,
              proto::terminal< placeholder<2> >,
              proto::terminal< placeholder<3> >,
              proto::terminal< placeholder<4> >,
              proto::terminal< placeholder<5> >,
              proto::terminal< placeholder<6> >,
              proto::terminal< placeholder<7> >
                 >
     {};
     struct placeholders_8_15
     : proto::or_< 
              proto::terminal< placeholder<8> >,
              proto::terminal< placeholder<9> >,
              proto::terminal< placeholder<10> >,
              proto::terminal< placeholder<11> >,
              proto::terminal< placeholder<12> >,
              proto::terminal< placeholder<13> >,
              proto::terminal< placeholder<14> >,
              proto::terminal< placeholder<15> >
                 >
     {};
     struct placeholders_16_23
     : proto::or_< 
              proto::terminal< placeholder<16> >,
              proto::terminal< placeholder<17> >,
              proto::terminal< placeholder<18> >,
              proto::terminal< placeholder<19> >,
              proto::terminal< placeholder<20> >,
              proto::terminal< placeholder<21> >,
              proto::terminal< placeholder<22> >,
              proto::terminal< placeholder<23> >
                 >
     {};
     struct placeholders_24_31
     : proto::or_< 
              proto::terminal< placeholder<24> >,
              proto::terminal< placeholder<25> >,
              proto::terminal< placeholder<26> >,
              proto::terminal< placeholder<27> >,
              proto::terminal< placeholder<28> >,
              proto::terminal< placeholder<29> >,
              proto::terminal< placeholder<30> >,
              proto::terminal< placeholder<31> >
                 >
     {};
     struct placeholders_32_39
     : proto::or_< 
              proto::terminal< placeholder<32> >,
              proto::terminal< placeholder<33> >,
              proto::terminal< placeholder<34> >,
              proto::terminal< placeholder<35> >,
              proto::terminal< placeholder<36> >,
              proto::terminal< placeholder<37> >,
              proto::terminal< placeholder<38> >,
              proto::terminal< placeholder<39> >
                 >
     {};
     
     struct placeholders_t
     : proto::or_< 
              placeholders_0_7,
              placeholders_8_15,
              placeholders_16_23,
              placeholders_24_31,
              placeholders_32_39
                 >
     {};
  
     struct litteral
     :  proto::terminal< proto::convertible_to< std::size_t > >
     {};
     
     //  proto::function< placeholder_n, litteral >
     
     struct converter_grammar 
     : proto::or_<
              placeholders_t
              proto::subscript< placeholder_n , litteral >,
              proto::shift_left< converter_grammar, converter_grammar >
                 > 
     {};
     
     template< typename IntegralT = void >
     struct conversion_context
     { 
       // std::pair< std::size_t, std::size_t > pair_type;
       // typedef void result_type;
       
       typedef std::vector< std::size_t > data_type; 
      
       data_type res_m;
       
       template<
        typename Expr
         // defaulted template parameters, so we can
         // specialize on the expressions that need
         // special handling.
        , typename Tag = typename proto::tag_of<Expr>::type
        , typename Arg0 = typename proto::child_c<Expr, 0>::type
       // , typename Arg1 = typename proto::child_c<Expr, 1>::type
       // , typename Arg2 = typename proto::child_c<Expr, 2>::type
        >
       struct eval;

       // Handle placeholder terminals here...
       template<typename Expr, std::size_t I>
       struct eval<Expr, proto::tag::terminal, placeholder<I> >
       {
         typedef void result_type;

         result_type operator()(Expr &, conversion_context &ctx) const
         {
             ctx.res_m.push_back(I);
         }
       };

       template<typename Expr >
       struct eval<Expr, proto::tag::terminal, std::size_t >
       {
         typedef void result_type;

         result_type operator()(Expr & expr, conversion_context &ctx) const
         {
             ctx.res_m.push_back( proto::value(expr) );
         }
       };
       
       template<typename Expr, typename Arg0>
       struct eval<Expr, proto::tag::subscript, Arg0>
       {
         typedef void result_type;

         result_type operator()(Expr &expr, MyContext &ctx) const
         {
             proto::eval(proto::right(expr), ctx);
         }
       };

       template<typename Expr, typename Arg0>
       struct eval<Expr, proto::tag::shift_left, Arg0>
       {
         typedef void result_type;

         result_type operator()(Expr &expr, MyContext &ctx) const
         {
             proto::eval(proto::left(expr), ctx);
             proto::eval(proto::right(expr), ctx);
         }
       };

       
       template<std::size_t I>
       result_type operator()(proto::tag::terminal, placeholder<I>) const
       {
        res_m.push_back( I );
       }//end of...
       
       // I hope this is right ...
       result_type operator()( proto::tag::terminal, 
                               std::size_t i )const
       {
        res_m.push_back(i);
       }//end of ...
       
       
     };
     
     template< typename Expr, class Dummy = proto::is_proto_expr >
     struct converter_expr
     {
        BOOST_PROTO_BASIC_EXTENDS(Expr, converter_expr<Expr>, converter_domain)
        BOOST_PROTO_EXTENDS_SUBSCRIPT()
        BOOST_PROTO_EXTENDS_FUNCTION()
        
        typedef void return_type;
        /***************************
        * template< typename Expr >
        * struct result
        * {
        *  typedef void type;
        * }; 
        ****************************/
     };
     
     enum conversion_direction { null = 0, To, From };
     
     template< typename IntegralT = void >
     class converter
     {
       BOOST_MPL_ASSERT( boost::is_integral<IntegralT>::value );
       BOOST_MPL_ASSERT_NOT(( boost::is_same<IntegralT,void> ));
       
       conversion_context<IntegralT> ctx_m;
      
      public:
             
           typedef typename IntegralT int_type;
           typedef typename converter<int_type> type;
           // static int_type To = 1;
             
           template< typename Expr >
           explicit converter( Expr const & expr )
           {
                   reset( expr );
           }// end of ...
           virtual ~converter_x(){}
           template< typename Expr >
           void
           reset( Expr const & expr )
           {
            res_m.clear();
            proto::eval( expr , ctx_m );
           }
           
           types::boolean 
           check();
           
           int_type
           convert( int_type i__, 
            conversion_direction dir_ = conversion_direction::null );
           
     };
     
     
     
    } // end of namespace v1;
   } // end of namespace format;
  } // end of namespace bits;
 } // end of namespace utility;
} // end of namespace SK;
 

#endif
