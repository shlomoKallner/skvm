#ifndef SK_UTILITY_BITS_ENDIANESS_DSEL_V1_OBSOLETE_HPP
#define SK_UTILITY_BITS_ENDIANESS_DSEL_V1_OBSOLETE_HPP

#include <boost/proto/proto.hpp>
#include <boost/mpl/assert.hpp>
#include <boost/mpl/if.hpp>
#include <boost/type_traits.hpp>
#include <boost/utility/result_of.hpp>


namespace SK
{
 namespace utility
 {
  namespace bits
  {
   namespace format
   {
    namespace v1
    {
     namespace proto = boost::proto;
     namespace mpl = boost::mpl;

/*   // -- forget the zeroth placeholder it`s not important.  
#ifdef SK_UTILITY_BITS_FORMAT_USE_ZEROTH_PLACEHOLDER     
     proto::terminal<placeholder<0> >::type const _0 = {{}};
     proto::terminal<placeholder<1> >::type const _1 = {{}};
#else
     proto::terminal<placeholder<0> >::type const _1 = {{}};
     proto::terminal<placeholder<1> >::type const _2 = {{}};
#endif
*/

// #define SK_UTILITY_BITS_FORMAT_V1_USE_MACROS
#ifdef SK_UTILITY_BITS_FORMAT_V1_USE_MACROS

#define MAKE_PLACEHOLDER( n )  \
  converter_expr< proto::terminal< placeholder< (n) > >::type \
                > const BOOST_PP_CAT(_, n) = {{}};

#ifdef SK_UTILITY_BITS_FORMAT_V1_USE_MACROS_PROTO

#define M2( n ) BOOST_PP_MUL( BOOST_PROTO_MAX_ARITY, (n) )

#define M1 0
        
#define M0(N, typename_A, A_const_ref, A_const_ref_a, ref_a)  \
    MAKE_PLACEHOLDER( BOOST_PP_ADD(N , M1) )

#define M1 0
BOOST_PROTO_REPEAT_FROM_TO(1, BOOST_PROTO_MAX_ARITY, M0)
#define M1 M2( 1 )
BOOST_PROTO_REPEAT_FROM_TO(1, BOOST_PROTO_MAX_ARITY, M0)
#define M1 M2( 2 )
BOOST_PROTO_REPEAT_FROM_TO(1, BOOST_PROTO_MAX_ARITY, M0)
#define M1 M2( 3 )
BOOST_PROTO_REPEAT_FROM_TO(1, BOOST_PROTO_MAX_ARITY, M0)
#define M1 M2( 4 )
BOOST_PROTO_REPEAT_FROM_TO(1, BOOST_PROTO_MAX_ARITY, M0)
#define M1 M2( 5 )
BOOST_PROTO_REPEAT_FROM_TO(1, BOOST_PROTO_MAX_ARITY, M0)
#define M1 M2( 6 )
BOOST_PROTO_REPEAT_FROM_TO(1, BOOST_PROTO_MAX_ARITY, M0)
#define M1 M2( 7 )
BOOST_PROTO_REPEAT_FROM_TO(1, BOOST_PROTO_MAX_ARITY, M0)

#undef M2
#undef M1
#undef MO

#else

#define MY_PP_MACRO( z, n, data ) MAKE_PLACEHOLDER(n)

BOOST_PP_REPEAT_FROM_TO(0, 32, MY_PP_MACRO, 0 )

#undef MY_PP_MACRO

#endif

#undef MAKE_PLACEHOLDER  
#else
/* use this syntax to create directly placeholders, redefining n as you go.
 converter_expr< proto::terminal< placeholder< (n) > >::type 
               > const _ ## n = {{}}; 
 // we used this syntax for creating _0 to _32, use it for creating more.
*/
               
#endif
  


     template< typename F >
     struct get_integer_type
     {
      using boost::is_integral;
      using boost::is_function;
      using boost::remove_pointer;
      using boost::is_pointer;
      using boost::function_traits;
      using boost::result_of;
      //using boost:: ;
      //using boost:: ;
      //using boost:: ;
      
      struct default_empty_t{};
      
      typedef mpl::if_c< 
               is_integral<F>::value || 
                is_integral< result_of< F() >::type >::value, 
               F, 
               mpl::if_c< 
                is_function<F>::value && function_traits<F>::arity == 0
                 && is_integral< function_traits<F>::return_type >::value, 
                F,              
                mpl::if_c< 
                 is_pointer<F>::value && 
                  is_function<remove_pointer<F>::type>::value &&
                  function_traits<remove_pointer<F>::type>::arity == 0 &&
                  is_integral< function_traits< remove_pointer<F>::type 
                                              >::return_type >::value,  
                 F,             
                 default_empty_t 
                         >::type
                        >::type
                       >::type type;
             
       BOOST_MPL_ASSERT_NOT(( boost::is_same<type,default_empty_t> ));
     };
     
     template< typename F >
     struct litteral_t
     {
      typedef converter_expr< 
                proto::terminal< 
                       get_integer_type<F>::type // calling custom metafunction.
                               >::type 
                       > type;
     };
    } // end of namespace v1;
   } // end of namespace format;
  } // end of namespace bits;
 } // end of namespace utility;
} // end of namespace SK;
 

#endif
   
