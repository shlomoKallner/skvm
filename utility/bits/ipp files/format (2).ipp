#ifndef SK_UTILITY_BITS_ENDIANESS_DSEL_HPP
#define SK_UTILITY_BITS_ENDIANESS_DSEL_HPP

#include <boost/proto/proto.hpp>
#include <boost/mpl/assert.hpp>
#include <boost/mpl/if.hpp>
#include <boost/type_traits.hpp>
#include <boost/utility/result_of.hpp>

#include "endianess_dsel_v1.hpp"

namespace SK
{
 namespace utility
 {
  namespace bits
  {
   namespace format
   {
    namespace v2
    {
    } // end of namespace v2;
    
   } // end of namespace format;
  } // end of namespace bits;
 } // end of namespace utility;
} // end of namespace SK;
 

#endif
