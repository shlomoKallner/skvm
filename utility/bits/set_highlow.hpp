#ifndef SK_UTILITIES_BITS_SET_HIGHLOW_H
#define SK_UTILITIES_BITS_SET_HIGHLOW_H

#include <boost/static_assert.hpp> 
#include <boost/mpl/mpl.hpp>
#include "SK/utility/types.hpp"

namespace SK
{
 namespace utility
 {
  namespace bits
  {
   namespace types = SK::utility::types;
   namespace emul = SK::utility::types::emulation;
   namespace mpl = boost::mpl;       
     
// make functions that take a low and high 
// of a smaller type to make a bigger type.
/*
* mpl::if_c< N % 2 == 0 ,  
            mpl::divides< mpl::size_t<N> , mpl::size_t<2> >::type,
            mpl::plus< 
               mpl::divides< mpl::size_t<N> , mpl::size_t<2> >,  
               mpl::size_t<1>  
                     >::type 
                    >::type::value
* a second option is to use:
     emul::uint< N >::high_type
     emul::uint< N >::low_type
     emul::sint< N >::high_type
     emul::sint< N >::low_type
     along with emul::is_specialized< T >::value and mpl::if_c

*/                    
 template< std::size_t N , 
          typename T1 = types::uint< N >::type, 
          typename T2 = types::uint< 
        mpl::divides< mpl::size_t<N> , mpl::size_t<2> >::type::value   
                                   >::type 
          >
 T1 make_uint( T2 high, T2 low )
  {
    return static_cast<T1>(high << (N/2) ) | static_cast<T1>(low);
  }// end of ...

 template< std::size_t N, 
            typename T1 = types::sint< N >::type,
            typename T2 = types::sint<
       mpl::divides< mpl::size_t<N> , mpl::size_t<2> >::type::value
                                     >::type 
          >
 T1 make_sint( T2 high, T2 low )
  {
    return static_cast<T1>(high << (N/2) ) | static_cast<T1>(low);
  }// end of ...



// template<> <16, uint16,uint8>
   template<>
   types::uint16 make_uint<16, uint16,uint8>
    ( types::uint8 high, types::uint8 low  )
   {
    return static_cast<types::uint16>( high << 8 ) 
            | static_cast<types::uint16>(low);
   } // end of ...

// template<> <16, sint16,sint8>
   template<> 
   types::sint16 make_sint<16, sint16,sint8>
    ( types::sint8 high, types::sint8 low )
   {
    return static_cast<types::sint16>( high << 8 ) 
            | static_cast<types::sint16>(low); 
   }// end of .....

// template<> <32, uint32, uint16>
   template<>
   types::uint32 make_uint<32, uint32, uint16>
    ( types::uint16 high, types::uint16 low )
   {
    return static_cast<types::uint32>( high << 16 ) 
            | static_cast<types::uint32>(low);
   }// end of ...

// template<> <32, sint32, sint16>
   template<>
   types::sint32 make_sint<32, sint32, sint16>
    ( types::sint16 high, types::sint16 low )
   {
    return static_cast<types::sint32>( high << 16 ) 
            | static_cast<types::sint32>(low);
   }// end of ...

// template< std::size_t N , 
//           typename T1 = uint_of_size<N>::type, 
//           typename T2 = uint_of_size<N/2>::type >
// <64, uint64, uint32>
   template<>
   types::uint64 make_uint<64, uint64, uint32>
    ( types::uint32 high, types::uint32 low )
   {
    return static_cast<types::uint64>( high << 32 ) 
            | static_cast<types::uint64>(low);
   }// end of ....

// template< std::size_t N , 
//           typename T1 = sint_of_size<N>::type ,
//           typename T2 = sint_of_size<N/2>::type >
// <64, sint64, sint32>
   template<>
   types::sint64 make_sint<64, sint64, sint32>
    ( types::sint32 high, types::sint32 low )
   {
    return static_cast<types::sint64>( high << 32 ) 
            | static_cast<types::sint64>(low);
   }// end of ....

            
  }// end of namespace bits;
 }// end of namespace utility;
}// end of namespace SK;  
