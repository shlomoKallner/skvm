#ifndef SK_UTILITIES_LIBGC_ALLOCATOR_HPP
#define SK_UTILITIES_LIBGC_ALLOCATOR_HPP

#include "gc_fwd.hpp"
#include "pointer.hpp"
#include "reference.hpp"

namespace SK
{
 namespace utility
 {
  namespace GC
  {
  
   template< typename _Tp, typename SizeT, typename MutexT, typename ThreadT >
   class allocator
   {
     
     public:
      typedef typename allocator<_Tp,SizeT,MutexT,ThreadT> 
                                this_type;  
      typedef typename SizeT    size_type;
      typedef typename MutexT   mutex_type;
      typedef typename ThreadT  thread_type;
      typedef typename coordinator<size_type,mutex_type,thread_type> 
                                coordinator_type;
      
      typedef std::ptrdiff_t            difference_type;
      typedef typename coordinator_type::pointer<_Tp>::type 
                                        pointer;
      typedef typename coordinator_type::const_pointer<_Tp>::type   
                                        const_pointer;
      typedef typename coordinator_type::reference<_Tp>::type
                                        reference;
      typedef typename coordinator_type::const_reference<_Tp>::type  
                                        const_reference;
      typedef typename _Tp              value_type;
      
      

      template<typename _Tp1 >
        struct rebind // note: other is *only* of the *same* Coordinator!!!
        { typedef typename coordinator_type::allocator<_Tp1>::type other; };

      allocator() throw() { }

      allocator(const allocator&) throw() { }

      template< typename _Tp1 >
      allocator(const rebind<_Tp1>::other& ) throw() { }

      ~allocator() throw() { }

      pointer
      address(reference __x) const { return &__x; }

      const_pointer
      address(const_reference __x) const { return &__x; }

      // NB: __n is permitted to be 0.  The C++ standard says nothing
      // about what the return value is when __n == 0.
      pointer
      allocate(size_type __n, const void* = 0)
      { 
	   if (__n > this->max_size())
	    std::__throw_bad_alloc();
 
	   return pointer( coordinator_type::gc_pool.allocate( __n * sizeof(_Tp) ) );
      }

      // __p is not permitted to be a null pointer.
      void
      deallocate(pointer __p, size_type __n = 1 )
      { coordinator_type::gc_pool.deallocate(__p,__n); }

      size_type
      max_size() const throw() 
      { return coordinator_type::gc_pool.max_size() / sizeof(_Tp); }

      // _GLIBCXX_RESOLVE_LIB_DEFECTS
      // 402. wrong new expression in [some_] allocator::construct
      void 
      construct(pointer __p, const _Tp& __val) 
      { ::new((void *)__p.get() ) _Tp(__val); }

#ifdef __GXX_EXPERIMENTAL_CXX0X__
      template<typename... _Args>
        void
        construct(pointer __p, _Args&&... __args)
	{ ::new((void *)__p.get() ) _Tp(std::forward<_Args>(__args)...); }
#endif

      void 
      destroy(pointer __p) { __p.get()->~_Tp(); }
     
   };
  
  } // end of namespace GC;
 } // end of namespace utility; 
} // end of namespace SK;


#endif
