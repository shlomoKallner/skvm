#ifndef SK_UTILITIES_LIBGC_BLOCK_HPP
#define SK_UTILITIES_LIBGC_BLOCK_HPP

#include <utility>
#include <list>

#include "SK/utility/types.hpp"
#include "gc_fwd.hpp"


namespace SK
{
 namespace utility
 {
  namespace GC
  {
   namespace types = SK::utility::types;
   
   // we need a template const (mpl::integral_c) of block size...
   template< typename SizeT, typename MutexT, typename ThreadT >
   struct block
   {
    typedef types::anyptr pointer_type;
    typedef std::pair< pointer_type, pointer_type > address_range;
    typedef std::list< address_range > used_address_ranges;
    typedef used_address_ranges::iterator iterator;
    typedef typename MutexT mutex_type;
    typedef typename ThreadT thread_type;
    typedef typename SizeT size_type;
    typedef typename block< size_type, mutex_type, thread_type > this_type;
    
    pointer_type begin_m, end_m;
    used_address_ranges used_address_m;
    mutex_type readwrite_mutex_m, move_mutex_m;
    
    public:
           
     block( pointer_type beg, pointer_type end );
     block( const this_type& );
     virtual ~block();
           
     types::boolean read( size_type offset, size_type num,
                          pointer_type& dest );
     types::boolean write( size_type offset, size_type num, 
                           pointer_type scr );
           
     size_type allocate( size_type __n, pointer_type = 0 );
     void deallocate( size_type offset, size_type num = 1 );
           
     pointer_type addressof_( size_type offset );
   }; 
   
  } // end of namespace GC;
 } // end of namespace utility; 
} // end of namespace SK;


#endif  
