#ifndef SK_UTILITIES_LIBGC_CHUNK_HPP
#define SK_UTILITIES_LIBGC_CHUNK_HPP

#include "gc_fwd.hpp"

namespace SK
{
 namespace utility
 {
  namespace GC
  {
   // we need a template const (mpl::integral_c) of chunck size 
   // and a second of number of blocks per chunk ...
   template< typename size_type, typename MutexT, typename ThreadT >
   struct chunck
   {}; 
  
  } // end of namespace GC;
 } // end of namespace utility; 
} // end of namespace SK;


#endif  
