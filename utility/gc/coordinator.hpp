#ifndef SK_UTILITIES_LIBGC_COORDINATOR_HPP
#define SK_UTILITIES_LIBGC_COORDINATOR_HPP

#include "gc_fwd.hpp"

namespace SK
{
 namespace utility
 {
  namespace GC
  {
    
   template< typename size_type_, typename MutexT, typename ThreadT >
    struct coordinator
    {
      typedef size_type_ size_type;
      typedef MutexT mutex_type;
      typedef ThreadT thread_type;

      static pool< size_type, mutex_type, thread_type > gc_pool;
      
/*    // template for meta-functions.
      template< typename U >
      struct 
      {
        typedef <U,size_type,mutex_type,thread_type> type;
      };

*/
      
      template< typename U >
      struct pointer
      {
        typedef pointer<U, size_type,mutex_type,thread_type> type;
      };
      
      template< typename U >
      struct reference
      {
        typedef reference<U,size_type,mutex_type,thread_type> type;
      };

      template< typename U >
      struct allocator
      {
        typedef allocator<U,size_type,mutex_type,thread_type> type;
      };


      template< typename U >
      struct synched_pointer
      {
        typedef synched_pointer<U,size_type,mutex_type,thread_type> type;
      };
      
      template< typename U >
      struct synched_reference
      {
        typedef synched_reference<U,size_type,mutex_type,thread_type> type;
      };

      template< typename U >
      struct synched_allocator
      {
        typedef synched_allocator<U,size_type,mutex_type,thread_type> type;
      };

    }; // end of struct coordinator;  
  
  } // end of namespace GC;
 } // end of namespace utility; 
} // end of namespace SK;


#endif
