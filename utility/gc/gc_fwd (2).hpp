#ifndef SK_GARBAGE_COLLECTOR_LIBRARY_FWD_HPP_JAN_22_2012
#define SK_GARBAGE_COLLECTOR_LIBRARY_FWD_HPP_JAN_22_2012

#include <boost/thread.hpp>
#include "SK/utility/types.hpp"

namespace SK
{
 namespace utility
 {
  namespace GC
  {
    namespace types = SK::utility::types;
    static const types::uint64 basicBlockSize = ( 16 * 1024 );
     
    template< typename SizeT = types::uint64, 
              typename MutexT, typename ThreadT >
    struct index; 
    
    template< typename SizeT = types::uint64,
              typename MutexT, typename ThreadT >
    struct block; 
    
    template< typename SizeT = types::uint64,
              typename MutexT, typename ThreadT >
    struct chunck;  
   
    template< typename SizeT = types::uint64,
              typename MutexT, typename ThreadT >
    class pool;


    template< typename T, typename SizeT = types::uint64,
              typename MutexT, typename ThreadT >
    class pointer;

    template< typename T, typename SizeT = types::uint64,
              typename MutexT, typename ThreadT >   
    class reference;
    
    template< typename _Tp, typename SizeT = types::uint64,
              typename MutexT, typename ThreadT >
    class allocator;
    
    // I think these "synched_*" classes are obsolete since the 
    // difference between them and the others was just 
    // synchronization of read, writes and memory access features...
    // I think that I can add such capability into the other classes
    // without an appreciatable proformance drop.
    
    template< typename T, typename SizeT, typename MutexT, typename ThreadT >
    class synched_pointer;

    template< typename T, typename SizeT, typename MutexT, typename ThreadT >
    class synched_reference;

    template< typename T, typename SizeT, typename MutexT, typename ThreadT >   
    class synched_allocator;



    template< typename SizeT, typename MutexT, typename ThreadT >
    struct coordinator;
    
    static const types::uint64 basicBlockSize = ( 16 * 1024 );

  } // end of namespace GC;
 } // end of namespace utility; 
} // end of namespace SK;



#endif
