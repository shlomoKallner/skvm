#ifndef SK_UTILITIES_LIBGC_INDEX_HPP
#define SK_UTILITIES_LIBGC_INDEX_HPP

#include "gc_fwd.hpp"

namespace SK
{
 namespace utility
 {
  namespace GC
  {
   
   template< typename size_type, typename MutexT, typename ThreadT >
   struct index
   {}; 
    
  } // end of namespace GC;
 } // end of namespace utility; 
} // end of namespace SK;


#endif  
