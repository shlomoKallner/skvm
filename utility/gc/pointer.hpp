#ifndef SK_UTILITIES_LIBGC_POINTER_HPP
#define SK_UTILITIES_LIBGC_POINTER_HPP

#include "gc_fwd.hpp"
#include "reference.hpp"

namespace SK
{
 namespace utility
 {
  namespace GC
  {
  
   template< typename T, typename size_type, typename MutexT, typename ThreadT >
   class pointer
   {
    size_type size_m, num_elements_m;
    
    public:
     typedef typename MutexT   mutex_type;
     typedef typename ThreadT  thread_type;
     typedef typename coordinator<size_type,mutex_type,thread_type> 
                               coordinator_type;
     typedef typename coordinator_type::const_pointer<_Tp>::type   
                               const_pointer;
     typedef typename coordinator_type::reference<_Tp>::type
                               reference;
     typedef typename coordinator_type::const_reference<_Tp>::type  
                               const_reference;
     typedef typename _Tp      value_type;
           
           
   };
  
  } // end of namespace GC;
 } // end of namespace utility; 
} // end of namespace SK;


#endif
