#ifndef SK_UTILITIES_ICONV_CHARSETSNAMES_HPP
#define SK_UTILITIES_ICONV_CHARSETSNAMES_HPP

#include <boost/spirit/include/qi.hpp>

#include "iconv_consts.hpp"

namespace SK
{
 namespace utility
 {
  namespace iconv
  {

   namespace qi = boost::spirit::qi;
   namespace consts = SK::utility::iconv::consts::encodings; 
   // qi::symbol<char,SK_UTIL::uint32> charsetIndexes;
   struct encodings : qi::symbol<char,SK_UTIL::uint32>
   {
    encodings()
    {
     add( "", consts::null_ )
        // ascii:  ( "", consts::ascii_ )
        ( "US-ASCII", consts::ascii_ ) // "US-ASCII" - IANA 
        ( "ASCII", consts::ascii_ ) // "ASCII" - IANA, JDK 1.1 
        ( "ISO646-US", consts::ascii_ ) // "ISO646-US" -  IANA 
        ( "ISO_646.IRV:1991", consts::ascii_ ) // "ISO_646.IRV:1991" - IANA 
        ( "ISO-IR-6", consts::ascii_ ) // "ISO-IR-6" - IANA 
        ( "ANSI_X3.4-1968", consts::ascii_ ) // "ANSI_X3.4-1968" - IANA 
        ( "ANSI_X3.4-1986", consts::ascii_ ) // "ANSI_X3.4-1986" - IANA 
        ( "CP367", consts::ascii_ ) // "CP367" - IANA 
        ( "IBM367", consts::ascii_ ) // "IBM367" - IANA 
        ( "US", consts::ascii_ ) // "US" - IANA 
        ( "csASCII", consts::ascii_ ) // "csASCII" - IANA 
   //   ( "ISO646.1991-IRV", consts::ascii_ ) // /* "ISO646.1991-IRV" - X11R6.4 */
        
        // utf8: ( "", consts::utf8_ )
        ( "UTF-8", consts::utf8_ ) // "UTF-8" - IANA, RFC 2279 
        ( "UTF8", consts::utf8_ ) // "UTF8" - JDK 1.1 
        ( "CP65001", consts::utf8_ ) // "CP65001" - Windows 
        
        // ucs2 : ( "", consts::ucs2_ )
        ( "UCS-2", consts::ucs2_ ) // "UCS-2" - glibc 
        ( "ISO-10646-UCS-2", consts::ucs2_ ) // "ISO-10646-UCS-2" - IANA 
        ( "csUnicode", consts::ucs2_ ) // "csUnicode" - IANA 
        
        // ucs2be: ( "", consts::ucs2be_ )
        ( "UCS-2BE", consts::ucs2be_ ) // "UCS-2BE" - glibc 
        ( "UNICODEBIG", consts::ucs2be_ ) // "UNICODEBIG" - glibc
        ( "UNICODE-1-1", consts::ucs2be_ ) // "UNICODE-1-1" - IANA 
        ( "csUnicode11", consts::ucs2be_ ) // "csUnicode11" - IANA 
        // ( "CP1201", consts::ucs2be_ ) // "CP1201" - Windows 
        
        // ucs2le: ( "", consts::ucs2le_ )
        ( "UCS-2LE", consts::ucs2le_ ) // "UCS-2LE" - glibc 
        ( "UNICODELITTLE", consts::ucs2le_ ) // "UNICODELITTLE" - glibc 
        // ( "CP1200", consts::ucs2le_ ) // "CP1200" - Windows 
        
        // ucs4:  ( "", consts::ucs4_ ) 
        ( "UCS-4", consts::ucs4_ ) // "UCS-4" - glibc
        ( "ISO-10646-UCS-4", consts::ucs4_ ) // "ISO-10646-UCS-4" - IANA 
        ( "csUCS4", consts::ucs4_ ) // "csUCS4" - IANA 
               
        // ucs4be: ( "", consts::ucs4be_ )
        ( "UCS-4BE", consts::ucs4be_ ) // "UCS-4BE" - glibc 
        // ( "CP12001", consts::ucs4be_ )  // "CP12001" - Windows 
        
        // ucs4le: ( "", consts::ucs4le_ )
        ( "UCS-4LE", consts::ucs4le_ ) // "UCS-4LE" - glibc 
        // ( "CP12000", consts::ucs4le_ )   // "CP12000" - Windows 
        
        // utf16: ( "", consts::utf16_ )
        ( "UTF-16", consts::utf16_ ) // "UTF-16" - IANA, RFC 2781 
       
       // utf16be: ( "", consts::utf16be_ ) 
       ( "UTF-16BE", consts::utf16be_ ) // "UTF-16BE" - IANA, RFC 2781 
            
       // utf16le: ( "", consts::utf16le_ )       
       ( "UTF-16LE", consts::utf16le_ ) // "UTF-16LE" - IANA, RFC 2781 
       
       // utf32: ( "", consts::utf32_ )
       ( "UTF-32", consts::utf32_ ) // "UTF-32" - IANA, Unicode 3.1 
       
       // utf32be: ( "", consts::utf32be_ )
       ( "UTF-32BE", consts::utf32be_ ) // "UTF-32BE" - IANA, Unicode 3.1 
            
       // utf32le: ( "", consts::utf32le_ )
       ( "UTF-32LE", consts::utf32le_ ) // "UTF-32LE" - IANA, Unicode 3.1 
       
       // utf7: ( "", consts::utf7_ )
       ( "UTF-7", consts::utf7_ ) // "UTF-7" - IANA, RFC 2152 
       ( "UNICODE-1-1-UTF-7", consts::utf7_ ) // "UNICODE-1-1-UTF-7" - IANA, RFC 1642 
       ( "csUnicode11UTF7", consts::utf7_ ) // "csUnicode11UTF7" - IANA 
       // ( "CP65000", consts::utf7_ ) // "CP65000" - Windows
       
       // ucs2internal: ( "", consts::ucs2internal_ )
       ( "UCS-2-INTERNAL", consts::ucs2internal_ ) // "UCS-2-INTERNAL" - libiconv 
            
       // ucs2swapped: ( "", consts::ucs2swapped_ )
       ( "UCS-2-SWAPPED", consts::ucs2swapped_ )// "UCS-2-SWAPPED" - libiconv 
       
       // ucs4internal: ( "", consts::ucs4internal_ )
       ( "UCS-4-INTERNAL", consts::ucs4internal_ ) // "UCS-4-INTERNAL" - libiconv 
        
       // ucs4swapped: ( "", consts::ucs4swapped_ )
       ( "UCS-4-SWAPPED", consts::ucs4swapped_ ) // ( "UCS-4-SWAPPED" - libiconv 

       // c99: ( "", consts::c99_ )              
       ( "C99", consts::c99_ )// "C99" - c99 
          
       // java: ( "", consts::java_ )
       ( "JAVA", consts::java_ ) // "JAVA" - java
          
       // iso8859_1: ( "", consts::iso8859_1_ )
       ( "ISO-8859-1", consts::iso8859_1_ )// "ISO-8859-1" - IANA 
       ( "ISO_8859-1", consts::iso8859_1_ )// "ISO_8859-1" - IANA 
       ( "ISO_8859-1:1987", consts::iso8859_1_ )// "ISO_8859-1:1987" - IANA 
       ( "ISO-IR-100", consts::iso8859_1_ )// "ISO-IR-100" - IANA 
       ( "CP819", consts::iso8859_1_ )// "CP819" - IANA 
       ( "IBM819", consts::iso8859_1_ )// "IBM819" - IANA 
       ( "LATIN1", consts::iso8859_1_ )// "LATIN1" - IANA 
       ( "L1", consts::iso8859_1_ )// "L1" - IANA 
       ( "csISOLatin1", consts::iso8859_1_ )// "csISOLatin1" - IANA 
       ( "ISO8859-1", consts::iso8859_1_ )// "ISO8859-1" - X11R6.4, glibc, FreeBSD, AIX, IRIX, OSF/1, Solaris 
       // ( "ISO8859_1", consts::iso8859_1_ ) // "ISO8859_1" - JDK 1.1 
       // ( "CP28591", consts::iso8859_1_ ) // "CP28591" - Windows 
       
       //  iso8859_2: ( "", consts::iso8859_2_ )   
       ( "ISO-8859-2", consts::iso8859_2_ )// "ISO-8859-2" - IANA 
       ( "ISO_8859-2", consts::iso8859_2_ )// "ISO_8859-2" - IANA */
       ( "ISO_8859-2:1987", consts::iso8859_2_ )// "ISO_8859-2:1987" - IANA */
       ( "ISO-IR-101", consts::iso8859_2_ )// "ISO-IR-101" - IANA */
       ( "LATIN2", consts::iso8859_2_ )// "LATIN2" - IANA */
       ( "L2", consts::iso8859_2_ )// "L2" - IANA */
       ( "csISOLatin2", consts::iso8859_2_ )// "csISOLatin2" - IANA */
       ( "ISO8859-2", consts::iso8859_2_ )// "ISO8859-2" - X11R6.4, glibc, FreeBSD, AIX, IRIX, OSF/1, Solaris */
       // ( "ISO8859_2", consts::iso8859_2_ ) // "ISO8859_2" - JDK 1.1 */
       // ( "CP28592", consts::iso8859_2_ ) // "CP28592" - Windows */
       
       // iso8859_3: ( "", consts::iso8859_3_ ) 
       ( "ISO-8859-3", consts::iso8859_3_ )// "ISO-8859-3" - IANA 
       ( "ISO_8859-3", consts::iso8859_3_ )// "ISO_8859-3" - IANA 
       ( "ISO_8859-3:1988", consts::iso8859_3_ )// "ISO_8859-3:1988" - IANA 
       ( "ISO-IR-109", consts::iso8859_3_ )// "ISO-IR-109" - IANA 
       ( "LATIN3", consts::iso8859_3_ )// "LATIN3" - IANA 
       ( "L3", consts::iso8859_3_ )// "L3" - IANA 
       ( "csISOLatin3", consts::iso8859_3_ )// "csISOLatin3" - IANA 
       ( "ISO8859-3", consts::iso8859_3_ )// "ISO8859-3" - X11R6.4, glibc, FreeBSD, Solaris 
       // ( "ISO8859_3", consts::iso8859_3_ ) // "ISO8859_3" - JDK 1.1 
       // ( "CP28593", consts::iso8859_3_ ) // "CP28593" - Windows 
       
       // iso8859_4: ( "", consts::iso8859_4_ )
       ( "ISO-8859-4", consts::iso8859_4_ )// "ISO-8859-4" - IANA 
       ( "ISO_8859-4", consts::iso8859_4_ )// "ISO_8859-4" - IANA 
       ( "ISO_8859-4:1988", consts::iso8859_4_ )// "ISO_8859-4:1988" - IANA 
       ( "ISO-IR-110", consts::iso8859_4_ )// "ISO-IR-110" - IANA 
       ( "LATIN4", consts::iso8859_4_ )// "LATIN4" - IANA 
       ( "L4", consts::iso8859_4_ )// "L4" - IANA 
       ( "csISOLatin4", consts::iso8859_4_ )// "csISOLatin4" - IANA 
       ( "ISO8859-4", consts::iso8859_4_ )// "ISO8859-4" - X11R6.4, glibc, FreeBSD, OSF/1, Solaris 
       // ( "ISO8859_4", consts::iso8859_4_ ) // "ISO8859_4" - JDK 1.1 
       // ( "CP28594", consts::iso8859_4_ ) // "CP28594" - Windows 
       
       // iso8859_5: ( "", consts::iso8859_5_ )
       ( "ISO-8859-5", consts::iso8859_5_ )// "ISO-8859-5"  - IANA 
       ( "ISO_8859-5", consts::iso8859_5_ )// "ISO_8859-5" - IANA 
       ( "ISO_8859-5:1988", consts::iso8859_5_ )// "ISO_8859-5:1988" - IANA 
       ( "ISO-IR-144", consts::iso8859_5_ )// "ISO-IR-144" - IANA 
       ( "CYRILLIC", consts::iso8859_5_ )// "CYRILLIC" - IANA 
       ( "csISOLatinCyrillic", consts::iso8859_5_ )// "csISOLatinCyrillic" - IANA 
       ( "ISO8859-5", consts::iso8859_5_ )// "ISO8859-5" - X11R6.4, glibc, FreeBSD, AIX, IRIX, OSF/1, Solaris 
       // ( "ISO8859_5", consts::iso8859_5_ ) // "ISO8859_5" - JDK 1.1 
       // ( "CP28595", consts::iso8859_5_ ) // "CP28595" - Windows 
          
       // iso8859_6: ( "", consts::iso8859_6_ )
       ( "ISO-8859-6", consts::iso8859_6_ )// "ISO-8859-6" - IANA 
       ( "ISO_8859-6", consts::iso8859_6_ )// "ISO_8859-6" - IANA  
       ( "ISO_8859-6:1987", consts::iso8859_6_ )// "ISO_8859-6:1987" - IANA  
       ( "ISO-IR-127", consts::iso8859_6_ )// "ISO-IR-127" - IANA  
       ( "ECMA-114", consts::iso8859_6_ )// "ECMA-114" - IANA  
       ( "ASMO-708", consts::iso8859_6_ )// "ASMO-708" - IANA  
       ( "ARABIC", consts::iso8859_6_ )// "ARABIC" - IANA  
       ( "csISOLatinArabic", consts::iso8859_6_ )// "csISOLatinArabic" - IANA  
       ( "ISO8859-6", consts::iso8859_6_ )// "ISO8859-6" - X11R6.4, glibc, FreeBSD, AIX, Solaris  
       // ( "ISO8859_6", consts::iso8859_6_ ) // "ISO8859_6" - JDK 1.1  
       // ( "CP28596", consts::iso8859_6_ ) // "CP28596" - Windows  
             
       // iso8859_7: ( "", consts::iso8859_7_ )
       ( "ISO-8859-7", consts::iso8859_7_ )// "ISO-8859-7" - IANA, RFC 1947 
       ( "ISO_8859-7", consts::iso8859_7_ )// "ISO_8859-7" - IANA 
       ( "ISO_8859-7:1987", consts::iso8859_7_ )// "ISO_8859-7:1987" - IANA 
       ( "ISO_8859-7:2003", consts::iso8859_7_ )// "ISO_8859-7:2003" 
       ( "ISO-IR-126", consts::iso8859_7_ )// "ISO-IR-126" - IANA 
       ( "ECMA-118", consts::iso8859_7_ )// "ECMA-118" - IANA 
       ( "ELOT_928", consts::iso8859_7_ )// "ELOT_928" - IANA 
       ( "GREEK8", consts::iso8859_7_ )// "GREEK8" - IANA 
       ( "GREEK", consts::iso8859_7_ )// "GREEK" - IANA 
       ( "csISOLatinGreek", consts::iso8859_7_ )// "csISOLatinGreek" - IANA 
       ( "ISO8859-7", consts::iso8859_7_ )// "ISO8859-7" - X11R6.4, glibc, FreeBSD, AIX, IRIX, OSF/1, Solaris 
       // ( "ISO8859_7", consts::iso8859_7_ ) // "ISO8859_7" - JDK 1.1 
       // ( "CP28597", consts::iso8859_7_ ) // "CP28597" - Windows 
             
       // iso8859_8: ( "", consts::iso8859_8_ )
       ( "ISO-8859-8", consts::iso8859_8_ )// "ISO-8859-8" - IANA 
       ( "ISO_8859-8", consts::iso8859_8_ )// "ISO_8859-8" - IANA 
       ( "ISO_8859-8:1988", consts::iso8859_8_ )// "ISO_8859-8:1988" - IANA 
       ( "ISO-IR-138", consts::iso8859_8_ )// "ISO-IR-138" - IANA 
       ( "HEBREW", consts::iso8859_8_ )// "HEBREW" - IANA 
       ( "csISOLatinHebrew", consts::iso8859_8_ )// "csISOLatinHebrew" - IANA 
       ( "ISO8859-8", consts::iso8859_8_ )// "ISO8859-8" - X11R6.4, glibc, FreeBSD, AIX, OSF/1, Solaris 
       // ( "ISO8859_8", consts::iso8859_8_ )// "ISO8859_8" - JDK 1.1 
       // ( "CP28598", consts::iso8859_8_ )// "CP28598" - Windows 
       // ( "CP38598", consts::iso8859_8_ )// "CP38598" - Windows 
           
       // iso8859_9: ( "", consts::iso8859_9_ )
       ( "ISO-8859-9", consts::iso8859_9_ )// "ISO-8859-9" - IANA 
       ( "ISO_8859-9", consts::iso8859_9_ )// "ISO_8859-9" - IANA 
       ( "ISO_8859-9:1989", consts::iso8859_9_ )// "ISO_8859-9:1989" - IANA 
       ( "ISO-IR-148", consts::iso8859_9_ )// "ISO-IR-148" - IANA 
       ( "LATIN5", consts::iso8859_9_ )// "LATIN5" - IANA 
       ( "L5", consts::iso8859_9_ )// "L5" - IANA 
       ( "csISOLatin5", consts::iso8859_9_ )// "csISOLatin5" - IANA 
       ( "ISO8859-9", consts::iso8859_9_ )// "ISO8859-9" - X11R6.4, glibc, FreeBSD, AIX, IRIX, OSF/1, Solaris 
       // ( "ISO8859_9", consts::iso8859_9_ )// "ISO8859_9" - JDK 1.1 
       // ( "CP28599", consts::iso8859_9_ )// "CP28599" - Windows 
             
       // iso8859_10: ( "", consts::iso8859_10_ )
       ( "ISO-8859-10", consts::iso8859_10_ )// "ISO-8859-10" - IANA 
       ( "ISO_8859-10", consts::iso8859_10_ )// "ISO_8859-10"
       ( "ISO_8859-10:1992", consts::iso8859_10_ )// "ISO_8859-10:1992" - IANA 
       ( "ISO-IR-157", consts::iso8859_10_ )// "ISO-IR-157" - IANA 
       ( "LATIN6", consts::iso8859_10_ )// "LATIN6" - IANA 
       ( "L6", consts::iso8859_10_ )// "L6" - IANA 
       ( "csISOLatin6", consts::iso8859_10_ )// "csISOLatin6" - IANA 
       ( "ISO8859-10", consts::iso8859_10_ )// "ISO8859-10" - X11R6.4, glibc, FreeBSD 
             
       // iso8859_11: ( "", consts::iso8859_11_ )
       ( "ISO-8859-11", consts::iso8859_11_ )// "ISO-8859-11" - glibc 
       ( "ISO_8859-11", consts::iso8859_11_ )// "ISO_8859-11"
       ( "ISO8859-11", consts::iso8859_11_ )// "ISO8859-11" - X11R6.7, glibc 
            
       // iso8859_13: ( "", consts::iso8859_13_ )                                      
       ( "ISO-8859-13", consts::iso8859_13_ )// "ISO-8859-13" - IANA, glibc  
       ( "ISO_8859-13", consts::iso8859_13_ )// "ISO_8859-13" 
       ( "ISO-IR-179", consts::iso8859_13_ )// "ISO-IR-179" - glibc  
       ( "LATIN7", consts::iso8859_13_ )// "LATIN7" - glibc  
       ( "L7", consts::iso8859_13_ )// "L7" - glibc  
       ( "ISO8859-13", consts::iso8859_13_ )// "ISO8859-13" - glibc, FreeBSD  
       
       // iso8859_14: ( "", consts::iso8859_14_ )
       ( "ISO-8859-14", consts::iso8859_14_ )// "ISO-8859-14" - IANA, glibc 
       ( "ISO_8859-14", consts::iso8859_14_ )// "ISO_8859-14" - IANA 
       ( "ISO_8859-14:1998", consts::iso8859_14_ )// "ISO_8859-14:1998" - IANA, glibc 
       ( "ISO-IR-199", consts::iso8859_14_ )// "ISO-IR-199" - IANA 
       ( "LATIN8", consts::iso8859_14_ )// "LATIN8" - IANA, glibc 
       ( "L8", consts::iso8859_14_ )// "L8" - IANA, glibc 
       ( "ISO-CELTIC", consts::iso8859_14_ )// "ISO-CELTIC" - IANA 
       ( "ISO8859-14", consts::iso8859_14_ )// "ISO8859-14" - glibc, FreeBSD 
           
       // iso8859_15: ( "", consts::iso8859_15_ ) 
       ( "ISO-8859-15", consts::iso8859_15_ )// "ISO-8859-15" - IANA, glibc 
       ( "ISO_8859-15", consts::iso8859_15_ )// "ISO_8859-15" - IANA 
       ( "ISO_8859-15:1998", consts::iso8859_15_ )// "ISO_8859-15:1998" - glibc 
       ( "ISO-IR-203", consts::iso8859_15_ )// "ISO-IR-203" 
       ( "LATIN-9", consts::iso8859_15_ )// "LATIN-9" - IANA 
       ( "ISO8859-15", consts::iso8859_15_ )// "ISO8859-15" - glibc, FreeBSD, AIX, OSF/1, Solaris 
       // ( "CP28605", consts::iso8859_15_ )// "CP28605" - Windows 
        
       // iso8859_16: ( "", consts::iso8859_16_ )  
       ( "ISO-8859-16", consts::iso8859_16_ )// "ISO-8859-16" - IANA 
       ( "ISO_8859-16", consts::iso8859_16_ )// "ISO_8859-16" - IANA 
       ( "ISO_8859-16:2001", consts::iso8859_16_ )// "ISO_8859-16:2001" - IANA 
       ( "ISO-IR-226", consts::iso8859_16_ )// "ISO-IR-226" - IANA 
       ( "LATIN10", consts::iso8859_16_ )// "LATIN10" - IANA 
       ( "L10", consts::iso8859_16_ )// "L10" - IANA 
       ( "ISO8859-16", consts::iso8859_16_ )// "ISO8859-16" - glibc, FreeBSD 
             
       // koi8_r: ( "", consts::koi8_r_ )
       ( "KOI8-R", consts::koi8_r_ )// "KOI8-R" - IANA, RFC 1489, X11R6.4, JDK 1.1 
       ( "csKOI8R", consts::koi8_r_ )// "csKOI8R" - IANA 
       // ( "CP20866", consts::koi8_r_ )// "CP20866" - Windows 
            
       // koi8_u: ( "", consts::koi8_u_ )
       ( "KOI8-U", consts::koi8_u_ )// "KOI8-U" - IANA, RFC 2319 
           
       // koi8_ru: ( "", consts::koi8_ru_ )
       ( "KOI8-RU", consts::koi8_ru_ )// "KOI8-RU" 
             
       // cp1250: ( "", consts::cp1250_ )
       ( "CP1250", consts::cp1250_ )// "CP1250" - JDK 1.1 
       ( "WINDOWS-1250", consts::cp1250_ )// "WINDOWS-1250" - IANA 
       ( "MS-EE", consts::cp1250_ )// "MS-EE"
            
       // cp1251: ( "", consts::cp1251_ )
       ( "CP1251", consts::cp1251_ )// "CP1251" - JDK 1.1  
       ( "WINDOWS-1251", consts::cp1251_ )// "WINDOWS-1251" - IANA  
       ( "MS-CYRL", consts::cp1251_ )// "MS-CYRL"
             
       // cp1252: ( "", consts::cp1252_ )
       ( "CP1252", consts::cp1252_ )// "CP1252" - JDK 1.1 */
       ( "WINDOWS-1252", consts::cp1252_ )// "WINDOWS-1252" - IANA */
       ( "MS-ANSI", consts::cp1252_ )// "MS-ANSI" 
            
       // cp1253: ( "", consts::cp1253_ )
       ( "CP1253", consts::cp1253_ )// "CP1253" - JDK 1.1 */
       ( "WINDOWS-1253", consts::cp1253_ )// "WINDOWS-1253" - IANA */
       ( "MS-GREEK", consts::cp1253_ )// "MS-GREEK" 
           
       // cp1254: ( "", consts::cp1254_ )
       ( "CP1254", consts::cp1254_ )// "CP1254" - JDK 1.1 */
       ( "WINDOWS-1254", consts::cp1254_ )// "WINDOWS-1254" - IANA */
       ( "MS-TURK", consts::cp1254_ )// "MS-TURK" 
             
       // cp1255: ( "", consts::cp1255_ )
       ( "CP1255", consts::cp1255_ )// "CP1255" - JDK 1.1  
       ( "WINDOWS-1255", consts::cp1255_ )// "WINDOWS-1255" - IANA  
       ( "MS-HEBR", consts::cp1255_ )// "MS-HEBR" 
             
       // cp1256: ( "", consts::cp1256_ )
       ( "CP1256", consts::cp1256_ )// "CP1256" - JDK 1.1 */
       ( "WINDOWS-1256", consts::cp1256_ )// "WINDOWS-1256" - IANA */
       ( "MS-ARAB", consts::cp1256_ )// "MS-ARAB" 
             
       // cp1257: ( "", consts::cp1257_ )
       ( "CP1257", consts::cp1257_ )// "CP1257" - JDK 1.1 */
       ( "WINDOWS-1257", consts::cp1257_ )// "WINDOWS-1257" - IANA */
       ( "WINBALTRIM", consts::cp1257_ )// "WINBALTRIM" 
           
       // cp1258: ( "", consts::cp1258_ )
       ( "CP1258", consts::cp1258_ )// "CP1258" - JDK 1.1 */
       ( "WINDOWS-1258", consts::cp1258_ )// "WINDOWS-1258" - IANA */
            
       // cp850: ( "", consts::cp850_ )
       ( "CP850", consts::cp850_ )// "CP850" - IANA, JDK 1.1 */
       ( "IBM850", consts::cp850_ )// "IBM850" - IANA */
       ( "850", consts::cp850_ )// "850" - IANA */
       ( "csPC850Multilingual", consts::cp850_ )// "csPC850Multilingual" - IANA */
           
       // cp862: ( "", consts::cp862_ )
      ( "CP862", consts::cp862_ ) // "CP862" - IANA, JDK 1.1 
      ( "IBM862", consts::cp862_ ) // "IBM862" - IANA
      ( "862", consts::cp862_ ) // "862" - IANA 
      ( "csPC862LatinHebrew", consts::cp862_ ) // "csPC862LatinHebrew" - IANA 
      
      // cp866: ( "", consts::cp866_ ) 
      ( "CP866", consts::cp866_ )// "CP866",                  /* IANA, JDK 1.1 */
      ( "IBM866", consts::cp866_ )//  "IBM866",                 /* IANA */
      ( "866", consts::cp866_ )//  "866",                    /* IANA */
      ( "csIBM866", consts::cp866_ )//  "csIBM866",               /* IANA */
            
                                                    
                                                    
                                         
                                           
       // ( "", consts::
       // ("",0)
              ;
    }//end of ...
   };

   struct allias : qi::symbol<char,SK_UTIL::uint32>
   {
    allias()
    {
     add( "", consts::null_ )
        ( "646", consts::ascii_ ) // "646" - Solaris 
        ( "ISO88591", consts::iso8859_1_ ) // "ISO88591" - HP-UX 
        ( "ISO88592", consts::iso8859_2_ ) // "ISO88592" - HP-UX   
        ( "ISO88595", consts::iso8859_5_ ) // "ISO88595" - HP-UX 
        ( "ISO88596", consts::iso8859_6_ ) // "ISO88596" - HP-UX 
        ( "ISO88597", consts::iso8859_7_ ) // "ISO88597" - HP-UX 
        ( "ISO88598", consts::iso8859_8_ ) // "ISO88598" - HP-UX 
        ( "ISO88599", consts::iso8859_9_ ) // "ISO88599" - HP-UX 
        ( "IBM-921",  consts::iso8859_13_ )// "IBM-921" - AIX 
        ( "ISO885915",consts::iso8859_15_ )// "ISO885915" - HP-UX 
        ( "ANSI-1251",consts::cp1251_ )// "ANSI-1251" Solaris 
        ( "IBM-1252", consts::cp1252_ )// "IBM-1252" - AIX 
        ( "IBM-850",  consts::cp850_ ) // "IBM-850" - AIX        
       // ("",0)
              ;
    }// end of ctor;
   };

/*
// make use of these macros ( take them apart ) to make the above 
//  and other sybol table parsers for determining *fromcode*
//  and *tocode*.

#define DEFENCODING(xxx_names,xxx,xxx_ifuncs1,xxx_ifuncs2,xxx_ofuncs1,xxx_ofuncs2) \
  {                                                           \
    static const char* const names[] = BRACIFY xxx_names;     \
    emit_encoding(stdout,stdout2,names,sizeof(names)/sizeof(names[0]),#xxx); \
  }
#define BRACIFY(...) { __VA_ARGS__ }
#define DEFALIAS(xxx_alias,xxx) emit_alias(stdout,xxx_alias,#xxx);
*/ 
/* By convention, an encoding named FOOBAR or FOO_BAR or FOO-BAR is defined
   in a file named "foobar.h" through the functions foobar_mbtowc and
   foobar_wctomb (and possibly foobar_reset). */

/* DEFENCODING(( name, alias1, ..., ),
               xxx,
               { xxx_mbtowc, xxx_flushwc },
               { xxx_wctomb, xxx_reset })
   defines an encoding with the given name and aliases. (There is no
   difference between a name and an alias. By convention, the name is chosen
   as the preferred MIME name or the standard name.)
   All names and aliases must be in ASCII. Case is not significant, but
   for the "cs*" aliases mixed case is preferred, otherwise UPPERCASE is
   preferred. For all names and aliases, note where it comes from.
   xxx is the name as used in the C code (lowercase).
 */ 



  } // end of namespace iconv;
 } // end of namespace utility;
} // end of namespace SK;

#endif

