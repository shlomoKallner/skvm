#ifndef SK_UTILITIES_ICOVN_CONSTSANTS_HPP
#define SK_UTILITIES_ICOVN_CONSTSANTS_HPP

#include "SK/utility/types.hpp"

namespace SK
{
 namespace utility
 {
  namespace iconv
  {
   namespace consts
   {
    namespace encodings
    {
     namespace SK_Types = SK::utility::types;
     const SK_Types::uint32 null_ = 0;
     // const SK_Types::uint32 _ = 0;
     const SK_Types::uint32 ascii_ = 1;
     const SK_Types::uint32 utf8_ = 2;
     const SK_Types::uint32 ucs2_ = 3;
     const SK_Types::uint32 ucs2be_ = 4;
     const SK_Types::uint32 ucs2le_ = 5;
     const SK_Types::uint32 ucs4_ = 6;
     const SK_Types::uint32 ucs4be_ = 7;
     const SK_Types::uint32 ucs4le_ = 8;
     const SK_Types::uint32 utf16_ = 9;
     const SK_Types::uint32 utf16be_ = 10;
     const SK_Types::uint32 utf16le_ = 11;
     const SK_Types::uint32 utf32_ = 12;
     const SK_Types::uint32 utf32be_ = 13;
     const SK_Types::uint32 utf32le_ = 14;
     const SK_Types::uint32 utf7_ = 15;
     const SK_Types::uint32 ucs2internal_ = 16;
     const SK_Types::uint32 ucs2swapped_ = 17;
     const SK_Types::uint32 ucs4internal_ = 18;
     const SK_Types::uint32 ucs4swapped_ = 19;
     const SK_Types::uint32 c99_ = 20;
     const SK_Types::uint32 java_ = 21;
     const SK_Types::uint32 iso8859_1_ = 22;
     const SK_Types::uint32 iso8859_2_ = 23;
     const SK_Types::uint32 iso8859_3_ = 24;
     const SK_Types::uint32 iso8859_4_ = 25;
     const SK_Types::uint32 iso8859_5_ = 26;
     const SK_Types::uint32 iso8859_6_ = 27;
     const SK_Types::uint32 iso8859_7_ = 28;
     const SK_Types::uint32 iso8859_8_ = 29;
     const SK_Types::uint32 iso8859_9_ = 30;
     const SK_Types::uint32 iso8859_10_ = 31;
     const SK_Types::uint32 iso8859_11_ = 32;
     const SK_Types::uint32 reserved_1_ = 33; // reserved for iso8859_12
     const SK_Types::uint32 iso8859_13_ = 34;
     const SK_Types::uint32 iso8859_14_ = 35;
     const SK_Types::uint32 iso8859_15_ = 36;
     const SK_Types::uint32 iso8859_16_ = 37;
     const SK_Types::uint32 koi8_r_ = 38;
     const SK_Types::uint32 koi8_u_ = 39;
     const SK_Types::uint32 koi8_ru_ = 40;
     const SK_Types::uint32 cp1250_ = 41;
     const SK_Types::uint32 cp1251_ = 42;
     const SK_Types::uint32 cp1252_ = 43;
     const SK_Types::uint32 cp1253_ = 44;
     const SK_Types::uint32 cp1254_ = 45;
     const SK_Types::uint32 cp1255_ = 46;
     const SK_Types::uint32 cp1256_ = 47;
     const SK_Types::uint32 cp1257_ = 48;
     const SK_Types::uint32 cp1258_ = 49;
     const SK_Types::uint32 cp850_ = 50;
     const SK_Types::uint32 cp862_ = 51;
     const SK_Types::uint32 cp866_ = 52;
     // const SK_Types::uint32 _ = 0;
     
     
     
    } // end of namespace encodings;
   } // end of namespace consts;
  } // end of namespace iconv;
 } // end of namespace utility;
} // end of namespace SK;

#endif
