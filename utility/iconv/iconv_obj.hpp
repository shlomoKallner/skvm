#ifndef SK_UTILITIES_ICOVN_OBJ_HPP
#define SK_UTILITIES_ICOVN_OBJ_HPP

#iclude <string>

#include "../types.hpp"

namespace SK
{
 namespace utility
 {
  namespace iconv
  {
   
   // qi::rule<> r = encodings_and_aliass >> 
   //               -( qi::lit("//TRANSLIT")| qi::lit("//IGNORE") );
   
   template< typename CharT = char, 
             typename _InputT = CharT, 
             typename _OutputT = CharT >
   class iconv_base
   {
    public:
           typedef typename CharT    char_type;
           typedef typename _InputT * input_type;
           typedef typename _OutputT * return_type;
             
           iconv_base(const char_type * tocode,
                      const char_type * fromcode);
           iconv_base( const SK::utility::types::uint32 tocode,
                       const SK::utility::types::uint32 fromcode);
           virtual ~iconv_base();
           
           return_type conv( input_type );
   };
   
  } // end of namespace iconv;
 } // end of namespace utility;
} // end of namespace SK;

#endif
