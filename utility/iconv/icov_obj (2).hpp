#ifndef SK_UTILITIES_ICOV_OBJ_HPP
#define SK_UTILITIES_ICOV_OBJ_HPP


#include <string>

#include "../types.hpp"

namespace SK
{
 namespace utility
 {
  namespace icov
  {
   
   class icov_t
   {
    public:
           icov_t( const char * );
   };
   
  } // end of namespace icov;
 } // end of namespace utility;
}

#endif
