

namespace SK
{
 namespace plugin
 {
     template< typename EmbedT >
     class mutex_wrapper
     {
      public:
             typedef EmbedT embed_t;
             typedef typename mutex_wrapper< embed_t >      self_t;
             typedef typename boost::shared_ptr< self_t >   self_ptr;
             mutex_wrapper() : embed_m(){}
             virtual ~mutex_wrapper(){}
             
             void lock()
             { embed_m.lock(); }
             void unlock()
             { embed_m.unlock(); }    
      private:
              embed_t embed_m;
              mutex_wrapper(const mutex_wrapper&);
              const mutex_wrapper& operator=(const mutex_wrapper&)
              { return *this; }
     };
     
     
     template< typename DerivedT >
     class mutex_base // base class for a mutex type that satisfies mutex_wrapper`s embeded type requirements.
     {
      public:
       typedef typename mutex_base< DerivedT >        self_t;
       typedef typename boost::shared_ptr< self_t >   self_ptr;
       typedef typename DerivedT                      derived_t;
       typedef typename DerivedT::handle_t            handle_t; // unnecessary.
       typedef typename DerivedT::embed_t             embed_t; // unnecessary.
       
   
       mutex_base( void ){ }// end of ...
       mutex_base( const self_t& ) {} //end of ...
       virtual ~mutex_base() {}//end of ...
      
       derived_t& derived(void)
       {
        return *static_cast<derived_t*>(this);
       }//end of ...
       derived_t const& derived(void)
       {
        return *static_cast<derived_t const*>(this);
       }//end of ...
     
       void lock()
       {
        this->derived().lock();
       }
      
       void unlock()
       {
        this->derived().unlock();
       }           
      };
  
 /*  ---  OBSOLETE!!!!! THERE IS NO SUCH MUTEX TYPE THAT CAN BE SIMPLY DEFRENTIATED BY HANDLE AND EMBEDED TYPES!!!
  template< typename DerivedT, typename HandleT = DerivedT::handle_t, 
            typename EmbedT = DerivedT::embed_t >
  class mutex_base : public mutex< mutex_base<DerivedT,HandleT,EmbedT> >
  {
   public:
         
         typedef typename mutex< mutex_base<DerivedT,HandleT,EmbedT> >    base_t;
         typedef typename mutex_base<DerivedT,HandleT,EmbedT>             self_t;
         typedef typename boost::shared_ptr< self_t >                     self_ptr;
         typedef typename HandleT                                         handle_t;
         typedef typename EmbedT                                          embed_t;
          
         DerivedT& derived(void)
         {
          return *static_cast<DerivedT*>(this);
         }//end of ...
         DerivedT const& derived(void)
         {
          return *static_cast<DerivedT const*>(this);
         }//end of ...
     
         void lock()
         {
          this->derived().lock();
         }
      
         void unlock()
         {
          this->derived().unlock();
         }
       
  };
  */

  class null_mutex : public mutex_base< null_mutex >
  {
   public:
         
         typedef typename mutex_base< null_mutex >          base_t;
         typedef typename null_mutex                        self_t;
         typedef typename boost::shared_ptr< self_t >       self_ptr;
         typedef typename void                              handle_t;
         typedef typename handle_t                          embed_t;
         
         
         mutex( void ){ }// end of ...
         mutex( const self_t& ) {} //end of ...
         virtual ~mutex() {}//end of ...
         
         void lock(){} // this is a no-op
         void unlock(){} // this is a no-op  
  };
  
  
  #include "SDL/SDL_mutex.h"
 
  class sdl_mutex : public mutex_base< sdl_mutex >
  {
   public:     
          typedef typename mutex_base< sdl_mutex >           base_t;
          typedef typename sdl_mutex                         self_t;
          typedef typename boost::shared_ptr< self_t >       self_ptr;
          typedef typename SDL_mutex                         handle_t;
          typedef typename handle_t*                         embed_t;
         
          sdl_mutex(void): handle_m(SDL_CreateMutex()){}
         
          virtual ~sdl_mutex()
          {  
            if(handle_m)    
            {   SDL_DestroyMutex(handle_m); }
          }
                    
          void lock()
          { 
           if(handle_m)    
           {    SDL_LockMutex(handle_m); }
          }
          
          void unlock()
          {  
           if(handle_m)    
           {    SDL_UnlockMutex(handle_m); }
          }
          
   private:
           embed_t hnadle_m;
           
           sdl_mutex( const self_t& s ) : handle_m( s.handle_m ? s.handle_m : 0 )
          {}
           
          const sdl_mutex& operator=( const sdl_mutex& s )
          { return *this; } 
           
  };
 } 
}
