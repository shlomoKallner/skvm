#ifndef SK_PLUGIN_HPP
#define SK_PLUGIN_HPP

// system includes
#include <list>
//project includes
#include "plugin_mutex.h"

namespace SK
{
 namespace plugin
 {
  template<typename CharT = char,  typename MutexT = null_mutex>
  class basic_plugin;         
  
  template<typename CharT = char,  typename MutexT = null_mutex>
  class plugin_context
  {     
   public:
          typedef typename CharT char_t;
          typedef typename mutex_wrapper<MutexT> mutex_t;
          typedef typename basic_plugin< char_t,MutexT > plugin_t;
          typedef basic_string<char_t> string_t;
          typedef std::list< plugin_t * >   plugin_list_type;
          
          void add( plugin_t* p);
          void remove( plugin_t* );
   protected:
             mutex_t mutex_m;
             plugin_list_type plugin_list_m;
  };        
                    
  template<typename CharT, typename MutexT >
  class basic_plugin
  {
   public:
          typedef typename CharT char_t;
          typedef typename basic_plugin< char_t,MutexT > plugin_t;
          typedef typename plugin_context< char_t,MutexT > context_t;
          typedef typename mutex_wrapper<MutexT> mutex_t;
          typedef basic_string<char_t> string_t;
          
          explicit basic_plugin( string_t& s)
          { }
          basic_plugin( plugin_t& p)
          { this->swap(p); }
          void swap( plugin_t&  );
          void set_context_ptr( context_t* ctx)
          {
           if(ctx)
           {    
            if(context_ptr)
            { context_ptr->remove(this); }
            ctx->add(this);
            context_ptr = ctx;
           } 
          }
   protected:
             context_t * context_ptr;
  };
 }
          
}

#endif
