#ifndef SK_UTILITY_TYPES_BASIC_TYPES_HPP_FEB_19_2012
#define SK_UTILITY_TYPES_BASIC_TYPES_HPP_FEB_19_2012

#include <stdint.h>
#include <limits.h>

namespace SK
{
 namespace utility
 {
  namespace types
  { 
      
   typedef bool         boolean; // boolean type.
   typedef ::int8_t     sint8;   // signed 8-bit integer.
   typedef ::uint8_t    uint8;   // unsigned 8-bit integer.
   typedef ::int16_t    sint16;  // signed 16-bit integer.
   typedef ::uint16_t   uint16;  // unsigned 16-bit integer.
   typedef ::int32_t    sint32;  // signed 32-bit integer.
   typedef ::uint32_t   uint32;  // unsigned 32-bit integer.
   typedef ::int64_t    sint64;  // signed 64-bit integer.
   typedef ::uint64_t   uint64;  // unsigned 64-bit integer.
   typedef ::intptr_t   ptrsint; // pointer holding signed integer.
   typedef ::uintptr_t  prtuint; // pointer holding unsigned integer.
   typedef void *       anyptr;  // pointer type that can point to anything.
   typedef float        float32; // 32 - bit floating-point type.
   typedef double       float64; // 64 - bit floating-point type.
   typedef long double  floatn;

#ifdef SK_UTILITY_TYPES_BASIC_TYPES_USE_128BIT_INTS
   // these types exist on windows C compiler platforms!
   typedef __int128           sint128;
   typedef unsigned __int128  uint128;
#ifdef SK_UTILITY_TYPES_BASIC_TYPES_USE_256BIT_INTS
   // these types are antisipatory only 
   //- due to the Intel AVX extentions
   // ( Advanced Vector eXtensions ) 
   // see file immintrin.h on Intel platforms.
   typedef __int256           sint256;
   typedef unsigned __int256  uint256; 
#endif

#endif

  } // end of namespace types;
  
  // this constant is not redifined in <limits>!!!
  const SK::utility::types::uint32 bitsPerByte = CHAR_BIT;
  
 } // end of namespace utility;
} // end of namespace SK;

#endif 
