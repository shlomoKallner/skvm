#ifndef SK_UTILITY_TYPES_EMUL_TYPES_HPP_FEB_19_2012
#define SK_UTILITY_TYPES_EMUL_TYPES_HPP_FEB_19_2012

// TO BE DONE:
// 
#include <boost/mpl/mpl.hpp>
#include "SK/utility/types/meta.hpp"

namespace SK
{
 namespace utility
 {
  namespace types
  {
   namespace emulation
   {
    namespace _impl
    {
     namespace types = SK::utility::types; 
     namespace mpl = boost::mpl;
     
     template< std::size_t NBits >
     struct get_num_bytes : mpl::size_t< 
         mpl::if_c<
          ( mpl::modulus< 
                   mpl::size_t<NBits>,
                   mpl::size_t<8> 
                        >::type::value > 0 ),
          mpl::plus< 
                     mpl::divides< 
                       mpl::size_t<NBits>,
                       mpl::size_t<8> 
                                 >::type::value, 
                     mpl::size_t<1>::type::value 
                   >::type::value,
          mpl::divides< 
                     mpl::size_t<NBits>,
                     mpl::size_t<8> 
                      >::type::value
                  >::type::value       >
     {};
     
     template< std::size_t N >
     struct get_high_of_uint
     { // mpl::divides< mpl::size_t<N> , mpl::size_t<2> >::type::value
       typedef mpl::if_c< ( N > 8 && N <= 16 ) , types::uint8,
                     
                        >::type type;
     };
     
     template< std::size_t N >
     struct get_high_of_sint
     { // mpl::divides< mpl::size_t<N> , mpl::size_t<2> >::type::value
       typedef mpl::if_c< ( N > 8 && N <= 16 ) , types::sint8, 
       
                        >::type type;
     };
     
     template< std::size_t N >
     struct get_low_of_uint
     { // mpl::divides< mpl::size_t<N> , mpl::size_t<2> >::type::value
       typedef /**/ type;
     };
     
     template< std::size_t N >
     struct get_low_of_sint
     { // mpl::divides< mpl::size_t<N> , mpl::size_t<2> >::type::value
       typedef /**/ type;
     };
     
    }// end of namespace _impl;
   }// end of namespace emulation;
  }// end of namespace types;
 }// end of namespace utility;
}// end of namespace SK;

#endif
    
