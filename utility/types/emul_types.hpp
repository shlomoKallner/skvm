#ifndef SK_UTILITY_TYPES_EMUL_TYPES_HPP_FEB_19_2012
#define SK_UTILITY_TYPES_EMUL_TYPES_HPP_FEB_19_2012

// TO BE DONE:
// 
#include <boost/mpl/mpl.hpp>
#include "SK/utility/types/meta.hpp"
#include "SK/utility/types/emul_details.hpp"

namespace SK
{
 namespace utility
 {
  namespace types
  {
   namespace emulation
   {
    namespace types = SK::utility::types; 
    namespace mpl = boost::mpl;
    namespace _impl = SK::utility::types::emulation::_impl;
    
    template< std::size_t N, 
              typename TH = sint< 
                      _impl::get_high_of_sint<N>::type 
                                >::type, 
              typename TL = uint< 
                      _impl::get_low_of_sint<N>::type 
                                >::type,
              types::boolean B = false                   
            >
    struct sint
    {
      typedef typename mpl::bool_<B> is_specialized;
      typedef typename sint<N,TH,TL,B> type;     
      typedef typename TH high_type;
      typedef typename TL low_type;
     private:
      high_type high_m;
      low_type  low_m;
    };
   
    template< std::size_t N, 
              typename TH = uint< 
           mpl::divides< mpl::size_t<N> , mpl::size_t<2> >::type::value
                                >::type, 
              typename TL = uint< 
           mpl::divides< mpl::size_t<N> , mpl::size_t<2> >::type::value
                                >::type,
              types::boolean B = false  >
    struct uint
    {
      typedef typename mpl::bool_<B> is_specialized;
      typedef typename uint<N,TH,TL,B> type;     
      typedef typename TH high_type;
      typedef typename TL low_type;
     private:
      high_type high_m;
      low_type  low_m;
    };
    
    template< typename T >
    struct is_specialized 
    : mpl::false_
    {};
    
    
#define SK_UTILITY_BITS_EMUL_TYPES_BUILTIN( n , st, ut, sth, uth, tl ) \
    template<> \
    struct sint< (n), (sth), (tl), true > \
    { \
     typedef typename mpl::true_ is_specialized; \
     typedef (st) type; \
     typedef typename (sth) high_type; \
     typedef typename (tl) low_type; \
    };  \
    template<> \
    struct is_specialized< sint< (n), (sth), (tl), true > > \
    : mpl::true_ \
    {};\
    template<> \
    struct uint< (n), (uth), (tl), true > \
    { \
     typedef typename mpl::true_ is_specialized; \
     typedef (ut) type; \
     typedef typename (uth) high_type; \
     typedef typename (tl) low_type; \
    };  \
    template<> \
    struct is_specialized< uint< (n), (uth), (tl), true > > \
    : mpl::true_ \
    {};
    
SK_UTILITY_BITS_EMUL_TYPES_BUILTIN(8, types::sint8, types::uint8,
                types::sint8,types::uint8,types::uint8 )
SK_UTILITY_BITS_EMUL_TYPES_BUILTIN(16, types::sint16, types::uint16,
                types::sint8, types::uint8, types::uint8)
SK_UTILITY_BITS_EMUL_TYPES_BUILTIN(32, types::sint32, types::uint32,
                types::sint16, types::uint16, types::uint16)
SK_UTILITY_BITS_EMUL_TYPES_BUILTIN(64, types::sint64, types::uint64,
                types::sint32, types::uint32, types::uint32)
#ifdef SK_UTILITY_TYPES_BASIC_TYPES_USE_128BIT_INTS
// only if we have 128 bit ints can we have 256 bit ints.
SK_UTILITY_BITS_EMUL_TYPES_BUILTIN(128, types::sint128, types::uint128, 
                types::sint64, types::uint64, types::uint64)
#ifdef SK_UTILITY_TYPES_BASIC_TYPES_USE_256BIT_INTS
SK_UTILITY_BITS_EMUL_TYPES_BUILTIN(256, types::sint256, types::uint265,
                types::sint128, types::uint128, types::uint128)
#else
// put here specializations of emul::sint<256> and emul::uint<256>
#endif
#else
// put here specializations of emul::sint<128> and emul::uint<128>
#endif
#undef SK_UTILITY_BITS_EMUL_TYPES_BUILTIN
   }// end of namespace emulation;
  }// end of namespace types;
 }// end of namespace utility;
}// end of namespace SK;

#endif
