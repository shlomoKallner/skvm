#ifndef SK_UTILITIES_VALUE_PTR_H
#define SK_UTILITIES_VALUE_PTR_H

namespace SK
{
 namespace types
 {
  
  
  // a fake pointer type - to simulate a pointer when needed.
  template< typename T >
  class value_ptr
  {
   public:
          typedef T  value_type;
          typedef T& reference;
          typedef T* pointer;
          typedef value_ptr<T> self_t;
          
          explicit value_ptr( T& t ) : t_(t){}//end of ...
          value_ptr( self_t const& v ) : t_(v.t_){}//end of ...
          virtual ~value_ptr(void){}//end of ...
          
          const self_t& operator=( const self_t& r )
          {
           this->t_ = r.t_;
           return *this;
          }//end of ...
          
          pointer operator->(void)
          {
           return &t_;
          }//end of ...
          
          reference operator*(void)
          {
           return t_; 
          }//end of ...
   protected:
             value_type t_;
  };
  
  
 }
}

#endif
