#ifndef SK_UTILITY_TYPES_META_HPP_FEB_19_2012
#define SK_UTILITY_TYPES_META_HPP_FEB_19_2012

#include <boost/static_assert.hpp> 
#include <boost/mpl/mpl.hpp>
#include <boost/static_assert.hpp> 
#include <boost/type_traits.hpp> 
#include "basic_types.hpp" 

namespace SK
{
 namespace utility
 {
  namespace types
  {
   namespace mpl = boost::mpl;
   
   // inexact integer making metafunctions:
   // can accept bit sizes between 1 and 64. 
   // ( 64+ bit integers are not yet supported!!! )  
   // if we have 128 bit and/or 256 bit ints then ...
   
   template< std::size_t N > // N is in bits.
   struct sint  // sint of inexact size.
   { 
     typedef mpl::if_c< ( N > 0 && N <= 8 ), sint8,
               mpl::if_c< ( N > 8 && N <= 16 ), sint16, 
                 mpl::if_c< ( N > 16 && N <= 32 ), sint32, 
                   mpl::if_c< ( N > 32 && N <= 64 ), sint64,
#ifdef SK_UTILITY_TYPES_BASIC_TYPES_USE_128BIT_INTS
                     mpl::if_c< ( N > 64 && N <= 128 ), sint128,
// only if we have 128 bit ints can we have 256 bit ints.                             
#ifdef SK_UTILITY_TYPES_BASIC_TYPES_USE_256BIT_INTS
                       mpl::if_c< ( N > 128 && N <= 256 ), sint256,
#endif
#endif                       
                                 void
#ifdef SK_UTILITY_TYPES_BASIC_TYPES_USE_128BIT_INTS
                                >::type  
#ifdef SK_UTILITY_TYPES_BASIC_TYPES_USE_256BIT_INTS
                              >::type
#endif
#endif                                   
                            >::type
                          >::type 
                        >::type 
                      >::type type;
     BOOST_MPL_ASSERT_NOT(( boost::is_same<type,void> ));
   };
   
   template< std::size_t N >  // N is in bits.
   struct uint  // uint of inexact size.
   {
     typedef mpl::if_c< ( N > 0 && N <= 8 ), uint8,
               mpl::if_c< ( N > 8 && N <= 16 ), uint16, 
                 mpl::if_c< ( N > 16 && N <= 32 ), uint32, 
                   mpl::if_c< ( N > 32 && N <= 64 ), uint64,
#ifdef SK_UTILITY_TYPES_BASIC_TYPES_USE_128BIT_INTS
                     mpl::if_c< ( N > 64 && N <= 128 ), uint128,
// only if we have 128 bit ints can we have 256 bit ints.                             
#ifdef SK_UTILITY_TYPES_BASIC_TYPES_USE_256BIT_INTS
                       mpl::if_c< ( N > 128 && N <= 256 ), uint256,
#endif
#endif                             
                                 void 
#ifdef SK_UTILITY_TYPES_BASIC_TYPES_USE_128BIT_INTS
                                >::type  
#ifdef SK_UTILITY_TYPES_BASIC_TYPES_USE_256BIT_INTS
                              >::type
#endif
#endif                                   
                            >::type
                          >::type 
                        >::type 
                      >::type type;
    BOOST_MPL_ASSERT_NOT(( boost::is_same<type,void> ));
   };
   
   // exact size integer making meta functions
   // can accept bit sizes of 8,16,32 or 64 only.
   // 64+ bit sizes are not yet supported!
   
   template< std::size_t N > // N is in bits.
   struct sint_of_size // sint of exact size.
   {
    BOOST_STATIC_ASSERT( N == 8 || N == 16 || N == 32 || N == 64 );
    typedef sint< N >::type type;
   };
   
   template< std::size_t N >  // N is in bits.
   struct uint_of_size  // uint of exact size.
   {
    BOOST_STATIC_ASSERT( N == 8 || N == 16 || N == 32 || N == 64 );
    typedef uint< N >::type type;
   };
   
   template< typename T, T t = T() > 
   struct High : boost::mpl::integral_c< T, t >
   {
    BOOST_STATIC_ASSERT( boost::is_integral<T>::value );
    typedef T return_type;
    typedef High<T,t> self_type;
    const T& operator()( const T& t)
    {
     return t & self_type::value;
    }// end of ...
   };
   
   
   template< typename T, T t = T() >
   struct Low : boost::mpl::integral_c<T, t >
   {
    BOOST_STATIC_ASSERT( boost::is_integral<T>::value );
    typedef Low<T,t> self_type;
    typedef T return_type;
    return_type operator()( T t)
    {
     return t & self_type::value;
    }// end of ...
   };
   
#define SK_TYPES_META_HIGH( type, val ) \
   template<>  \
   struct SK::utility::types::High<(type),(val)> \
   : boost::mpl::integral_c<(type),(val)> \
   {             \
    BOOST_STATIC_ASSERT( is_integral<(type)>::value ); \
    typedef High<(type),(val)> self_type; \
    typedef (type) return_type; \
    (type) operator()( (type) t ) \
    { \
     return t & (val) ; \
    } \
   }; \
  /* SK::utility::types::High<(type)>::value; */
     
#define SK_TYPES_META_LOW( type, val ) \
   template<>  \
   struct SK::utility::types::Low<(type),(val)>  \
   : boost::mpl::integral_c<(type),(val) > \
   { \
    typedef (type) return_type; \
    BOOST_STATIC_ASSERT( is_integral<(type)>::value ); \
    typedef Low<(type),(val)> self_type; \
    const (type) & operator()( (type) t ) \
    { \
     return t & (val) ; \
    } \
   };   \
 /*  SK::utility::types::Low<(type)>::value; */
 
  }// end of namespace types;
 }// end of namespace utility;
}// end of namespace SK;

SK_TYPES_META_HIGH(SK::utility::types::sint8,0xf0)
SK_TYPES_META_LOW(SK::utility::types::sint8,0x0f)
SK_TYPES_META_HIGH(SK::utility::types::uint8,0xf0)
SK_TYPES_META_LOW(SK::utility::types::uint8, 0x0f)
SK_TYPES_META_HIGH(SK::utility::types::sint16,0xff00) 
SK_TYPES_META_LOW(SK::utility::types::sint16,0x00ff)
SK_TYPES_META_HIGH(SK::utility::types::uint16,0xff00) 
SK_TYPES_META_LOW(SK::utility::types::uint16,0x00ff)
SK_TYPES_META_HIGH(SK::utility::types::sint32,0xffff0000) 
SK_TYPES_META_LOW(SK::utility::types::sint32,0x0000ffff)
SK_TYPES_META_HIGH(SK::utility::types::uint32,0xffff0000) 
SK_TYPES_META_LOW(SK::utility::types::uint32,0x0000ffff)
SK_TYPES_META_HIGH(SK::utility::types::sint64,0xffffffff00000000) 
SK_TYPES_META_LOW(SK::utility::types::sint64,0x00000000ffffffff)
SK_TYPES_META_HIGH(SK::utility::types::uint64,0xffffffff00000000) 
SK_TYPES_META_LOW(SK::utility::types::uint64,0x00000000ffffffff)

#ifdef SK_UTILITY_TYPES_BASIC_TYPES_USE_128BIT_INTS
SK_TYPES_META_HIGH(SK::utility::types::sint128,
                   0xffffffffffffffff00000000000000000)
SK_TYPES_META_LOW(SK::utility::types::sint128, 
                  0x00000000000000000ffffffffffffffff)
SK_TYPES_META_HIGH(SK::utility::types::uint128, 
                   0xffffffffffffffff00000000000000000)
SK_TYPES_META_LOW(SK::utility::types::uint128, 
                  0x00000000000000000ffffffffffffffff)
#ifdef SK_UTILITY_TYPES_BASIC_TYPES_USE_256BIT_INTS
// only if we have 128 bit ints can we have 256 bit ints.
SK_TYPES_META_HIGH(SK::utility::types::sint256, 
   0xffffffffffffffffffffffffffffffff0000000000000000000000000000000000)
SK_TYPES_META_LOW(SK::utility::types::sint256,
   0x0000000000000000000000000000000000ffffffffffffffffffffffffffffffff)
SK_TYPES_META_HIGH(SK::utility::types::uint256, 
   0xffffffffffffffffffffffffffffffff0000000000000000000000000000000000)
SK_TYPES_META_LOW(SK::utility::types::uint256,
   0x0000000000000000000000000000000000ffffffffffffffffffffffffffffffff)
#endif
#endif


#endif
