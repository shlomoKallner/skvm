#ifndef SK_UTILITY_TYPES_TYPES_OBSOLETE_HPP_FEB_19_2012
#define SK_UTILITY_TYPES_TYPES_OBSOLETE_HPP_FEB_19_2012

#include <stdint.h>
#include <boost/utility.hpp>
#include <boost/static_assert.hpp> 
#include <boost/mpl/mpl.hpp>

namespace SK
{
 namespace utility
 {
  namespace types
  {
   namespace obsolete
   {
   } // end of namespace obsolete;
  } // end of namespace types;
 } // end of namespace utility;
} // end of namespace SK;

#endif 
